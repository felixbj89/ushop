function eliminarCatalogo(route,dataforms,selection,id){
	var url = window.location.href.substr(0,window.location.href.lastIndexOf("ushop"));
	$.ajax({
		url: (url + route),
		headers:{
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		},
		method:"post",
		data:dataforms,
		processData: false,
		contentType: false,
		beforeSend:function(respuesta){
			$("#modal_wait").modal("show");
			$(id).modal("hide");
		},
		success:function(respuesta){
			$("#modal_wait").modal("hide");
			$(id).modal("hide");
			if(respuesta.code=="200"){		
				$("#messageadvertencia").empty();
				$("#messageadvertencia").append("<strong>" + respuesta.respuesta + "</strong>");
				$("#modal_alerta").modal("show");
				$("#secciones").val("NULL");
				$("#list_catalogos").DataTable().rows().remove().draw();
				$.each(respuesta.catalogo,function(indice,data){
						var htmlbox = "";
						if(data.catalogo_estado=="1"){
							htmlbox = "<div class=\"box bg-ushop-on\" data-activar=\"" + data.catalogo_estado + "\">" +
										"<div class=\"box-body\">" +
											"<div class=\"row\">" + 
												"<div class=\"col-xs-12 flex-center\">" +
													"<span><i class=\"fa fa-power-off fa-2x\" aria-hidden=\"true\"></i><span>" +
												"</div>" +
											"</div>" +
										"</div>" +
									"</div>";
						}else{
							htmlbox = "<div class=\"box bg-ushop-off\" id=\"btnActivarFile\" data-activar=\"" + data.catalogo_estado + "\" data-codigocatalogoactive=\"" + data.catalogo_codigo + "\">" +
										"<div class=\"box-body\">" +
											"<div class=\"row\">" + 
												"<div class=\"col-xs-12 flex-center\">" +
													"<span><i class=\"fa fa-power-off fa-2x\" aria-hidden=\"true\"></i><span>" +
												"</div>" +
											"</div>" +
										"</div>" +
									"</div>";
						}
						
					$("#list_catalogos").DataTable().row.add( [
						data.catalogo_name,
						"<div class=\"box\">" + 
							"<div class=\"box-body\">" + 
								"<div class=\"row\">" +
									"<div class=\"col-xs-12\">" +
										"<a href=" + (url + "img/catalogo/" + data.catalogo_ruta) + "><i class=\"fa fa-file-pdf-o fa-2x pull-left\" aria-hidden=\"true\"></i></a>" +
										"<a href=" + (url + "img/catalogo/" + data.catalogo_ruta) + "><i class=\"fa fa-download  fa-2x pull-right\" aria-hidden=\"true\"></i></a>" +
									"</div>" +
								"</div>" +
							"</div>" +
						"</div>",
						"<div class=\"box\">" + 
							"<div class=\"box-body\">" +
								"<div class=\"row\">" + 
									"<div class=\"col-xs-12 flex-center\">" +
										"<a href=" + (url + "img/catalogo/" + data.catalogo_ruta) + " target=\"_blank\"><i class=\"fa fa-eye fa-2x\" aria-hidden=\"true\"></i></a>" +
									"</div>" +
								"</div>" +
							"</div>" +
						"</div>",
						htmlbox,
						"<div class=\"row\">" + 
							"<div class=\"col-xs-12\">" +
								"<button type=\"button\" id=\"btnRemoverFile\" class=\"btn btn-primary btn-raised btn-block btn-flat btn-ushop\" data-codigocatalogoremover=\"" + data.catalogo_codigo + "\">" + 
									"<i class=\"fa fa-times\" aria-hidden=\"true\"></i> REMOVER" + 
									"<div class=\"ripple-container\"></div>" +
								"</button>" + 
							"</div>" +
						"</div>"
					]).draw( false );
				});
				
				$("#contadorcatalogo").empty();
				$("#contadorcatalogo").text($("#list_catalogos").DataTable().rows().data().length + "/100");
			}
		},
		error:function(respuesta){
			$("#modal_wait").modal("hide");
			$(id).modal("hide");
			if(respuesta.responseJSON.code=="404" || respuesta.responseJSON.code=="409" || respuesta.responseJSON.code=="500"){
				$("#messageadvertencia").empty();
				$("#messageadvertencia").append("<strong>" + respuesta.responseJSON.respuesta + "</strong>");
				$("#modal_alerta").modal("show");
			}
		}
	});
}

function activarCatalogo(route,dataforms,selection,id){
	var url = window.location.href.substr(0,window.location.href.lastIndexOf("ushop"));
	$.ajax({
		url: (url + route),
		headers:{
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		},
		method:"post",
		data:dataforms,
		processData: false,
		contentType: false,
		beforeSend:function(respuesta){
			$("#modal_wait").modal("show");
			$(id).modal("hide");
		},
		success:function(respuesta){
			$("#modal_wait").modal("hide");
			$(id).modal("hide");
			if(respuesta.code=="200"){		
				$("#messageadvertencia").empty();
				$("#messageadvertencia").append("<strong>" + respuesta.respuesta + "</strong>");
				$("#modal_alerta").modal("show");
				$("#secciones").val("NULL");
				
				$("#list_catalogos").DataTable().rows().remove().draw();
				$.each(respuesta.catalogo,function(indice,data){
						var htmlbox = "";
						if(data.catalogo_estado=="1"){
							htmlbox = "<div class=\"box bg-ushop-on\" data-activar=\"" + data.catalogo_estado + "\">" +
										"<div class=\"box-body\">" +
											"<div class=\"row\">" + 
												"<div class=\"col-xs-12 flex-center\">" +
													"<span><i class=\"fa fa-power-off fa-2x\" aria-hidden=\"true\"></i><span>" +
												"</div>" +
											"</div>" +
										"</div>" +
									"</div>";
						}else{
							htmlbox = "<div class=\"box bg-ushop-off\" id=\"btnActivarFile\" data-activar=\"" + data.catalogo_estado + "\" data-codigocatalogoactive=\"" + data.catalogo_codigo + "\">" +
										"<div class=\"box-body\">" +
											"<div class=\"row\">" + 
												"<div class=\"col-xs-12 flex-center\">" +
													"<span><i class=\"fa fa-power-off fa-2x\" aria-hidden=\"true\"></i><span>" +
												"</div>" +
											"</div>" +
										"</div>" +
									"</div>";
						}
						
					$("#list_catalogos").DataTable().row.add( [
						data.catalogo_name,
						"<div class=\"box\">" + 
							"<div class=\"box-body\">" + 
								"<div class=\"row\">" +
									"<div class=\"col-xs-12\">" +
										"<a href=" + (url + "img/catalogo/" + data.catalogo_ruta) + "><i class=\"fa fa-file-pdf-o fa-2x pull-left\" aria-hidden=\"true\"></i></a>" +
										"<a href=" + (url + "img/catalogo/" + data.catalogo_ruta) + "><i class=\"fa fa-download  fa-2x pull-right\" aria-hidden=\"true\"></i></a>" +
									"</div>" +
								"</div>" +
							"</div>" +
						"</div>",
						"<div class=\"box\">" + 
							"<div class=\"box-body\">" +
								"<div class=\"row\">" + 
									"<div class=\"col-xs-12 flex-center\">" +
										"<a href=" + (url + "img/catalogo/" + data.catalogo_ruta) + " target=\"_blank\"><i class=\"fa fa-eye fa-2x\" aria-hidden=\"true\"></i></a>" +
									"</div>" +
								"</div>" +
							"</div>" +
						"</div>",
						htmlbox,
						"<div class=\"row\">" + 
							"<div class=\"col-xs-12\">" +
								"<button type=\"button\" id=\"btnRemoverFile\" class=\"btn btn-primary btn-raised btn-block btn-flat btn-ushop\" data-codigocatalogoremover=\"" + data.catalogo_codigo + "\">" + 
									"<i class=\"fa fa-times\" aria-hidden=\"true\"></i> REMOVER" + 
									"<div class=\"ripple-container\"></div>" +
								"</button>" + 
							"</div>" +
						"</div>"
					]).draw( false );
				});
			}
		},
		error:function(respuesta){
			$("#modal_wait").modal("hide");
			$(id).modal("hide");
			if(respuesta.responseJSON.code=="404" || respuesta.responseJSON.code=="409" || respuesta.responseJSON.code=="500"){
				$("#messageadvertencia").empty();
				$("#messageadvertencia").append("<strong>" + respuesta.responseJSON.respuesta + "</strong>");
				$("#modal_alerta").modal("show");
			}
		}
	});
}

function pdfCatalogo(route,dataforms){
	var url = window.location.href.substr(0,window.location.href.lastIndexOf("ushop"));
	$.ajax({
		url: (url + route),
		headers:{
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		},
		method:"post",
		data:dataforms,
		processData: false,
		contentType: false,
		beforeSend:function(respuesta){
			$("#modal_wait").modal("show");
		},
		success:function(respuesta){
			$("#modal_wait").modal("hide");
			if(respuesta.code=="200"){		
				$("#messageadvertencia").empty();
				$("#messageadvertencia").append("<strong>" + respuesta.respuesta + "</strong>");
				$("#modal_alerta").modal("show");
				$("#secciones").val("NULL");
				$("#list_catalogos").DataTable().rows().remove().draw();
				$.each(respuesta.catalogo,function(indice,data){
					for(i = 0; i < data.length; i++){
						var htmlbox = "";
						if(data[i].catalogo_estado=="1"){
							htmlbox = "<div class=\"box bg-ushop-on\" data-activar=\"" + data[i].catalogo_estado + "\">" +
										"<div class=\"box-body\">" +
											"<div class=\"row\">" + 
												"<div class=\"col-xs-12 flex-center\">" +
													"<span><i class=\"fa fa-power-off fa-2x\" aria-hidden=\"true\"></i><span>" +
												"</div>" +
											"</div>" +
										"</div>" +
									"</div>";
						}else{
							htmlbox = "<div class=\"box bg-ushop-off\" id=\"btnActivarFile\" data-activar=\"" + data[i].catalogo_estado + "\">" +
										"<div class=\"box-body\">" +
											"<div class=\"row\">" + 
												"<div class=\"col-xs-12 flex-center\">" +
													"<span><i class=\"fa fa-power-off fa-2x\" aria-hidden=\"true\"></i><span>" +
												"</div>" +
											"</div>" +
										"</div>" +
									"</div>";
						}
						
						$("#list_catalogos").DataTable().row.add( [
							data[i].catalogo_name,
							"<div class=\"box\">" + 
								"<div class=\"box-body\">" + 
									"<div class=\"row\">" +
										"<div class=\"col-xs-12\">" +
											"<a href=" + (url + "img/catalogo/" + data[i].catalogo_ruta) + "><i class=\"fa fa-file-pdf-o fa-2x pull-left\" aria-hidden=\"true\"></i></a>" +
											"<a href=" + (url + "img/catalogo/" + data[i].catalogo_ruta) + "><i class=\"fa fa-download  fa-2x pull-right\" aria-hidden=\"true\"></i></a>" +
										"</div>" +
									"</div>" +
								"</div>" +
							"</div>",
							"<div class=\"box\">" + 
								"<div class=\"box-body\">" +
									"<div class=\"row\">" + 
										"<div class=\"col-xs-12 flex-center\">" +
											"<a href=" + (url + "img/catalogo/" + data[i].catalogo_ruta) + " target=\"_blank\"><i class=\"fa fa-eye fa-2x\" aria-hidden=\"true\"></i></a>" +
										"</div>" +
									"</div>" +
								"</div>" +
							"</div>",
							htmlbox,
							"<div class=\"row\">" + 
								"<div class=\"col-xs-12\">" +
									"<button type=\"button\" id=\"btnRemoverFile\" class=\"btn btn-primary btn-raised btn-block btn-flat btn-ushop\" data-codigocatalogoremover=\"" + data[i].catalogo_codigo + "\">" + 
										"<i class=\"fa fa-times\" aria-hidden=\"true\"></i> REMOVER" + 
										"<div class=\"ripple-container\"></div>" +
									"</button>" + 
								"</div>" +
							"</div>"
						]).draw( false );
					}
				});
				
				$("#contadorcatalogo").empty();
				$("#contadorcatalogo").text($("#list_catalogos").DataTable().rows().data().length + "/100");
			}
		},
		error:function(respuesta){
			$("#modal_wait").modal("hide");
			if(respuesta.responseJSON.code=="404" || respuesta.responseJSON.code=="409" || respuesta.responseJSON.code=="500"){
				$("#messageadvertencia").empty();
				$("#messageadvertencia").append("<strong>" + respuesta.responseJSON.respuesta + "</strong>");
				$("#modal_alerta").modal("show");
			}
		}
	});
}