function managmentLinks(route,dataforms,config_flag){
	var url = window.location.href.substr(0,window.location.href.lastIndexOf("ushop"));
	$.ajax({
		url: (url + route),
		headers:{
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		},
		method:"post",
		data:dataforms,
		processData: false,
		contentType: false,
		beforeSend:function(respuesta){
			$("#modal_config").modal("hide");
			$("#modal_wait").modal("show");
		},
		success:function(respuesta){
			$("#modal_config").modal("hide");
			$("#modal_wait").modal("hide");
			if(respuesta.code=="200"){		
				$("#enlacesadvertencia").empty();
				$("#enlacesadvertencia").append("<strong>" + respuesta.respuesta + "</strong>");
				$("#modal_alerta_enlaces").modal("show");
			}
		},
		error:function(respuesta){
			$("#modal_config").modal("hide");
			$("#modal_wait").modal("hide");
			if(respuesta.responseJSON.code=="401" || respuesta.responseJSON.code=="404" || respuesta.responseJSON.code=="409" || respuesta.responseJSON.code=="500"){
				$("#enlacesadvertencia").empty();
				$("#enlacesadvertencia").append("<strong>" + respuesta.responseJSON.respuesta + "</strong>");
				$("#modal_alerta_enlaces").modal("show");
			}
		}
	});
	
	$("#modal_alerta_enlaces").bind("hidden.bs.modal",function(){
		if(config_flag!=undefined && config_flag==1){
			$("#modal_config").modal("show");
			config_flag = undefined;
		}
	});
}