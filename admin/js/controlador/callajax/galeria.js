function galeriaservicios(route,dataforms){
	var url = window.location.href.substr(0,window.location.href.lastIndexOf("ushop"));
	$.ajax({
		url: (url + route),
		headers:{
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		},
		method:"post",
		data:dataforms,
		processData: false,
		contentType: false,
		beforeSend:function(respuesta){
			$("#modal_wait").modal("show");
		},
		success:function(respuesta){
            $("#modal_wait").modal("hide");
			if(respuesta.code=="200"){		
				$("#messageadvertencia").empty();
				$("#messageadvertencia").append("<strong>" + respuesta.respuesta + "</strong>");
				$("#modal_alerta").modal("show");
				
            }
		},
		error:function(respuesta){
			$("#modal_wait").modal("hide");
			if(respuesta.code=="404" || respuesta.code=="409" || respuesta.code=="500"){
				$("#messageadvertencia").empty();
				$("#messageadvertencia").append("<strong>" + respuesta.respuesta + "</strong>");
				$("#modal_alerta").modal("show");
			}
		}
	});
}
function galeriaserviciosdel(route,dataforms,tr){
	var url = window.location.href.substr(0,window.location.href.lastIndexOf("ushop"));
	$.ajax({
		url: (url + route),
		headers:{
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		},
		method:"post",
		data:{'cod':dataforms},
		beforeSend:function(respuesta){
			$('#modal_confirmar').modal('hide');
			$("#modal_wait").modal("show");
		},
		success:function(respuesta){
            $("#modal_wait").modal("hide");
            if(respuesta.code=="200"){		
				$("#messageadvertencia").empty();
				$("#messageadvertencia").append("<strong>" + respuesta.respuesta + "</strong>");
				$("#modal_alerta").modal("show");
				if(tr != ""){        
					table.row( tr )
						.remove()
						.draw();
						$('#inputsfile input[type="file"][name="'+$('#btnConfirmarRemoverMedia').data('file')+'"]').remove();
						$('#inputstext input[type="text"][name="'+$('#btnConfirmarRemoverMedia').data('text')+'"]').remove();
				}		
            }
		},
		error:function(respuesta){
			$("#modal_wait").modal("hide");
			if(respuesta.code=="404" || respuesta.code=="409" || respuesta.code=="500"){
				$("#messageadvertencia").empty();
				$("#messageadvertencia").append("<strong>" + respuesta.respuesta + "</strong>");
				$("#modal_alerta").modal("show");
			}
		}
	});
}