function eliminarPicturePrincipal(route,dataforms,selection,id){
	var url = window.location.href.substr(0,window.location.href.lastIndexOf("ushop"));
	$.ajax({
		url: (url + route),
		headers:{
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		},
		method:"post",
		data:dataforms,
		processData: false,
		contentType: false,
		beforeSend:function(respuesta){
			$("#modal_wait").modal("show");
			$(id).modal("hide");
		},
		success:function(respuesta){
			$("#modal_wait").modal("hide");
			$(id).modal("hide");
			if(respuesta.code=="200"){		
				$("#messageadvertencia").empty();
				$("#messageadvertencia").append("<strong>" + respuesta.respuesta + "</strong>");
				$("#modal_alerta").modal("show");
				$("#secciones").val("NULL");
				$("#list_imagenesprincipales").DataTable().row(selection).remove().draw();
				if($("#list_imagenesprincipales").DataTable().rows().data().length < 3 && atob($("#modo").val())=="h"){
					$("#btnGuardarPrincipal").prop("disabled",false);
					$("#btnGuardarCompleto").prop("disabled",false);
				}
			}
		},
		error:function(respuesta){
			$("#modal_wait").modal("hide");
			$(id).modal("hide");
			if(respuesta.responseJSON.code=="404" || respuesta.responseJSON.code=="409" || respuesta.responseJSON.code=="500"){
				$("#messageadvertencia").empty();
				$("#messageadvertencia").append("<strong>" + respuesta.responseJSON.respuesta + "</strong>");
				$("#modal_alerta").modal("show");
			}
		}
	});
}

function actualizarPrincipal(route,dataforms){
	var url = window.location.href.substr(0,window.location.href.lastIndexOf("ushop"));
	$.ajax({
		url: (url + route),
		headers:{
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		},
		method:"post",
		data:dataforms,
		processData: false,
		contentType: false,
		beforeSend:function(respuesta){
			$("#modal_wait").modal("show");
		},
		success:function(respuesta){
			$("#modal_wait").modal("hide");
			if(respuesta.code=="200"){		
				$("#messageadvertencia").empty();
				$("#messageadvertencia").append("<strong>" + respuesta.respuesta + "</strong>");
				$("#modal_alerta").modal("show");
			}
		},
		error:function(respuesta){
			$("#modal_wait").modal("hide");
			if(respuesta.responseJSON.code=="404" || respuesta.responseJSON.code=="409" || respuesta.responseJSON.code=="500"){
				$("#messageadvertencia").empty();
				$("#messageadvertencia").append("<strong>" + respuesta.responseJSON.respuesta + "</strong>");
				$("#modal_alerta").modal("show");
			}
		}
	});
}

function picturePrincipal(route,dataforms){
	var url = window.location.href.substr(0,window.location.href.lastIndexOf("ushop"));
	var flag = undefined;
	$.ajax({
		url: (url + route),
		headers:{
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		},
		method:"post",
		data:dataforms,
		processData: false,
		contentType: false,
		beforeSend:function(respuesta){
			$("#modal_wait").modal("show");
		},
		success:function(respuesta){
			$("#modal_wait").modal("hide");
			if(respuesta.code=="200"){		
				flag = 1;
				$("#messageadvertencia").empty();
				$("#messageadvertencia").append("<strong>" + respuesta.respuesta + "</strong>");
				$("#modal_alerta").modal("show");
				$("#secciones").val("NULL");
				$("#fuentes").val("NULL");
				$("#url").val("");
				$("#tipomedia").val("NULL");
				$("#tipofuentes").val("NULL");
				$("#descripcion").summernote("code","");
				
				$("#logo-a").attr("href",(url + "img/logo/logosidebar.png"));
				$("#logo-img").attr("src",(url + "img/logo/logosimple_cf_ushop.svg"));
				$("#list_imagenesprincipales").DataTable().rows().remove().draw();
				
				$.each(respuesta.items,function(indice,data){
					if(data.galeria_tipo=="0"){
						$("#list_imagenesprincipales").DataTable().row.add( [
							data.item_name,
							"<div class=\"row\"><div class=\"col-xs-12 flex-center\"><img src=\"" + (url + "img/logo/logosimple_cf_ushop.svg") + "\" width=\"128\" height=\"64\"></div></div>",
							"<div class=\"row\">" + 
								"<div class=\"col-xs-12\">" + 
									"<a id=\"logo-a\" class=\"logo-link flex-center\" href=\"" + (url + "img/media/" + data.item_ruta) +"\" data-lightbox=\"logo\" class=\"spacing-picture\">" +
										"<img id=\"logo-img\" class=\"logo logo-table\" src=\"" + (url + "img/media/" + data.item_ruta) +"\" class=\"img-responsive img-thumbnail\" width=\"256\" height=\"64\">" +
									"</a>" +
								"</div>" +
							"</div>",
							"<div class=\"row\">" +
								"<div class=\"col-xs-12 ushop-col-6-12\">" +
									"<button type=\"button\" id=\"btnModificarTimes\" class=\"btn btn-primary btn-raised btn-block btn-flat btn-ushop\">" +
										"<i class=\"fa fa-edit\" aria-hidden=\"true\"></i> MODIFICAR" +
										"<div class=\"ripple-container\"></div>" +
									"</button>" +
								"</div>" +
								"<div class=\"col-xs-12 ushop-col-6-12\">" +
									"<button type=\"button\" id=\"btnRemoverMedia\" data-codigogaleria='" + data.galeria_codigo + "' class=\"btn btn-primary btn-raised btn-block btn-flat btn-ushop\">" +
										"<i class=\"fa fa-times\" aria-hidden=\"true\"></i> REMOVER" +
										"<div class=\"ripple-container\"></div>" +
									"</button>" +
								"</div>" +
							"</div>"
						]).draw( false );
					}else{
						$("#list_imagenesprincipales").DataTable().row.add( [
							data.item_name,
							"<div class=\"row\"><div class=\"col-xs-12 flex-center\"><img src=\"" + (url + "img/logo/logosimple_cf_ushop.svg") + "\" width=\"128\" height=\"64\"></div></div>",
							"<div class=\"row\">" + 
								"<div class=\"flex-center\">" + 
									"<img id=\"logo-img\" class=\"logo logo-table\" src=\"" + (url + "img/defaults/defaults.svg") + "\" class=\"img-responsive img-thumbnail\" width=\"256\" height=\"64\">" +
								"</div>" + 
							"</div>",
							"<div class=\"row\">" +
								"<div class=\"col-xs-12 ushop-col-6-12\">" +
									"<button type=\"button\" id=\"btnModificarTimes\" class=\"btn btn-primary btn-raised btn-block btn-flat btn-ushop\">" +
										"<i class=\"fa fa-edit\" aria-hidden=\"true\"></i> MODIFICAR" +
										"<div class=\"ripple-container\"></div>" +
									"</button>" +
								"</div>" +
								"<div class=\"col-xs-12 ushop-col-6-12\">" +
									"<button type=\"button\" id=\"btnRemoverMedia\" data-codigogaleria='" + data.galeria_codigo + "' class=\"btn btn-primary btn-raised btn-block btn-flat btn-ushop\">" +
										"<i class=\"fa fa-times\" aria-hidden=\"true\"></i> REMOVER" +
										"<div class=\"ripple-container\"></div>" +
									"</button>" +
								"</div>" +
							"</div>"
						]).draw( false );
					}
				});
				
				if($("#list_imagenesprincipales").DataTable().rows().data().length >= 3 && atob($("#modo").val())=="h"){
					$("#btnGuardarPrincipal").prop("disabled",true);
					$("#btnGuardarCompleto").prop("disabled",true);
					$("#messageadvertencia").empty();
					$("#messageadvertencia").append("<strong>Ya no puede ingresar más elementos a este bloque.</strong>");
					$("#modal_alerta").modal("show");
				}
			}
		},
		error:function(respuesta){
			$("#modal_wait").modal("hide");
			if(respuesta.responseJSON.code=="404" || respuesta.responseJSON.code=="409" || respuesta.responseJSON.code=="500"){
				$("#messageadvertencia").empty();
				$("#messageadvertencia").append("<strong>" + respuesta.responseJSON.respuesta + "</strong>");
				$("#modal_alerta").modal("show");
			}
		}
	});

	$("#modal_alerta").bind("click",function(){
		if(flag){
			window.location.reload();
		}
	});
}