function productosViewPrincipal(route,dataforms){
	var url = window.location.href.substr(0,window.location.href.lastIndexOf("ushop"));
	$.ajax({
		url: (url + route),
		headers:{
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		},
		method:"post",
		data:dataforms,
		processData: false,
		contentType: false,
		beforeSend:function(respuesta){
			$("#modal_wait").modal("show");
		},
		success:function(respuesta){
			$("#modal_wait").modal("hide");
			if(respuesta.code=="200"){		
				$("#messageadvertencia").empty();
				$("#messageadvertencia").append("<strong>" + respuesta.respuesta + "</strong>");
				$("#modal_alerta").modal("show");
				
				var productos = JSON.parse(respuesta.productos);
				$("#productoname").val(productos.productos_name);
				$("#tipoproducto").val(productos.productos_tipo);
				$("#estilo_one").val(productos.productos_estilo_one);
				$("#estilo_two").val(productos.productos_estilo_two);
				$("#estilo_thrid").val(productos.productos_estilo_thrid);
				$("#descripcionproducto").empty();
				$("#descripcionproducto").val(productos.productos_descripcion);
				$("#logo-a").attr("href",(url + "/" + productos.productos_ruta));
				$("#logo-img").attr("src",(url + "/" + productos.productos_ruta));
			}
		},
		error:function(respuesta){
			$("#modal_wait").modal("hide");
			if(respuesta.responseJSON.code=="404" || respuesta.responseJSON.code=="409" || respuesta.responseJSON.code=="500"){
				$("#messageadvertencia").empty();
				$("#messageadvertencia").append("<strong>" + respuesta.responseJSON.respuesta + "</strong>");
				$("#modal_alerta").modal("show");
			}
		}
	});
}

function productosEliminar(route,dataforms,selection){
	var url = window.location.href.substr(0,window.location.href.lastIndexOf("ushop"));
	$.ajax({
		url: (url + route),
		headers:{
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		},
		method:"post",
		data:dataforms,
		processData: false,
		contentType: false,
		beforeSend:function(respuesta){
			$("#modal_wait").modal("show");
			$("#modal_confirmar").modal("hide");
		},
		success:function(respuesta){
			$("#modal_wait").modal("hide");
			$("#modal_confirmar").modal("hide");
			if(respuesta.code=="200"){		
				$("#messageadvertencia").empty();
				$("#messageadvertencia").append("<strong>" + respuesta.respuesta + "</strong>");
				$("#modal_alerta").modal("show");
				$("#list_productos").DataTable().row(selection).remove().draw();
				if($("#list_productos").DataTable().rows().data().length > 1 && $("#list_productos").DataTable().rows().data().length < 15){
					$("#btnGuardarCompleto").prop("disabled",false);
				}else{
					$("#btnGuardarCompleto").prop("disabled",true);
				}		
			}
		},
		error:function(respuesta){
			$("#modal_wait").modal("hide");
			$("#modal_confirmar").modal("hide");
			if(respuesta.responseJSON.code=="404" || respuesta.responseJSON.code=="409" || respuesta.responseJSON.code=="500"){
				$("#messageadvertencia").empty();
				$("#messageadvertencia").append("<strong>" + respuesta.responseJSON.respuesta + "</strong>");
				$("#modal_alerta").modal("show");
			}
		}
	});
}

function productosPrincipal(route,dataforms,flagcreate){
	var url = window.location.href.substr(0,window.location.href.lastIndexOf("ushop"));
	$.ajax({
		url: (url + route),
		headers:{
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		},
		method:"post",
		data:dataforms,
		processData: false,
		contentType: false,
		beforeSend:function(respuesta){
			$("#modal_wait").modal("show");
		},
		success:function(respuesta){
			$("#modal_wait").modal("hide");
			if(respuesta.code=="200"){		
				$("#messageadvertencia").empty();
				$("#messageadvertencia").append("<strong>" + respuesta.respuesta + "</strong>");
				$("#modal_alerta").modal("show");
				flagcreate = 1;
				
				$("#descripcionproducto").val("");
				$("#estilo_one").val("");
				$("#estilo_two").val("");
				$("#estilo_thrid").val("");
				$("#tipoproducto").val("");
				$("#productoname").val("");
			}
		},
		error:function(respuesta){
			$("#modal_wait").modal("hide");
			if(respuesta.responseJSON.code=="404" || respuesta.responseJSON.code=="409" || respuesta.responseJSON.code=="500"){
				$("#messageadvertencia").empty();
				$("#messageadvertencia").append("<strong>" + respuesta.responseJSON.respuesta + "</strong>");
				$("#modal_alerta").modal("show");
			}
		}
	});
	
	$("#modal_alerta").bind("hidden.bs.modal",function(){
		if(flagcreate){
			window.location.reload();
			flagcreate = undefined;
		}
	})
}