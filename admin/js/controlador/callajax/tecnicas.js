function tecnicasProductos(route,dataforms){
function tecnicasProductos(route,dataforms){
	var url = window.location.href.substr(0,window.location.href.lastIndexOf("ushop"));
	$.ajax({
		url: (url + route),
		headers:{
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		},
		method:"post",
		data:dataforms,
		processData: false,
		contentType: false,
		beforeSend:function(respuesta){
			$("#modal_wait").modal("show");
		},
		success:function(respuesta){
			$("#modal_wait").modal("hide");
			if(respuesta.code=="200"){		
				$("#messageadvertencia").empty();
				$("#messageadvertencia").append("<strong>" + respuesta.respuesta + "</strong>");
				$("#modal_alerta").modal("show");
				
			}
		},
		error:function(respuesta){
			$("#modal_wait").modal("hide");
			if(respuesta.responseJSON.code=="404" || respuesta.responseJSON.code=="409" || respuesta.responseJSON.code=="500"){
				$("#messageadvertencia").empty();
				$("#messageadvertencia").append("<strong>" + respuesta.responseJSON.respuesta + "</strong>");
				$("#modal_alerta").modal("show");
			}
		}
	});
}

function empresaServicios(route,dataforms){
	var url = window.location.href.substr(0,window.location.href.lastIndexOf("ushop"));
	$.ajax({
		url: (url + route),
		headers:{
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		},
		method:"post",
		data:dataforms,
		processData: false,
		contentType: false,
		beforeSend:function(respuesta){
			$("#modal_wait").modal("show");
		},
		success:function(respuesta){
			$("#modal_wait").modal("hide");
			if(respuesta.code=="200"){		
				$("#messageadvertencia").empty();
				$("#messageadvertencia").append("<strong>" + respuesta.respuesta + "</strong>");
				$("#modal_alerta").modal("show");
				
			}
		},
		error:function(respuesta){
			$("#modal_wait").modal("hide");
			if(respuesta.responseJSON.code=="404" || respuesta.responseJSON.code=="409" || respuesta.responseJSON.code=="500"){
				$("#messageadvertencia").empty();
				$("#messageadvertencia").append("<strong>" + respuesta.responseJSON.respuesta + "</strong>");
				$("#modal_alerta").modal("show");
			}
		}
	});
}

function updateCliente(route,dataforms,estado){
	var url = window.location.href.substr(0,window.location.href.lastIndexOf("ushop"));
	$.ajax({
		url: (url + route),
		headers:{
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		},
		method:"post",
		data:dataforms,
		processData: false,
		contentType: false,
		beforeSend:function(respuesta){
			$("#modal_wait").modal("show");
		},
		success:function(respuesta){
			$("#modal_wait").modal("hide");
			if(respuesta.code=="200"){		
				$("#messageadvertencia").empty();
				$("#messageadvertencia").append("<strong>" + respuesta.respuesta + "</strong>");
				$("#modal_alerta").modal("show");
				
			}
		},
		error:function(respuesta){
			$("#modal_wait").modal("hide");
			if(respuesta.responseJSON.code=="404" || respuesta.responseJSON.code=="409" || respuesta.responseJSON.code=="500"){
				$("#messageadvertencia").empty();
				$("#messageadvertencia").append("<strong>" + respuesta.responseJSON.respuesta + "</strong>");
				$("#modal_alerta").modal("show");
			}
		}
	});
}

function updateServicios(route,dataforms,estado){
	var url = window.location.href.substr(0,window.location.href.lastIndexOf("ushop"));
	$.ajax({
		url: (url + route),
		headers:{
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		},
		method:"put",
		data:dataforms,
		processData: false,
		contentType: false,
		beforeSend:function(respuesta){
			$("#modal_activar").modal("hide");
			$("#modal_wait").modal("show");
		},
		success:function(respuesta){
			$("#modal_wait").modal("hide");
			if(respuesta.code=="200"){		
				$("#messageadvertencia").empty();
				$("#messageadvertencia").append("<strong>" + respuesta.respuesta + "</strong>");
				$("#modal_alerta").modal("show");
			}
		},
		error:function(respuesta){
			$("#modal_wait").modal("hide");
			if(respuesta.responseJSON.code=="404" || respuesta.responseJSON.code=="409" || respuesta.responseJSON.code=="500"){
				$("#messageadvertencia").empty();
				$("#messageadvertencia").append("<strong>" + respuesta.responseJSON.respuesta + "</strong>");
				$("#modal_alerta").modal("show");
			}
		}
	});
	
	$("#modal_alerta").bind("hidden.bs.modal",function(){
		if(estado!=undefined){
			window.location.reload();
		}
	});
}