var pic = undefined;

function openFile(event){
  pic = event;
}

function choiceSelection(choice){
	switch(choice){
		case "0":
			return "Home/Inicio";
		break;
		case "1":
			return "Empresa";
		break;
		default:
			return "Otros";
		break;
	}
}

$(document).ready(function(){
	//BOTONES
	$(document).on("click","#btnGuardarCompleto",function(){
		if($("#secciones").val()=="NULL" && pic==undefined){
			$("#messageadvertencia").empty();
			$("#messageadvertencia").append("<strong>Todos los campos son obligatorios.</strong>");
			$("#modal_alerta").modal("show");
		}else if($("#secciones").val()=="NULL"){
			$("#messageadvertencia").empty();
			$("#messageadvertencia").append("<strong>Seleccione una sección válida.</strong>");
			$("#modal_alerta").modal("show");
		}else if(pic==undefined){
			$("#messageadvertencia").empty();
			$("#messageadvertencia").append("<strong>Seleccione un elemento media válida.</strong>");
			$("#modal_alerta").modal("show");
		}else{
			formData = new FormData();
			formData.append("seccion",seleccion);
			formData.append("media",$("#imagenpicture")[0].files[0]);
			formData.append("origen",btoa("2"));
			if($("#archivo").val()!=undefined){
				formData.append("codigoarchivo",$("#archivo").val());
			}
			
			pic = undefined;
			pdfCatalogo("ushop/catalogos/pdf",formData);
		}
	});
	
	/*SETUP-SECCIONES*/
	$("#secciones").val("NULL");
	$(document).on("change","#secciones",function(){
		if($(this).val()=="NULL"){
			$("#messageadvertencia").empty();
			$("#messageadvertencia").append("<strong>Seleccione una sección válida.</strong>");
			$("#modal_alerta").modal("show");
		}else{
			seleccion = $(this).val();
		}
	});
	
	//CAMBIO LA IMAGEN
	var _URL = window.URL || window.webkitURL;
	var seleccion = undefined;
	$("#imagenpicture").bind("change",function(){//CARGO LA PRIMERA IMAGEN
		if ((file = this.files[0])) {
			img = new Image();
			img.onload = function(){
			if((img.width!=2048) && (img.height!=500)){
					var url = window.location.href.substr(0,window.location.href.lastIndexOf("ushop"));
					$("#logo-a").attr("href",(url + "img/logo/logosimple_cf_ushop.svg"));
					$("#logo-img").attr("src",(url + "img/logo/logosimple_cf_ushop.svg"));
			
					$("#messageadvertencia").empty();
					$("#messageadvertencia").append("<strong>La medida ha ingresar no cumple con las medidas establecidas.</strong>");
					$("#modal_alerta").modal("show");
					pic = undefined;
				}
			}

			img.onerror = function() {
				//$("#avataradvertencia").text("Extensiones permitidas jpg, png");
				//$("#alerta_avatar").modal("show");
			};
			
			if(seleccion==undefined){
				$("#messageadvertencia").empty();
				$("#messageadvertencia").append("<strong>Debe seleccionar una sección destino antes de continuar.</strong>");
				$("#modal_alerta").modal("show");
			}else{
				img.src = _URL.createObjectURL(file);
				if((file.size/1024) > 30000){
					$("#messageadvertencia").empty();
					$("#messageadvertencia").append("<strong>El archivo ha utilizar no puede exceder los 2MB.</strong>");
					$("#modal_alerta").modal("show");
					pic = undefined;
				}else{
					if(file.type!="application/pdf"){
						$("#messageadvertencia").empty();
						$("#messageadvertencia").append("<strong>Extensión del archivo no permitida.</strong>");
						$("#modal_alerta").modal("show");
						pic = undefined;
					}else{
						if(file.type!="application/pdf"){
							$("#logo-a").attr("href",img.src);
							$("#logo-img").attr("src",img.src);
							$("#list_principal").DataTable().row.add( [
								file.name,
								choiceSelection(atob(seleccion))
							]).draw( false );
						}
						
					}
				}
			}
		}
	});
	
	var codigopdf = undefined;
	var codigopdfactivar = undefined;
	var seleccion = undefined;
	var activar = undefined;
	//ACTIVAR
	$(document).on("click","#btnConfirmarActivarMedia",function(){
		formData = new FormData();
		formData.append("codigo",codigopdfactivar);
		if($("#archivo").val()!=undefined){
			formData.append("codigoarchivo",$("#archivo").val());
		}
		activarCatalogo(("ushop/catalogos/active/" + codigopdfactivar),formData,seleccion,"#modal_confirmar_activar");
	});
	
	$(document).on("click","#btnActivarFile",function(){
		codigopdfactivar = $(this).data("codigocatalogoactive");
		activar = $(this).data("activar");
		if(activar=="1"){
			$("#activar_messageadvertencia").empty();
			$("#activar_messageadvertencia").append("<strong>¿Está usted seguro de desactivar su archivo?</strong>");
			$("#btnConfirmarActivarMedia").empty();
			$("#btnConfirmarActivarMedia").append("<i class=\"fa fa-power-off\" aria-hidden=\"true\"></i> DESACTIVAR<div class=\"ripple-container\"></div>");
			$("#modal_confirmar_activar").modal("show");
		}else{
			$("#activar_messageadvertencia").empty();
			$("#activar_messageadvertencia").append("<strong>¿Está usted seguro de activar su archivo?</strong>");
			$("#btnConfirmarActivarMedia").empty();
			$("#btnConfirmarActivarMedia").append("<i class=\"fa fa-power-off\" aria-hidden=\"true\"></i> ACTIVAR<div class=\"ripple-container\"></div>");
			$("#modal_confirmar_activar").modal("show");
		}
		
		seleccion = $(this).parents("tr");
	});
	
	//REMOVER
	$(document).on("click","#btnConfirmarRemoverMedia",function(){
		formData = new FormData();
		formData.append("codigo",codigopdf);
		eliminarCatalogo(("ushop/catalogos/pdf/delete/" + codigopdf),formData,seleccion,"#modal_confirmar");
	});
	
	$(document).on("click","#btnRemoverFile",function(){
		codigopdf = $(this).parents("tr").data("codigocatalogo");
		seleccion = $(this).parents("tr");
		if(codigopdf==undefined){
			codigopdf = $(this).data("codigocatalogoremover");
		}
		
		$("#modal_confirmar").modal("show");
	});
	
	//$("html, body").animate({ scrollTop: $("#" + atob($("#scroll").val())).offset().top }, 1500);
});