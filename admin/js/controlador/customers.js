var cliente =  "";

function generarURL(customerscodigo){
	return window.location.href.substr(0,window.location.href.lastIndexOf("ushop")) + "ushop/clientes/views/" + customerscodigo;
}

$(document).ready(function(){
	
	$(document).on("click","#btnGestionarcustomer",function(){
		var customerscodigo = $(this).parents("tr").data("customerscodigo");
		window.location.href = generarURL(customerscodigo);
	});
	
});

$(document).on("click",".btnEliminarcustomer",function(){
	cliente = $(this).closest('tr').data('customerscodigo');
	$('#modal_confirmar').modal('show');
});

$(document).on("click","#btnConfirmarRemoverMedia",function(){
	var url = window.location.href.substr(0,window.location.href.lastIndexOf("ushop"));
	var route = "ushop/empresas/clientes/d/" + cliente;
	$("#modal_confirmar").modal("hide");
	$.ajax({
		url: (url + route),
		headers:{
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		},
		method:"post",
		beforeSend:function(respuesta){
			$("#modal_wait").modal("show");
		},
		success:function(respuesta){
			$("#modal_wait").modal("hide");
			if(respuesta.code=="200"){		
				$("#messageadvertencia").empty();
				$("#messageadvertencia").append("<strong>" + respuesta.respuesta + "</strong>");
				$("#modal_alerta").modal("show");
				$("#logo-img-"+cliente).attr('src',url+'img/defaults/defaults.svg');
			}
			cliente = ""
		},
		error:function(respuesta){
			$("#modal_wait").modal("hide");
			cliente = ""
			console.log(respuesta);
			if(respuesta.responseJSON.code=="404" || respuesta.responseJSON.code=="409" || respuesta.responseJSON.code=="500"){
				$("#messageadvertencia").empty();
				$("#messageadvertencia").append("<strong>" + respuesta.responseJSON.respuesta + "</strong>");
				$("#modal_alerta").modal("show");
			}
		}
	});
});
