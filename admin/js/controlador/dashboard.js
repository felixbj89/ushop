function goURL(route){
	return window.location.href.substr(0,window.location.href.lastIndexOf("ushop")) + route;
}

$(document).ready(function(){
	$('.sidebar-menu').tree()
	
	lightbox.option({
		'resizeDuration': 200,
		'wrapAround': true,
		'albumLabel':''
	});
	
	if(localStorage.getItem("usushop")!=undefined){
		var usuario = JSON.parse(atob(localStorage.getItem("usushop")));
		$("#nombresidebarusuario").text(usuario.users_name);
		
		$(document).on("click","#clicksalirheader, #salirlock",function(){
			delete localStorage.usushop;
			delete localStorage.lock;
			window.location.href = goURL("ushop/salir");
		});
		
		$(document).on("click","#goinstrucciones",function(){
			var modo = $(this).data("modo");
			window.location.href = goURL("ushop/instrucciones/" + modo);
		});
		
		$(document).on("mouseover","#verpdf",function(){
			setTimeout(function(){
				$(".titulobox").text("Listar catálogo");
			},0);
			
		});
		
		$(document).on("mouseout","#verpdf",function(){
			setTimeout(function(){
				$(".titulobox").text("Catálogo");
			},0);			
		});
		
		if(atob(localStorage.getItem("lock"))=="1"){
			$("#modal_bloqueo").modal("show");
		}
		
		$("#lock").on("click",function(){
			localStorage.setItem("lock",btoa("1"));
			$("#modal_bloqueo").modal("show");
		});
		
		var estado = undefined;
		$("#btnAccederPassword").on("click",function(){
			formData = new FormData();
			formData.append("password",btoa($("#password").val()));
			formData.append("codigousuario",usuario.users_codigo);
			delete localStorage.lock;
			unlockAccount("ushop/dashboard/unlock",formData,estado);
		});
		
						
		var config_flag = undefined;
		$("#destinoenlace").val("NULL");
		$("#btnGuardarConfig").on("click",function(){
			if($("#enlaces").val().length==0 && $("#destinoenlace").val()=="NULL"){
				$("#modal_config").modal("hide");
				$("#enlacesadvertencia").empty();
				$("#enlacesadvertencia").append("<strong>Todos los campos son obligatorios</strong>");
				$("#modal_alerta_enlaces").modal("show");
				config_flag = 1;
			}else if($("#enlaces").val().length==0){
				$("#modal_config").modal("hide");
				$("#enlacesadvertencia").empty();
				$("#enlacesadvertencia").append("<strong>Ingrese un enlace válido</strong>");
				$("#modal_alerta_enlaces").modal("show");
				config_flag = 1;
			}else if($("#destinoenlace").val()=="NULL"){
				$("#modal_config").modal("hide");
				$("#enlacesadvertencia").empty();
				$("#enlacesadvertencia").append("<strong>Ingrese un destino válido</strong>");
				$("#modal_alerta_enlaces").modal("show");
				config_flag = 1;
			}else{
				formData = new FormData();
				formData.append("enlaces",btoa($("#enlaces").val()));
				formData.append("destinoenlace",btoa($("#destinoenlace").val()));
				managmentLinks("ushop/dashboard/config",formData,config_flag);
			}
		});
		
		$("#modal_alerta_enlaces").bind("hidden.bs.modal",function(){
			if(config_flag!=undefined && config_flag==1){
				$("#modal_config").modal("show");
				config_flag = undefined;
			}
		});
		
		$("#config").on("click",function(){
			$("#enlaces").val("");
			$("#destinoenlace").val("NULL");
			$("#modal_config").modal("show");
		});
	}
});