function generarURL(codigoempresa){
	return window.location.href.substr(0,window.location.href.lastIndexOf("ushop")) + "ushop/empresa/equipo/views/" + codigoempresa;
}

$(document).ready(function(){
	
	$(document).on("click","#btnGestionarEquipo",function(){
		var codigoempresa = $(this).parents("tr").data("codigoempresa");
		window.location.href = generarURL(codigoempresa);
	});
	
});