var reader,src,tr;
var item = "";
var count = 0;
var table = $('#example').DataTable({
    columns:[{data: 'Item'}],
    "language": {
        "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
    },
    "pageLength": 3,
    "ordering": false,
    "searching": false,
    "lengthChange": false,
    "drawCallback": function( settings ) {
        if(count > 0 && $('#img').hasClass('valid') && $('#des').hasClass('valid') && item != ""){
            var file = $("#img").clone();
            file.removeAttr('id');
            file.attr('name','img_'+count);
            file.addClass('atras');
            file.addClass('img');
            file.data('des','des_'+count);
            $('#inputsfile').append(file);
            var des = $('#des').clone();
            des.addClass('des');
            des.removeAttr('id');
            des.attr('name','des_'+count);
            $('#inputstext').append(des);
        }
        $('#img').removeClass('valid');
        $('#img').val('');
        $('#des').val('');
        $('#des').removeClass('valid');
        $('#managimg i').removeClass('fa-file-image-o');
        $('#managimg i').addClass('fa-file-o');
        $('#managimg label').removeClass('word-break-all');
        $('#managimg label').addClass('word-break-normal');
        $('#managimg label').text('Selecciona una imagen');
        $('.spanvalidate').hide();
        item = "";
        count = 0;
    }
});
$('#img').bind("change",function(){
    var input = this;
    if (input.files && input.files[0]){
        reader = new FileReader();
        reader.onload = function (e) {
        //   $('#img').attr('src', e.target.result);
            $('#managimg i').removeClass('fa-file-o');
            $('#managimg i').addClass('fa-file-image-o');
            $('#managimg label').removeClass('word-break-normal');
            $('#managimg label').addClass('word-break-all');
            $('#managimg label').text(input.files[0].name);
            $('#img').removeClass('invalid');
            $('#img').addClass('valid');
            $('#img').siblings('span.novalid').hide();
            $('#img').siblings('span.isvalid').show();
            src = e.target.result;
            $('#des').focus()
        }
        reader.readAsDataURL(input.files[0]);
    }else{
        //$('#img').attr('src', '/assets/no_preview.png');
        $('#managimg i').removeClass('fa-file-image-o');
        $('#managimg i').addClass('fa-file-o');
        $('#managimg label').removeClass('word-break-all');
        $('#managimg label').addClass('word-break-normal');
        $('#managimg label').text('Selecciona una imagen');
        $('#img').addClass('invalid');
        $('#img').removeClass('valid');
        $('#img').siblings('span.novalid').show();
        $('#img').siblings('span.isvalid').hide();
        src = "";
    }
});
$(document).on("change",".atras",function(eventObject){
    var input = this;
    if (input.files && input.files[0]){
        reader = new FileReader();
        var img = $('#'+$(this).attr('name'));
        var a = img.parents('tr').find('a.lb');
        reader.onload = function (e) {
            img.attr('src', e.target.result);
            a.attr('href', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
});
$('#formgaleriaemp input[type="text"]').bind('focusin focusout keyup',function(eventObject){
	if(!$(this).val() || ($(this).val().length < 20)){
        $(this).addClass('invalid');
        $(this).removeClass('valid');
        $(this).siblings('span.novalid').show();
        $(this).siblings('span.isvalid').hide();
	}else{
        $(this).removeClass('invalid');
        $(this).addClass('valid');
        $(this).siblings('span.novalid').hide();
        $(this).siblings('span.isvalid').show();
	}
});
$('#formgaleriaemp input').on('keyup blur', function (){
    if ($('#img').hasClass('valid') && $('#des').hasClass('valid')){
		$('#formgaleriaemp #addpregal').attr('disabled', false);
	} else {
		$('#formgaleriaemp #addpregal').attr('disabled', true);
	}
});
$( "#formgaleriaemp" ).on('submit',function( event ) {
    event.preventDefault();
    if ($('#img').hasClass('valid') && $('#des').hasClass('valid') && src!=""){
        var des = $('#des').val();
        count = new Date().getTime();
        item = "<div class=\"media\" data-count=\""+count+"\">\
            <div class=\"media-left media-middle\">\
                <a class=\"lb\" href=\""+src+"\" data-lightbox=\"example-set\" data-title=\"\">\
                    <img id=\"img_"+count+"\" src=\""+src+"\" width=\"128px\">\
                </a>\
            </div>\
            <div class=\"media-body\">\
                <div class=\"row\" style=\"width:100%;\">\
                    <div class=\"col-xs-12\">\
                        <div class=\"form-group w-100\">\
                            <input type=\"text\" class=\"des form-control w-100\" value=\""+des+"\" data-text=\"des_"+count+"\" disabled>\
                        </div>\
                    </div>\
                    <div class=\"col-xs-12\">\
                        <a class=\"btn btn-default btn-sm mt-0 btn-editpic\" href=\"img_"+count+"\">\
                        <i class=\"fa fa-picture-o\"></i> Cambiar</a>\
                        <a class=\"btn btn-default btn-sm mt-0 btn-editgal\" href=\"#\">\
                        <i class=\"fa fa-pencil\"></i> Editar</a>\
                        <a class=\"btn btn-default btn-sm  mt-0 btn-removgal\" href=\""+count+"\">\
                        <i class=\"fa fa-trash-o\"></i> Remover</a>\
                    </div>\
                </div>\
            </div>\
        </div>";
        table.rows.add( [ {
            "Item": item
        }] )
        .draw();     
	}
});
$('#addpregal').on('click',function(){
    $('#formgaleriaemp #addpregal').attr('disabled', true);
    $( "#formgaleriaemp" ).submit();
});
$(document).on('click','.btn-editgal',function(event){
    event.preventDefault();
    var mediabody = $(this).closest('.media-body');
    var input = mediabody.find('input[type="text"]');
    input.attr('disabled', false);
    input.focus();
});
$(document).on('click','.btn-removgal',function(event){
    event.preventDefault();
    tr = $(this).parents('tr');
    $('#btnConfirmarRemoverMedia').data('file','img_'+$(this).attr('href'));
    $('#btnConfirmarRemoverMedia').data('text','des_'+$(this).attr('href'));
    $('#modal_confirmar').modal('show');
});
$(document).on('click','.btn-editpic',function(event){
    event.preventDefault();
    $("#inputsfile").find('input[type="file"][name="'+$(this).attr('href')+'"]').click();
});
$(document).on('blur','#example input[type="text"]',function(){
    $('#inputstext input[type="text"][name="'+$(this).data('text')+'"]').val($(this).val());
    $(this).attr('disabled', 'disabled');
});
$('#btnConfirmarRemoverMedia').on('click',function(event){
    var codigoservicios = $('#formtablegal input[type="hidden"][name="servicio"]').val();
    if(codigoservicios != undefined){
        galeriaserviciosdel("ushop/galeria/servivio/d/"+codigoservicios,$('#btnConfirmarRemoverMedia').data('text'),tr);
        tr = "";    
    }
});
$('#btnGuardarPrincipal').on('click',function(event){
    $( "#formtablegal" ).submit();
});
$("#formtablegal").submit(function(e) {
    e.preventDefault();    
    var formData = new FormData();
    var des = $('#formtablegal').find('.des');
    var img = $('#formtablegal').find('.img');
    var codigoservicios = $('#formtablegal input[type="hidden"][name="servicio"]').val();
    $.each(des, function (index, value) {
        formData.append(value['name'],value['value']);
    });
    $.each(img, function (index, value) {
        if(value.files.length == 1)
          formData.append(value['name'],value.files[0]);
    });
    if(codigoservicios != undefined){
        galeriaservicios("ushop/galeria/servivio/"+codigoservicios,formData);
    }
});
$(document).ready(function() {

});