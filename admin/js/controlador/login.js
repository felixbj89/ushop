function generarURL(){
	return window.location.href.substr(0,window.location.href.search("admin"));
}

$(document).ready(function(){
	let alerta = "";
	
	$("#btnAcceder").on("click",function(){
		if(checkLength($("#email").val()) && checkLength($("#password").val())){
			$("#messageadvertencia").text("Todos los campos son obligatorios.");
			$("#modal_alerta").modal("show");
		}else if(checkLength($("#email").val())){
			$("#messageadvertencia").text("El E-mail ha ingresar es obligatorio.");
			$("#modal_alerta").modal("show");
		}else if(!checkEmail($("#email").val())){
			$("#messageadvertencia").text("El Formato de su e-mail ha ingresar es obligatorio.");
			$("#modal_alerta").modal("show");
		}else if(checkLength($("#password").val())){
			$("#messageadvertencia").text("El password ha ingresar es obligatorio.");
			$("#modal_alerta").modal("show");
		}else{
			formData = new FormData();
			formData.append("correo",$("#email").val());
			formData.append("password",$("#password").val());
			acceder("ushop/login",formData);
		}
	})
	
	$("#regresarsitio").on("click",function(){
		window.location.href = generarURL();
	});
});