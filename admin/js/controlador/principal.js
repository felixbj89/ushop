var pic = undefined;

function openFile(event){
  pic = event;
}

function generarURL(codigotecnica,modo){
	return window.location.href.substr(0,window.location.href.lastIndexOf("ushop")) + "ushop/principal/views/" + codigotecnica + "/" + modo;
}


$(document).ready(function(){
	//TEXTO
	$("#descripcion").summernote({
		lang: 'es-ES',
		toolbar: [
			['style', ['bold', 'italic', 'underline', 'clear']],
			['font', ['strikethrough', 'superscript', 'subscript']],
			['fontsize', ['fontsize']],
			['color', ['color']],
			['para', ['ul', 'ol', 'paragraph']],
		],
		fontSizes: ['8', '9', '10', '11', '12', '14', '18', '24', '32'],
		height: 300,
		minHeight: null,
		maxHeight: null,
		callbacks: {
			onChange:function(event){},
			onFocus: function() {},
			onKeyup: function(e) {},
			onPaste: function(e){
				//e.originalEvent.clipboardData.getData('text')
				var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('text');
				e.preventDefault();
				setTimeout(function () {
					document.execCommand('insertText', false, bufferText);                    
				}, 10);						
			}
		},
		placeholder:"Ingrese un texto de su agrado"
	});
	
	//CAMBIO LA IMAGEN
	var _URL = window.URL || window.webkitURL;
	var seleccion = undefined;
	$("#imagenpicture").bind("change",function(){//CARGO LA PRIMERA IMAGEN
		if ((file = this.files[0])) {
			img = new Image();
			img.onload = function(){
				if(atob($("#modo").val())=="cp" || atob($("#modo").val())=="bp"){
					if((img.width != 2048) && (img.height !=1500) || (img.width == 2048) && (img.height != 1500)){
						var url = window.location.href.substr(0,window.location.href.search("ushop"));
						$("#logo-a").attr("href",(url + "img/logo/logosimple_cf_ushop.svg"));
						$("#logo-img").attr("src",(url + "img/logo/logosimple_cf_ushop.svg"));
				
						$("#messageadvertencia").empty();
						$("#messageadvertencia").append("<strong>El archivo ingresado no cumple con las medidas establecidas.</strong>");
						$("#modal_alerta").modal("show");
						pic = undefined;
					}
				}else if(atob($("#modo").val())=="pi"){
					if((img.width != 2048) && (img.height > 600) || (img.width == 2048) && (img.height != 600)){
						var url = window.location.href.substr(0,window.location.href.search("ushop"));
						$("#logo-a").attr("href",(url + "img/logo/logosimple_cf_ushop.svg"));
						$("#logo-img").attr("src",(url + "img/logo/logosimple_cf_ushop.svg"));
				
						$("#messageadvertencia").empty();
						$("#messageadvertencia").append("<strong>El archivo ingresado no cumple con las medidas establecidas.</strong>");
						$("#modal_alerta").modal("show");
						pic = undefined;
					}
				}else{
					if((img.width != 2048) && (img.height > 500) || (img.width == 2048) && (img.height != 500)){
						var url = window.location.href.substr(0,window.location.href.search("ushop"));
						$("#logo-a").attr("href",(url + "img/logo/logosimple_cf_ushop.svg"));
						$("#logo-img").attr("src",(url + "img/logo/logosimple_cf_ushop.svg"));
				
						$("#messageadvertencia").empty();
						$("#messageadvertencia").append("<strong>El archivo ingresado no cumple con las medidas establecidas.</strong>");
						$("#modal_alerta").modal("show");
						pic = undefined;
					}
				}
			}
	

			img.onerror = function() {
				//$("#avataradvertencia").text("Extensiones permitidas jpg, png");
				//$("#alerta_avatar").modal("show");
			};
			
			if(seleccion==undefined){
				$("#messageadvertencia").empty();
				$("#messageadvertencia").append("<strong>Debe seleccionar una sección destino antes de continuar.</strong>");
				$("#modal_alerta").modal("show");
			}else{
				img.src = _URL.createObjectURL(file);
				if((file.size/1024) > 30000){
					$("#messageadvertencia").empty();
					$("#messageadvertencia").append("<strong>El archivo ha utilizar no puede exceder los 3MB.</strong>");
					$("#modal_alerta").modal("show");
					pic = undefined;
				}else{
					if(file.type!="image/jpeg" && file.type!="image/png" && file.type!="image/jpg" && file.type!="video/mp4"){
						$("#messageadvertencia").empty();
						$("#messageadvertencia").append("<strong>Extensión del archivo no permitida.</strong>");
						$("#modal_alerta").modal("show");
						pic = undefined;
					}else{
						if(file.type!="video/mp4"){
							$("#logo-a").attr("href",img.src);
							$("#logo-img").attr("src",img.src);
						}
						
					}
				}
			}
		}
	});
	
	var codigogaleria = undefined;
	var seleccion = undefined;
	$(document).on("click","#btnConfirmarRemoverMedia",function(){
		formData = new FormData();
		formData.append("codigo",codigogaleria);
		eliminarPicturePrincipal(("ushop/imagen/principal/delete/" + codigogaleria),formData,seleccion,"#modal_confirmar");
	});
	
	$(document).on("click","#btnRemoverMedia",function(){
		codigogaleria = $(this).parents("tr").data("codigogaleria");
		seleccion = $(this).parents("tr");
		if(codigogaleria==undefined){
			codigogaleria = $(this).data("codigogaleria");
		}
		
		$("#modal_confirmar").modal("show");
	});
	
	/*SETUP - SECCIONES*/
	$("#secciones").val("NULL");
	$(document).on("change","#secciones",function(){
		if($(this).val()=="NULL"){
			$("#messageadvertencia").empty();
			$("#messageadvertencia").append("<strong>Seleccione una sección válida.</strong>");
			$("#modal_alerta").modal("show");
			
			$("#tipomedia").prop("disabled",true);
			$("#tipomedia").empty();
		}else{
			seleccion = $(this).val();
			$("#tipomedia").prop("disabled",false);
			$("#tipomedia").empty();
			$("#tipomedia").append("<option selected=\"selected\" value=\"NULL\">Seleccione un tipo de archivo de su agrado</option>");
			if(atob(seleccion)=="0"){
				$("#tipomedia").append("<option value=\"" + btoa("0") + "\">Vídeo</option>");
				$("#tipomedia").append("<option value=\"" + btoa("1") + "\">Imágenes</option>");
			}else{
				if(atob(seleccion)=="3"){
					$("#bloquetitleurl").hide();
					$("#bloqueurlcompleto").hide();
					$("#tipomedia").append("<option value=\"" + btoa("1") + "\">Imágenes</option>");
				}else{
					$("#bloquetitleurl").show();
					$("#bloqueurlcompleto").show();
					$("#tipomedia").append("<option value=\"" + btoa("1") + "\">Imágenes</option>");
				}
			}
		}
	});
	
	//SETUP-FUENTES
	$("#fuentes").val("NULL");
	$("#fuentes").on("change",function(){
		if($(this).val()=="NULL"){
			$("#messageadvertencia").empty();
			$("#messageadvertencia").append("<strong>Seleccione una fuente válida.</strong>");
			$("#modal_alerta").modal("show");
		}else{
			var elementos = JSON.parse(atob($(this).val()));
			
			$.each(elementos,function(indice,fuentes){
				$("#tipofuentes").empty();
				$("#tipofuentes").append("<option value='NULL'>Seleccione la fuente de su agrado</option>");
				$.each(fuentes,function(clave,nombre){
					$("#tipofuentes").append("<option value='" + btoa(nombre) + "'>" + nombre + "</option>");
				});
			});
						
			$("#tipofuentes").prop("disabled",false);
		}
	});
	
	$(document).on("click","#btnGuardarCompleto",function(){
		var texto = $("#descripcion").summernote("code").replace(/\<br>/g, "");
		if($("#secciones").val() && pic==undefined && $("#fuentes").val()=="NULL" && $("#tipofuentes").val()=="NULL"){
			$("#messageadvertencia").empty();
			$("#messageadvertencia").append("<strong>Todos los campos son obligatorios.</strong>");
			$("#modal_alerta").modal("show");
		}else if($("#secciones").val()=="NULL"){
			$("#messageadvertencia").empty();
			$("#messageadvertencia").append("<strong>Seleccione una sección válida.</strong>");
			$("#modal_alerta").modal("show");
		}else if(pic==undefined){
			$("#messageadvertencia").empty();
			$("#messageadvertencia").append("<strong>Seleccione un elemento media válida.</strong>");
			$("#modal_alerta").modal("show");
		}else if(atob($("#secciones").val())!="3" && atob($("#secciones").val())!="2" && atob($("#secciones").val())!="1" && atob($("#modo").val())!="h"){
			$("#messageadvertencia").empty();
			$("#messageadvertencia").append("<strong>Seleccione una url válida.</strong>");
			$("#modal_alerta").modal("show");
		}else if($("#fuentes").val()=="NULL"){
			$("#messageadvertencia").empty();
			$("#messageadvertencia").append("<strong>Seleccione una fuente válida.</strong>");
			$("#modal_alerta").modal("show");
		}else if($("#tipofuentes").val()=="NULL"){
			$("#messageadvertencia").empty();
			$("#messageadvertencia").append("<strong>Seleccione un tipo de fuente válido.</strong>");
			$("#modal_alerta").modal("show");
		}else if(texto.length == 0){
			$("#messageadvertencia").empty();
			$("#messageadvertencia").append("<strong>Seleccione un texto válido.</strong>");
			$("#modal_alerta").modal("show");
		}else{
			formData = new FormData();
			formData.append("seccion",$("#secciones").val());
			formData.append("tipofuente",$("#tipofuentes").val());
			formData.append("media",$("#imagenpicture")[0].files[0]);
			if(atob($("#modo").val())=="cp"){
				formData.append("origen",btoa("2"));
			}else if(atob($("#modo").val())=="pi"){
				formData.append("origen",btoa("3"));
			}else if(atob($("#modo").val())=="bp"){
				formData.append("origen",btoa("4"));
			}else{
				formData.append("origen",btoa("0"));
			}
			
			formData.append("textos",texto);
			formData.append("url",$("#url").val());
			formData.append("tipomedia",$("#tipomedia").val());
			picturePrincipal("ushop/principal/propiedades",formData);
		}
	});
	
	$(document).on("click","#btnModificarMedia",function(){
		var codigogaleria = $(this).parents("tr").data("codigogaleria");
		var modo = $(this).parents("tr").data("modo");
		window.location.href = generarURL(codigogaleria,modo);
	});
	
	$(document).on("click","#btnGuardarPrincipal",function(){
		if($("#secciones").val()=="NULL" && pic==undefined){
			$("#messageadvertencia").empty();
			$("#messageadvertencia").append("<strong>Todos los campos son obligatorios.</strong>");
			$("#modal_alerta").modal("show");
		}else if($("#secciones").val()=="NULL"){
			$("#messageadvertencia").empty();
			$("#messageadvertencia").append("<strong>Seleccione una sección válida.</strong>");
			$("#modal_alerta").modal("show");
		}else if($("#tipomedia").val()=="NULL"){
			$("#messageadvertencia").empty();
			$("#messageadvertencia").append("<strong>Seleccione tipo de archivo válido.</strong>");
			$("#modal_alerta").modal("show");
		}else if(pic==undefined){
			$("#messageadvertencia").empty();
			$("#messageadvertencia").append("<strong>Seleccione un elemento media válida.</strong>");
			$("#modal_alerta").modal("show");
		}else{
			formData = new FormData();
			formData.append("seccion",$("#secciones").val());
			formData.append("media",$("#imagenpicture")[0].files[0]);
			if(atob($("#modo").val())=="cp"){
				formData.append("origen",btoa("2"));
			}else if(atob($("#modo").val())=="pi"){
				formData.append("origen",btoa("3"));
			}else if(atob($("#modo").val())=="bp"){
				formData.append("origen",btoa("4"));
			}else{
				formData.append("origen",btoa("0"));
			}
			
			formData.append("tipomedia",$("#tipomedia").val());
			picturePrincipal("ushop/imagen/principal",formData);
		}
	});
});