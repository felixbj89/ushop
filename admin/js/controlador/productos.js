var pic = undefined;

function openFile(event){
  pic = event;
}


$(document).ready(function(){
	//CAMBIO LA IMAGEN
	var _URL = window.URL || window.webkitURL;
	var seleccion = undefined;
	$("#imagenpicture").bind("change",function(){//CARGO LA PRIMERA IMAGEN
		if ((file = this.files[0])) {
			img = new Image();
			img.onload = function(){
				if((img.width != 260 && img.height != 350) || (img.width == 2048 && img.height != 350)){
					var url = window.location.href.substr(0,window.location.href.lastIndexOf("ushop"));
					$("#logo-a").attr("href",(url + "img/logo/logosimple_cf_ushop.svg"));
					$("#logo-img").attr("src",(url + "img/logo/logosimple_cf_ushop.svg"));
			
					$("#messageadvertencia").empty();
					$("#messageadvertencia").append("<strong>El archivo ingresado no cumple con las medidas establecidas.</strong>");
					$("#modal_alerta").modal("show");
					pic = undefined;
				}
			}
	

			img.onerror = function() {
				//$("#avataradvertencia").text("Extensiones permitidas jpg, png");
				//$("#alerta_avatar").modal("show");
			};
			
			img.src = _URL.createObjectURL(file);
			if((file.size/1024) > 30000){
				$("#messageadvertencia").empty();
				$("#messageadvertencia").append("<strong>El archivo ha utilizar no puede exceder los 3MB.</strong>");
				$("#modal_alerta").modal("show");
				pic = undefined;
			}else{
				if(file.type!="image/jpeg" && file.type!="image/png" && file.type!="image/jpg" && file.type!="video/mp4"){
					$("#messageadvertencia").empty();
					$("#messageadvertencia").append("<strong>Extensión del archivo no permitida.</strong>");
					$("#modal_alerta").modal("show");
					pic = undefined;
				}else{
					if(file.type!="video/mp4"){
						$("#logo-a").attr("href",img.src);
						$("#logo-img").attr("src",img.src);
					}
					
				}
			}
		}
	});
	
	var flagcreate = undefined;
	$(document).on("click","#btnGuardarCompleto",function(){
		if($("#productoname").val().length==0 && pic==undefined && $("#tipoproducto").val().length==0 && $("#estilo_one").val().length==0
		&& $("#estilo_two").val().length==0 && $("#estilo_thrid").val().length==0 && $("#descripcionproducto").val().length==0){
			$("#messageadvertencia").empty();
			$("#messageadvertencia").append("<strong>Todos los campos son obligatorios.</strong>");
			$("#modal_alerta").modal("show");
		}else if($("#productoname").val().length==0){
			$("#messageadvertencia").empty();
			$("#messageadvertencia").append("<strong>Seleccione un nombre válido.</strong>");
			$("#modal_alerta").modal("show");
		}else if($("#tipoproducto").val().length==0){
			$("#messageadvertencia").empty();
			$("#messageadvertencia").append("<strong>Seleccione un tipo de producto válido.</strong>");
			$("#modal_alerta").modal("show");
		}else if($("#estilo_one").val().length==0 || $("#estilo_two").val().length==0){
			$("#messageadvertencia").empty();
			$("#messageadvertencia").append("<strong>Seleccione un estilo para su producto válido.</strong>");
			$("#modal_alerta").modal("show");
		}else if($("#descripcionproducto").val().length==0){
			$("#messageadvertencia").empty();
			$("#messageadvertencia").append("<strong>Seleccione una descripción para su producto.</strong>");
			$("#modal_alerta").modal("show");
		}else{
			formData = new FormData();
			formData.append("seccion",btoa("1"));
			formData.append("origen",btoa("1"));
			formData.append("descripcion",$("#descripcionproducto").val());
			formData.append("estilo_one",$("#estilo_one").val());
			formData.append("estilo_two",$("#estilo_two").val());
			formData.append("estilo_thrid",$("#estilo_thrid").val());
			formData.append("tipoproducto",$("#tipoproducto").val());
			formData.append("media",$("#imagenpicture")[0].files[0]);
			formData.append("productoname",$("#productoname").val());
			if(codigoproducto!=undefined){
				formData.append("codigoproducto",codigoproducto);
			}
			
			productosPrincipal("ushop/productos",formData,flagcreate);
		}
	});
	
	//SETUP-MODIFICAR
	var codigoproducto = undefined;
	var seleccion = undefined;
	$(document).on("click","#btnModificarMedia",function(){
		//VACIO LOS INPUTS
		$("#descripcionproducto").val("");
		$("#estilo_one").val("");
		$("#estilo_two").val("");
		$("#estilo_thrid").val("");
		$("#tipoproducto").val("");
		$("#productoname").val("");
				
		codigoproducto = $(this).parents("tr").data("codigoproducto");
		formData = new FormData();
		productosViewPrincipal("ushop/productos/view/" + codigoproducto,formData);
	});
	
	$(document).on("click","#btnConfirmarRemoverProductos",function(){
		$("#modal_confirmar").modal("hide");
		formData = new FormData();
		formData.append("codigoproducto",codigoproducto);
		productosPrincipal("ushop/productos/delete/" + codigoproducto,formData,seleccion);
	});
	
	$(document).on("click","#btnRemoverMedia",function(){
		codigoproducto = $(this).parents("tr").data("codigoproducto");
		seleccion = $(this).parents("tr");
		$("#modal_confirmar").modal("show");
	});
});