function generarURL(codigoservicios){
	return window.location.href.substr(0,window.location.href.lastIndexOf("ushop")) + "ushop/empresa/servicios/views/" + codigoservicios;
}

$(document).ready(function(){
	
	$(document).on("click","#btnGestionarServicios",function(){
		var codigoservicios = $(this).parents("tr").data("codigoservicios");
		window.location.href = generarURL(codigoservicios);
	});
		
	var codigoservicios = undefined;
	var estado = undefined;
	$("#btnConfirmarRemoverMedia").on("click",function(){
		if(codigoservicios!=undefined){
			formData = new FormData();
			formData.append("codigoservicios",codigoservicios);
			updateServicios("ushop/empresas/servicios/" + codigoservicios,formData,estado);
		}
	});
	
	$(document).on("click","#btnActivado",function(){
		codigoservicios = $(this).parents("tr").data("codigoservicios");
		estado = atob($(this).parents("tr").data("estado"));
		if(estado=="1"){
			$("#confirmardadvertencia").empty();
			$("#confirmardadvertencia").append("<strong>¿Está usted seguro de desactivar a su servicio?</strong>");
			$("#btnConfirmarRemoverMedia").empty();
			$("#btnConfirmarRemoverMedia").append("<i class=\"fa fa-power-off\" aria-hidden=\"true\"></i> DESACTIVAR");
		}else{
			$("#confirmardadvertencia").empty();
			$("#confirmardadvertencia").append("<strong>¿Está usted seguro de activar su servicio?</strong>");
			$("#btnConfirmarRemoverMedia").empty();
			$("#btnConfirmarRemoverMedia").append("<i class=\"fa fa-power-off\" aria-hidden=\"true\"></i> ACTIVAR");
		}
		
		$("#modal_activar").modal("show");
	});
});