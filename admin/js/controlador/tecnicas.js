function generarURL(codigotecnica){
	return window.location.href.substr(0,window.location.href.lastIndexOf("ushop")) + "ushop/productos/tecnicas/views/" + codigotecnica;
}

$(document).ready(function(){
	
	$(document).on("click","#btnGestionarTecnica",function(){
		var codigotecnica = $(this).parents("tr").data("codigotecnica");
		window.location.href = generarURL(codigotecnica);
	});
	
});