var pic = undefined;

function openFile(event){
  pic = event;
}

$(document).ready(function(){
	var inputf = $("#imagenpicture");
	$(document).on("click","#btnGuardarPrincipal",function(){
		console.log();
		if ($("#imagenpicture")[0].files.length == 1) {
			formData = new FormData();
			formData.append("seccion",btoa("1"));
			formData.append("media",$("#imagenpicture")[0].files[0]);
			formData.append("origen",btoa("3"));
			updateCliente("ushop/empresas/clientes/" + $("#codigocustomers").val(),formData);
		}else{
			$("#messageadvertencia").empty();
			$("#messageadvertencia").append("<strong>El archivo ingresado no cumple con las medidas establecidas.</strong>");
			$("#modal_alerta").modal("show");
		}

	});
	
	//PRIMERA IMAGEN
	//CAMBIO LA IMAGEN
	var _URL = window.URL || window.webkitURL;
	var seleccion = undefined;
	$("#imagenpicture").bind("change",function(event){//CARGO LA PRIMERA IMAGEN
		if ((file = this.files[0])) {
			img = new Image();
			img.onload = function(){
				if(file.type!="image/svg+xml"){
					//if((img.width != 450) && (img.height != 450) || (img.width == 450) && (img.height != 450)){
					if((img.width > img.height) || (img.width == img.height)){
						var url = window.location.href.substr(0,window.location.href.lastIndexOf("ushop"));
						$("#logo-a").attr("href",(url + "img/logo/logosimple_cf_ushop.svg"));
						$("#logo-img").attr("src",(url + "img/logo/logosimple_cf_ushop.svg"));
				
						$("#messageadvertencia").empty();
						$("#messageadvertencia").append("<strong>El archivo ingresado no cumple con las medidas establecidas.</strong>");
						$("#modal_alerta").modal("show");
						pic = undefined;

						//resetear
						event.target.value = '';
						$('#imagenpicture').val('');
					}
				}
			}

			img.onerror = function() {
				//$("#avataradvertencia").text("Extensiones permitidas jpg, png");
				//$("#alerta_avatar").modal("show");
			};
						
			img.src = _URL.createObjectURL(file);
			if((file.size/1024) > 30000){
				$("#messageadvertencia").empty();
				$("#messageadvertencia").append("<strong>El archivo ha utilizar no puede exceder los 3MB.</strong>");
				$("#modal_alerta").modal("show");
				pic1 = undefined;
			}else{
				if(file.type!="image/svg+xml" && file.type!="image/jpeg" && file.type!="image/png" && file.type!="image/jpg" && file.type!="video/mp4"){
					$("#messageadvertencia").empty();
					$("#messageadvertencia").append("<strong>Extensión del archivo no permitida.</strong>");
					$("#modal_alerta").modal("show");
					pic1 = undefined;
				}else{
					if(file.type!="video/mp4"){
						$("#logo-a").attr("href",img.src);
						$("#logo-img").attr("src",img.src);
					}
					
				}
			}
		}
	});
});