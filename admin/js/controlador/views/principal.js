$(document).ready(function(){
	/*SETUP-MODIFICAR*/
	$(document).on("click","#btnModificarPrincipal",function(){
		formData = new FormData();
		formData.append("codigogaleria",$("#codigogaleria").val());
		formData.append("seccion",$("#section").val());
		formData.append("media",$("#imagenpicture")[0].files[0]);
		actualizarPrincipal("ushop/imagen/principal/" + $("#codigogaleria").val(),formData);
	});
	
	$(document).on("click","#btnModificarCompleto",function(){
		var texto = $("#descripcion").summernote("code").replace(/\<br>/g, "");
		if(texto.length == 0){
			$("#messageadvertencia").empty();
			$("#messageadvertencia").append("<strong>Seleccione un texto válido.</strong>");
			$("#modal_alerta").modal("show");
		}else{
			formData = new FormData();
			formData.append("codigogaleria",$("#codigogaleria").val());
			formData.append("seccion",$("#section").val());
			formData.append("media",$("#imagenpicture")[0].files[0]);
			formData.append("tipofuente",$("#tipofuentes").val());
			formData.append("url",$("#url").val());
			formData.append("todo","1");
			formData.append("textos",texto);
			actualizarPrincipal("ushop/imagen/principal/all/" + $("#codigogaleria").val(),formData);
		}
	});
	
	//SETUP-FUENTES
	$("#fuentes").val("NULL");
	$("#fuentes").on("change",function(){
		if($(this).val()=="NULL"){
			$("#messageadvertencia").empty();
			$("#messageadvertencia").append("<strong>Seleccione una fuente válida.</strong>");
			$("#modal_alerta").modal("show");
		}else{
			var elementos = JSON.parse(atob($(this).val()));
			
			$.each(elementos,function(indice,fuentes){
				$("#tipofuentes").empty();
				$("#tipofuentes").append("<option value='NULL'>Seleccione la fuente de su agrado</option>");
				$.each(fuentes,function(clave,nombre){
					$("#tipofuentes").append("<option value='" + btoa(nombre) + "'>" + nombre + "</option>");
				});
			});
						
			$("#tipofuentes").prop("disabled",false);
		}
	});
	
	//TEXTO
	$("#descripcion").summernote({
		lang: 'es-ES',
		toolbar: [
			['style', ['bold', 'italic', 'underline', 'clear']],
			['font', ['strikethrough', 'superscript', 'subscript']],
			['fontsize', ['fontsize']],
			['color', ['color']],
			['para', ['ul', 'ol', 'paragraph']],
		],
		height: 300,
		minHeight: null,
		maxHeight: null,
		callbacks: {
			onChange:function(event){},
			onFocus: function() {},
			onKeyup: function(e) {},
			onPaste: function(e){
				//e.originalEvent.clipboardData.getData('text')
				var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('text');
				e.preventDefault();
				setTimeout(function () {
					document.execCommand('insertText', false, bufferText);                    
				}, 10);						
			}
		},
		placeholder:"Ingrese un texto de su agrado"
	});
	
	//CAMBIO LA IMAGEN
	var _URL = window.URL || window.webkitURL;
	var seleccion = undefined;
	$("#imagenpicture").bind("change",function(){//CARGO LA PRIMERA IMAGEN
		if ((file = this.files[0])) {
			img = new Image();
			img.onload = function(){
				if(atob($("#modo").val())=="cp" || atob($("#modo").val())=="bp"){
					if((img.width != 2048) && (img.height !=1500) || (img.width == 2048) && (img.height != 1500)){
						var url = window.location.href.substr(0,window.location.href.lastIndexOf("ushop"));
						$("#logo-a").attr("href",(url + "img/logo/logosimple_cf_ushop.svg"));
						$("#logo-img").attr("src",(url + "img/logo/logosimple_cf_ushop.svg"));
				
						$("#messageadvertencia").empty();
						$("#messageadvertencia").append("<strong>El archivo ingresado no cumple con las medidas establecidas.</strong>");
						$("#modal_alerta").modal("show");
						pic = undefined;
					}
				}else if(atob($("#modo").val())=="pi"){
					if((img.width != 2048) && (img.height > 600) || (img.width == 2048) && (img.height != 600)){
						var url = window.location.href.substr(0,window.location.href.lastIndexOf("ushop"));
						$("#logo-a").attr("href",(url + "img/logo/logosimple_cf_ushop.svg"));
						$("#logo-img").attr("src",(url + "img/logo/logosimple_cf_ushop.svg"));
				
						$("#messageadvertencia").empty();
						$("#messageadvertencia").append("<strong>El archivo ingresado no cumple con las medidas establecidas.</strong>");
						$("#modal_alerta").modal("show");
						pic = undefined;
					}
				}else{
					if((img.width != 2048) && (img.height > 500) || (img.width == 2048) && (img.height != 500)){
						var url = window.location.href.substr(0,window.location.href.lastIndexOf("ushop"));
						$("#logo-a").attr("href",(url + "img/logo/logosimple_cf_ushop.svg"));
						$("#logo-img").attr("src",(url + "img/logo/logosimple_cf_ushop.svg"));
				
						$("#messageadvertencia").empty();
						$("#messageadvertencia").append("<strong>El archivo ingresado no cumple con las medidas establecidas.</strong>");
						$("#modal_alerta").modal("show");
						pic = undefined;
					}
				}
			}
	

			img.onerror = function() {
				//$("#avataradvertencia").text("Extensiones permitidas jpg, png");
				//$("#alerta_avatar").modal("show");
			};
			
			img.src = _URL.createObjectURL(file);
			if((file.size/1024) > 30000){
				$("#messageadvertencia").empty();
				$("#messageadvertencia").append("<strong>El archivo ha utilizar no puede exceder los 3MB.</strong>");
				$("#modal_alerta").modal("show");
				pic = undefined;
			}else{
				if(file.type!="image/jpeg" && file.type!="image/png" && file.type!="image/jpg" && file.type!="video/mp4"){
					$("#messageadvertencia").empty();
					$("#messageadvertencia").append("<strong>Extensión del archivo no permitida.</strong>");
					$("#modal_alerta").modal("show");
					pic = undefined;
				}else{
					if(file.type!="video/mp4"){
						$("#logo-a").attr("href",img.src);
						$("#logo-img").attr("src",img.src);
					}
					
				}
			}
		}
	});
});