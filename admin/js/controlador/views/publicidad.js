var pic = undefined;

function openFile(event){
  pic = event;
}

$(document).ready(function(){
	//TEXTO
	$("#descripcion_publicidad").summernote({
		lang: 'es-ES',
		toolbar: [
			['style', ['bold', 'italic', 'underline', 'clear']],
			['font', ['strikethrough', 'superscript', 'subscript']],
			['fontsize', ['fontsize']],
			['color', ['color']],
			['para', ['ul', 'ol', 'paragraph']],
		],
		height: 300,
		minHeight: null,
		maxHeight: null,
		callbacks: {
			onChange:function(event){},
			onFocus: function() {},
			onKeyup: function(e) {},
			onPaste: function(e){
				//e.originalEvent.clipboardData.getData('text')
				var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('text');
				e.preventDefault();
				setTimeout(function () {
					document.execCommand('insertText', false, bufferText);                    
				}, 10);						
			}
		},
		placeholder:"Ingrese un texto de su agrado"
	});
    
	//CAMBIO LA IMAGEN
	var _URL = window.URL || window.webkitURL;
	var seleccion = undefined;
	$("#imagenpicture").bind("change",function(){//CARGO LA PRIMERA IMAGEN
		if ((file = this.files[0])) {
			img = new Image();
			img.onload = function(){
                if(atob($("#modo").val())=="h"){
					if((img.width!=2048) && (img.height!=1500)){
						var url = window.location.href.substr(0,window.location.href.lastIndexOf("ushop"));
						$("#logo-a").attr("href",(url + "img/logo/logosimple_cf_ushop.svg"));
						$("#logo-img").attr("src",(url + "img/logo/logosimple_cf_ushop.svg"));
				
						$("#messageadvertencia").empty();
						$("#messageadvertencia").append("<strong>El archivo ingresado no cumple con las medidas establecidas.</strong>");
						$("#modal_alerta").modal("show");
						//pic = undefined;
					}
				}else if(atob($("#modo").val())=="c"){
					if(img.width!=2048 && img.height != 300 || img.width==2048 && img.height != 300){
						var url = window.location.href.substr(0,window.location.href.lastIndexOf("ushop"));
						$("#logo-a").attr("href",(url + "img/logo/logosimple_cf_ushop.svg"));
						$("#logo-img").attr("src",(url + "img/logo/logosimple_cf_ushop.svg"));
				
						$("#messageadvertencia").empty();
						$("#messageadvertencia").append("<strong>El archivo ingresado no cumple con las medidas establecidas.</strong>");
						$("#modal_alerta").modal("show");
						//pic = undefined;
					}
				}else if(atob($("#modo").val())=="p"){
					if(img.width!=2048 && img.height != 400 || img.width==2048 && img.height != 400){
						var url = window.location.href.substr(0,window.location.href.lastIndexOf("ushop"));
						$("#logo-a").attr("href",(url + "img/logo/logosimple_cf_ushop.svg"));
						$("#logo-img").attr("src",(url + "img/logo/logosimple_cf_ushop.svg"));
				
						$("#messageadvertencia").empty();
						$("#messageadvertencia").append("<strong>El archivo ingresado no cumple con las medidas establecidas.</strong>");
						$("#modal_alerta").modal("show");
						pic = undefined;
					}
				}else{
					if((img.width!=2048) && (img.height!=1500)){
						var url = window.location.href.substr(0,window.location.href.lastIndexOf("ushop"));
						$("#logo-a").attr("href",(url + "img/logo/logosimple_cf_ushop.svg"));
						$("#logo-img").attr("src",(url + "img/logo/logosimple_cf_ushop.svg"));
				
						$("#messageadvertencia").empty();
						$("#messageadvertencia").append("<strong>El archivo ingresado no cumple con las medidas establecidas.</strong>");
						$("#modal_alerta").modal("show");
						//pic = undefined;
					}
				}
			}

			img.onerror = function() {
				//$("#avataradvertencia").text("Extensiones permitidas jpg, png");
				//$("#alerta_avatar").modal("show");
			};
			
			img.src = _URL.createObjectURL(file);
            if((file.size/1024) > 30000){
                $("#messageadvertencia").empty();
                $("#messageadvertencia").append("<strong>El archivo ha utilizar no puede exceder los 3MB.</strong>");
                $("#modal_alerta").modal("show");
                pic = undefined;
            }else{
                if(file.type!="image/jpeg" && file.type!="image/png" && file.type!="image/jpg"){
                    $("#messageadvertencia").empty();
                    $("#messageadvertencia").append("<strong>Extensión del archivo no permitida.</strong>");
                    $("#modal_alerta").modal("show");
                    pic = undefined;
                }else{
                    $("#logo-a").attr("href",img.src);
                    $("#logo-img").attr("src",img.src);
                }
            }
		}
	});
	
	//SETUP-FUENTES
	$("#fuentes").val("NULL");
	$("#fuentes").on("change",function(){
		if($(this).val()=="NULL"){
			$("#messageadvertencia").empty();
			$("#messageadvertencia").append("<strong>Seleccione una fuente válida.</strong>");
			$("#modal_alerta").modal("show");
		}else{
			var elementos = JSON.parse(atob($(this).val()));
			
			$.each(elementos,function(indice,fuentes){
				$("#tipofuentes").empty();
				$("#tipofuentes").append("<option value='NULL'>Seleccione la fuente de su agrado</option>");
				$.each(fuentes,function(clave,nombre){
					$("#tipofuentes").append("<option value='" + btoa(nombre) + "'>" + nombre + "</option>");
				});
			});
						
			$("#tipofuentes").prop("disabled",false);
		}
	});
	
	/*SETUP-SECCIÓN*/
	var seleccion = undefined;
	$(document).on("change","#secciones",function(){
		if($(this).val()=="NULL"){
			$("#messageadvertencia").empty();
			$("#messageadvertencia").append("<strong>Seleccione una sección válida.</strong>");
			$("#modal_alerta").modal("show");
			
			$("#tipomedia").prop("disabled",true);
			$("#tipomedia").empty();
		}else{
			seleccion = $(this).val();
			$("#tipomedia").prop("disabled",false);
			$("#tipomedia").empty();
			$("#tipomedia").append("<option selected=\"selected\" value=\"NULL\">Seleccione un tipo de archivo de su agrado</option>");
			if(atob(seleccion)=="0"){
				$("#tipomedia").append("<option value=\"" + btoa("0") + "\">Vídeo</option>");
				$("#tipomedia").append("<option value=\"" + btoa("1") + "\">Imágenes</option>");
			}else{
				$("#tipomedia").append("<option value=\"" + btoa("1") + "\">Imágenes</option>");
			}
		}
	});
	
	//REMOVER
	var codigogaleria = undefined;
	$(document).on("click","#btnConfirmarRemoverMedia",function(){
		formData = new FormData();
		formData.append("codigo",codigogaleria);
		eliminarPublicitaria(("ushop/imagen/principal/delete/" + codigogaleria),formData,seleccion,"#modal_confirmar");
	});
	
	$(document).on("click","#btnRemoverMedia",function(){
		codigogaleria = $(this).parents("tr").data("codigogaleria");
		seleccion = $(this).parents("tr");
		if(codigogaleria==undefined){
			codigogaleria = $(this).data("codigogaleria");
		}
		
		$("#modal_confirmar").modal("show");
	});
	
	//BOTONES
	var codigogaleria = undefined;
	$(document).on("click","#btnModificarPublicidad",function(){
        var texto = $("#descripcion_publicidad").summernote("code").replace(/\<br>/g, "");
		if(atob($("#modo").val())=="co" && $("#url").val().length == 0){
			$("#messageadvertencia").empty();
			$("#messageadvertencia").append("<strong>Seleccione una url válida.</strong>");
			$("#modal_alerta").modal("show");
		}else if(texto.length == 0){
			$("#messageadvertencia").empty();
			$("#messageadvertencia").append("<strong>Seleccione un texto válido.</strong>");
			$("#modal_alerta").modal("show");
		}else{
            formData = new FormData();
            formData.append("codigo",$("#codigo").val());
            formData.append("media",$("#imagenpicture")[0].files[0]);
            formData.append("textos",texto);
            formData.append("modo",$("#modo").val());
            formData.append("seccion",$("#seccion").val());
            formData.append("url",$("#url").val());
            formData.append("fuentes",$("#fuentes").val());
            formData.append("tipofuente",$("#tipofuentes").val());
            actualizarPublicidad("ushop/imagen/publicidad/all/" + $("#codigo").val(),formData);
        }
	});
});