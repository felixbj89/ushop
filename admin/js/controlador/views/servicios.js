var pic = undefined;

function openFile(event){
  pic = event;
}

/*SETUP-IMÁGENES*/
var pic1 = undefined;
var pic2 = undefined;
var pic3 = undefined;
function openFile1(event){
  pic1 = event;
}

function openFile2(event){
  pic2 = event;
}

function openFile3(event){
  pic3 = event;
}


$(document).ready(function(){
	
	//TEXTO
	$("#descripcion").summernote({
		lang: 'es-ES',
		toolbar: [
			['style', ['bold', 'italic', 'underline', 'clear']],
			['font', ['strikethrough', 'superscript', 'subscript']],
			['fontsize', ['fontsize']],
			['color', ['color']],
			['para', ['ul', 'ol', 'paragraph']],
		],
		height: 300,
		minHeight: null,
		maxHeight: null,
		callbacks: {
			onChange:function(event){},
			onFocus: function() {},
			onKeyup: function(e) {},
			onPaste: function(e){
				//e.originalEvent.clipboardData.getData('text')
				var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('text');
				e.preventDefault();
				setTimeout(function () {
					document.execCommand('insertText', false, bufferText);                    
				}, 10);						
			}
		},
		placeholder:"Ingrese una descripción de su agrado"
	});
	
	//CAMBIO LA IMAGEN
	var _URL = window.URL || window.webkitURL;
	var seleccion = undefined;
	$("#imagenpicture").bind("change",function(){//CARGO LA PRIMERA IMAGEN
		if ((file = this.files[0])) {
			img = new Image();
			img.onload = function(){
				if(file.type!="image/svg+xml" && (img.width != 360) && (img.height > 150) || (img.width == 360) && (img.height != 150)){
					var url = window.location.href.substr(0,window.location.href.lastIndexOf("ushop"));
					$("#logo-a").attr("href",(url + "img/logo/logosimple_cf_ushop.svg"));
					$("#logo-img").attr("src",(url + "img/logo/logosimple_cf_ushop.svg"));
			
					$("#messageadvertencia").empty();
					$("#messageadvertencia").append("<strong>El archivo ingresado no cumple con las medidas establecidas.</strong>");
					$("#modal_alerta").modal("show");
					pic = undefined;
				}
			}
	

			img.onerror = function() {
				//$("#avataradvertencia").text("Extensiones permitidas jpg, png");
				//$("#alerta_avatar").modal("show");
			};
						
			var texto = $("#descripcion").summernote("code").replace(/\<br>/g, "");
			if(texto.trim().length <= 7){
				$("#messageadvertencia").empty();
				$("#messageadvertencia").append("<strong>Debe ingresar una descripción válida antes de continuar.</strong>");
				$("#modal_alerta").modal("show");
			}else{
				img.src = _URL.createObjectURL(file);
				if((file.size/1024) > 30000){
					$("#messageadvertencia").empty();
					$("#messageadvertencia").append("<strong>El archivo ha utilizar no puede exceder los 3MB.</strong>");
					$("#modal_alerta").modal("show");
					pic = undefined;
				}else{
					if(file.type!="image/svg+xml" && file.type!="image/jpeg" && file.type!="image/png" && file.type!="image/jpg" && file.type!="video/mp4"){
						$("#messageadvertencia").empty();
						$("#messageadvertencia").append("<strong>Extensión del archivo no permitida.</strong>");
						$("#modal_alerta").modal("show");
						pic = undefined;
					}else{
						if(file.type!="video/mp4"){
							$("#logo-a").attr("href",img.src);
							$("#logo-img").attr("src",img.src);
						}
						
					}
				}
			}
		}
	});
	
	//PRIMERA IMAGEN
	//CAMBIO LA IMAGEN
	var _URL = window.URL || window.webkitURL;
	var seleccion = undefined;
	$("#imagenpicture_one").bind("change",function(){//CARGO LA PRIMERA IMAGEN
		if ((file = this.files[0])) {
			img = new Image();
			img.onload = function(){
				if((img.width != 250) && (img.height != 450) || (img.width == 250) && (img.height != 450)){
					var url = window.location.href.substr(0,window.location.href.lastIndexOf("ushop"));
					$("#logo-a_one").attr("href",(url + "img/logo/logosimple_cf_ushop.svg"));
					$("#logo-img_one").attr("src",(url + "img/logo/logosimple_cf_ushop.svg"));
			
					$("#messageadvertencia").empty();
					$("#messageadvertencia").append("<strong>El archivo ingresado no cumple con las medidas establecidas.</strong>");
					$("#modal_alerta").modal("show");
					pic = undefined;
				}
			}
	

			img.onerror = function() {
				//$("#avataradvertencia").text("Extensiones permitidas jpg, png");
				//$("#alerta_avatar").modal("show");
			};
						
			var texto = $("#descripcion").summernote("code").replace(/\<br>/g, "");
			if(texto.trim().length <= 7){
				$("#messageadvertencia").empty();
				$("#messageadvertencia").append("<strong>Debe ingresar una descripción válida antes de continuar.</strong>");
				$("#modal_alerta").modal("show");
			}else{
				img.src = _URL.createObjectURL(file);
				if((file.size/1024) > 30000){
					$("#messageadvertencia").empty();
					$("#messageadvertencia").append("<strong>El archivo ha utilizar no puede exceder los 3MB.</strong>");
					$("#modal_alerta").modal("show");
					pic1 = undefined;
				}else{
					if(file.type!="image/svg+xml" && file.type!="image/jpeg" && file.type!="image/png" && file.type!="image/jpg" && file.type!="video/mp4"){
						$("#messageadvertencia").empty();
						$("#messageadvertencia").append("<strong>Extensión del archivo no permitida.</strong>");
						$("#modal_alerta").modal("show");
						pic1 = undefined;
					}else{
						if(file.type!="video/mp4"){
							$("#logo-a_one").attr("href",img.src);
							$("#logo-img_one").attr("src",img.src);
						}
						
					}
				}
			}
		}
	});
	
	//SEGUNDA IMAGEN
	//CAMBIO LA IMAGEN
	var _URL = window.URL || window.webkitURL;
	var seleccion = undefined;
	$("#imagenpicture_two").bind("change",function(){//CARGO LA PRIMERA IMAGEN
		if ((file = this.files[0])) {
			img = new Image();
			img.onload = function(){
				if((img.width != 200) && (img.height != 450) || (img.width == 200) && (img.height != 450)){
					var url = window.location.href.substr(0,window.location.href.lastIndexOf("ushop"));
					$("#logo-a_two").attr("href",(url + "img/logo/logosimple_cf_ushop.svg"));
					$("#logo-img_two").attr("src",(url + "img/logo/logosimple_cf_ushop.svg"));
					
					$("#messageadvertencia").empty();
					$("#messageadvertencia").append("<strong>El archivo ingresado no cumple con las medidas establecidas.</strong>");
					$("#modal_alerta").modal("show");
					pic2 = undefined;
				}
			}
	

			img.onerror = function() {
				//$("#avataradvertencia").text("Extensiones permitidas jpg, png");
				//$("#alerta_avatar").modal("show");
			};
						
			var texto = $("#descripcion").summernote("code").replace(/\<br>/g, "");
			if(texto.trim().length <= 7){
				$("#messageadvertencia").empty();
				$("#messageadvertencia").append("<strong>Debe ingresar una descripción válida antes de continuar.</strong>");
				$("#modal_alerta").modal("show");
			}else{
				img.src = _URL.createObjectURL(file);
				if((file.size/1024) > 30000){
					$("#messageadvertencia").empty();
					$("#messageadvertencia").append("<strong>El archivo ha utilizar no puede exceder los 3MB.</strong>");
					$("#modal_alerta").modal("show");
					pic2 = undefined;
				}else{
					if(file.type!="image/svg+xml" && file.type!="image/jpeg" && file.type!="image/png" && file.type!="image/jpg" && file.type!="video/mp4"){
						$("#messageadvertencia").empty();
						$("#messageadvertencia").append("<strong>Extensión del archivo no permitida.</strong>");
						$("#modal_alerta").modal("show");
						pic2 = undefined;
					}else{
						if(file.type!="video/mp4"){
							$("#logo-a_two").attr("href",img.src);
							$("#logo-img_two").attr("src",img.src);
						}
						
					}
				}
			}
		}
	});
	
	//TERCERA IMAGEN
	//CAMBIO LA IMAGEN
	var _URL = window.URL || window.webkitURL;
	var seleccion = undefined;
	$("#imagenpicture_thrid").bind("change",function(){//CARGO LA PRIMERA IMAGEN
		if ((file = this.files[0])) {
			img = new Image();
			img.onload = function(){
				if((img.width != 150) && (img.height != 450) || (img.width == 150) && (img.height != 450)){
					var url = window.location.href.substr(0,window.location.href.lastIndexOf("ushop"));
					$("#logo-a_thrid").attr("href",(url + "img/logo/logosimple_cf_ushop.svg"));
					$("#logo-img_thrid").attr("src",(url + "img/logo/logosimple_cf_ushop.svg"));
			
					$("#messageadvertencia").empty();
					$("#messageadvertencia").append("<strong>El archivo ingresado no cumple con las medidas establecidas.</strong>");
					$("#modal_alerta").modal("show");
					pic3 = undefined;
				}
			}
	

			img.onerror = function() {
				//$("#avataradvertencia").text("Extensiones permitidas jpg, png");
				//$("#alerta_avatar").modal("show");
			};
						
			var texto = $("#descripcion").summernote("code").replace(/\<br>/g, "");
			if(texto.trim().length <= 7){
				$("#messageadvertencia").empty();
				$("#messageadvertencia").append("<strong>Debe ingresar una descripción válida antes de continuar.</strong>");
				$("#modal_alerta").modal("show");
			}else{
				img.src = _URL.createObjectURL(file);
				if((file.size/1024) > 30000){
					$("#messageadvertencia").empty();
					$("#messageadvertencia").append("<strong>El archivo ha utilizar no puede exceder los 3MB.</strong>");
					$("#modal_alerta").modal("show");
					pic2 = undefined;
				}else{
					if(file.type!="image/svg+xml" && file.type!="image/jpeg" && file.type!="image/png" && file.type!="image/jpg" && file.type!="video/mp4"){
						$("#messageadvertencia").empty();
						$("#messageadvertencia").append("<strong>Extensión del archivo no permitida.</strong>");
						$("#modal_alerta").modal("show");
						pic2 = undefined;
					}else{
						if(file.type!="video/mp4"){
							$("#logo-a_thrid").attr("href",img.src);
							$("#logo-img_thrid").attr("src",img.src);
						}
						
					}
				}
			}
		}
	});
	
	$(document).on("click","#btnGuardarPrincipal",function(){
		var texto = $("#descripcion").summernote("code").replace(/\<br>/g, "");
		if(texto.trim().length <= 7){
			$("#messageadvertencia").empty();
			$("#messageadvertencia").append("<strong>Debe ingresar una descripción válida antes de continuar.</strong>");
			$("#modal_alerta").modal("show");
		}else{
			formData = new FormData();
			formData.append("seccion",btoa("1"));
			formData.append("media",$("#imagenpicture")[0].files[0]);
			formData.append("one",$("#imagenpicture_one")[0].files[0]);
			formData.append("two",$("#imagenpicture_two")[0].files[0]);
			formData.append("thrid",$("#imagenpicture_thrid")[0].files[0]);
			formData.append("origen",btoa("1"));
			formData.append("textos",texto);
			empresaServicios("ushop/empresas/servicios/" + $("#codigoservicios").val(),formData);
		}
	});
});