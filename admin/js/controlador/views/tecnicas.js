var pic = undefined;

function openFile(event){
  pic = event;
}

$(document).ready(function(){
	
	//TEXTO
	$("#descripcion").summernote({
		lang: 'es-ES',
		toolbar: [
			['style', ['bold', 'italic', 'underline', 'clear']],
			['font', ['strikethrough', 'superscript', 'subscript']],
			['fontsize', ['fontsize']],
			['color', ['color']],
			['para', ['ul', 'ol', 'paragraph']],
		],
		height: 300,
		minHeight: null,
		maxHeight: null,
		callbacks: {
			onChange:function(event){},
			onFocus: function() {},
			onKeyup: function(e) {},
			onPaste: function(e){
				//e.originalEvent.clipboardData.getData('text')
				var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('text');
				e.preventDefault();
				setTimeout(function () {
					document.execCommand('insertText', false, bufferText);                    
				}, 10);						
			}
		},
		placeholder:"Ingrese una descripción de su agrado"
	});
	
	//CAMBIO LA IMAGEN
	var _URL = window.URL || window.webkitURL;
	var seleccion = undefined;
	$("#imagenpicture").bind("change",function(){//CARGO LA PRIMERA IMAGEN
		if ((file = this.files[0])) {
			img = new Image();
			img.onload = function(){
				if((img.width != 160) && (img.height > 400) || (img.width == 160) && (img.height != 400)){
					var url = window.location.href.substr(0,window.location.href.lastIndexOf("ushop"));
					$("#logo-a").attr("href",(url + "img/logo/logosimple_cf_ushop.svg"));
					$("#logo-img").attr("src",(url + "img/logo/logosimple_cf_ushop.svg"));
			
					$("#messageadvertencia").empty();
					$("#messageadvertencia").append("<strong>El archivo ingresado no cumple con las medidas establecidas.</strong>");
					$("#modal_alerta").modal("show");
					pic = undefined;
				}
			}
	

			img.onerror = function() {
				//$("#avataradvertencia").text("Extensiones permitidas jpg, png");
				//$("#alerta_avatar").modal("show");
			};
			
			var texto = $("#descripcion").summernote("code").replace(/\<br>/g, "");
			if(texto.trim().length <= 7){
				$("#messageadvertencia").empty();
				$("#messageadvertencia").append("<strong>Debe ingresar una descripción válida antes de continuar.</strong>");
				$("#modal_alerta").modal("show");
			}else{
				img.src = _URL.createObjectURL(file);
				if((file.size/1024) > 30000){
					$("#messageadvertencia").empty();
					$("#messageadvertencia").append("<strong>El archivo ha utilizar no puede exceder los 3MB.</strong>");
					$("#modal_alerta").modal("show");
					pic = undefined;
				}else{
					if(file.type!="image/jpeg" && file.type!="image/png" && file.type!="image/jpg" && file.type!="video/mp4"){
						$("#messageadvertencia").empty();
						$("#messageadvertencia").append("<strong>Extensión del archivo no permitida.</strong>");
						$("#modal_alerta").modal("show");
						pic = undefined;
					}else{
						if(file.type!="video/mp4"){
							$("#logo-a").attr("href",img.src);
							$("#logo-img").attr("src",img.src);
						}
						
					}
				}
			}
		}
	});
	
	$(document).on("click","#btnGuardarPrincipal",function(){
		var texto = $("#descripcion").summernote("code").replace(/\<br>/g, "");
		if(texto.trim().length <= 7){
			$("#messageadvertencia").empty();
			$("#messageadvertencia").append("<strong>Debe ingresar una descripción válida antes de continuar.</strong>");
			$("#modal_alerta").modal("show");
		}else{
			formData = new FormData();
			formData.append("seccion",btoa("2"));
			formData.append("media",$("#imagenpicture")[0].files[0]);
			formData.append("origen",btoa("2"));
			formData.append("textos",texto);
			tecnicasProductos("ushop/productos/tecnicas/" + $("#codigotecnica").val(),formData);
		}
	});
});