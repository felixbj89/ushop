function checkLength(campo){
	if(campo.length==0){
		return 1;
	}else{
		return 0;
	}
}

function checkEmail(correo){
	var expresion = /^[a-z0-9_\.\-]+(.[a-z0-9_\.\-]+)*@[a-z0-9]+(.[a-z0-9_\.\-]+)*(\.[a-z]{2,4})+$/;
	if(!expresion.test(correo)){
		return 0;
	}else{
		return 1;
	}
}