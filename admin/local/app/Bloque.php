<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bloque extends Model
{
    protected $table = "bloque";
	
    protected $fillable = [
        'bloque_name',
		'bloque_codigo',
		'bloque_color',
		'bloque_size',
		'bloque_url',
		'fonts_id'
    ];

    
    protected $hidden = ["id",'fonts_id'];
	
	public static function confirmarURL($lista){
		foreach($lista as $l){
			$url = Bloque::where("id",$l->bloque_id)->first();
			if($url->bloque_url!=NULL || $url->bloque_url!="undefined"){
				return true;
			}
		}
		
		return false;
	}
}
