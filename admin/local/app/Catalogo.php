<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Session;
use DB;
use Lang;

class Catalogo extends Model
{
	//LIST
    public static function listarcatalogopdf(){
		$respuesta = DB::select("CALL InCatalogo(?,?,?,?,?,?,?,?,?)",array(
			'2',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL
		));
		
		return [$respuesta,200];
	}
	
	//VIEW
	public static function viewcatalogopdf($codigo){
		$respuesta = DB::select("CALL InCatalogo(?,?,?,?,?,?,?,?,?)",array(
			'3',NULL,$codigo,NULL,NULL,NULL,NULL,NULL,NULL
		));
		
		return [$respuesta,200];
	}
	
	//DELETE
	public static function eliminarcatalogo($codigo,$inputs){
		$catalogo = Catalogo::viewcatalogopdf($codigo)[0];
		if(count($catalogo) > 0){
			$ruta_archivo = "img/catalogo/".$catalogo[0]->catalogo_ruta;
			if(file_exists($ruta_archivo))unlink($ruta_archivo);
			$respuesta = DB::select("CALL InCatalogo(?,?,?,?,?,?,?,?,?)",array(
				'4',NULL,$inputs["codigo"],NULL,NULL,NULL,NULL,NULL,NULL
			));
			
			return [Lang::get("message.yes_delete"),$respuesta,200];
		}else{
			return ["eliminarcatalogo",NULL,500];
		}
	}
	
	//ACTIVE
	public static function catalogoActivo(){
		$respuesta = DB::select("CALL InCatalogo(?,?,?,?,?,?,?,?,?)",array(
			'5',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL
		));
		
		return [$respuesta,200];
	}
	
	public static function utilizarCatalogo($codigo,$inputs){
		if(array_key_exists("codigoarchivo",$inputs) && $inputs["codigoarchivo"]!=""){
			$respuesta = DB::select("CALL InCatalogo(?,?,?,?,?,?,?,?,?)",array(
				'6',NULL,$inputs["codigo"],NULL,NULL,$inputs["codigoarchivo"],NULL,NULL,NULL
			));
		}else{
			$respuesta = DB::select("CALL InCatalogo(?,?,?,?,?,?,?,?,?)",array(
				'6',NULL,$inputs["codigo"],NULL,NULL,NULL,NULL,NULL,NULL
			));
		}
		
		return [Lang::get("message.yes_modify"),$respuesta,200];
	}
	
	//ADD
	public static function catalogospdf($inputs){
		if(array_key_exists("seccion",$inputs) && $inputs["seccion"]!=""){
			$section = base64_decode($inputs["seccion"]);
			$origen = base64_decode($inputs["origen"]);
			//DISCRIMINANTE-SECCIONES
			if($section=="0"){
				$nombre = "HOME/INICIO";
			}else if($seccion=="1"){
				$nombre = "EMPRESA";
			}else{
				$nombre = "OTROS";
				
			}
			
			$codigoarchivo = "";
			if(array_key_exists("codigoarchivo",$inputs) && $inputs["codigoarchivo"]!=""){
				$codigoarchivo = $inputs["codigoarchivo"];
			}
			
			//CONFIGURO EL ALMACENAMIENTO DEL PDF
			if(array_key_exists("media",$inputs) && $inputs["media"]!=""){
				$nombre = $_FILES["media"]["name"];
				$codigo = str_random(10);
				$media = hash("md5",$inputs["seccion"])."/".md5($_FILES["media"]["name"]).".pdf";
				$acceso = \Cache::store('database')->get('acceso');
				$usuario = json_decode(base64_decode(\Cache::store('database')->get('usuario_'.$acceso)));
				$respuesta = DB::select("CALL InCatalogo(?,?,?,?,?,?,?,?,?)",array(
					'1',$nombre,$codigo,$section,$origen,$usuario->users_codigo,$media,"1",$codigoarchivo
				));
				
				if(!isset($respuesta[0]->confirmar)){
					\Storage::disk('catalogoupload')->put($media,  \File::get($inputs["media"]));
					return [Lang::get("message.yes_register"),Catalogo::listarcatalogopdf(),200];
				}else{
					if($respuesta[0]->confirmar=="1")
						return [Lang::get("message.match_pdf"),NULL,409];
					else
						return [Lang::get("message.size_pdf"),NULL,409];
				}
			}
			
			return ["catalogospdf",NULL,500];
		}
	}
}
