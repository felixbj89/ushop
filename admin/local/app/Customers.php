<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Lang;

class Customers extends Model
{
    protected $table = "customers";

	protected $fillable = [
		"customers_codigo",
		'customers_logo_off',
		"customers_logo_on",
		"users_id",
	];

	protected $hidden = [
	  'id'
	];
	
	public static function listCustomers(){
		$customers = Customers::get();
		if($customers!=NULL){
			return [$customers->toJson(),200];
		}else{
			return [NULL,404];
		}
	}
	
	public static function viewCustomers($codigo){
		$clientes = Customers::where("customers_codigo",$codigo);
		if($clientes!=NULL){
			return $clientes->first()->toJSon();
		}else{
			return [];
		}
	}
	
	public static function actualizarClientes($codigo,$inputs){
		$clientes = Customers::where("customers_codigo",$codigo);
		if($clientes!=NULL){
			$media = NULL;
			if(array_key_exists("media",$inputs) && $inputs["media"]!="undefined"){
				$imagen = $clientes->first()->customers_logo_on;
				//if($imagen!="img/defaults/defaults.svg" && file_exists($imagen))unlink($imagen);
				$originalname = $_FILES["media"]["name"];
				//$archivo = substr($archivo,strrpos($archivo,"."),strlen($archivo));
				$logo = "";
				$partes = explode(".", $originalname);
				$i = count($partes);
				$extension = $partes[ $i-1 ];
				$logo = hash("md5",$inputs["seccion"])."/".md5($_FILES["media"]["name"]).".".$extension;
				/*if(strtoupper($archivo)==".SVG"){
					$logo = hash("md5",$inputs["seccion"])."/".md5($_FILES["media"]["name"]).".svg";
				}else{
					$logo = hash("md5",$inputs["seccion"])."/".md5($_FILES["media"]["name"]);
				}*/
				$media = ("img/media/customers/".$logo);
				if(!file_exists($media)){
					\Storage::disk('customerupload')->put($logo,  \File::get($inputs["media"]));
				}
				if(file_exists($imagen)){//borrar el viejo
					if(strcmp($imagen, 'img/defaults/defaults.svg') !== 0)
						unlink($imagen);
				}
			}else{
				$media = $clientes->first()->customers_logo_on;
			}
			
			$clientes->update([
				"customers_logo_on" => $media,
				"customers_logo_off" => $media
			]);
			
			return [Lang::get("message.yes_modify"),200];
		}else{
			return [Lang::get("message.not_modify"),409];
		}
	}

	public static function deletecliente($codigo){
		$clientes = Customers::where("customers_codigo",$codigo);
		if($clientes!=NULL){
			$count = Customers::where('customers_logo_on','img/defaults/defaults.svg')->count();
			if($count < 3){
				$imagen = $clientes->first()->customers_logo_on;
				if(file_exists($imagen)){//borrar el viejo
					if(strcmp($imagen, 'img/defaults/defaults.svg') !== 0)
						unlink($imagen);
				}
	
				$clientes->update([
					"customers_logo_on" => 'img/defaults/defaults.svg',
					"customers_logo_off" => 'img/defaults/defaults.svg'
				]);
				
				return [Lang::get("message.yes_modify"),200];
			}else{
				return ["Debe existri un mínimo de cinco items para mostar",409];
			}
		}else{
			return [Lang::get("message.not_modify"),409];
		}
	}
}
