<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Lang;

class Equipo extends Model
{
    protected $table = "equipo";

	protected $fillable = [
		"equipo_nombre",
		'equipo_codigo',
		"equipo_section",
		"equipo_origen",
		"equipo_ruta",
		"users_id"
	];

	protected $hidden = [
	  'id'
	];
	
	public static function viewEquipo($codigo){
		return Equipo::where("equipo_codigo",$codigo)->first()->toJson();
	}
	
	public static function listEquipo(){
		return Equipo::get()->toJson();
	}
	
	public static function listEquipos(){
		$equipos = Equipo::get();
		if($equipos!=NULL){
			return [$equipos->toJson(),200];
		}else{
			return [NULL,404];
		}
	}
	
	public static function equipo($codigo,$inputs){
		$equipo = Equipo::where("equipo_codigo",$codigo);
		if($equipo!=NULL){
			$media = NULL;
			if(array_key_exists("media",$inputs) && $inputs["media"]!="undefined"){
				$imagen = $equipo->first()->equipo_ruta;
				if(file_exists($imagen))unlink($imagen);
				$archivo = $_FILES["media"]["name"];
				$archivo = substr($archivo,strrpos($archivo,"."),strlen($archivo));
				$media = hash("md5",$inputs["seccion"])."/".md5($_FILES["media"]["name"])."".$archivo;
				\Storage::disk('equipoupload')->put($media,  \File::get($inputs["media"]));
				$media = ("img/media/equipos/".$media);
			}else{
				$media = $equipo->first()->equipo_ruta;
			}
			
			$equipo->update([
				"equipo_ruta" => $media
			]);
			
			return [Lang::get("message.yes_modify"),200];
		}else{
			return [Lang::get("message.not_modify"),409];
		}
	}
}
