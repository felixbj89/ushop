<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fonts extends Model
{
    protected $table = "fonts";
	
    protected $fillable = [
        'fonts_family',
		'fonts_codigo',
    ];

    
    protected $hidden = ["id"];

}
