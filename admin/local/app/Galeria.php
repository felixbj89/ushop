<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Galeria extends Model
{
    protected $table = "galeriaservicio";
    protected $fillable = [
        'galeria_codigo',
        'servicios_codigo',
        'galeria_imagen',
        'galeria_descripcion'
    ];
    protected $hidden = ["id"];

	//ADD
	public static function tituloportafolio($codigo,$cod,$input){
        $titulo = Galeria::where("servicios_codigo",$codigo)
            ->where("galeria_codigo",$cod)
            ->first();
        if(is_null($titulo)){//create
            Galeria::insert([
                'galeria_codigo' =>  $cod,
                'servicios_codigo' => $codigo,
                'galeria_imagen' => 'img/defaults/defaults.svg',
                'galeria_descripcion' => $input
            ]);
        }else{//update
            $titulo->update([
				'galeria_descripcion' => $input
			]);
        }
        return ["tituloportafolio",$titulo,200];
    }
    public static function imagenportafolio($codigo,$cod,$input){
        $imagen = Galeria::where("servicios_codigo",$codigo)
            ->where("galeria_codigo",$cod)
            ->first();
		try {
            $originalname = $input->getClientOriginalName();
            $partes = explode(".", $originalname);
            $i = count($partes);
            $extension = $partes[ $i-1 ];
            $date = Carbon::now('America/Santiago');
            $name = $date->year.$date->month.$date->day."". rand()."".$date->hour.$date->minute.$date->second.".".$extension;
            $filepath = 'img/media/servicios/galeriasecundaria/'.$name; 
		} catch (Exception $e) {
            $filepath = 'img/defaults/defaults.svg';
		}
        if(is_null($imagen)){//create
            if(!file_exists($filepath) && strcmp($filepath, 'img/defaults/defaults.svg') !== 0){
                \Storage::disk('logoserviciosupload')->put($name,  \File::get($input));
            }
            Galeria::insert([
                'galeria_codigo' =>  $cod,
                'servicios_codigo' => $codigo,
                'galeria_imagen' =>  $filepath,
                'galeria_descripcion' =>""
            ]);
        }else{//update
            if(!file_exists($filepath) && strcmp($filepath, 'img/defaults/defaults.svg') !== 0){
                \Storage::disk('logoserviciosupload')->put($name, \File::get($input));
            }
            if(file_exists($imagen->galeria_imagen)){//borrar el viejo
                if(strcmp($imagen->galeria_imagen, 'img/defaults/defaults.svg') !== 0)
                    unlink($imagen->galeria_imagen);
            }
            $imagen->update([
				'galeria_imagen' => $filepath
			]);
        }
        return ["imagenportafolio",$imagen,200];
    }
    public static function removegaleriaservices($codigo,$cod){
        $portafolio = Galeria::where("servicios_codigo",$codigo)
            ->where("galeria_codigo",$cod)
            ->first();
        if(!is_null($portafolio)){
            if(file_exists($portafolio->galeria_imagen)){//borrar el viejo
                if(strcmp($portafolio->galeria_imagen, 'img/defaults/defaults.svg') !== 0)
                    unlink($portafolio->galeria_imagen);
            }
            $portafolio->delete();
        }
        return ["viewgaleriaservices",$portafolio,200];
    }
    public static function viewgaleriaservices($codigo){
        $galeriaservices = Galeria::where("servicios_codigo",$codigo)
            ->get();
        return ["viewgaleriaservices",$galeriaservices,200];
    }
    public static function listGaleria(){
        $galeriaservices = Galeria::select('servicios_codigo','galeria_imagen','galeria_descripcion')->get();
        return [$galeriaservices,200];
    }
}
