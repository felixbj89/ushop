<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Customers;

class CustomersController extends Controller
{
    public static function listCustomers(){
		$estado = Customers::listCustomers();
		return response()->json([
			"respuesta" => $estado[0],
			"code" => $estado[1]
		],$estado[1]);
	}
	
	public function actualizarClientes($codigo, Request $request){
		$estado = Customers::actualizarClientes($codigo,$request->all());
		return response()->json([
			"respuesta" => $estado[0],
			"code" => $estado[1]
		],$estado[1]);
	}

	public function deletecliente($codigo){
		$estado = Customers::deletecliente($codigo);
		return response()->json([
			"respuesta" => $estado[0],
			"code" => $estado[1]
		],$estado[1]);
	}
}
