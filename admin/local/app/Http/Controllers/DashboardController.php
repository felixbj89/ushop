<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use App\Media;
use App\Catalogo;
use App\Tecnicas;
use App\Equipo;
use App\Customers;
use App\Servicios;
use App\Users;
use App\Productos;
use App\Bloque;
use App\Galeria;
use DB;
use Lang;
use Config;

class DashboardController extends Controller
{
	//UTILIDADES
	private function listArchivos($ruta){
		$directorio = opendir($ruta); //ruta actual
		$fuentes = [];
		$i = 0;
		while ($archivo = readdir($directorio)){
			if (!is_dir($archivo)){
				$fuentes[$i++] = $archivo;
			}
		}
		
		return $fuentes;
	}
	
	private function listContactos(){
		return json_encode(DB::table("contacto")->get());
	}
	
	private function countVisitas(){
		$visitas = $section = \DB::table("section")->get();
		$array = [];
		$i = 0;
		foreach($visitas as $v){
			$mes = explode("-",explode(" ",$v->section_fecha)[0]);
			if($v->section_type=="0"){
				$array[$i][0] = [$v->section_visitas,$mes[1]."/".$mes[0]];
			}else if($v->section_type=="1"){
				$array[$i][1] = [$v->section_visitas,$mes[1]."/".$mes[0]];
			}else if($v->section_type=="2"){
				$array[$i][2] = [$v->section_visitas,$mes[1]."/".$mes[0]];
			}else if($v->section_type=="3"){
				$array[$i][3] = [$v->section_visitas,$mes[1]."/".$mes[0]];
			}
			
			$i++;
		}
		
		return $array;
	}
	
	private function countBrowser(){
		$browser = $section = \DB::table("browser")->get();
		$array = [];
		$i = 0;
		
		$mf = 0;
		$me = 0;
		$gc = 0;
		foreach($browser as $b){
			if($b->browser_type=="0"){
				$mf++;
			}else if($b->browser_type=="1"){
				$me++;
			}else if($b->browser_type=="2"){
				$gc++;
			}
			
			$i++;
		}
		
		return [$mf,$me,$gc];
	}
	
	public function eliminarSolicitud($codigo){
		DB::table("contacto")->where("contacto_codigo",$codigo)->delete();
		
		return response()->json([
			"codigo" => $codigo,
			"respuesta" => "La solicitud ha sido removida",
			"code" => 200
		],200);
	}
	
	//DASHBOARD
    public function dashboard(){
		$locationes = \DB::table("location")->get()->toJson();
		
		$route = url('/');
		$rutabase = substr($route,0,strrpos($route,"admin"));
		
		$archivo = Catalogo::catalogoActivo()[0];
		$tecnicas = Tecnicas::listTecnicas();
		$contactos = $this->listContactos();
		return view("backend.dashboard",[
			"ruta" => $archivo,
			"contactos" => json_decode($contactos),
			"tecnicas" => json_decode($tecnicas[0]),
			"visitaActual" => json_encode($this->countVisitas()),
			"navegadores" => json_encode($this->countBrowser()),
			"locationes" => json_decode($locationes),
			"rutabase" => $rutabase,
			"banners" => [
				count(Media::listImagenPrincipal("0","0")[0]),
				count(Media::listImagenPrincipal("1","0")[0]),
				count(Media::listImagenPrincipal("2","0")[0]),
				count(Media::listImagenPrincipal("3","0")[0])
			]
		]);
	}
	
	public function instrucciones($modo){
		return view("backend.instrucciones",[
			"modo" => base64_decode($modo)
		]);
	}
	
	public function solicitudes(){
		$contactos = $this->listContactos();
		return view("backend.solicitudes",[
			"contactos" => json_decode($contactos),
		]);
	}
	
	public function unlock(Request $request){
		$is = Users::isPassword($request->all()["codigousuario"],$request->all()["password"]);
		return response()->json([
			"respuesta" => $is[0],
			"code" => $is[1]
		],$is[1]);
	}
	
	public function config(Request $request){
		$cadena_enlaces = '<?php return [';		
		$enlaces = Config::get("enlaces");
		if($enlaces==NULL){
			$destino = base64_decode(base64_decode($request->input("destinoenlace")));
			$enlace = base64_decode($request->input("enlaces"));
			$cadena_enlaces.= '"'.$destino.'" => "'.$enlace.'"';
			$cadena_enlaces.= ']; ?>';
			$file = fopen('local/config/enlaces.php', "a");
			fwrite($file, $cadena_enlaces . PHP_EOL);
			fclose($file);
			
			return response()->json([
				"respuesta" => Lang::get("message.yes_register"),
				"code" => 200
			],200);
		}else{
			$destino = base64_decode(base64_decode($request->input("destinoenlace")));
			$enlace = base64_decode($request->input("enlaces"));
			$confirmar = 0;
			foreach($enlaces as $key => $e){
				if($key==$destino){
					$cadena_enlaces.= '"'.$key.'" => "'.$enlace.'",';
					$confirmar = 1;
				}else{
					$cadena_enlaces.= '"'.$key.'" => "'.$e.'",';
				}
			}
			
			if(!$confirmar){
				$cadena_enlaces.= '"'.$destino.'" => "'.$enlace.'"';
			}else{
				$cadena_enlaces = (substr($cadena_enlaces,0,strlen($cadena_enlaces)-1));
			}
			
			$cadena_enlaces.= ']; ?>';
			unlink('local/config/enlaces.php');
			$file = fopen('local/config/enlaces.php', "a");
			fwrite($file, $cadena_enlaces . PHP_EOL);
			fclose($file);
			
			return response()->json([
				"respuesta" => Lang::get("message.yes_register"),
				"code" => 200
			],200);
		}
	}
	
	//ADD/LIST/EDIT
	public function principal($modo){
		$fuentes = $this->listArchivos('fonts');
		$tipos = [];
		$j = 0;
		foreach($fuentes as $fonts){
			$tipos[$fonts][$j++] = $this->listArchivos('fonts/'.$fonts);
		}
		
		$lista = NULL;
		$confirmarURL = false;
		if(base64_decode($modo)=="h"){
			$lista = Media::listImagenPrincipal("0","0")[0];
			$confirmarURL = Bloque::confirmarURL($lista);
		}else if(base64_decode($modo)=="c"){
			$lista = Media::listImagenPrincipal("3","0")[0];
		}else if(base64_decode($modo)=="cp"){
			$lista = Media::listImagenPrincipal("3","2")[0];
		}else if(base64_decode($modo)=="p"){
			$lista = Media::listImagenPrincipal("2","0")[0];
		}else if(base64_decode($modo)=="pi"){
			$lista = Media::listImagenPrincipal("2","3")[0];
		}else if(base64_decode($modo)=="bp"){
			$lista = Media::listImagenPrincipal("2","4")[0];
		}else if(base64_decode($modo)=="e"){
			$lista = Media::listImagenPrincipal("1","0")[0];
		}
		
		return view("backend.principal",[
			"lista" => $lista,
			"modo" => base64_decode($modo),
			"fuentes" => json_encode($tipos),
			"confirmarurl" => $confirmarURL
		]);
	}
	
	public function publicidad($modo){
		$fuentes = $this->listArchivos('fonts');
		$tipos = [];
		$j = 0;
		foreach($fuentes as $fonts){
			$tipos[$fonts][$j++] = $this->listArchivos('fonts/'.$fonts);
		}
		
		$lista = NULL;
		if(base64_decode($modo)=="h"){
			$lista = Media::listImagenPrincipal("0","1")[0];
		}else if(base64_decode($modo)=="c"){
			$lista = Media::listImagenPrincipal("3","1")[0];
		}else if(base64_decode($modo)=="p"){
			$lista = Media::listImagenPrincipal("2","1")[0];
		}else if(base64_decode($modo)=="bp"){
			$lista = Media::listImagenPrincipal("2","4")[0];
		}
		
		return view("backend.publicidad",[
			"fuentes" => json_encode($tipos),
			"lista" => $lista,
			"modo" => base64_decode($modo)
		]);
	}
	
	public function equipo(){
		$lista = Media::listImagenPrincipal("1","1")[0];
		return view("backend.equipo",[
			"lista" => $lista,
		]);
	}
	
	public function empresaEquipo(){
		return view("backend.empresaequipo",[
			"equipo" => json_decode(Equipo::listEquipo())
		]);
	}
	
	public function catalogospdf($scroll){
		$lista = Catalogo::listarcatalogopdf()[0];
		$archivo = Catalogo::catalogoActivo()[0];
		return view("backend.catalogospdf",[
			"lista" => $lista,
			"scroll" => $scroll,
			"archivo" => $archivo
		]);
	}
	
	//VIEWS
	public function viewstecnicas($codigo){
		$tecnica = Tecnicas::viewTecnica($codigo);
		return view("backend.views.viewtecnica",[
			"tecnica" => json_decode($tecnica)
		]);
	}

	public function viewPublicidad($codigo, $modo){
		$fuentes = $this->listArchivos('fonts');
		$tipos = [];
		$j = 0;
		foreach($fuentes as $fonts){
			$tipos[$fonts][$j++] = $this->listArchivos('fonts/'.$fonts);
		}
		
		$lista = Media::viewImagenPrincipal($codigo)[0][0];
		$confirmarURL = false;
		if(base64_decode($modo)=="h"){
			if($lista->bloque_url!=NULL || $lista->bloque_url!="undefined" || $lista->bloque_url!="#"){
				$confirmarURL = true;
			}
		}
		
		return view("backend.views.viewspublicidad",[
			"publicidad" => Media::viewImagenPrincipal($codigo)[0][0],
			"codigo" => $codigo,
			"fuentes" => json_encode($tipos),
			"modo" => base64_decode($modo),
			"confirmarurl" => $confirmarURL
		]);
	}
	
	public function viewsEquipo($codigo){
		$equipo = Equipo::viewEquipo($codigo);
		return view("backend.views.viewequipo",[
			"equipo" => json_decode($equipo)
		]);
	}
	
	public function viewServicios($codigo){
		$servicios = Servicios::viewServicios($codigo);
		return view("backend.views.viewservicios",[
			"servicios" => json_decode($servicios)
		]);
	}
	
	public function viewsClientes($codigo){
		$clientes = Customers::viewCustomers($codigo);
		return view("backend.views.viewclientes",[
			"clientes" => json_decode($clientes,true)
		]);
	}
	
	public function viewPrincipal($codigo, $modo){
		$fuentes = $this->listArchivos('fonts');
		$tipos = [];
		$j = 0;
		foreach($fuentes as $fonts){
			$tipos[$fonts][$j++] = $this->listArchivos('fonts/'.$fonts);
		}
		
		$lista = Media::viewImagenPrincipal($codigo)[0][0];
		$confirmarURL = false;
		if(base64_decode($modo)=="h"){
			$galeria = Media::listImagenPrincipal("0","0")[0];
			foreach($galeria as $gal){
				if(intval($gal->bloque_id) > 1){
					$confirmarURL = true;
					break;
				}
			}
		}

		return view("backend.views.viewsprincipal",[
			"principal" => Media::viewImagenPrincipal($codigo)[0][0],
			"fuentes" => json_encode($tipos),
			"modo" => base64_decode($modo),
			"confirmarurl" => $confirmarURL
		]);
	}
	
	//ADD
	public function productos(){
		return view("backend.productos",[
			"productos" => json_decode(Productos::listProductos()[0])
		]);
	}
	
	public function tecnicas(){
		$tecnicas = Tecnicas::listTecnicas();
		return view("backend.tecnicas",[
			"tecnicas" => json_decode($tecnicas[0])
		]);
	}
	
	public function servicios(){
		$servicios = Servicios::listServicios();
		return view("backend.servicios",[
			"servicios" => json_decode($servicios[0])
		]);
	}
	
	public function clientes(){
		$clientes = Customers::listCustomers();
		return view("backend.clientes",[
			"clientes" => json_decode($clientes[0])
		]);
	}
	
	//SALIR
	public function salir(){
		Session::flush();
		if(\Cache::store('database')->has('acceso')){
			$acceso = \Cache::store('database')->get('acceso');
			DB::table("cache")->where("key","laravel_cacheacceso")->delete();
			DB::table("cache")->where("key","laravel_cacheusuario_".$acceso)->delete();
			return redirect()->to("ushop/login");
		}else{
			Session::flush();
			return response(view("errors.403"),403);
		}
	}

	//GALERIA
	public function viewgaleriaServicios($codigo){
		$route = url('/');
		$rutabase = substr($route,0,strrpos($route,"admin"));
		$servicios = Servicios::viewServicios($codigo);
		$items = Galeria::viewgaleriaservices($codigo);
		return view("backend.views.viewserviciosgaleria",[
			"servicios" => json_decode($servicios),
			"items" => $items[1],
			"rutabase" => $rutabase.'admin/'
		]);
	}
	public function galeriaservices($codigo,Request $request){
		$titulo = "";	
		try {
			$servicios = Servicios::viewServicios($codigo);
			foreach($request->all() as $key => $value){
				$cod = substr($key, 4, strlen($key));
				if ((strcmp($value, "undefined") !== 0) || (strcmp($value, "") !== 0)) //si no es un file vacio -> entra
					if(is_object($value)){//es una imagen
						$respuesta = Galeria::imagenportafolio($codigo,$cod,$value);
					}else{//es titulo
						$respuesta = Galeria::tituloportafolio($codigo,$cod,$value);
					}
			}
		} catch (Exception $e) {
			return response()->json([
				"respuesta" => \Lang::get("message.not_register"),
				"code" => 409
			],409);
		}
		return response()->json([
			"respuesta" => \Lang::get("message.yes_register"),
			"code" => 200
		],200);
	}
	public function removegaleriaservices($codigo,Request $request){
		try {
			$servicios = Servicios::viewServicios($codigo);
			$cod = substr($request->cod, 4, strlen($request->cod));
			$respuesta = Galeria::removegaleriaservices($codigo,$cod);
		} catch (Exception $e) {
			return response()->json([
				"respuesta" => \Lang::get("message.not_delete"),
				"code" => 409
			],409);
		}
		return response()->json([
			"respuesta" => \Lang::get("message.yes_delete"),
			"code" => 200
		],200);
	}	
}
