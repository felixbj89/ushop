<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Catalogo;

class FileController extends Controller
{
	//ADD
	public function catalogospdf(Request $request){
		$estado = Catalogo::catalogospdf($request->all());
		return response()->json([
			"respuesta" => $estado[0],
			"catalogo" => $estado[1],
			"code" => $estado[2]
		],$estado[2]);
	}
	
	//DELETE
	public function eliminarcatalogo($codigo, Request $request){
		$estado = Catalogo::eliminarcatalogo($codigo,$request->all());
		return response()->json([
			"respuesta" => $estado[0],
			"catalogo" => $estado[1],
			"code" => $estado[2]
		],$estado[2]);
	}
	
	//ACTIVE
	public function utilizarCatalogo($codigo, Request $request){
		$estado = Catalogo::utilizarCatalogo($codigo,$request->all());
		return response()->json([
			"respuesta" => $estado[0],
			"catalogo" => $estado[1],
			"code" => $estado[2]
		],$estado[2]);
	}
	
	//LIST
	public function listcatalogospdf(){
		$estado = Catalogo::listarcatalogopdf();
		return response()->json([
			"respuesta" => $estado[0],
			"code" => $estado[1]
		],$estado[1]);
	}
	
	public static function listcatalogo(){
		$estado = Catalogo::listarcatalogopdf();
		return response()->json([
			"respuesta" => $estado[0],
			"code" => $estado[1]
		],$estado[1]);
	}
}
