<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Users;

class LoginController extends Controller
{
    public function login(){
		return view("backend.login");
	}
	
	public function autenticarse(Request $request){
		$estado = Users::login($request->all());
		return response()->json([
			"respuesta" => $estado[0],
			"code" => $estado[1]
		],$estado[1]);
	}
}
