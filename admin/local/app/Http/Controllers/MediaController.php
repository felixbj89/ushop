<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Media;
use App\Equipo;
use App\Galeria;

class MediaController extends Controller
{
	//ADD
    public function imagenPrincipal(Request $request){
		$estado = Media::imagenPrincipal($request->all());
		return response()->json([
			"respuesta" => $estado[0],
			"items" => $estado[1],
			"code" => $estado[2]
		],$estado[2]);
	}
	
	public function imagenPublicidad(Request $request){
		$estado = Media::imagenPublicidad($request->all());
		return response()->json([
			"respuesta" => $estado[0],
			"items" => $estado[1],
			"code" => $estado[2]
		],$estado[2]);
	}
	
	public function imagenPrincipalAll(Request $request){
		$estado = Media::imagenPrincipalAll($request->all());
		return response()->json([
			"respuesta" => $estado[0],
			"items" => $estado[1],
			"code" => $estado[2]
		],$estado[2]);
	}
	
	public function imagenPublicidadAll(Request $request){
		$estado = Media::imagenPublicidadAll($request->all());
		return response()->json([
			"respuesta" => $estado[0],
			"items" => $estado[1],
			"code" => $estado[2]
		],$estado[2]);
	}
	
	//DELETE
	public function eliminarImagen(Request $request){
		$estado = Media::eliminarImagen($request->all());
		return response()->json([
			"respuesta" => $estado[0],
			"items" => $estado[1],
			"code" => $estado[2]
		],$estado[2]);
	}
	
	//UPDATE
	public function equipo($codigo, Request $request){
		$estado = Equipo::equipo($codigo,$request->all());
		return response()->json([
			"respuesta" => $estado[0],
			"code" => $estado[1]
		],$estado[1]);
	}
	
	public function actualizarPrincipal($codigo, Request $request){
		$estado = Media::actualizarPrincipal($request->all());
		return response()->json([
			"respuesta" => $estado[0],
			"imagen" => $estado[1],
			"code" => $estado[2]
		],$estado[2]);
	}
	
	public function actualizarPrincipalAll($codigo, Request $request){
		$estado = Media::actualizarPrincipalAll($request->all());
		return response()->json([
			"respuesta" => $estado[0],
			"imagen" => $estado[1],
			"code" => $estado[2]
		],$estado[2]);
	}

	public function actualizarPublicidadAll($codigo, Request $request){
		$estado = Media::actualizarPublicidadAll($request->all());
		return response()->json([
			"respuesta" => $estado[0],
			"imagen" => $estado[1],
			"code" => $estado[2]
		],$estado[2]);
	}
	
	//LIST
	public static function listImagenes($section,$origen){
		$estado = Media::listImagenes($section,$origen);
		return response()->json([
			"respuesta" => $estado[0],
			"code" => $estado[1]
		],$estado[1]);
	}
	
	public static function listEquipos(){
		$estado = Equipo::listEquipos();
		return response()->json([
			"respuesta" => $estado[0],
			"code" => $estado[1]
		],$estado[1]);
	}
	
	public function listImagenPrincipal(){
		$estado = Media::listImagenPrincipal();
		return response()->json([
			"respuesta" => $estado[0],
			"code" => $estado[1]
		],$estado[1]);
	}

	public static function listGaleria(){
		$estado = Galeria::listGaleria();
		return response()->json([
			"respuesta" => $estado[0],
			"code" => $estado[1]
		],$estado[1]);
	}	
}
