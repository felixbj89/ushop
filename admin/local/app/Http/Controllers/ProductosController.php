<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tecnicas;
use App\Productos;

class ProductosController extends Controller
{
    public function tecnicas($codigo, Request $request){
		$estado = Tecnicas::tecnicas($codigo,$request->all());
		return response()->json([
			"respuesta" => $estado[0],
			"code" => $estado[1]
		],$estado[1]);
	}
	
	public static function listProductos(){
		$estado = Productos::listProductos();
		return response()->json([
			"respuesta" => $estado[0],
			"code" => $estado[1]
		],$estado[1]);
	}
	
	public static function listTecnicas(){
		$estado = Tecnicas::listTecnicas();
		return response()->json([
			"respuesta" => $estado[0],
			"code" => $estado[1]
		],$estado[1]);
	}
	
	public function eliminarProductos($codigo, Request $request){
		$estado = Productos::eliminarProductos($codigo,$request->all());
		return response()->json([
			"respuesta" => $estado[0],
			"code" => $estado[1]
		],$estado[1]);
	}
	
	public function viewProductos($codigo){
		$estado = Productos::viewProductos($codigo);
		return response()->json([
			"respuesta" => "Hemos encontrado su producto",
			"productos" => $estado[0],
			"code" => $estado[1]
		],$estado[1]);
	}
	
	public function productos(Request $request){
		$estado = Productos::productos($request->all());
		return response()->json([
			"respuesta" => $estado[0],
			"productos" => $estado[1],
			"code" => $estado[2]
		],$estado[2]);
	}
	
}
