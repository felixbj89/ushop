<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\MediaController;
use App\Http\Controllers\ProductosController;
use App\Http\Controllers\ServiciosController;
use App\Http\Controllers\CustomersController;
use App\Http\Controllers\FileController;
use Config;

class RestController extends Controller
{
	//LISTA TODAS LAS TECNICAS/EQUIPOS
	public function listecnicas(){
		return ProductosController::listTecnicas();
	}
	
	public function listServicios(){
		return ServiciosController::listServicios();
	}
	
	public function listequipos(){
		return MediaController::listEquipos();
	}
	
	public function listCustomers(){
		return CustomersController::listCustomers();
	}
	
	public function listProductos(){
		return ProductosController::listProductos();
	}
	
	public function listenlaces(){
		return Config::get("enlaces");
	}
	
	public function listcatalogospdf(){
		return FileController::listcatalogo();
	}
	
    public function listPictues($section,$origen){
		$lista = MediaController::listImagenes(base64_decode($section),base64_decode($origen))->original;
		$imagenes = '[';
		$pictures = $lista["respuesta"];
		if(count($pictures) > 0){
			foreach($pictures as $elementos){
				if(is_null($elementos->fonts_family)){
					$imagenes .= '{"fuentes":"0","codigogaleria":"'.$elementos->galeria_codigo.'","imagen":"'.$elementos->item_ruta.'"},';
				}else{
					$imagenes .= '{"fuentes":"1","fontsfamily":"'.$elementos->fonts_family.'","url":"'.$elementos->bloque_url.'","codigogaleria":"'.$elementos->galeria_codigo.'","imagen":"'.$elementos->item_ruta.'","textos":"'.$elementos->bloque_textos.'"},';
				}
			}
			
			$imagenes = substr($imagenes,0,strlen($imagenes)-1).']';
			return response()->json([
				"respuesta" => $imagenes,
				"code" => 200,
				"origen" => "listPictues"
			],200);
		}else{
			return response()->json([
				"respuesta" => "En este origen no posee imágenes asociadas",
				"code" => 404,
				"origen" => "listPictues"
			],404);
		}
	}
	
	public function listGaleria(){
		return MediaController::listGaleria();
	}
}
