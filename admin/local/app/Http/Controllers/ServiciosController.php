<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Servicios;

class ServiciosController extends Controller
{
    public static function listServicios(){
		$estado = Servicios::listServicios();
		return response()->json([
			"respuesta" => $estado[0],
			"code" => $estado[1]
		],$estado[1]);
	}
	
	public function servicios($codigo, Request $request){
		$estado = Servicios::servicios($codigo,$request->all());
		return response()->json([
			"respuesta" => $estado[0],
			"code" => $estado[1]
		],$estado[1]);
	}
	
	public function actualizarServicios($codigo, Request $request){
		$estado = Servicios::actualizarServicios($codigo,$request->all());
		return response()->json([
			"respuesta" => $estado[0],
			"estado" => $estado[1],
			"code" => $estado[2]
		],$estado[2]);
	}
}
