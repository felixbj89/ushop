<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Users;

class UserController extends Controller
{
    public static function isCode($codigo){
		$estado = Users::isCode($codigo);
		return response()->json([
			"respuesta" => $estado[0],
			"code" => $estado[1]
		],$estado[1]);
	}
}
