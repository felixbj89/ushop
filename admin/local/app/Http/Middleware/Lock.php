<?php

namespace App\Http\Middleware;

use Closure;
use Session;
use App\Http\Controllers\UserController;

class Lock
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
		if(\Cache::store('database')->has('acceso')){
			if(file_exists("local/config/tecnicas.php")){
				$acceso = \Cache::store('database')->get('acceso');
				$usuario = json_decode(base64_decode(\Cache::store('database')->get('usuario_'.$acceso)));
				$respuesta = UserController::isCode($usuario->users_codigo)->original["respuesta"];
				if($respuesta){
					return $next($request);
				}else{
					dd("hay tabla! 1");
				}
			}else{
				dd("hay tabla! 2");
			}
		}else{
			Session::flush();
			return response(view("errors.403"),403);
		}
    }
}
