<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Lang;
use DB;

class Media extends Model
{	
	
	//LIST
	public static function listImagenPrincipal($seccion,$origen){
		$items = DB::select("CALL InMediaPicture(?,?,?,?,?,?,?,?,?,?,?,?)",array(
			'2',
			NULL,
			NULL,
			NULL,
			NULL,
			NULL,
			$seccion,
			$origen,
			NULL,
			NULL,
			NULL,
			NULL
		));
		
		return [$items,200];
	}
	
	public static function listImagenes($section,$origen){
		$items = DB::select("CALL InMediaPicture(?,?,?,?,?,?,?,?,?,?,?,?)",array(
			'7',
			NULL,
			NULL,
			NULL,
			NULL,
			NULL,
			$section,
			$origen,
			NULL,
			NULL,
			NULL,
			NULL
		));
		
		return [$items,200];
	}
	
	//DELETE 
	public static function eliminarImagen($inputs){
		$media = Media::viewImagenPrincipal($inputs["codigo"])[0];
		if(count($media) > 0){
			//REMOVIENDO EL ARCHIVO MEDIA
			$ruta_archivo = "img/media/".$media[0]->item_ruta;
			if(file_exists($ruta_archivo))unlink($ruta_archivo);
				
			$items = DB::select("CALL InMediaPicture(?,?,?,?,?,?,?,?,?,?,?,?)",array(
				'5',
				NULL,
				NULL,
				NULL,
				$inputs["codigo"],
				NULL,
				NULL,
				NULL,
				NULL,
				NULL,
				NULL,
				NULL
			));
			
			$media = Media::viewImagenPrincipal($inputs["codigo"])[0];
			if(count($media) == 0){
				return [Lang::get("message.yes_delete"),$items,200];
			}else
				return [Lang::get("message.not_delete"),NULL,409];
		}else{
			return ["eliminarImagen",NULL,500];
		}
	}
	
	//ACTUALIZAR
	public static function actualizarPrincipal($inputs){
		$bannerprincipal = Media::viewImagenPrincipal(base64_decode($inputs["codigogaleria"]));
		if($bannerprincipal!=NULL){
			if(array_key_exists("media",$inputs) && $inputs["media"]!="undefined"){
				$archivo = $_FILES["media"]["name"];
				$archivo = substr($archivo,strrpos($archivo,"."),strlen($archivo));
				if(strtoupper($archivo)!=".JPG" && strtoupper($archivo)!=".JPEG" && strtoupper($archivo)!=".PNG"){
					return [Lang::get("message.need_picture"),NULL,200];
				}else{
					if(file_exists("img/media/".$bannerprincipal[0][0]->item_ruta))unlink("img/media/".$bannerprincipal[0][0]->item_ruta);
					$media = hash("md5",$inputs["seccion"])."/".md5($_FILES["media"]["name"])."".$archivo;
				}
				
				\Storage::disk('mediaupload')->put($media,  \File::get($inputs["media"]));
			}else{
				$media = $bannerprincipal[0][0]->item_ruta;
			}
			
			$respuesta = DB::select("CALL InMediaPicture(?,?,?,?,?,?,?,?,?,?,?,?)",array(
				'9',
				NULL,
				NULL,
				$media,
				base64_decode($inputs["codigogaleria"]),
				NULL,
				NULL,
				NULL,
				NULL,
				NULL,
				NULL,
				NULL
			));
			
			return [Lang::get("message.yes_modify"),$respuesta,200];
		}else{
			return ["actualizarPrincipal",NULL,500];
		}
	}
	
	public static function actualizarPrincipalAll($inputs){
		$bannerprincipal = Media::viewImagenPrincipal(base64_decode($inputs["codigogaleria"]));
		if($bannerprincipal!=NULL){
			$media = "";
			if(array_key_exists("media",$inputs) && $inputs["media"]!="undefined"){
				$archivo = $_FILES["media"]["name"];
				$archivo = substr($archivo,strrpos($archivo,"."),strlen($archivo));
				if(strtoupper($archivo)!=".JPG" && strtoupper($archivo)!=".JPEG" && strtoupper($archivo)!=".PNG"){
					return [Lang::get("message.need_picture"),NULL,200];
				}else{
					if(file_exists("img/media/".$bannerprincipal[0][0]->item_ruta))unlink("img/media/".$bannerprincipal[0][0]->item_ruta);
					$media = hash("md5",$inputs["seccion"])."/".md5($_FILES["media"]["name"])."".$archivo;
				}
				
				\Storage::disk('mediaupload')->put($media,  \File::get($inputs["media"]));
			}else{
				$media = $bannerprincipal[0][0]->item_ruta;
			}
			
			$fuente = "";
			if(array_key_exists("tipofuente",$inputs) && $inputs["tipofuente"]!="NULL"){
				$fuente = $inputs["tipofuente"];
			}else{
				$fuente = $bannerprincipal[0][0]->fonts_family;
			}
			
			$textos = "";
			if(array_key_exists("textos",$inputs) && $inputs["textos"]!=""){
				$textos = base64_encode($inputs["textos"]);
			}else{
				$textos = $bannerprincipal[0][0]->bloque_textos;
			}

			$url = NULL;
			if(array_key_exists("url",$inputs) && $inputs["url"]!="undefined"){
				$url = $inputs["url"];
			}else{
				$url = $bannerprincipal[0][0]->bloque_url;
			}

			if($bannerprincipal[0][0]->id==1){
				if(isset($inputs["todo"])){
					$respuesta = DB::select("CALL InMediaPicture(?,?,?,?,?,?,?,?,?,?,?,?)",array(
						'12',
						NULL,
						NULL,
						$media,
						base64_decode($inputs["codigogaleria"]),
						NULL,
						NULL,
						NULL,
						$fuente,
						str_random(5),
						$url,
						$textos
					));
				}
			}else{
				$respuesta = DB::select("CALL InMediaPicture(?,?,?,?,?,?,?,?,?,?,?,?)",array(
					'10',
					NULL,
					NULL,
					$media,
					base64_decode($inputs["codigogaleria"]),
					NULL,
					NULL,
					NULL,
					$fuente,
					NULL,
					$url,
					$textos
				));
			}
			
			return [Lang::get("message.yes_modify"),$respuesta,200];
		}else{
			return ["actualizarPrincipal",NULL,500];
		}
	}
	
	public static function actualizarPublicidadAll($inputs){
		$bannerprincipal = Media::viewImagenPrincipal($inputs["codigo"]);
		if($bannerprincipal!=NULL){
			$media = "";
			if(array_key_exists("media",$inputs) && $inputs["media"]!="undefined"){
				$archivo = $_FILES["media"]["name"];
				$archivo = substr($archivo,strrpos($archivo,"."),strlen($archivo));
				if(strtoupper($archivo)!=".JPG" && strtoupper($archivo)!=".JPEG" && strtoupper($archivo)!=".PNG"){
					return [Lang::get("message.need_picture"),NULL,200];
				}else{
					if(file_exists("img/media/".$bannerprincipal[0][0]->item_ruta))unlink("img/media/".$bannerprincipal[0][0]->item_ruta);
					$media = hash("md5",$inputs["seccion"])."/".md5($_FILES["media"]["name"])."".$archivo;
				}
				
				\Storage::disk('mediaupload')->put($media,  \File::get($inputs["media"]));
			}else{
				$media = $bannerprincipal[0][0]->item_ruta;
			}

			$fuente = "";
			if(array_key_exists("tipofuente",$inputs) && $inputs["tipofuente"]!="NULL"){
				$fuente = $inputs["tipofuente"];
			}else{
				$fuente = $bannerprincipal[0][0]->fonts_family;
			}
			
			$textos = "";
			if(array_key_exists("textos",$inputs) && $inputs["textos"]!=""){
				$textos = base64_encode($inputs["textos"]);
			}else{
				$textos = $bannerprincipal[0][0]->bloque_textos;
			}

			$url = "";
			if(array_key_exists("url",$inputs) && $inputs["url"]!="undefined"){
				$url = $inputs["url"];
			}else{
				$url = $bannerprincipal[0][0]->bloque_url;
			}

			if(base64_decode($inputs["modo"])=="h"){
				$respuesta = DB::select("CALL InMediaPicture(?,?,?,?,?,?,?,?,?,?,?,?)",array(
					'10',
					NULL,
					NULL,
					$media,
					$inputs["codigo"],
					NULL,
					NULL,
					NULL,
					$fuente,
					NULL,
					NULL,
					$textos
				));
			}else{
				$respuesta = DB::select("CALL InMediaPicture(?,?,?,?,?,?,?,?,?,?,?,?)",array(
					'11',
					NULL,
					NULL,
					$media,
					$inputs["codigo"],
					NULL,
					NULL,
					NULL,
					$fuente,
					NULL,
					$url,
					$textos
				));
			}
			
			return [Lang::get("message.yes_modify"),$respuesta,200];
		}else{
			return ["actualizarPublicidadAll",NULL,500];
		}
	}

	//VIEW
	public static function viewImagenPrincipal($codigo){
		$items = DB::select("CALL InMediaPicture(?,?,?,?,?,?,?,?,?,?,?,?)",array(
			'6',
			NULL,
			NULL,
			NULL,
			$codigo,
			NULL,
			NULL,
			NULL,
			NULL,
			NULL,
			NULL,
			NULL
		));
		
		return [$items,200];
	}
	
	//ADD
	public static function imagenPrincipalAll($inputs){
		if(array_key_exists("seccion",$inputs) && $inputs["seccion"]!=""){			
			$tipo = "";
			$nombre = "";
			$section = base64_decode($inputs["seccion"]);
			$origen = base64_decode($inputs["origen"]);
			if($section=="0"){
				$nombre = "HOME/INICIO";
			}else if($section=="1"){
				$nombre = "EMPRESA";
			}else if($section=="2"){
				$nombre = "PRODUCTOS";
			}else if($section=="3"){
				$nombre = "CONTACTO";
			}else{
				$nombre = "OTROS";
				
			}
			
			//VÍDEO - IMÁGENES
			if(base64_decode($inputs["tipomedia"])=="0"){
				$tipo = "1";
				if(array_key_exists("media",$inputs) && $inputs["media"]!=""){
					$archivo = $_FILES["media"]["name"];
					$archivo = substr($archivo,strrpos($archivo,"."),strlen($archivo));
					if(strtoupper($archivo)!=".MP4"){
						return [Lang::get("message.need_video"),NULL,200];
					}else{
						$media = hash("md5",$inputs["seccion"])."/".md5($_FILES["media"]["name"]).".mp4";
					}
				}
			}else{
				$tipo = "0";
				if(array_key_exists("media",$inputs) && $inputs["media"]!=""){
					$archivo = $_FILES["media"]["name"];
					$archivo = substr($archivo,strrpos($archivo,"."),strlen($archivo));
					if(strtoupper($archivo)!=".JPG" && strtoupper($archivo)!=".JPEG" && strtoupper($archivo)!=".PNG"){
						return [Lang::get("message.need_picture"),NULL,200];
					}else{
						$media = hash("md5",$inputs["seccion"])."/".md5($_FILES["media"]["name"])."".$archivo;
					}
				}
			}
			
			if(base64_decode($inputs["tipomedia"])=="1"){
				if($section=="1" || $section=="2" || $section=="3"){
					if($origen=="0" || $origen=="2"){//SOLO ACTUALIZA PARA TENER UNA SOLA IMAGEN EN E/P/C
						$listimagenes = Media::listImagenes($section,$origen);
						if(count($listimagenes[0]) >= 1){
							Media::eliminarImagen(["codigo" => $listimagenes[0][0]->galeria_codigo]);
							if(file_exists("img/media/".$listimagenes[0][0]->item_ruta))unlink("img/media/".$listimagenes[0][0]->item_ruta);
						}
					}
				}
			}
			
			$url = NULL;
			if(array_key_exists("url",$inputs) && $inputs["url"]!="undefined"){
				$url = $inputs["url"];
			}

			$respuesta = DB::select("CALL InMediaPicture(?,?,?,?,?,?,?,?,?,?,?,?)",array(
				'4',
				$nombre,
				str_random(10),
				$media,
				str_random(15),
				$tipo,
				$section,
				$origen,
				$inputs["tipofuente"],
				str_random(5),
				$url,
				base64_encode($inputs["textos"])
			));
			
			if($respuesta[0]->confirmar==1){
				$items = DB::select("CALL InMediaPicture(?,?,?,?,?,?,?,?,?,?,?,?)",array(
					'2',
					NULL,
					NULL,
					NULL,
					NULL,
					NULL,
					$section,
					$origen,
					NULL,
					NULL,
					NULL,
					NULL
				));
			
				\Storage::disk('mediaupload')->put($media,  \File::get($inputs["media"]));
				return [Lang::get("message.yes_register"),$items,200];
			}else if($respuesta[0]->confirmar==0){
				return [Lang::get("message.match_picture"),NULL,409];
			}else{
				return ["ERROR: imagenPrincipal#".$respuesta[0]->confirmar,NULL,500];
			}
		}else{
			return ["imagenPrincipalAll",NULL,500];
		}
	}
	
	public static function imagenPublicidadAll($inputs){
		if(array_key_exists("seccion",$inputs) && $inputs["seccion"]!=""){
			if(array_key_exists("media",$inputs) && $inputs["media"]!=""){
				$archivo = $_FILES["media"]["name"];
				$archivo = substr($archivo,strrpos($archivo,"."),strlen($archivo));
				if(strtoupper($archivo)!=".JPG" && strtoupper($archivo)!=".JPEG" && strtoupper($archivo)!=".PNG"){
					return [Lang::get("message.need_picture"),NULL,200];
				}else{
					$media = hash("md5",$inputs["seccion"])."/".md5($_FILES["media"]["name"])."".$archivo;
				}
			}
			
			$nombre = "";
			$section = base64_decode($inputs["seccion"]);
			$origen = base64_decode($inputs["origen"]);
			if($section=="0"){
				$nombre = "HOME/INICIO";
			}else if($section=="1"){
				$nombre = "EMPRESA";
			}else if($section=="2"){
				$nombre = "PRODUCTOS";
			}else if($section=="3"){
				$nombre = "CONTACTO";
			}
			
			$url = NULL;
			if(array_key_exists("url",$inputs) && $inputs["url"]!="undefined"){
				$url = $inputs["url"];
			}
			
			//EN CONTACTO REMUEVO LA IMAGEN COMPLETAMENTE
			if($section=="3"){
				if($origen=="1"){//SOLO ACTUALIZA PARA TENER UNA SOLA IMAGEN EN E/P/C
					$listimagenes = Media::listImagenes($section,$origen);
					if(count($listimagenes[0]) >= 1){
						Media::eliminarImagen(["codigo" => $listimagenes[0][0]->galeria_codigo]);
						if(file_exists("img/media/".$listimagenes[0][0]->item_ruta))unlink("img/media/".$listimagenes[0][0]->item_ruta);
					}
				}
			}
			
			//ACTUALIZO PARA EMPRESA Ó PRODUCTOS
			if($section=="1" || $section=="2"){
				if($origen=="0" || $origen=="1" || $origen=="2"){//SOLO ACTUALIZA PARA TENER UNA SOLA IMAGEN EN E/P/C
					$listimagenes = Media::listImagenes($section,$origen);
					if(count($listimagenes[0]) >= 1){
						if(file_exists("img/media/".$listimagenes[0][0]->item_ruta))unlink("img/media/".$listimagenes[0][0]->item_ruta);
					}
				}
			}
			
			$listimagenes = Media::listImagenes($section,$origen);
			if($section!="3" && count($listimagenes[0]) >= 1){//ACTUALIZO LA IMAGEN
				$respuesta = DB::select("CALL InMediaPicture(?,?,?,?,?,?,?,?,?,?,?,?)",array(
					'8',
					NULL,
					NULL,
					$media,
					NULL,
					NULL,
					$section,
					$origen,
					NULL,
					NULL,
					NULL,
					NULL
				));
			}else{//INSERTO POR PRIMERA VEZ
				$respuesta = DB::select("CALL InMediaPicture(?,?,?,?,?,?,?,?,?,?,?,?)",array(
					'4',
					$nombre,
					str_random(10),
					$media,
					str_random(15),
					"0",
					$section,
					$origen,
					$inputs["tipofuente"],
					str_random(5),
					$url,
					base64_encode($inputs["textos"])
				));
			}
			
			if($respuesta[0]->confirmar==1){
				$items = DB::select("CALL InMediaPicture(?,?,?,?,?,?,?,?,?,?,?,?)",array(
					'2',
					NULL,
					NULL,
					NULL,
					NULL,
					NULL,
					$section,
					$origen,
					NULL,
					NULL,
					NULL,
					NULL
				));
			
				\Storage::disk('mediaupload')->put($media,  \File::get($inputs["media"]));
				return [Lang::get("message.yes_register"),$items,200];
			}else if($respuesta[0]->confirmar==0){
				return [Lang::get("message.match_picture"),NULL,409];
			}else{
				return ["ERROR: imagenPublicidadAll#".$respuesta[0]->confirmar,NULL,500];
			}
		}else{
			return ["imagenPublicidadAll",NULL,500];
		}
	}
	
	public static function imagenPublicidad($inputs){
		if(array_key_exists("seccion",$inputs) && $inputs["seccion"]!=""){
			$tipo = "";
			$nombre = "";
			$section = base64_decode($inputs["seccion"]);
			$origen = base64_decode($inputs["origen"]);
			//DISCRIMINANTE-SECCIONES
			if($section=="0"){
				$nombre = "HOME/INICIO";
			}else if($section=="1"){
				$nombre = "EMPRESA";
			}else if($section=="2"){
				$nombre = "PRODUCTOS";
			}else if($section=="3"){
				$nombre = "CONTACTO";
			}else{
				$nombre = "OTROS";
			}
										
			if(array_key_exists("media",$inputs) && $inputs["media"]!=""){
				if(array_key_exists("media",$inputs) && $inputs["media"]!=""){
					$archivo = $_FILES["media"]["name"];
					$archivo = substr($archivo,strrpos($archivo,"."),strlen($archivo));
					if(strtoupper($archivo)!=".JPG" && strtoupper($archivo)!=".JPEG" && strtoupper($archivo)!=".PNG"){
						return [Lang::get("message.need_picture"),NULL,200];
					}else{
						$media = hash("md5",$inputs["seccion"])."/".md5($_FILES["media"]["name"])."".$archivo;
					}
				}
				
				if($section=="1" || $section=="2" || $section=="3"){
					if($origen=="0" || $origen=="1" || $origen=="2"){//SOLO ACTUALIZA PARA TENER UNA SOLA IMAGEN EN E/P/C
						$listimagenes = Media::listImagenes($section,$origen);
						if(count($listimagenes[0]) >= 1){
							if(file_exists("img/media/".$listimagenes[0][0]->item_ruta))unlink("img/media/".$listimagenes[0][0]->item_ruta);
						}
					}
				}
				
				$listimagenes = Media::listImagenes($section,$origen);
				if(count($listimagenes[0]) >= 1){//ACTUALIZO LA IMAGEN
					$respuesta = DB::select("CALL InMediaPicture(?,?,?,?,?,?,?,?,?,?,?,?)",array(
						'8',
						NULL,
						NULL,
						$media,
						NULL,
						NULL,
						$section,
						$origen,
						NULL,
						NULL,
						NULL,
						NULL
					));
				}else{//INSERTO POR PRIMERA VEZ
					$respuesta = DB::select("CALL InMediaPicture(?,?,?,?,?,?,?,?,?,?,?,?)",array(
						'1',
						$nombre,
						str_random(10),
						$media,
						str_random(15),
						"0",
						$section,
						$origen,
						NULL,
						NULL,
						NULL,
						NULL
					));
				}
				
				if($respuesta[0]->confirmar==1){
					$items = DB::select("CALL InMediaPicture(?,?,?,?,?,?,?,?,?,?,?,?)",array(
						'2',
						NULL,
						NULL,
						NULL,
						NULL,
						NULL,
						$section,
						$origen,
						NULL,
						NULL,
						NULL,
						NULL
					));
			
					\Storage::disk('mediaupload')->put($media,  \File::get($inputs["media"]));
					return [Lang::get("message.yes_register"),$items,200];
				}else if($respuesta[0]->confirmar==0){
					return [Lang::get("message.match_picture"),NULL,409];
				}else{
					return ["ERROR: imagenPublicidad#".$respuesta[0]->confirmar,NULL,500];
				}
			}else{
				return ["imagenPublicidad",NULL,500];
			}
		}else{
			return ["imagenPublicidad",NULL,500];
		}
	}
	
    public static function imagenPrincipal($inputs){
		if(array_key_exists("seccion",$inputs) && $inputs["seccion"]!=""){
			$tipo = "";
			$nombre = "";
			$section = base64_decode($inputs["seccion"]);
			$origen = base64_decode($inputs["origen"]);
			//DISCRIMINANTE-SECCIONES
			if($section=="0"){
				$nombre = "HOME/INICIO";
			}else if($section=="1"){
				$nombre = "EMPRESA";
			}else if($section=="2"){
				$nombre = "PRODUCTOS";
			}else if($section=="3"){
				$nombre = "CONTACTO";
			}else{
				$nombre = "OTROS";
				
			}
			
			//VÍDEO - IMÁGENES
			if(array_key_exists("tipomedia",$inputs) && $inputs["tipomedia"]!="undefined"){//HOME/INICIO
				if(base64_decode($inputs["tipomedia"])=="0"){
					$tipo = "1";
					if(array_key_exists("media",$inputs) && $inputs["media"]!=""){
						$archivo = $_FILES["media"]["name"];
						$archivo = substr($archivo,strrpos($archivo,"."),strlen($archivo));
						if(strtoupper($archivo)!=".MP4"){
							return [Lang::get("message.need_video"),NULL,200];
						}else{
							$media = hash("md5",$inputs["seccion"])."/".md5($_FILES["media"]["name"]).".mp4";
						}
					}
				}else{
					$tipo = "0";
					if(array_key_exists("media",$inputs) && $inputs["media"]!=""){
						$archivo = $_FILES["media"]["name"];
						$archivo = substr($archivo,strrpos($archivo,"."),strlen($archivo));
						if(strtoupper($archivo)!=".JPG" && strtoupper($archivo)!=".JPEG" && strtoupper($archivo)!=".PNG"){
							return [Lang::get("message.need_picture"),NULL,200];
						}else{
							$media = hash("md5",$inputs["seccion"])."/".md5($_FILES["media"]["name"])."".$archivo;
						}
					}
				}
			}else{//EMPRESA-PRODUCTOS-CONTACTO
				$tipo = "0";
				if(array_key_exists("media",$inputs) && $inputs["media"]!=""){
					$archivo = $_FILES["media"]["name"];
					$archivo = substr($archivo,strrpos($archivo,"."),strlen($archivo));
					if(strtoupper($archivo)!=".JPG" && strtoupper($archivo)!=".JPEG" && strtoupper($archivo)!=".PNG"){
						return [Lang::get("message.need_picture"),NULL,200];
					}else{
						$media = hash("md5",$inputs["seccion"])."/".md5($_FILES["media"]["name"])."".$archivo;
					}
				}
			}
			
			if(base64_decode($inputs["tipomedia"])=="1"){//ACTUALIZO LA IMAGEN SI SOY DE UNA SECCIÓN QUE NO SEA EL HOME
				if($section=="1" || $section=="2" || $section=="3"){
					if($origen=="0" || $origen=="2"){//SOLO ACTUALIZA PARA TENER UNA SOLA IMAGEN EN E/P/C
						$listimagenes = Media::listImagenes($section,$origen);
						if(count($listimagenes[0]) >= 1){
							if(file_exists("img/media/".$listimagenes[0][0]->item_ruta))unlink("img/media/".$listimagenes[0][0]->item_ruta);
						}
					}
				}
			}
					
			
			$listimagenes = Media::listImagenes($section,$origen);
			if($section!="0" && $origen!="0" && count($listimagenes[0]) >= 1){//ACTUALIZO LA IMAGEN
				$respuesta = DB::select("CALL InMediaPicture(?,?,?,?,?,?,?,?,?,?,?,?)",array(
					'8',
					NULL,
					NULL,
					$media,
					NULL,
					NULL,
					$section,
					$origen,
					NULL,
					NULL,
					NULL,
					NULL
				));
			}else{//INSERTO POR PRIMERA VEZ
				$respuesta = DB::select("CALL InMediaPicture(?,?,?,?,?,?,?,?,?,?,?,?)",array(
					'1',
					$nombre,
					str_random(10),
					$media,
					str_random(15),
					$tipo,
					$section,
					$origen,
					NULL,
					NULL,
					NULL,
					NULL
				));
			}
			
			if($respuesta[0]->confirmar==1){
				$items = DB::select("CALL InMediaPicture(?,?,?,?,?,?,?,?,?,?,?,?)",array(
					'2',
					NULL,
					NULL,
					NULL,
					NULL,
					NULL,
					$section,
					$origen,
					NULL,
					NULL,
					NULL,
					NULL
				));
				
				\Storage::disk('mediaupload')->put($media,  \File::get($inputs["media"]));
				return [Lang::get("message.yes_register"),$items,200];
			}else if($respuesta[0]->confirmar==0){
				return [Lang::get("message.match_picture"),NULL,409];
			}else{
				return ["ERROR: imagenPrincipal#".$respuesta[0]->confirmar,NULL,500];
			}
		}else{
			return ["imagenPrincipal",NULL,500];
		}
	}
}
