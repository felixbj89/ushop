<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Lang;

class Productos extends Model
{
    protected $table = "productos";

	protected $fillable = [
		"productos_name",
		"productos_codigo",
		'productos_descripcion',
		"productos_ruta",
		"productos_tipo",
		"productos_estilo_one",
		"productos_estilo_two",
		"productos_estilo_thrid",
		"users_id"
	];

	protected $hidden = [
	  'id'
	];
	
	private static function countRutas($ruta){
		$productos = Productos::get();
		$size = 0;
		foreach($productos as $p){
			if($ruta==$p->productos_ruta){
				++$size;
			}
		}
		
		return $size;
	}
	
	public static function eliminarProductos($codigo,$inputs){
		$productos = NULL;
		if(isset($inputs["codigoproducto"])){
			$productos = json_decode(Productos::viewProductos($inputs["codigoproducto"])[0],true);
			if(Productos::countRutas($productos["productos_ruta"]) > 1){
				return [Lang::get("message.not_delete_productos"),409];
			}else{
				if(file_exists($productos["productos_ruta"]))unlink($productos["productos_ruta"]);
				Productos::where("productos_codigo",$inputs["codigoproducto"])->delete();
				return [Lang::get("message.yes_delete_producto"),200];
			}
		}else{
			return ["eliminarProductos",500];
		}
	}
	
	public static function listProductos(){
		$productos = Productos::get();
		if($productos!=NULL){
			return [$productos->toJson(),200];
		}else{
			return [NULL,404];
		}
	}
	
	public static function viewProductos($codigo){
		$productos = Productos::where("productos_codigo",$codigo)->first();
		if($productos!=NULL){
			return [$productos->toJson(),200];
		}else{
			return [NULL,404];
		}
	}
	
	public static function productos($inputs){
		$productos = NULL;
		if(isset($inputs["codigoproducto"]))
			$productos = json_decode(Productos::viewProductos($inputs["codigoproducto"])[0],true);
		
		if($productos!=NULL){
			$nombre = "";
			$tipo = "";
			$descripcion = "";
			$one = "";
			$two = "";
			$thrid = "";
			$media = "";
			
			//NOMBRE
			if(array_key_exists("productoname",$inputs) && $inputs["productoname"]!=""){
				$nombre = $inputs["productoname"];
			}else{
				$nombre = $productos["productos_name"];
			}
			
			//TIPO
			if(array_key_exists("tipoproducto",$inputs) && $inputs["tipoproducto"]!=""){
				$tipo = $inputs["tipoproducto"];
			}else{
				$tipo = $productos["productos_tipo"];
			}
			
			//DESCRIPCIÓN
			if(array_key_exists("descripcion",$inputs) && $inputs["descripcion"]!=""){
				$descripcion = $inputs["descripcion"];
			}else{
				$descripcion = $productos["productos_descripcion"];
			}
			
			//ESTILOS-ONE
			if(array_key_exists("estilo_one",$inputs) && $inputs["estilo_one"]!=""){
				$one = $inputs["estilo_one"];
			}else{
				$one = $productos["productos_estilo_one"];
			}
			
			//ESTILOS-TWO
			if(array_key_exists("estilo_two",$inputs) && $inputs["estilo_two"]!=""){
				$two = $inputs["estilo_two"];
			}else{
				$two = $productos["productos_estilo_two"];
			}
			
			//ESTILOS-THRID
			if(array_key_exists("estilo_thrid",$inputs) && $inputs["estilo_thrid"]!=""){
				$thrid = $inputs["estilo_thrid"];
			}else{
				$thrid = $productos["productos_estilo_thrid"];
			}
			
			//MEDIA
			if(array_key_exists("media",$inputs) && $inputs["media"]!="undefined"){
				$archivo = $_FILES["media"]["name"];
				$archivoImagen = "";
				$archivo = substr($archivo,strrpos($archivo,"."),strlen($archivo));
				if(strtoupper($archivo)!=".JPG" && strtoupper($archivo)!=".PNG"){
					return [Lang::get("message.need_picture"),NULL,200];
				}else{
					if(file_exists($productos["productos_ruta"]))unlink($productos["productos_ruta"]);
					$media = hash("md5",$inputs["seccion"])."/".md5($_FILES["media"]["name"]);
					$archivoImagen = "img/media/productos/".$media;
				}
			}else{
				$archivoImagen = $productos["productos_ruta"];
			}
			
			Productos::where("productos_codigo",$inputs["codigoproducto"])->update([
				"productos_name" => $nombre,
				'productos_descripcion' => $descripcion,
				"productos_ruta" => $archivoImagen,
				"productos_tipo" => $tipo,
				"productos_estilo_one" => $one,
				"productos_estilo_two" => $two,
				"productos_estilo_thrid" => $thrid,
			]);
			
			$productos = json_decode(Productos::viewProductos($inputs["codigoproducto"])[0],true);
			return [Lang::get("message.yes_modify"),$productos,200];
		}else{
			if(array_key_exists("seccion",$inputs) && $inputs["seccion"]!=""){			
				$section = base64_decode($inputs["seccion"]);
				$origen = base64_decode($inputs["origen"]);
				
				$media = "";
				if(array_key_exists("media",$inputs) && $inputs["media"]!=""){
					$archivo = $_FILES["media"]["name"];
					$archivoImagen = "";
					$archivo = substr($archivo,strrpos($archivo,"."),strlen($archivo));
					if(strtoupper($archivo)!=".JPG" && strtoupper($archivo)!=".PNG"){
						return [Lang::get("message.need_picture"),NULL,200];
					}else{
						$media = hash("md5",$inputs["seccion"])."/".md5($_FILES["media"]["name"]);
						$archivoImagen = "img/media/productos/".$media;
					}
					
					$codigo = str_random(10);
					Productos::create([
						"productos_name" => $inputs["productoname"],
						"productos_codigo" => $codigo,
						'productos_descripcion' => $inputs["descripcion"],
						"productos_ruta" => $archivoImagen,
						"productos_tipo" => $inputs["tipoproducto"],
						"productos_estilo_one" => $inputs["estilo_one"],
						"productos_estilo_two" => $inputs["estilo_two"],
						"productos_estilo_thrid" => $inputs["estilo_thrid"],
						"users_id" => 1
					]);
					
					$productos = Productos::where("productos_codigo",$codigo)->first()->toJson();
					\Storage::disk('productosupload')->put($media,  \File::get($inputs["media"]));
					return [Lang::get("message.yes_register"),$productos,200];
				}else{
					return [Lang::get("message.not_register"),$productos,409];
				}
			}else{
				return ["productos",NULL,500];
			}
		}
	}
}
