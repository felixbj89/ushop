<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Lang;

class Servicios extends Model
{
    protected $table = "servicios";

	protected $fillable = [
		"servicios_name",
		"servicios_codigo",
		'servicios_logo',
		"servicios_descripcion",
		"servicios_one",
		"servicios_two",
		"servicios_thrid",
		"servicios_estado",
		"users_id"
	];

	protected $hidden = [
	  'id'
	];
	
	public static function listServicios(){
		$servicios = Servicios::get();
		if($servicios!=NULL){
			return [$servicios->toJson(),200];
		}else{
			return [NULL,404];
		}
	}
	
	public static function viewServicios($codigo){
		$servicios = Servicios::where("servicios_codigo",$codigo);
		if($servicios!=NULL){
			return $servicios->first()->toJSon();
		}else{
			return [];
		}
	}
	
	public static function actualizarServicios($codigo,$inputs){
		$servicios = Servicios::where("servicios_codigo",$codigo);
		if($servicios!=NULL){
			$on = $servicios->first()->servicios_estado;
			if($on=="1"){
				$servicios->update([
					"servicios_estado" => "0"
				]);
				
				return [Lang::get("message.yes_desaactive"),"0",200];
			}else{
				$servicios->update([
					"servicios_estado" => "1"
				]);
				
				return [Lang::get("message.yes_active"),"1",200];
			}
			
			
		}else{
			return [Lang::get("message.not_active"),NULL,409];
		}
	}
	
	public static function servicios($codigo,$inputs){
		$servicios = Servicios::where("servicios_codigo",$codigo);
		if($servicios!=NULL){
			$media = NULL;
			$one = NULL;
			$two = NULL;
			$thrid = NULL;
			
			//MEDIA
			if(array_key_exists("media",$inputs) && $inputs["media"]!="undefined"){
				$imagen = $servicios->first()->servicios_logo;
				if($imagen!="img/servicios/logosimple_ushop.svg" && file_exists($imagen))unlink($imagen);
				$archivo = $_FILES["media"]["name"];
				$archivo = substr($archivo,strrpos($archivo,"."),strlen($archivo));
				$logo = "";
				if(strtoupper($archivo)==".SVG"){
					$logo = hash("md5",$inputs["seccion"])."/".md5($_FILES["media"]["name"]).".svg";
				}else{
					$logo = hash("md5",$inputs["seccion"])."/".md5($_FILES["media"]["name"])."".$archivo;
				}
								
				\Storage::disk('serviciosupload')->put($logo,  \File::get($inputs["media"]));
				$media = ("img/media/servicios/".$logo);
			}else{
				$media = $servicios->first()->servicios_logo;
			}
			
			//ONE
			if(array_key_exists("one",$inputs) && $inputs["one"]!="undefined"){
				$imagen = $servicios->first()->servicios_one;
				if($imagen!="img/servicios/galeria/secc4_empresa_250x400_1.jpg" && file_exists($imagen))unlink($imagen);
				$archivo = $_FILES["one"]["name"];
				$archivo = substr($archivo,strrpos($archivo,"."),strlen($archivo));
				$one = hash("md5",$inputs["seccion"])."/".md5($_FILES["one"]["name"])."".$archivo;
				\Storage::disk('logoserviciosupload')->put($one,  \File::get($inputs["one"]));
				$one = ("img/media/servicios/galeriasecundaria/".$one);
			}else{
				$one = $servicios->first()->servicios_one;
			}
			
			//TWO
			if(array_key_exists("two",$inputs) && $inputs["two"]!="undefined"){
				$imagen = $servicios->first()->servicios_two;
				if($imagen!="img/servicios/galeria/secc4_empresa_200x400_2.jpg" && file_exists($imagen))unlink($imagen);
				$archivo = $_FILES["two"]["name"];
				$archivo = substr($archivo,strrpos($archivo,"."),strlen($archivo));
				$two = hash("md5",$inputs["seccion"])."/".md5($_FILES["two"]["name"])."".$archivo;
				\Storage::disk('logoserviciosupload')->put($two,  \File::get($inputs["two"]));
				$two = ("img/media/servicios/galeriasecundaria/".$two);
			}else{
				$two = $servicios->first()->servicios_two;
			}
			
			//THRID
			if(array_key_exists("thrid",$inputs) && $inputs["thrid"]!="undefined"){
				$imagen = $servicios->first()->servicios_thrid;
				if($imagen!="img/servicios/galeria/secc4_empresa_150x400_3.jpg" && file_exists($imagen))unlink($imagen);
				$archivo = $_FILES["thrid"]["name"];
				$archivo = substr($archivo,strrpos($archivo,"."),strlen($archivo));
				$thrid = hash("md5",$inputs["seccion"])."/".md5($_FILES["thrid"]["name"])."".$archivo;
				\Storage::disk('logoserviciosupload')->put($thrid,  \File::get($inputs["thrid"]));
				$thrid = ("img/media/servicios/galeriasecundaria/".$thrid);
			}else{
				$thrid = $servicios->first()->servicios_thrid;
			}
			
			$servicios->update([
				'servicios_logo' => $media,
				"servicios_descripcion" => $inputs["textos"],
				"servicios_one" => $one,
				"servicios_two" => $two,
				"servicios_thrid" => $thrid
			]);
			
			return [Lang::get("message.yes_modify"),200];
		}else{
			return [Lang::get("message.not_modify"),409];
		}
	}
}

