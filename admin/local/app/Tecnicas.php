<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Lang;

class Tecnicas extends Model
{
    protected $table = "tecnicas";

	protected $fillable = [
		"tecnicas_name",
		'tecnicas_codigo',
		"tecnicas_section",
		"tecnicas_origen",
		"tecnicas_ruta",
		"tecnicas_descripcion",
		"users_id"
	];

	protected $hidden = [
      'remember_token',
	  'id'
	];
	
	public static function listTecnicas(){
		$tecnicas = Tecnicas::get();
		if($tecnicas!=NULL){
			return [$tecnicas->toJson(),200];
		}else{
			return [NULL,404];
		}
	}
	
	public static function viewTecnica($codigo){
		$tecnicas = Tecnicas::where("tecnicas_codigo",$codigo);
		if($tecnicas!=NULL){
			return $tecnicas->first()->toJSon();
		}else{
			return [];
		}
	}
	
	public static function tecnicas($codigo, $inputs){
		$tecnicas = Tecnicas::where("tecnicas_codigo",$codigo);
		if($tecnicas!=NULL){
			$media = NULL;
			if(array_key_exists("media",$inputs) && $inputs["media"]!="undefined"){
				$imagen = $tecnicas->first()->tecnicas_ruta;
				if($imagen!="img/defaults/defaults.svg" && file_exists($imagen))unlink($imagen);
				$archivo = $_FILES["media"]["name"];
				$archivo = substr($archivo,strrpos($archivo,"."),strlen($archivo));
				
				$media = hash("md5",$inputs["seccion"])."/".md5($_FILES["media"]["name"])."".$archivo;
				\Storage::disk('tecnicasupload')->put($media,  \File::get($inputs["media"]));
				$media = ("img/media/tecnicas/".$media);
			}else{
				$media = $tecnicas->first()->tecnicas_ruta;
			}
			
			$tecnicas->update([
				"tecnicas_section" => "2",
				"tecnicas_origen" => "2",
				"tecnicas_ruta" => $media,
				"tecnicas_descripcion" => $inputs["textos"],
			]);
			
			return [Lang::get("message.yes_modify"),200];
		}else{
			return [Lang::get("message.not_modify"),409];
		}
	}
}
