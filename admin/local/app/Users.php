<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Lang;
use Session;

class Users extends Model
{
    protected $table = "users";

	protected $fillable = [
		"users_name",
		'users_codigo',
		"users_email",
		"users_password",
		"users_role",
	];

	protected $hidden = [
      'remember_token',
	  'id'
	];
		
	public static function login($inputs){
		$usuario = Users::where("users_email",$inputs["correo"])->where("users_password",hash("md5",$inputs["password"]));
		if($usuario->first()!=NULL){
			$contrasena = hash("md5",$inputs["password"]);
			\Cache::store('database')->put('acceso',$contrasena, 14400);				
			\Cache::store('database')->put('usuario_'.$contrasena, base64_encode($usuario->first()->toJson()),14400);				
			return [$usuario->first()->toJson(),200];
		}else{
			return [Lang::get("message.not_credentials"),401];
		}
	}
	
	public static function isCode($codigo){
		$usuario = Users::where("users_codigo",$codigo);
		if($usuario->first()!=NULL){
			return [Lang::get("message.yes_user_active"),200];
		}else{
			return [Lang::get("message.access_invalid"),401];
		}
	}
	
	public static function isPassword($codigo,$password){
		$usuario = Users::where("users_codigo",$codigo);
		if($usuario->first()!=NULL){
			if($usuario->first()->users_password==hash("md5",base64_decode($password))){
				return [1,200];
			}else{
				return [Lang::get("message.not_credentials"),401];
			}
		}else{
			return [Lang::get("message.access_invalid"),401];
		}
	}
}
