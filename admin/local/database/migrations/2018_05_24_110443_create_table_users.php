<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Users;

class CreateTableUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('users_name');
			$table->string('users_codigo');
            $table->string('users_email')->unique();
            $table->string('users_password');
			$table->enum('users_role',["0","1"]);
            $table->rememberToken();
            $table->timestamps();
        });
		
		Users::create([
			"users_name" => "USHOP",
			"users_codigo" => str_random(10),
			"users_email" => "haciendacreativa@gmail.com",
			"users_password" => hash("md5","hacienda"),
			"users_role" => "0"
		]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
