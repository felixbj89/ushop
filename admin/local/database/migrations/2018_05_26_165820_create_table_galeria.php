<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableGaleria extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
	 
	//SECTION / ORIGEN.
	//0 => HOME / 0 => BANNER PRINCIPAL
	//0 => HOME / 1 => PARALLAX
	//0 => HOME / 2 => CATÁLOGO PDF
	//1 => EMPRESA / 0 => BANNER PRINCIPAL
    public function up()
    {
        Schema::create('galeria', function (Blueprint $table) {
            $table->increments('id');
            $table->string('galeria_name');
			$table->string('galeria_codigo');
            $table->enum('galeria_tipo',['0','1']);
            $table->enum('galeria_section',['0','1','2','3']);
			$table->enum('galeria_origen',["0","1","2",'3','4','5','6']);
            $table->integer('item_id')->unsigned();
			$table->foreign('item_id')->references('id')->on('item');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('galeria');
    }
}
