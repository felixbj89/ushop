<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Bloque;
use App\Fonts;

class CreateTableFonts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fonts', function (Blueprint $table) {
            $table->increments('id');
            $table->text('fonts_family')->nullable();
			$table->string('fonts_codigo')->nullable();
            $table->timestamps();
        });
		
		Fonts::create([
			'fonts_family' => NULL,
			'fonts_codigo' => str_random(10),
		]);
		
		Schema::table('bloque', function (Blueprint $table) {
            $table->integer('fonts_id')->unsigned();
			$table->foreign('fonts_id')->references('id')->on('fonts');
        });
		
		Bloque::create([
			'bloque_textos' => NULL,
			'bloque_codigo' => str_random(10),
			'bloque_url' => NULL,
			'fonts_id' => 1
		]);
		
		Schema::table('galeria', function (Blueprint $table) {
            $table->integer('bloque_id')->unsigned();
			$table->foreign('bloque_id')->references('id')->on('bloque');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bloque');
    }
}
