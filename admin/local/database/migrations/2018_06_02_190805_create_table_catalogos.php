<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableCatalogos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		//0 => HOME/INICIO - 0 => CATALOGO/PDF
        Schema::create('catalogo', function (Blueprint $table) {
            $table->increments('id');
            $table->string('catalogo_name');
			$table->string('catalogo_codigo');
            $table->enum('catalogo_section',['0','1']);
			$table->enum('catalogo_origen',["0","1","2"]);
            $table->text('catalogo_ruta');
			$table->enum('catalogo_estado',['0','1']);
            $table->integer('users_id')->unsigned();
			$table->foreign('users_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::dropIfExists('catalogo');
    }
}
