<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Tecnicas;

class CreateTableTecnicas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tecnicas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('tecnicas_name');
			$table->string('tecnicas_codigo');
            $table->enum('tecnicas_section',['0','1','2']);
			$table->enum('tecnicas_origen',["0","1","2"]);
            $table->text('tecnicas_ruta');
			$table->text('tecnicas_descripcion');
            $table->integer('users_id')->unsigned();
			$table->foreign('users_id')->references('id')->on('users');
            $table->timestamps();
        });
		
		Tecnicas::create([
			"tecnicas_name" => "Serigrafía",
			"tecnicas_codigo" => str_random(10),
			"tecnicas_section" => "2",
			"tecnicas_origen" => "2",
			"tecnicas_ruta" => "img/defaults/defaults.svg",
			"tecnicas_descripcion" => "<strong>SIN DESCRIPCIÓN</strong>",
			"users_id" => 1
		]);
		
		Tecnicas::create([
			"tecnicas_name" => "Tampografía",
			"tecnicas_codigo" => str_random(10),
			"tecnicas_section" => "2",
			"tecnicas_origen" => "2",
			"tecnicas_ruta" => "img/defaults/defaults.svg",
			"tecnicas_descripcion" => "<strong>SIN DESCRIPCIÓN</strong>",
			"users_id" => 1
		]);
		
		Tecnicas::create([
			"tecnicas_name" => "Grabado Láser",
			"tecnicas_codigo" => str_random(10),
			"tecnicas_section" => "2",
			"tecnicas_origen" => "2",
			"tecnicas_ruta" => "img/defaults/defaults.svg",
			"tecnicas_descripcion" => "<strong>SIN DESCRIPCIÓN</strong>",
			"users_id" => 1
		]);
		
		Tecnicas::create([
			"tecnicas_name" => "Pantografía",
			"tecnicas_codigo" => str_random(10),
			"tecnicas_section" => "2",
			"tecnicas_origen" => "2",
			"tecnicas_ruta" => "img/defaults/defaults.svg",
			"tecnicas_descripcion" => "<strong>SIN DESCRIPCIÓN</strong>",
			"users_id" => 1
		]);
		
		Tecnicas::create([
			"tecnicas_name" => "Sublimación",
			"tecnicas_codigo" => str_random(10),
			"tecnicas_section" => "2",
			"tecnicas_origen" => "2",
			"tecnicas_ruta" => "img/defaults/defaults.svg",
			"tecnicas_descripcion" => "<strong>SIN DESCRIPCIÓN</strong>",
			"users_id" => 1
		]);
		
		Tecnicas::create([
			"tecnicas_name" => "Bordado",
			"tecnicas_codigo" => str_random(10),
			"tecnicas_section" => "2",
			"tecnicas_origen" => "2",
			"tecnicas_ruta" => "img/defaults/defaults.svg",
			"tecnicas_descripcion" => "<strong>SIN DESCRIPCIÓN</strong>",
			"users_id" => 1
		]);
		
		Tecnicas::create([
			"tecnicas_name" => "Transfers",
			"tecnicas_codigo" => str_random(10),
			"tecnicas_section" => "2",
			"tecnicas_origen" => "2",
			"tecnicas_ruta" => "img/defaults/defaults.svg",
			"tecnicas_descripcion" => "<strong>SIN DESCRIPCIÓN</strong>",
			"users_id" => 1
		]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tecnicas');
    }
}
