<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Equipo;

class CreateTableEquipo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('equipo', function (Blueprint $table) {
            $table->increments('id');
            $table->string('equipo_nombre');
			$table->string('equipo_codigo');
            $table->enum('equipo_section',['0','1','2']);
			$table->enum('equipo_origen',["0","1","2"]);
            $table->text('equipo_ruta');
            $table->integer('users_id')->unsigned();
			$table->foreign('users_id')->references('id')->on('users');
            $table->timestamps();
        });
		
		Equipo::create([
			"equipo_nombre" => "Creativos",
			'equipo_codigo' => str_random(10),
			"equipo_section" => "1",
			"equipo_origen" => "2",
			"equipo_ruta" => "img/equipo/Empresa_Equipo_1400x440.jpg",
			"users_id" => 1
		]);
		
		Equipo::create([
			"equipo_nombre" => "Producción",
			'equipo_codigo' => str_random(10),
			"equipo_section" => "1",
			"equipo_origen" => "2",
			"equipo_ruta" => "img/equipo/Empresa_Equipo_1400x440_2.jpg",
			"users_id" => 1
		]);
		
		Equipo::create([
			"equipo_nombre" => "Administrativos",
			'equipo_codigo' => str_random(10),
			"equipo_section" => "1",
			"equipo_origen" => "2",
			"equipo_ruta" => "img/equipo/Empresa_Equipo_1400x440_3.jpg",
			"users_id" => 1
		]);
		
		Equipo::create([
			"equipo_nombre" => "Ventas",
			'equipo_codigo' => str_random(10),
			"equipo_section" => "1",
			"equipo_origen" => "2",
			"equipo_ruta" => "img/equipo/Empresa_Equipo_1400x440_4.jpg",
			"users_id" => 1
		]);
		
		Equipo::create([
			"equipo_nombre" => "Directivos",
			'equipo_codigo' => str_random(10),
			"equipo_section" => "1",
			"equipo_origen" => "2",
			"equipo_ruta" => "img/equipo/Empresa_Equipo_1400x440_5.jpg",
			"users_id" => 1
		]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('equipo');
    }
}
