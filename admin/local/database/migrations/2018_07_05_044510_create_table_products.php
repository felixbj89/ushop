<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Productos;

class CreateTableProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('productos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('productos_name');
			$table->string('productos_codigo');
			$table->text('productos_descripcion');
			$table->text('productos_ruta');
			$table->string('productos_tipo')->nullable();
			$table->string('productos_estilo_one')->nullable();
			$table->string('productos_estilo_two')->nullable();
			$table->string('productos_estilo_thrid')->nullable();
            $table->integer('users_id')->unsigned();
			$table->foreign('users_id')->references('id')->on('users');
            $table->timestamps();
        });
		
		Productos::create([
			"productos_name" => "Bolso",
			"productos_codigo" => str_random(10),
			'productos_descripcion' => "27 x 19 x 14 cm. Capacidad 7 litros. Polyester. Conservadora con interior de polietileno. Correa ajustable. Tampografía-Serigrafía-Bordado",
			"productos_ruta" => "img/productos/Pop_bolso.jpg",
			"productos_tipo" => "Bolso",
			"productos_estilo_one" => "Tampografía",
			"productos_estilo_two" => "Serigrafía",
			"productos_estilo_thrid" => "Bordado",
			"users_id" => "1"
		]);
		
		Productos::create([
			"productos_name" => "Lapices",
			"productos_codigo" => str_random(10),
			'productos_descripcion' => "Plástico. Lápiz pasta con pulsador. Cuerpo plástico de color. Pulsador, clip y puntera plásticos color negro. Detalle en el clip. Pasta negra.",
			"productos_ruta" => "img/productos/Pop_lapices1.jpg",
			"productos_tipo" => "Lapices",
			"productos_estilo_one" => "Tampografía",
			"productos_estilo_two" => "Serigrafía",
			"productos_estilo_thrid" => NULL,
			"users_id" => "1"
		]);
		
		Productos::create([
			"productos_name" => "Lapices",
			"productos_codigo" => str_random(10),
			'productos_descripcion' => "Metálico. Pulsador, clip y puntera de plástico negro. Grip de goma negro. Cuerpo de color. Pasta negra.",
			"productos_ruta" => "img/productos/Pop_lapices2.jpg",
			"productos_tipo" => "Lapices",
			"productos_estilo_one" => "Tampografía",
			"productos_estilo_two" => "Grabado Láser",
			"productos_estilo_thrid" => "Pantografía",
			"users_id" => "1"
		]);
		
		Productos::create([
			"productos_name" => "Linterna",
			"productos_codigo" => str_random(10),
			'productos_descripcion' => "Ø 2,8 x 9,5 cm Metal. Con 9 luces blancas LED. Cordón de seguridad para mano. Botón de apagado y encendido. Funciona con 3 pilas AAA (no incluidas).",
			"productos_ruta" => "img/productos/Pop_linterna.jpg",
			"productos_tipo" => "Linterna",
			"productos_estilo_one" => "Tampografía",
			"productos_estilo_two" => "Pantografía",
			"productos_estilo_thrid" => NULL,
			"users_id" => "1"
		]);
		
		Productos::create([
			"productos_name" => "Mochila",
			"productos_codigo" => str_random(10),
			'productos_descripcion' => "41 x 31 cm. Non Woven. Mochila con dos cordones.  Los cordones sirven para llevar la mochila en la espalda. La parte superior se cierra ajustando los cordones. En la parte inferior los cordones pasan por un ojal.",
			"productos_ruta" => "img/productos/Pop_mochila.jpg",
			"productos_tipo" => "Mochila",
			"productos_estilo_one" => "Serigrafía",
			"productos_estilo_two" => "Transfer",
			"productos_estilo_thrid" => NULL,
			"users_id" => "1"
		]);
		
		Productos::create([
			"productos_name" => "Morral",
			"productos_codigo" => str_random(10),
			'productos_descripcion' => "42 x 22,5 x 10 cm. Capacidad 12 litros. Polyester. Mochila con un compartimento principal y bolsillo frontal con cierre. Correas ajustables con hebillas de plástico.",
			"productos_ruta" => "img/productos/Pop_morral.jpg",
			"productos_tipo" => "Morral",
			"productos_estilo_one" => "Bordado",
			"productos_estilo_two" => "Serigrafía",
			"productos_estilo_thrid" => "Transfer",
			"users_id" => "1"
		]);
		
		Productos::create([
			"productos_name" => "Mug",
			"productos_codigo" => str_random(10),
			'productos_descripcion' => "Ø 6,8 x 19,5 Acero inoxidable y plástico. Diseño de doble pared. Superficie exterior térmica. Mantiene la temperatura del interior. Irrompible. Capacidad: 380 ml.",
			"productos_ruta" => "img/productos/Pop_mug.jpg",
			"productos_tipo" => "Mug",
			"productos_estilo_one" => "Serigrafía",
			"productos_estilo_two" => NULL,
			"productos_estilo_thrid" => NULL,
			"users_id" => "1"
		]);
		
		Productos::create([
			"productos_name" => "Mug de Metal",
			"productos_codigo" => str_random(10),
			'productos_descripcion' => "Ø 6,5 x 16 cm. Capacidad 250 ml. Metal y plástico. Mug con tapa. Exterior de metal con detalles en relieve, interior plástico.  Tapa rosca en plástico. Base antideslizante.",
			"productos_ruta" => "img/productos/Pop_mugmetal.jpg",
			"productos_tipo" => "Mug de Metal",
			"productos_estilo_one" => "Grabado Láser",
			"productos_estilo_two" => "Pantografía",
			"productos_estilo_thrid" => NULL,
			"users_id" => "1"
		]);
		
		Productos::create([
			"productos_name" => "Necessaire",
			"productos_codigo" => str_random(10),
			'productos_descripcion' => "19 x 10 x 9,5 cm. Polyester. Necessaire de dos compartimientos, división movible sujeta con velcro. Detalles en símil cuero. Interior con forro. Tiradores de cierres de símil cuero.",
			"productos_ruta" => "img/productos/Pop_necessaire.jpg",
			"productos_tipo" => "Necessaire",
			"productos_estilo_one" => "Bordado",
			"productos_estilo_two" => "Serigrafía",
			"productos_estilo_thrid" => "Transfer",
			"users_id" => "1"
		]);
		
		Productos::create([
			"productos_name" => "Parlante",
			"productos_codigo" => str_random(10),
			'productos_descripcion' => "Ø 7 x 4 cm. Metal. Parlante bluetooth. Botones de volumen, encendido, Modo Bluetooth y TF card. En la base detalles  antideslizantes. Alcance de transmisión hasta 10 mts. Voltaje DC 5V. Incluye cable USB para carga",
			"productos_ruta" => "img/productos/Pop_parlante.jpg",
			"productos_tipo" => "Parlante",
			"productos_estilo_one" => "Serigrafía",
			"productos_estilo_two" => "Tampografía",
			"productos_estilo_thrid" => "Grabado Láser",
			"users_id" => "1"
		]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('productos');
    }
}