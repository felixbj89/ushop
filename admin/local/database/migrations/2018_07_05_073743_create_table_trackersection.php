<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Carbon\Carbon;

class CreateTableTrackersection extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('section', function (Blueprint $table) {
            $table->increments('id');
            $table->string('section_name');
			$table->string('section_codigo');
			$table->enum('section_type',['0','1','2','3']);
			$table->biginteger('section_visitas');
			$table->string('section_fecha');
        });
		
		\DB::table("section")->insert([
			"section_name" => "HOME/INICIO",
			"section_codigo" => str_random(10),
			"section_type" => "0",
			"section_visitas" => 0,
			"section_fecha" => Carbon::now(-4,"America/Caracas")
		]);
		
		\DB::table("section")->insert([
			"section_name" => "EMPRESA",
			"section_codigo" => str_random(10),
			"section_type" => "1",
			"section_visitas" => 0,
			"section_fecha" => Carbon::now(-4,"America/Caracas")
		]);
		
		\DB::table("section")->insert([
			"section_name" => "PRODUCTOS",
			"section_codigo" => str_random(10),
			"section_type" => "2",
			"section_visitas" => 0,
			"section_fecha" => Carbon::now(-4,"America/Caracas")
		]);
		
		\DB::table("section")->insert([
			"section_name" => "CONTACTO",
			"section_codigo" => str_random(10),
			"section_type" => "3",
			"section_visitas" => 0,
			"section_fecha" => Carbon::now(-4,"America/Caracas")
		]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('section');
    }
}
