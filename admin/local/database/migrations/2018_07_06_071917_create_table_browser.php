<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableBrowser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('browser', function (Blueprint $table) {
            $table->increments('id');
            $table->string('browser_name');
			$table->string('browser_codigo');
			$table->enum('browser_type',['0','1','2','3','4','5','6','7']);
			$table->integer('section_id')->unsigned();
			$table->foreign('section_id')->references('id')->on('section');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('browser');
    }
}
