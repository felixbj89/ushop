<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGaleriaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('galeriaservicio', function (Blueprint $table) {
            $table->increments('id');
            $table->string('galeria_codigo')->unique();
            $table->string('servicios_codigo');
            $table->string('galeria_imagen');
            $table->string('galeria_descripcion');
            $table->timestamps();
        });
        /****/
        /*Schema::table('galeriaservicio', function (Blueprint $table) {
            $table->foreign('servicios_codigo')->references('servicios_codigo')->on('servicios');
        });*/
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('galeria');
    }
}
