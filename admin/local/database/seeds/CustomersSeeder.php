<?php

use Illuminate\Database\Seeder;

class CustomersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		for($i = 0; $i < 8; $i++){
			\DB::table('customers')->insert([
				'customers_codigo' => str_random(10),
				"customers_logo_off" => "img/customers/Finales_".($i+1).".png",
				'customers_logo_on' => "img/customers/Finales_".($i+1).".png",
				"users_id" => 1
			]);
		}
    }
}
