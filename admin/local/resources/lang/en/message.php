<?php

return [

    'not_credentials' => 'Credenciales incorrectas.',
	'access_invalid' => 'ACCESO INVÁLIDO',
	"mediatitle" => "MEDIA",
	"yes_register" => "El registro se ha guardado satisfactoriamente.",
	"yes_delete" => "El archivo media ha sido removido satisfactoriamente.",
	"yes_delete_producto" => "El producto ha sido removido satisfactoriamente",
	"yes_modify" => "Los cambios han sido guardado satisfactoriamente.",
	"yes_active" => "Su servicio ha sido activado.",
	"yes_user_active" => "Su cuenta ha sido activada.",
	"yes_desaactive" => "Su servicio ha sido desactivado.",
	"match_picture" => "La imagen ha subir ya se encuentra en el sistema.",
	"match_pdf" => "El archivo ha subir ya se encuentra en el sistema.",
	"not_delete" => "Conflicto al momento de remover su archivo media",
	"not_delete_productos" => "Conflicto al momento de remover su producto",
	"not_register" => "El registro no pudo completarse, compruebe su registro.",
	"not_active" => "Su servicio no pudo ser activado.",
	"not_modify" => "Los cambios no pudieron ser guardados satisfactoriamente.",
	"need_video" => "Para una selección de vídeo se espera uno para ser registrado.",
	"need_picture" => "Para una selección de imagen se espera una para se registrada.",
	"size_pdf" => "Solo es permitido 100 archivos en este módulo, intente nuevamente." 
	
];
