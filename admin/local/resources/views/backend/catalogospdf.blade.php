@extends("layout.dashboard")
@section("section-title")
	Catálogo
	<small>Catálogo expresado en un archivo pdf presente en su sección Home/Inicio</small>
@endsection
@section("section-breadcrumb")
	<li><a href="{{url('ushop/dashboard')}}"><i class="fa fa-dashboard"></i> Inicio</a></li>
	<li class="active">Catálogo</li>
@endsection
@section("mi-css")
	<link rel="stylesheet" href="{{asset('css/wizardv3.css')}}">
@endsection
@section('menucatalogos')
	active
@endsection
@section("section-body")
	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title">GESTIONE SU BLOQUE #CATÁLOGO</h3>
			<div class="box-tools pull-right">
				<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Minimizar">
				<i class="fa fa-minus"></i></button>
			</div>
		</div>
		<div class="box-body">
			<section>
				<div class="wizard">
					<div class="wizard-inner">
						<div class="connecting-line"></div>
						<ul class="nav nav-tabs" role="tablist">
							<li role="presentation" class="active section mostrar">
								<a href="#step1" data-toggle="tab" aria-controls="step1" role="tab" title="Catálogo">
									<span class="round-tab">
										<i class="fa fa-file-pdf-o"></i>
									</span>
								</a>
							</li>
						</ul>
					</div>
					<form>
						<div class="tab-content">
							<div class="tab-pane active" role="tabpanel" id="step1">
								<div class="row">
									<div class="col-xs-12 col-sm-12 col-md-12 is-empty">
										<div id="bloqueestado" class="form-group has-feedback">
											<label for="secciones" class="secciones">Secciones</label>
											<select class="form-control" id="secciones" name="estadochocie"> 
												<option selected="selected" value="NULL">Seleccione una sección de su agrado</option>
												<option value="{{e(base64_encode('0'))}}">General</option>
											</select>
										</div>
										<span class="help-block-estado"></span>
									</div>
								</div>
								<div class="row">
									<div class="col-xs-12 col-md-12 is-empty">
										<div class="form-group">
											<label for="logodown">Ingrese su archivo</label>
											<a id="icologodown" href="#logo" data-toggle="collapse" data-parent="#accordion"><i class="fa fa-chevron-down" aria-hidden="true"></i></a></a>
											<div id="logo" class="panel-collapse in collapse" aria-expanded="false">
												<div class="box-body">
													<div class="item-grid-picture">
														<a id="logo-a" class="logo-link" href="{{url('/').'/img/logo/logosidebar.png'}}" data-lightbox="logo" class="spacing-picture">
															<img id="logo-img" class="logo" src="{{url('/').'/img/logo/logosimple_cf_ushop.svg'}}" class="img-responsive img-thumbnail"/>
														</a>
														<div class="row item-grid-picture-file">
															<div class="col-xs-12 col-md-12 is-empty">
																<div class="form-group item-grid-picture-file">
																	<label for="imagenpicture" class="clearfix control-label label-ushop">SUBA EL ARCHIVO DE SU AGRADO</label>
																	<input readonly="" id="namefile" class="form-control" placeholder="Suba su archivo" type="text">
																	<input type="file" id="imagenpicture" name="imagenpicture" accept="application/pdf" placeholder="Suba el archivo de su agrado." onchange="javascript:openFile(event)"  fileread="banner.imagen"/>
																	<p class="help-block">Solo es permitido archivos pdf.</p>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<hr/>
								<div class="row">
									<div class="col-xs-12 col-md-12">
										<button type="button" id="btnGuardarCompleto" class="btn btn-primary btn-raised btn-block btn-flat btn-ushop">
											<i class="fa fa-save" aria-hidden="true"></i> GUARDAR
											<div class="ripple-container"></div>
										</button>
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
			</section>
		</div>
	</div>
	<hr/>
	<!-- /.box -->
	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title">ELEMENTOS REGISTRADOS</h3>
			<div class="box-tools pull-right">
				<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Minimizar">
				<i class="fa fa-minus"></i></button>
			</div>
		</div>
		<div class="box-header with-border">
			<span id="contadorcatalogo">{{count($lista)}}/100</span>
		</div>
		<div class="box-body">
			<input type="hidden" id="scroll" value="{{e($scroll)}}"/>
			@if(isset($archivo[0]))
				<input type="hidden" id="archivo" value="{{e($archivo[0]->catalogo_codigo)}}"/>
			@endif
			<div class="row" id="tablacatalogos">
				<div class="col-xs-12">
					<table id="list_catalogos" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>Archivo</th>
								<th>Descargar</th>
								<th>Visualizar</th>
								<th>Estado</th>
								<th>Opciones</th>
							</tr>
						</thead>
						<tbody>
							@foreach($lista as $elementos)
								<tr data-codigocatalogo="{{e($elementos->catalogo_codigo)}}">
									<td>{{e($elementos->catalogo_name)}}</td>
									<td>
										<div class="box">
											<div class="box-body">
												<div class="row">
													<div class="col-xs-12">
														<a href="{{url('img/catalogo/'.$elementos->catalogo_ruta)}}"><i class="fa fa-file-pdf-o fa-2x pull-left" aria-hidden="true"></i></a>
														<a href="{{url('img/catalogo/'.$elementos->catalogo_ruta)}}"><i class="fa fa-download  fa-2x pull-right" aria-hidden="true"></i></a>
													</div>
												</div>
											</div>
										</div>
									</td>
									<td>
										<div class="box">
											<div class="box-body">
												<div class="row">
													<div class="col-xs-12 flex-center">
														<a href="{{url('img/catalogo/'.$elementos->catalogo_ruta)}}"  target="_blank"><i class="fa fa-eye fa-2x" aria-hidden="true"></i></a>
													</div>
												</div>
											</div>
										</div>
									</td>
									<td>
										@if($elementos->catalogo_estado=="1")
											<div class="box bg-ushop-on" id="" data-activar="{{e($elementos->catalogo_estado)}}">
												<div class="box-body">
													<div class="row">
														<div class="col-xs-12 flex-center">
															<span><i class="fa fa-power-off fa-2x" aria-hidden="true"></i><span>
														</div>
													</div>
												</div>
											</div>
										@else
											<div class="box bg-ushop-off" id="btnActivarFile" data-activar="{{e($elementos->catalogo_estado)}}" data-codigocatalogoactive="{{e($elementos->catalogo_codigo)}}">
												<div class="box-body">
													<div class="row">
														<div class="col-xs-12 flex-center">
															<span><i class="fa fa-power-off fa-2x" aria-hidden="true"></i></span>
														</div>
													</div>
												</div>
											</div>
										@endif
									</td>
									<td>
										<div class="row">
											<div class="col-xs-12 col-md-12">
												<button type="button" id="btnRemoverFile" class="btn btn-primary btn-raised btn-block btn-flat btn-ushop">
													<i class="fa fa-times" aria-hidden="true"></i> REMOVER
													<div class="ripple-container"></div>
												</button>
											</div>
										</div>
									</td>
								</tr>
							@endforeach
						</tbody>
						<tfoot>
							<tr>
								<th>Archivo</th>
								<th>Descargar</th>
								<th>Visualizar</th>
								<th>Estado</th>
								<th>Opciones</th>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
			<hr/>
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 is-empty">
					<a href="{{url('ushop/dashboard')}}" class="btn btn-primary btn-raised btn-block btn-flat btn-ushop-link">
						<i class="fa fa-dashboard" aria-hidden="true"></i> DASHBOARD
						<div class="ripple-container"></div>
					</a>
				</div>
			</div>
		</div>
		<!-- /.box-body -->
	</div>
	<div class="modal fade" id="modal_alerta" role="dialog" data-backdrop="static" data-keyboard="false">
		<div class="modal-dialog">
			<div class="modal-content item-modal-border">
				<div class="modal-header item-modal-header">
					<img src="{{asset('img/logo/logosimple_cf_ushop.svg')}}" width="128">
				</div>
				<div class="modal-body">
					<p id="messageadvertencia" class="text-center delsay-fonts"></p>
				</div>
				<div class="modal-footer item-modal-footer">
					<button type="button" class="btn btn-primary btn-raised btn-block btn-flat btn-ushop" data-dismiss="modal">
						<i class="fa fa-times" aria-hidden="true"></i> CERRAR
						<div class="ripple-container"></div>
					</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<div class="modal fade" id="modal_wait" role="dialog" data-backdrop="static" data-keyboard="false">
		<div class="modal-dialog">
			<div class="modal-content item-modal-border">
				<div class="modal-header item-modal-header">
					<img src="{{asset('img/logo/logosimple_cf_ushop.svg')}}" width="128">
				</div>
				<div class="modal-body">
					<p id="messageadvertencia" class="text-center delsay-fonts">Espere un momento.</p>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-xs-12 col-md-12 flex-center">
							<i class="fa fa-spinner fa-spin fa-2x"></i>
						</div>
					</div>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<div class="modal fade" id="modal_confirmar" role="dialog" data-backdrop="static" data-keyboard="false">
		<div class="modal-dialog">
			<div class="modal-content item-modal-border">
				<div class="modal-header item-modal-header">
					<img src="{{asset('img/logo/logosimple_cf_ushop.svg')}}" width="128">
				</div>
				<div class="modal-body">
					<p id="messageadvertencia" class="text-center delsay-fonts"><strong>¿Está usted seguro de remover su archivo?</strong></p>
				</div>
				<div class="modal-footer item-modal-footer">
					<button id="btnConfirmarRemoverMedia" type="button" class="btn btn-primary btn-raised btn-block btn-flat btn-ushop">
						<i class="fa fa-trash" aria-hidden="true"></i> REMOVER
						<div class="ripple-container"></div>
					</button>
					&nbsp;
					<button type="button" class="btn btn-primary btn-raised btn-block btn-flat btn-ushop" data-dismiss="modal">
						<i class="fa fa-times" aria-hidden="true"></i> CERRAR
						<div class="ripple-container"></div>
					</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<!-- /.box -->
	<div class="modal fade" id="modal_confirmar_activar" role="dialog" data-backdrop="static" data-keyboard="false">
		<div class="modal-dialog">
			<div class="modal-content item-modal-border">
				<div class="modal-header item-modal-header">
					<img src="{{asset('img/logo/logosimple_cf_ushop.svg')}}" width="128">
				</div>
				<div class="modal-body">
					<p id="activar_messageadvertencia" class="text-center delsay-fonts"><strong></strong></p>
				</div>
				<div class="modal-footer item-modal-footer">
					<button id="btnConfirmarActivarMedia" type="button" class="btn btn-primary btn-raised btn-block btn-flat btn-ushop"></button>
					&nbsp;
					<button type="button" class="btn btn-primary btn-raised btn-block btn-flat btn-ushop" data-dismiss="modal">
						<i class="fa fa-times" aria-hidden="true"></i> CERRAR
						<div class="ripple-container"></div>
					</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
@endsection
@section('mi-scripts')
	<script src="{{asset('js/controlador/callajax/catalogo.js')}}"></script>
	<script src="{{asset('js/wizard.js')}}"></script>
	<script src="{{asset('js/controlador/catalogo.js')}}"></script>
@endsection