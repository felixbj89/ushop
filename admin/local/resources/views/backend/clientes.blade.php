@extends("layout.dashboard")
@section("section-title")
	Técnicas
	<small>Administre sus técnicas</small>
@endsection
@section("section-breadcrumb")
	<li><a href="{{url('ushop/dashboard')}}"><i class="fa fa-dashboard"></i> Inicio</a></li>
	<li class="active">Clientes</li>
@endsection
@section("mi-css")
	<link rel="stylesheet" href="{{asset('css/wizardv3.css')}}">
	<link rel="stylesheet" href="{{asset('css/productos.css')}}">
@endsection
@section('clientes')
	active
@endsection
@section("section-body")
	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title">GESTIONE SU BLOQUE #CLIENTES</h3>
			<div class="box-tools pull-right">
				<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Minimizar">
				<i class="fa fa-minus"></i></button>
			</div>
		</div>
		<div class="box-body">
			<section>
				<div class="wizard">
					<div class="wizard-inner">
						<div class="connecting-line"></div>
						<ul class="nav nav-tabs" role="tablist">
							<li role="presentation" class="active section mostrar">
								<a href="#step1" data-toggle="tab" aria-controls="step1" role="tab" title="Clientes">
									<span class="round-tab">
										<i class="fa fa-male"></i>
									</span>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</section>
		</div>
	</div>
	<hr/>
	<!-- /.box -->
	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title">CLIENTES EXISTENTES</h3>
			<div class="box-tools pull-right">
				<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Minimizar">
				<i class="fa fa-minus"></i></button>
			</div>
		</div>
		<div class="box-body">
			<div class="row">
				<div class="col-xs-12">
					<table id="list_clientes" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>Cliente</th>
								<th>Opciones</th>
							</tr>
						</thead>
						<tbody>
							@foreach($clientes as $c)
								<tr data-customerscodigo="{{e($c->customers_codigo)}}">
									<td>
										<div class="row">
											<div class="col-xs-12 flex-center">
												<img id="logo-img-{{e($c->customers_codigo)}}" class="logo-table flex-center" src="{{url('/').'/'.$c->customers_logo_on}}" class="img-responsive img-thumbnail" width="auto" height="500">
											</div>
										</div>
									</td>
									<td>
										<div class="row flex-center">
											<button type="button" id="btnGestionarcustomer" class="btn btn-primary btn-raised btn-flat btn-ushop" aria-label="Administrar" height="50">
												<i class="fa fa-cog" aria-hidden="true"></i>
											</button>
											&nbsp;
											<button type="button" class="btnEliminarcustomer btn btn-primary btn-raised btn-flat btn-ushop" aria-label="Activar / Desactivar" height="50">
												<i class="fa fa-trash-o" aria-hidden="true"></i>
											</button>
										</div>
									</td>
								</tr>
							@endforeach
						</tbody>
						<tfoot>
							<tr>
								<th>Cliente</th>
								<th>Opciones</th>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
			<hr/>
			<div class="row">
				<div class="col-xs-12 col-md-6">
					<button type="button" id="btnGuardarPrincipal" class="btn btn-primary btn-raised btn-block btn-flat btn-ushop">
						<i class="fa fa-plus" aria-hidden="true"></i> AGREGAR
						<div class="ripple-container"></div>
					</button>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 is-empty">
					<a href="{{url('ushop/dashboard')}}" class="btn btn-primary btn-raised btn-block btn-flat btn-ushop-link">
						<i class="fa fa-dashboard" aria-hidden="true"></i> DASHBOARD
						<div class="ripple-container"></div>
					</a>
				</div>
			</div>
		</div>
		<!-- /.box-body -->
	</div>
	<div class="modal fade" id="modal_confirmar" role="dialog" data-backdrop="static" data-keyboard="false">
		<div class="modal-dialog">
			<div class="modal-content item-modal-border">
				<div class="modal-header item-modal-header">
					<img src="{{asset('img/logo/logosimple_cf_ushop.svg')}}" width="128">
				</div>
				<div class="modal-body">
					<p class="text-center delsay-fonts"><strong>¿Está usted seguro de remover el item de cliente?</strong></p>
				</div>
				<div class="modal-footer item-modal-footer">
					<button id="btnConfirmarRemoverMedia" type="button" class="btn btn-primary btn-raised btn-block btn-flat btn-ushop">
						<i class="fa fa-trash" aria-hidden="true"></i> REMOVER
						<div class="ripple-container"></div>
					</button>
					&nbsp;
					<button type="button" class="btn btn-primary btn-raised btn-block btn-flat btn-ushop" data-dismiss="modal">
						<i class="fa fa-times" aria-hidden="true"></i> CERRAR
						<div class="ripple-container"></div>
					</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>	
	<div class="modal fade" id="modal_alerta" role="dialog" data-backdrop="static" data-keyboard="false">
		<div class="modal-dialog">
			<div class="modal-content item-modal-border">
				<div class="modal-header item-modal-header">
					<img src="{{asset('img/logo/logosimple_cf_ushop.svg')}}" width="128">
				</div>
				<div class="modal-body">
					<p id="messageadvertencia" class="text-center delsay-fonts"></p>
				</div>
				<div class="modal-footer item-modal-footer">
					<button type="button" class="btn btn-primary btn-raised btn-block btn-flat btn-ushop" data-dismiss="modal">
						<i class="fa fa-times" aria-hidden="true"></i> CERRAR
						<div class="ripple-container"></div>
					</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<div class="modal fade" id="modal_wait" role="dialog" data-backdrop="static" data-keyboard="false">
		<div class="modal-dialog">
			<div class="modal-content item-modal-border">
				<div class="modal-header item-modal-header">
					<img src="{{asset('img/logo/logosimple_cf_ushop.svg')}}" width="128" height="64">
				</div>
				<div class="modal-body">
					<p class="text-center delsay-fonts">Espere un momento.</p>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-xs-12 col-md-12 flex-center">
							<i class="fa fa-spinner fa-spin fa-2x"></i>
						</div>
					</div>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>	
@endsection
@section('mi-scripts')
	<script src="{{asset('js/controlador/callajax/tecnicas.js')}}"></script>
	<script src="{{asset('js/wizard.js')}}"></script>
	<script src="{{asset('js/controlador/customers.js')}}"></script>
@endsection