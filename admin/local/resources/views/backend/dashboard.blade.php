@extends("layout.dashboard")
@section("section-title")
	Inicio
	<small>Resumen General</small>
@endsection
@section("section-breadcrumb")
	<li><a href="{{url('ushop/dashboard')}}"><i class="fa fa-dashboard"></i> Inicio</a></li>
	<li class="active">Inicio</li>
@endsection
@section("dashboard-link")
	<li id="link-to" class="">					
		<a id="" href="{{$rutabase}}" target="_blank"><i class="fa fa-link"></i></a>
	</li>
@endsection
@section("dashboard-config")
	<li id="config-to" class="">					
		<a id="config" href="#modal_config" data-toggle="modal" data-target="#moal_config"><i class="fa fa-cog"></i></a>
	</li>
@endsection
@section('dashboard')
	active
@endsection
@section("dashboard-notificaciones")
	<li class="dropdown messages-menu">
		<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
			<i class="fa fa-bell-o"></i>
			@if(count($contactos) > 0)
				<span class="label label-danger">{{count($contactos)}}</span>
			@else
				<span class="label label-danger">0</span>
			@endif
			<div class="ripple-container"></div>
		</a>
		<ul class="dropdown-menu">
			<li class="header">Solicitudes</li>
			<li>
				<!-- inner menu: contains the actual data -->
				<ul class="menu">
					@foreach($contactos as $c)
						<li><!-- start message -->
							<a href="{{url('ushop/dashboard/solicitudes')}}">
								<div class="pull-left">
									<i class="fa fa-globe fa-2x fa-color-ushop" style="color:#a40000"></i>
								</div>
								<h4>
									{{$c->contacto_nombre}}
								</h4>
								<p>{{$c->contacto_correo}}</p>
							</a>
						</li>
					@endforeach
					<!-- end message -->
				</ul>
			</li>
			<li class="footer"><a href="{{url('ushop/dashboard/solicitudes')}}">Listar</a></li>
		</ul>
	</li>
@endsection
@section("section-body")
	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title">Inicio</h3>
			<div class="box-tools pull-right">
				<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Minimizar">
				<i class="fa fa-minus"></i></button>
			</div>
		</div>
		<div class="box-body">
			<div clasS="row">
				<div class="col-md-6 col-sm-12 col-xs-12">
					<div class="info-box bg-red-ushop">
						<span class="info-box-icon" id="verpdf" title="Liste sus catalogos"><a href="{{url('ushop/catalogos/pdf/'.base64_encode('tablacatalogos'))}}"><i class="fa fa-file-pdf-o"></i></a></span>
						<div class="info-box-content">
							<span class="info-box-text titulobox">Catálogo</span>
							@if(isset($ruta[0]))
								<span class="info-box-number">1</span>
								<div class="progress">
									<div class="progress-bar" style="width: 100%"></div>
								</div>
							@else
								<span class="info-box-number">0</span>
								<div class="progress">
									<div class="progress-bar" style="width: 0%"></div>
								</div>
							@endif
							<span class="progress-description">
								<a href="{{url('ushop/catalogos/pdf/'.base64_encode('tablacatalogos'))}}" class="ushop-link">ADMINISTRAR</a>
							</span>
						</div>
						<!-- /.info-box-content -->
					</div>
					<!-- /.info-box -->
				</div>
				<div class="col-md-6 col-sm-12 col-xs-12">
					<div class="info-box bg-red-ushop">
						<span class="info-box-icon" id="vertecnicas" title="Liste sus Técnicas"><a href="{{url('ushop/productos/tecnicas/listar')}}"><i class="fa fa-pencil"></i></a></span>
						<div class="info-box-content">
							<span class="info-box-text">Técnicas</span>
							@if(isset($tecnicas))
								<span class="info-box-number">{{count($tecnicas)}}</span>
								<div class="progress">
									<div class="progress-bar" style="width: 100%"></div>
								</div>
							@else
								<span class="info-box-number">0</span>
								<div class="progress">
									<div class="progress-bar" style="width: 0%"></div>
								</div>
							@endif
							<span class="progress-description">
								<a href="{{url('ushop/productos/tecnicas/listar')}}" class="ushop-link">LISTAR</a>
							</span>
						</div>
						<!-- /.info-box-content -->
					</div>
					<!-- /.info-box -->
				</div>
			</div>
		</div>
		<!-- /.box-body -->
	</div>
	<!-- /.box -->
	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title">Banners Principales</h3>
		</div>
		<div class="box-body">
			<div clasS="row">
				<div class="col-md-3 col-sm-12 col-xs-12">
					<div class="info-box bg-red-ushop">
						<span class="info-box-icon" id="" title="Gestione su banner principal"><a href="{{url('ushop/imagen/principal/'.base64_encode('h'))}}"><i class="fa fa-picture-o"></i></a></span>
						<div class="info-box-content">
							<span class="info-box-text">Home/Inicio</span>
							@if($banners[0] == 3)
								<span class="info-box-number">3</span>
								<div class="progress">
									<div class="progress-bar" style="width: 100%"></div>
								</div>
							@elseif($banners[0] == 1)
								<span class="info-box-number">1</span>
								<div class="progress">
									<div class="progress-bar" style="width: 33%"></div>
								</div>
							@elseif($banners[0] == 2)
								<span class="info-box-number">2</span>
								<div class="progress">
									<div class="progress-bar" style="width: 66%"></div>
								</div>
							@else
								<span class="info-box-number">0</span>
								<div class="progress">
									<div class="progress-bar" style="width: 0%"></div>
								</div>
							@endif
							<span class="progress-description">
								<a href="{{url('ushop/imagen/principal/'.base64_encode('h'))}}" class="ushop-link">ADMINISTRAR</a>
							</span>
						</div>
						<!-- /.info-box-content -->
					</div>
					<!-- /.info-box -->
				</div>
				<div class="col-md-3 col-sm-12 col-xs-12">
					<div class="info-box bg-red-ushop">
						<span class="info-box-icon" id="" title="Gestione su banner principal"><a href="{{url('ushop/imagen/principal/'.base64_encode('e'))}}"><i class="fa fa-picture-o"></i></a></span>
						<div class="info-box-content">
							<span class="info-box-text">Empresa</span>
							@if($banners[1] > 0)
								<span class="info-box-number">1</span>
								<div class="progress">
									<div class="progress-bar" style="width: 100%"></div>
								</div>
							@else
								<span class="info-box-number">0</span>
								<div class="progress">
									<div class="progress-bar" style="width: 0%"></div>
								</div>
							@endif
							<span class="progress-description">
								<a href="{{url('ushop/imagen/principal/'.base64_encode('e'))}}" class="ushop-link">ADMINISTRAR</a>
							</span>
						</div>
						<!-- /.info-box-content -->
					</div>
					<!-- /.info-box -->
				</div>
				<div class="col-md-3 col-sm-12 col-xs-12">
					<div class="info-box bg-red-ushop">
						<span class="info-box-icon" id="" title="Gestione su banner principal"><a href="{{url('ushop/imagen/principal/'.base64_encode('p'))}}"><i class="fa fa-picture-o"></i></a></span>
						<div class="info-box-content">
							<span class="info-box-text">Productos</span>
							@if($banners[2] > 0)
								<span class="info-box-number">1</span>
								<div class="progress">
									<div class="progress-bar" style="width: 100%"></div>
								</div>
							@else
								<span class="info-box-number">0</span>
								<div class="progress">
									<div class="progress-bar" style="width: 0%"></div>
								</div>
							@endif
							<span class="progress-description">
								<a href="{{url('ushop/imagen/principal/'.base64_encode('p'))}}" class="ushop-link">ADMINISTRAR</a>
							</span>
						</div>
						<!-- /.info-box-content -->
					</div>
					<!-- /.info-box -->
				</div>
				<div class="col-md-3 col-sm-12 col-xs-12">
					<div class="info-box bg-red-ushop">
						<span class="info-box-icon" id="" title="Gestione su banner principal"><a href="{{url('ushop/imagen/principal/'.base64_encode('c'))}}"><i class="fa fa-picture-o"></i></a></span>
						<div class="info-box-content">
							<span class="info-box-text">Contacto</span>
							@if($banners[3] > 0)
								<span class="info-box-number">1</span>
								<div class="progress">
									<div class="progress-bar" style="width: 100%"></div>
								</div>
							@else
								<span class="info-box-number">0</span>
								<div class="progress">
									<div class="progress-bar" style="width: 0%"></div>
								</div>
							@endif
							<span class="progress-description">
								<a href="{{url('ushop/imagen/principal/'.base64_encode('c'))}}" class="ushop-link">ADMINISTRAR</a>
							</span>
						</div>
						<!-- /.info-box-content -->
					</div>
					<!-- /.info-box -->
				</div>
			</div>
		</div>
	</div>
	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title">Resumen de visita / Visita Anuales</h3>
			<input type="hidden" id="jsonvisitas" value="{{base64_encode($visitaActual)}}"/>
		</div>
		<div class="box-body">
			<div clasS="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					 <div class="chart">
						<canvas id="areaChart" style="height:250px"></canvas>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title">Resumen de dispositivos y visitas por país</h3>
			<input type="hidden" id="jsonbrowser" value="{{base64_encode($navegadores)}}"/>
		</div>
		<div class="box-body">
			<div clasS="row">
				<div class="col-md-6 col-sm-12 col-xs-12">
					 <div class="chart">
						<canvas id="areaChart_browser" style="height:350px"></canvas>
					</div>
				</div>
				<div class="col-md-6 col-sm-12 col-xs-12">
					<table id="list_ip" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>Código</th>
								<th>País</th>
								<th>Ip</th>
							</tr>
						</thead>
						<tbody>
							@foreach($locationes as $l)
								<tr>
									<td>{{$l->location_codigo}}</td>
									<td>{{$l->location_pais}}</td>
									<td>{{$l->location_ip}}</td>
								</tr>
							@endforeach
						</tbody>
						<tfoot>
							<tr>
								<th>Código</th>
								<th>País</th>
								<th>Ip</th>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="modal_config" role="dialog" data-backdrop="static" data-keyboard="false">
		<div class="modal-dialog">
			<div class="modal-content item-modal-border">
				<div class="modal-header item-modal-header">
					<img src="{{asset('img/logo/logosimple_cf_ushop.svg')}}" width="128">
				</div>
				<div class="modal-body">
					<form action="../../index2.html" method="post">
						<div class="row">
							<div class="col-xs-12">
								<div class="box item-box">
									<div class="box-header flex-center">
										<label>Ingrese su enlace.</label>
									</div>
								</div>
							</div>
						</div>
						<div class="box item-box">
							<div class="box-body item-box">
								<div class="form-group has-feedback item-form-group">
									<input type="url" class="form-control" id="enlaces" name="enlaces" placeholder="INGRESE SU ENLACE">
									<span class="glyphicon glyphicon-link form-control-feedback"></span>
								</div>
							</div>
						</div>
						<div class="box item-box">
							<div class="box-body item-box">
								<div class="form-group has-feedback item-form-group">
									<select class="form-control" id="destinoenlace" name="destinoenlace">
										<option value="NULL">Seleccione el destino de su agrado</option>
										<option value="{{base64_encode('AI')}}">INGRESAR</option>
										<option value="{{base64_encode('AP')}}">PRODUCTOS</option>
									</select>
									<span class="glyphicon glyphicon-bookmark form-control-feedback"></span>
								</div>
							</div>
						</div>
					</form>
				</div>
				<div class="modal-footer item-modal-footer">
					<button id="btnGuardarConfig" type="button" class="btn btn-primary btn-raised btn-block btn-flat btn-ushop">
						<i class="fa fa-save" aria-hidden="true"></i> GUARDAR
						<div class="ripple-container"></div>
					</button>
					&nbsp;
					<button type="button" class="btn btn-primary btn-raised btn-block btn-flat btn-ushop" data-dismiss="modal">
						<i class="fa fa-times" aria-hidden="true"></i> CERRAR
						<div class="ripple-container"></div>
					</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<div class="modal fade" id="modal_alerta_unlock" role="dialog" data-backdrop="static" data-keyboard="false">
		<div class="modal-dialog">
			<div class="modal-content item-modal-border">
				<div class="modal-header item-modal-header">
					<img src="{{asset('img/logo/logosimple_cf_ushop.svg')}}" width="128">
				</div>
				<div class="modal-body">
					<p id="messageadvertencia" class="text-center delsay-fonts"></p>
				</div>
				<div class="modal-footer item-modal-footer">
					<button type="button" class="btn btn-primary btn-raised btn-block btn-flat btn-ushop" data-dismiss="modal">
						<i class="fa fa-times" aria-hidden="true"></i> CERRAR
						<div class="ripple-container"></div>
					</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<div class="modal fade" id="modal_alerta_enlaces" role="dialog" data-backdrop="static" data-keyboard="false">
		<div class="modal-dialog">
			<div class="modal-content item-modal-border">
				<div class="modal-header item-modal-header">
					<img src="{{asset('img/logo/logosimple_cf_ushop.svg')}}" width="128">
				</div>
				<div class="modal-body">
					<p id="enlacesadvertencia" class="text-center delsay-fonts"></p>
				</div>
				<div class="modal-footer item-modal-footer">
					<button type="button" class="btn btn-primary btn-raised btn-block btn-flat btn-ushop" data-dismiss="modal">
						<i class="fa fa-times" aria-hidden="true"></i> CERRAR
						<div class="ripple-container"></div>
					</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<div class="modal fade" id="modal_bloqueo" role="dialog" data-backdrop="static" data-keyboard="false">
		<div class="modal-dialog">
			<div class="modal-content item-modal-border modal-md">
				<div class="modal-header item-modal-header">
					<img src="{{asset('img/logo/logosimple_cf_ushop.svg')}}" width="128">
				</div>
				<div class="modal-body">
					<div class="login-box-body item-border-login">
						<hr/>
						<form method="post">
							<div class="row">
								<div class="col-xs-12">
									<div class="box item-box">
										<div class="box-header flex-center">
											<label>Ingrese su contraseña para continuar.</label>
										</div>
									</div>
								</div>
							</div>
							<div class="box item-box">
								<div class="box-body item-box">
									<div class="form-group has-feedback item-form-group">
										<input type="password" class="form-control" id="password" name="password" placeholder="Password">
										<span class="glyphicon glyphicon-lock form-control-feedback"></span>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
				<div class="modal-footer item-modal-footer">
					<button id="btnAccederPassword" type="button" class="btn btn-primary btn-raised btn-block btn-flat btn-ushop">
						<i class="fa fa-sign-in" aria-hidden="true"></i> ACCEDER
						<div class="ripple-container"></div>
					</button>
					&nbsp;
					<button id="salirlock" type="button" class="btn btn-primary btn-raised btn-block btn-flat btn-ushop" data-dismiss="modal">
						<i class="fa fa-sign-out" aria-hidden="true"></i> SALIR
						<div class="ripple-container"></div>
					</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
@endsection