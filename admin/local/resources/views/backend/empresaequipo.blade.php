@extends("layout.dashboard")
@section("section-title")
	Equipo
	<small>Administre a su Equipo</small>
@endsection
@section("section-breadcrumb")
	<li><a href="{{url('ushop/dashboard')}}"><i class="fa fa-dashboard"></i> Inicio</a></li>
	<li class="active">Equipo</li>
@endsection
@section("mi-css")
	<link rel="stylesheet" href="{{asset('css/wizardv3.css')}}">
	<link rel="stylesheet" href="{{asset('css/productos.css')}}">
@endsection
@section('equipoempresa')
	active
@endsection
@section("section-body")
	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title">GESTIONE SU BLOQUE #EQUIPO</h3>
			<div class="box-tools pull-right">
				<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Minimizar">
				<i class="fa fa-minus"></i></button>
			</div>
		</div>
		<div class="box-body">
			<section>
				<div class="wizard">
					<div class="wizard-inner">
						<div class="connecting-line"></div>
						<ul class="nav nav-tabs" role="tablist">
							<li role="presentation" class="active section mostrar">
								<a href="#step1" data-toggle="tab" aria-controls="step1" role="tab" title="Catálogo">
									<span class="round-tab">
										<i class="fa fa-users"></i>
									</span>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</section>
		</div>
	</div>
	<hr/>
	<!-- /.box -->
	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title">EQUIPO USHOP</h3>
			<div class="box-tools pull-right">
				<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Minimizar">
				<i class="fa fa-minus"></i></button>
			</div>
		</div>
		<div class="box-body">
			<div class="row">
				<div class="col-xs-12">
					<table id="list_empresaquipo" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>Nombre</th>
								<th>Imagen</th>
								<th>Opciones</th>
							</tr>
						</thead>
						<tbody>
							@foreach($equipo as $e)
								<tr data-codigoempresa="{{$e->equipo_codigo}}">
									<td>{{$e->equipo_nombre}}</td>
									<td>
										<div class="row">
											<div class="col-xs-12">
												<a id="logo-a" class="logo-link flex-center" href="{{url('/').'/'.$e->equipo_ruta}}" data-lightbox="logo" class="spacing-picture">
													<img id="logo-img" class="logo logo-table" src="{{url('/').'/'.$e->equipo_ruta}}" class="img-responsive img-thumbnail" width="256" height="64">
												</a>
											</div>
										</div>
									</td>
									<td>
										<div class="row">
											<div class="col-xs-12">
												<button type="button" id="btnGestionarEquipo" class="btn btn-primary btn-raised btn-block btn-flat btn-ushop">
													<i class="fa fa-cog" aria-hidden="true"></i> ADMINISTRAR
													<div class="ripple-container"></div>
												</button>
											</div>
										</div>
									</td>
								</tr>
							@endforeach
						</tbody>
						<tfoot>
							<tr>
								<th>Nombre</th>
								<th>Imagen</th>
								<th>Opciones</th>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
			<hr/>
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 is-empty">
					<a href="{{url('ushop/dashboard')}}" class="btn btn-primary btn-raised btn-block btn-flat btn-ushop-link">
						<i class="fa fa-dashboard" aria-hidden="true"></i> DASHBOARD
						<div class="ripple-container"></div>
					</a>
				</div>
			</div>
		</div>
		<!-- /.box-body -->
	</div>
@endsection
@section('mi-scripts')
	<script src="{{asset('js/wizard.js')}}"></script>
	<script src="{{asset('js/controlador/equipoempresa.js')}}"></script>
@endsection