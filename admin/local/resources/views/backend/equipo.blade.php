@extends("layout.dashboard")
@section("section-title")
	Publicidad
	<small>Publicidad presente en su sección Home/Inicio</small>
@endsection
@section("section-breadcrumb")
	<li><a href="{{url('ushop/dashboard')}}"><i class="fa fa-dashboard"></i> Inicio</a></li>
	<li class="active">Publicidad</li>
@endsection
@section("mi-css")
	<link rel="stylesheet" href="{{asset('css/wizardv3.css')}}">
@endsection
@section('menuimagenes')
	active
@endsection
@section('equipo')
	active
@endsection
@section("section-body")
	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title">GESTIONE SU BLOQUE #IMAGEN EQUIPO</h3>
			<div class="box-tools pull-right">
				<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Minimizar">
				<i class="fa fa-minus"></i></button>
			</div>
		</div>
		<div class="box-body">
			<section>
				<div class="wizard">
					<div class="wizard-inner">
						<div class="connecting-line"></div>
						<ul class="nav nav-tabs" role="tablist">
							<li role="presentation" class="active section mostrar">
								<a href="#step1" data-toggle="tab" aria-controls="step1" role="tab" title="Equipo">
									<span class="round-tab">
										<i class="fa fa-users"></i>
									</span>
								</a>
							</li>
						</ul>
					</div>
					<form>
						<div class="tab-content">
							<div class="tab-pane active" role="tabpanel" id="step1">
								<div class="row">
									<div class="col-xs-12 col-sm-12 col-md-12 is-empty">
										<div id="bloqueestado" class="form-group has-feedback">
											<label for="secciones" class="secciones">Secciones</label>
											<select class="form-control" id="secciones" name="estadochocie"> 
												<option selected="selected" value="NULL">Seleccione una sección de su agrado</option>
												<option value="{{e(base64_encode('1'))}}">Empresa</option>
											</select>
										</div>
										<span class="help-block-estado"></span>
									</div>
								</div>
								<div class="row">
									<div class="col-xs-12 col-md-12 is-empty">
										<div class="form-group">
											<label for="logodown">Ingrese su imagen</label>
											<a id="icologodown" href="#logo" data-toggle="collapse" data-parent="#accordion"><i class="fa fa-chevron-down" aria-hidden="true"></i></a></a>
											<div id="logo" class="panel-collapse in collapse" aria-expanded="false">
												<div class="box-body">
													<div class="item-grid-picture">
														<a id="logo-a" class="logo-link" href="{{url('/').'/img/logo/logosidebar.png'}}" data-lightbox="logo" class="spacing-picture">
															<img id="logo-img" class="logo" src="{{url('/').'/img/logo/logosimple_cf_ushop.svg'}}" class="img-responsive img-thumbnail"/>
														</a>
														<div class="row item-grid-picture-file">
															<div class="col-xs-12 col-md-12 is-empty">
																<div class="form-group item-grid-picture-file">
																	<label for="imagenpicture" class="clearfix control-label label-ushop">SUBA LA IMAGEN DE SU AGRADO</label>
																	<input readonly="" id="namefile" class="form-control" placeholder="Suba su imagen" type="text">
																	<input type="file" id="imagenpicture" name="imagenpicture" accept="image/x-png,image/jpeg,image/jpg" placeholder="Suba la imagen de su agrado." onchange="javascript:openFile(event)"  fileread="banner.imagen"/>
																	<p class="help-block">Solo es permitido imágenes jpg/png.</p>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<hr/>
								<div class="row">
									<div class="col-xs-12 col-md-12">
										<button type="button" id="btnGuardarPrincipal" class="btn btn-primary btn-raised btn-block btn-flat btn-ushop">
											<i class="fa fa-save" aria-hidden="true"></i> GUARDAR
											<div class="ripple-container"></div>
										</button>
									</div>
								</div>
							</div>
							<div class="clearfix"></div>
						</div>
					</form>
				</div>
			</section>
		</div>
	</div>
	<hr/>
	<!-- /.box -->
	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title">ELEMENTOS REGISTRADOS</h3>
			<div class="box-tools pull-right">
				<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Minimizar">
				<i class="fa fa-minus"></i></button>
			</div>
		</div>
		<div class="box-body">
			<div class="row">
				<div class="col-xs-12">
					<table id="list_imagenesprincipales" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>Nombre</th>
								<th>Sección</th>
								<th>Preview</th>
								<th>Opciones</th>
							</tr>
						</thead>
						<tbody>
							@foreach($lista as $elementos)
								<tr data-codigogaleria="{{e($elementos->galeria_codigo)}}">
									<td>{{e($elementos->item_name)}}</td>
									<td>
										<div class="row">
											@if($elementos->galeria_section=="0")
												<div class="col-xs-12 flex-center">
													<img src="{{asset('img/logo/logosimple_cf_ushop.svg')}}" width="128" height="64">
												</div>
											@endif
										</div>
									</td>
									<td>
										<div class="row">
											<div class="col-xs-12">
												@if($elementos->galeria_tipo=="0")
													<a id="logo-a" class="logo-link flex-center" href="{{url('/').'/img/media/'.$elementos->item_ruta}}" data-lightbox="logo" class="spacing-picture">
														<img id="logo-img" class="logo logo-table" src="{{url('/').'/img/media/'.$elementos->item_ruta}}" class="img-responsive img-thumbnail" width="256" height="64">
													</a>
												@else
													<div class="flex-center">
														<img id="logo-img" class="logo logo-table" src="{{url('/').'/img/defaults/defaults.svg'}}" class="img-responsive img-thumbnail" width="256" height="64">
													</div>
												@endif
											</div>
										</div>
									</td>
									<td>
										<div class="row">
											<div class="col-xs-12 ushop-col-6-12">
												<button type="button" id="btnModificarTimes" class="btn btn-primary btn-raised btn-block btn-flat btn-ushop">
													<i class="fa fa-edit" aria-hidden="true"></i> MODIFICAR
													<div class="ripple-container"></div>
												</button>
											</div>
											<div class="col-xs-12 ushop-col-6-12">
												<button type="button" id="btnRemoverMedia" class="btn btn-primary btn-raised btn-block btn-flat btn-ushop">
													<i class="fa fa-times" aria-hidden="true"></i> REMOVER
													<div class="ripple-container"></div>
												</button>
											</div>
										</div>
									</td>
								</tr>
							@endforeach
						</tbody>
						<tfoot>
							<tr>
								<th>Nombre</th>
								<th>Sección</th>
								<th>Preview</th>
								<th>Opciones</th>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
			<hr/>
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-6 is-empty">
					<a href="{{url('ushop/imagen/publicidad/'.base64_encode('c'))}}" class="btn btn-primary btn-raised btn-block btn-flat btn-ushop-link">
						<i class="fa fa-picture-o" aria-hidden="true"></i> CATÁLOGO
						<div class="ripple-container"></div>
					</a>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 is-empty">
					<a href="{{url('ushop/dashboard')}}" class="btn btn-primary btn-raised btn-block btn-flat btn-ushop-link">
						<i class="fa fa-dashboard" aria-hidden="true"></i> DASHBOARD
						<div class="ripple-container"></div>
					</a>
				</div>
			</div>
		</div>
		<!-- /.box-body -->
	</div>
	<div class="modal fade" id="modal_alerta" role="dialog" data-backdrop="static" data-keyboard="false">
		<div class="modal-dialog">
			<div class="modal-content item-modal-border">
				<div class="modal-header item-modal-header">
					<img src="{{asset('img/logo/logosimple_cf_ushop.svg')}}" width="128">
				</div>
				<div class="modal-body">
					<p id="messageadvertencia" class="text-center delsay-fonts"></p>
				</div>
				<div class="modal-footer item-modal-footer">
					<button type="button" class="btn btn-primary btn-raised btn-block btn-flat btn-ushop" data-dismiss="modal">
						<i class="fa fa-times" aria-hidden="true"></i> CERRAR
						<div class="ripple-container"></div>
					</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<div class="modal fade" id="modal_wait" role="dialog" data-backdrop="static" data-keyboard="false">
		<div class="modal-dialog">
			<div class="modal-content item-modal-border">
				<div class="modal-header item-modal-header">
					<img src="{{asset('img/logo/logosimple_cf_ushop.svg')}}" width="128">
				</div>
				<div class="modal-body">
					<p id="messageadvertencia" class="text-center delsay-fonts">Espere un momento.</p>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-xs-12 col-md-12 flex-center">
							<i class="fa fa-spinner fa-spin fa-2x"></i>
						</div>
					</div>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<div class="modal fade" id="modal_confirmar" role="dialog" data-backdrop="static" data-keyboard="false">
		<div class="modal-dialog">
			<div class="modal-content item-modal-border">
				<div class="modal-header item-modal-header">
					<img src="{{asset('img/logo/logosimple_cf_ushop.svg')}}" width="128">
				</div>
				<div class="modal-body">
					<p id="messageadvertencia" class="text-center delsay-fonts"><strong>¿Está usted seguro de remover su archivo media?</strong></p>
				</div>
				<div class="modal-footer item-modal-footer">
					<button id="btnConfirmarRemoverMedia" type="button" class="btn btn-primary btn-raised btn-block btn-flat btn-ushop">
						<i class="fa fa-trash" aria-hidden="true"></i> REMOVER
						<div class="ripple-container"></div>
					</button>
					&nbsp;
					<button type="button" class="btn btn-primary btn-raised btn-block btn-flat btn-ushop" data-dismiss="modal">
						<i class="fa fa-times" aria-hidden="true"></i> CERRAR
						<div class="ripple-container"></div>
					</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<!-- /.box -->
@endsection
@section('mi-scripts')
	<script src="{{asset('js/controlador/callajax/principal.js')}}"></script>
	<script src="{{asset('js/wizard.js')}}"></script>
	<script src="{{asset('js/controlador/equipo.js')}}"></script>
@endsection