@extends("layout.dashboard")
@section("section-title")
	Instrucciones
	<small>Consule las instrucciones de uso de sus módulos</small>
@endsection
@section("section-breadcrumb")
	<li><a href="{{url('ushop/dashboard')}}"><i class="fa fa-dashboard"></i> Inicio</a></li>
	<li class="active">Instrucciones</li>
@endsection
@section("mi-css")
	<link rel="stylesheet" href="{{asset('css/wizardv2.css')}}">
@endsection
@if($modo=='h')
	@section('bannerhome')
		active
	@endsection
@elseif($modo=='cp')
	@section('menuimagenescatalogo')
		active
	@endsection
	@section('contactocatalogo')
		active
	@endsection
@elseif($modo=='p')
	@section('bannerproductos')
		active
	@endsection
@elseif($modo=='hp')
	@section('homeproductos')
		active
	@endsection
@elseif($modo=='pi')
	@section('menuimportadores')
		active
	@endsection
@elseif($modo=='bp')
	@section('menuchat')
		active
	@endsection
@elseif($modo=='c')
	@section('bannercontacto')
		active
	@endsection
@elseif($modo=='e')
	@section('bannerempresa')
		active
	@endsection
@else
	@section('menuimagenesprincipal')
		active
	@endsection
	@section('contacto')
		active
	@endsection
@endif
@section("section-body")
	<div class="box collapsed-box">
		<div class="box-header with-border">
			<h5 class="box-title"><i>BANNER CON TEXTO</i></h5>
			<div class="box-tools pull-right">
				<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Minimizar">
				<i class="fa fa-plus"></i></button>
			</div>
		</div>
		<div class="box-body" style="display:none">
			<section>
				<div class="row">
					<div class="col-xs-12">
						<p class="textos-ushop-p">
							<i class="fa fa-arrow-right" aria-hidden="true"></i>
							Paso #1: Seleccionar la sección ha utilizar.
						</p>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<p class="textos-ushop-p">
							<i class="fa fa-arrow-right" aria-hidden="true"></i>
							Paso #2: Seleccione el tipo de archivo media ha utilizar (Vídeo/Imagen).
						</p>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<p class="textos-ushop-p">
							<i class="fa fa-arrow-right" aria-hidden="true"></i>
							Paso #2.1: Resolución de la imagen ha utilizar: 2048x500px
						</p>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<p class="textos-ushop-p">
							<i class="fa fa-arrow-right" aria-hidden="true"></i>
							Paso #3: Hacer click en continuar
						</p>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<p class="textos-ushop-p">
							<i class="fa fa-arrow-right" aria-hidden="true"></i>
							Paso #4: Ingresar el tipo de fuente ha utilizar
						</p>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<p class="textos-ushop-p">
							<i class="fa fa-arrow-right" aria-hidden="true"></i>
							Paso #5: Ingresar el texto ha utilizar y haer click Guardar todo
						</p>
					</div>
				</div>
			</section>
		</div>
	</div>
	<div class="box collapsed-box">
		<div class="box-header with-border">
			<h5 class="box-title"><i>BANNER SIN TEXTO</i></h5>
			<div class="box-tools pull-right">
				<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Minimizar">
				<i class="fa fa-plus"></i></button>
			</div>
		</div>
		<div class="box-body" style="display:none">
			<section>
				<div class="row">
					<div class="col-xs-12">
						<p class="textos-ushop-p">
							<i class="fa fa-arrow-right" aria-hidden="true"></i>
							Paso #1: Seleccionar la sección ha utilizar.
						</p>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<p class="textos-ushop-p">
							<i class="fa fa-arrow-right" aria-hidden="true"></i>
							Paso #2: Seleccione el tipo de archivo media ha utilizar (Vídeo/Imagen).
						</p>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<p class="textos-ushop-p">
							<i class="fa fa-arrow-right" aria-hidden="true"></i>
							Paso #2.1: Resolución de la imagen ha utilizar: 2048x500px
						</p>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<p class="textos-ushop-p">
							<i class="fa fa-arrow-right" aria-hidden="true"></i>
							Paso #3: Seleccione y suba la imagen ha utilizar a través del botón Guardar
						</p>
					</div>
				</div>
			</section>
		</div>
	</div>
	<div class="box collapsed-box">
		<div class="box-header with-border">
			<h5 class="box-title"><i>SUSTITUCIONES DE IMÁGENES</i></h5>
			<div class="box-tools pull-right">
				<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Minimizar">
				<i class="fa fa-plus"></i></button>
			</div>
		</div>
		<div class="box-body" style="display:none">
			<section>
				<div class="row">
					<div class="col-xs-12">
						<p class="textos-ushop-p">
							<i class="fa fa-arrow-right" aria-hidden="true"></i>
							La susitución de imágen, se encargará de remover la imagen asociada anterior
						</p>
					</div>
				</div>
			</section>
		</div>
	</div>
	<div class="box collapsed-box">
		<div class="box-header with-border">
			<h5 class="box-title"><i>ELIMINACIÓN DE IMÁGENES</i></h5>
			<div class="box-tools pull-right">
				<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Minimizar">
				<i class="fa fa-plus"></i></button>
			</div>
		</div>
		<div class="box-body" style="display:none">
			<section>
				<div class="row">
					<div class="col-xs-12">
						<p class="textos-ushop-p">
							<i class="fa fa-arrow-right" aria-hidden="true"></i>
							El asociar una imagen ya registrada y su posterior remoción hará que se elimine para todo el sistema.
						</p>
					</div>
				</div>
			</section>
		</div>
	</div>
	<div class="box collapsed-box">
		<div class="box-header with-border">
			<h5 class="box-title"><i>PRODUCTOS</i></h5>
			<div class="box-tools pull-right">
				<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Minimizar">
				<i class="fa fa-plus"></i></button>
			</div>
		</div>
		<div class="box-body" style="display:none">
			<div class="box-header with-border">
				<h5 class="box-title"><i>PRODUCTOS: CANTIDAD</i></h5>
			</div>
			<div class="box-body">
				<section>
					<div class="row">
						<div class="col-xs-12">
							<p class="textos-ushop-p">
								<i class="fa fa-arrow-right" aria-hidden="true"></i>
								Debe existir por lo menos un producto registrado en el sistema
							</p>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<p class="textos-ushop-p">
								<i class="fa fa-arrow-right" aria-hidden="true"></i>
								El máximo de productos regisrtaso en el sistema es de 10 elementos
							</p>
						</div>
					</div>
				</section>
			</div>
			<div class="box-header with-border">
				<h5 class="box-title"><i>PRODUCTOS: ESTILOS</i></h5>
			</div>
			<div class="box-body">
				<section>
					<div class="row">
						<div class="col-xs-12">
							<p class="textos-ushop-p">
								<i class="fa fa-arrow-right" aria-hidden="true"></i>
								El mínimo de estilos para un producto debe ser de #2
							</p>
						</div>
					</div>
				</section>
			</div>
		</div>
	</div>
	<div class="box collapsed-box">
		<div class="box-header with-border">
			<h5 class="box-title"><i>MEDIDAS/TAMAÑOS DE IMÁGENES</i></h5>
			<div class="box-tools pull-right">
				<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Minimizar">
				<i class="fa fa-plus"></i></button>
			</div>
		</div>
		<div class="box-body" style="display:none">
			<section>
				<div class="row">
					<div class="col-xs-12">
						<p class="textos-ushop-p">
							<i class="fa fa-arrow-right" aria-hidden="true"></i>
							Bloque #Banner Principal / Sección #Todas
						</p>
						<p class="textos-ushop-p">
							Tamaño: 2048px/500px
						</p>
					</div>
					<div class="col-xs-12">
						<a id="logo-a" class="logo-link flex-center" href="{{url('/').'/img/defaults/banner.png'}}" data-lightbox="logo" class="spacing-picture">
							<img id="logo-img" class="logo logo-instrucciones" src="{{url('/').'/img/defaults/banner.png'}}" class="img-responsive img-thumbnail" width="1024" height="128">
						</a>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<p class="textos-ushop-p">
							<i class="fa fa-arrow-right" aria-hidden="true"></i>
							Bloque #Equipo / Sección #Contacto
						</p>
						<p class="textos-ushop-p">
							Tamaño: 2048px/300px
						</p>
					</div>
					<div class="col-xs-12">
						<a id="logo-a" class="logo-link flex-center" href="{{url('/').'/img/defaults/secc3_contacto_2048x500.png'}}" data-lightbox="logo" class="spacing-picture">
							<img id="logo-img" class="logo logo-instrucciones" src="{{url('/').'/img/defaults/secc3_contacto_2048x500.png'}}" class="img-responsive img-thumbnail" width="1024" height="128">
						</a>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<p class="textos-ushop-p">
							<i class="fa fa-arrow-right" aria-hidden="true"></i>
							Bloque #Productos / Sección #Home/Inicio
						</p>
						<p class="textos-ushop-p">
							Tamaño: 260px/350px
						</p>
					</div>
					<div class="col-xs-12">
						<a id="logo-a" class="logo-link flex-center" href="{{url('/').'/img/defaults/productos.png'}}" data-lightbox="logo" class="spacing-picture">
							<img id="logo-img" class="logo logo-instrucciones" src="{{url('/').'/img/defaults/productos.png'}}" class="img-responsive img-thumbnail" width="260" height="350">
						</a>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<p class="textos-ushop-p">
							<i class="fa fa-arrow-right" aria-hidden="true"></i>
							Bloque #Importamos / Sección #Productos
						</p>
						<p class="textos-ushop-p">
							Tamaño: 2048px/600px
						</p>
					</div>
					<div class="col-xs-12">
						<a id="logo-a" class="logo-link flex-center" href="{{url('/').'/img/defaults/banner.png'}}" data-lightbox="logo" class="spacing-picture">
							<img id="logo-img" class="logo logo-instrucciones" src="{{url('/').'/img/defaults/banner.png'}}" class="img-responsive img-thumbnail" width="1024" height="128">
						</a>
					</div>
				</div>
			</section>
		</div>
	</div>
	<div class="box">
		<div class="box-body">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 is-empty">
					<a href="{{url('ushop/dashboard')}}" class="btn btn-primary btn-raised btn-block btn-flat btn-ushop-link">
						<i class="fa fa-dashboard" aria-hidden="true"></i> DASHBOARD
						<div class="ripple-container"></div>
					</a>
				</div>
			</div>
		</div>
	</div>
@endsection
@section('mi-scripts')
	<script src="{{asset('js/controlador/callajax/principal.js')}}"></script>
	<script src="{{asset('js/wizard.js')}}"></script>
	<script src="{{asset('js/controlador/principal.js')}}"></script>
@endsection