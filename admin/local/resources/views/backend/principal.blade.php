@extends("layout.dashboard")
@section("section-title")
	Imágenes
	<small>Imágenes/Banners Principales de cada sección</small>
@endsection
@section("section-breadcrumb")
	<li><a href="{{url('ushop/dashboard')}}"><i class="fa fa-dashboard"></i> Inicio</a></li>
	<li class="active">Imágenes</li>
@endsection
@section("mi-css")
	<link rel="stylesheet" href="{{asset('css/wizardv2.css')}}">
@endsection
@if($modo=='h')
	@section('menubanners')
		active
	@endsection
	@section('bannerhome')
		active
	@endsection
@elseif($modo=='cp')
	@section('menuimagenescatalogo')
		active
	@endsection
	@section('contactocatalogo')
		active
	@endsection
@elseif($modo=='p')
	@section('menubanners')
		active
	@endsection
	@section('bannerproductos')
		active
	@endsection
@elseif($modo=='pi')
	@section('menuimportadores')
		active
	@endsection
@elseif($modo=='bp')
	@section('menuchat')
		active
	@endsection
@elseif($modo=='c')
	@section('menubanners')
		active
	@endsection
	@section('bannercontacto')
		active
	@endsection
@elseif($modo=='e')
	@section('menubanners')
		active
	@endsection
	@section('bannerempresa')
		active
	@endsection
@else
	@section('menuimagenesprincipal')
		active
	@endsection
	@section('contacto')
		active
	@endsection
@endif
@section("section-body")
	<div class="box">
		<div class="box-header with-border">
			@if($modo=='cp')
				<h3 class="box-title">GESTIONE SU BLOQUE #IMÁGENES SECUNDARIAS</h3>
			@else
				<h3 class="box-title">GESTIONE SU BLOQUE #IMÁGENES PRINCIPALES</h3>
			@endif
			<div class="box-tools pull-right">
				<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Minimizar">
				<i class="fa fa-minus"></i></button>
				<button id="goinstrucciones" type="button" class="btn btn-box-tool" title="Instrucciones" data-modo="{{base64_encode($modo)}}">
				<i class="fa fa-question"></i></button>
			</div>
		</div>
		<div class="box-header with-border">
			<small class="title-ushop-instrucciones"><i>Haga click en <i class="fa fa-question"></i> para visualizar sus instrucciones</i></small>
		</div>
		<div class="box-body">
			<section>
				<div class="wizard">
					<input type="hidden" id="modo" value="{{base64_encode($modo)}}"/>
					<div class="wizard-inner">
						<div class="connecting-line"></div>
						<ul class="nav nav-tabs" role="tablist">
							<li role="presentation" class="active section mostrar">
								<a href="#step1" data-toggle="tab" aria-controls="step1" role="tab" title="Secciones & Tipo fuentes">
									<span class="round-tab">
										<i class="fa fa-file-picture-o"></i>
									</span>
								</a>
							</li>
							<li role="presentation" class="disabled imagenes ocultar">
								<a href="#step2" data-toggle="tab" aria-controls="step2" role="tab" title="Fuentes">
									<span class="round-tab">
										<i class="fa fa-pencil"></i>
									</span>
								</a>
							</li>
						</ul>
					</div>
					<form>
						<div class="tab-content">
							<div class="tab-pane active" role="tabpanel" id="step1">
								<div class="row">
									<div class="col-xs-12 col-sm-6 col-md-6 is-empty">
										<div id="bloqueestado" class="form-group has-feedback">
											<label for="secciones" class="secciones">Secciones</label>
											<select class="form-control" id="secciones" name="estadochocie"> 
												<option selected="selected" value="NULL">Seleccione una sección de su agrado</option>
												@if($modo=='h')
													<option value="{{e(base64_encode('0'))}}">Home/Inicio</option>
												@elseif($modo=='c' || $modo=='cp')
													<option value="{{e(base64_encode('3'))}}">Contacto</option>
												@elseif($modo=='p' || $modo=='pi' || $modo=='bp')
													<option value="{{e(base64_encode('2'))}}">Productos</option>
												@elseif($modo=='e')
													<option value="{{e(base64_encode('1'))}}">Empresa</option>
												@endif
											</select>
										</div>
										<span class="help-block-estado"></span>
									</div>
									<div class="col-xs-12 col-sm-6 col-md-6 is-empty">
										<div id="bloquemedia" class="form-group has-feedback">
											<label for="secciones" class="secciones">Archivo media</label>
											<select class="form-control" id="tipomedia" name="estadochocie" disabled> 
												<option selected="selected" value="NULL">Seleccione un tipo de archivo de su agrado</option>
											</select>
										</div>
										<span class="help-block-media"></span>
									</div>
								</div>
								<div id="rowpicture" class="row">
									<div class="col-xs-12 col-md-12 is-empty">
										<div class="form-group">
											<label for="logodown">Ingrese su imagen</label>
											<a id="icologodown" href="#logo" data-toggle="collapse" data-parent="#accordion"><i class="fa fa-chevron-down" aria-hidden="true"></i></a></a>
											<div id="logo" class="panel-collapse in collapse" aria-expanded="false">
												<div class="box-body">
													<div class="item-grid-picture">
														<a id="logo-a" class="logo-link" href="{{url('/').'/img/logo/logosidebar.png'}}" data-lightbox="logo" class="spacing-picture">
															<img id="logo-img" class="logo" src="{{url('/').'/img/logo/logosimple_cf_ushop.svg'}}" class="img-responsive img-thumbnail"/>
														</a>
														<div class="row item-grid-picture-file">
															<div class="col-xs-12 col-md-12 is-empty">
																<div class="form-group item-grid-picture-file">
																	<label for="imagenpicture" class="clearfix control-label label-ushop">SUBA LA IMAGEN DE SU AGRADO</label>
																	<input readonly="" id="namefile" class="form-control" placeholder="Suba su imagen" type="text">
																	<input type="file" id="imagenpicture" name="imagenpicture" accept="image/x-png,image/jpeg,image/jpg" placeholder="Suba la imagen de su agrado." onchange="javascript:openFile(event)"  fileread="banner.imagen"/>
																	<p class="help-block">Solo es permitido imágenes jpg/png.</p>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<hr/>
								<div class="row">
									<div class="col-xs-12 col-sm-12 col-md-12 is-empty">
										<ul class="list-inline pull-right">
											<li><button type="button" class="btn btn-primary next-step goimagenes">CONTINUAR</button></li>
										</ul>
									</div>
								</div>
								<div class="box-header with-border">
									<small class="title-ushop-instrucciones"><i>Está opción solo guardaría una imagen</i></small>
								</div>
								<hr/>
								<div class="row">
									<div class="col-xs-12 col-md-12">
										@if($modo=='h')
											@if(count($lista)==3)
												<button type="button" id="btnGuardarPrincipal" class="btn btn-primary btn-raised btn-block btn-flat btn-ushop" disabled>
													<i class="fa fa-save" aria-hidden="true"></i> GUARDAR
													<div class="ripple-container"></div>
												</button>
											@else
												<button type="button" id="btnGuardarPrincipal" class="btn btn-primary btn-raised btn-block btn-flat btn-ushop">
													<i class="fa fa-save" aria-hidden="true"></i> GUARDAR
													<div class="ripple-container"></div>
												</button>
											@endif
										@else
											<button type="button" id="btnGuardarPrincipal" class="btn btn-primary btn-raised btn-block btn-flat btn-ushop">
												<i class="fa fa-save" aria-hidden="true"></i> GUARDAR
												<div class="ripple-container"></div>
											</button>
										@endif
									</div>
								</div>
							</div>
							<div class="tab-pane" role="tabpanel" id="step2">
								<!-- One "tab" for each step in the form: -->
								<div id="rowpicture" class="row">
									<div class="col-xs-12 col-md-12 is-empty">
										<ul class="list-inline pull-right">
											<li><button type="button" class="btn btn-default prev-step goseccion">ATRAS</button></li>
										</ul>
									</div>
								</div>
								<div class="row">
									<div class="col-xs-12 col-sm-12 col-md-6 is-empty">
										<div id="bloquefuentes" class="form-group has-feedback">
											<label for="secciones" class="secciones label-ushop">Fuentes</label>
											<select class="form-control" id="fuentes" name="fuentes"> 
												<option value="NULL">Seleccione la fuente de su agrado</option>
												@foreach(json_decode($fuentes,true) as $nombrefuente => $fonts)
													<option value="{{e(base64_encode(json_encode($fonts)))}}">{{$nombrefuente}}</option>
												@endforeach
											</select>
										</div>
										<span class="help-block-fuentes"></span>
									</div>
									<div class="col-xs-12 col-sm-12 col-md-6 is-empty">
										<div id="bloquetipofuentes" class="form-group has-feedback">
											<label for="secciones" class="secciones label-ushop">Tipo de fuente</label>
											<select class="form-control" id="tipofuentes" name="tipofuentes" disabled> 
												<option selected="selected" value="NULL">Seleccione la fuente de su agrado</option>
											</select>
										</div>
										<span class="help-block-fuentes"></span>
									</div>
								</div>
								<div class="row">
									<div class="col-xs-12 col-sm-12 col-md-12 is-empty">
										<h3 class="box-title">GESTIONE SU BLOQUE DE TEXTO.</h3>
									</div>
								</div>
								<div class="box-body">
									<div class="row">
										<div class="col-xs-12 col-md-12 is-empty">
											<div id="bloquetexto" class="form-group has-feedback">
												<label for="descripcionnotificacion">TEXTO</label>
												<div id="descripcion"></div>
											</div>
											<span class="help-block-contacto"></span>
										</div>
									</div>
								</div>
								@if($modo!='cp' && $modo!='p' && $modo!='pi' && $modo!='bp' && $modo!='e' && $modo!='c')
									<hr/>
									@if(!$confirmarurl)
										<div id="bloquetitleurl" class="row">
											<div class="col-xs-12 col-sm-12 col-md-12 is-empty">
												<h3 class="box-title">GESTIONE SU U.R.L.</h3>
											</div>
										</div>
										<div id="bloqueurlcompleto"  class="row">
											<div class="col-xs-12 col-sm-12 col-md-12 is-empty">
												<div id="bloqueurl" class="form-group has-feedback">
													<label class="label-ushop url">U.R.L.</label>
													<input class="form-control" type="text" id="url" name="url" placeholder="Ingrese la url de su agrado">
												</div>
											</div>
										</div>
									@endif
								@endif
								<div id="rowpicture" class="row">
									<div class="col-xs-12 col-md-12 is-empty">
										<ul class="list-inline pull-right">
											<li><button type="button" class="btn btn-default prev-step goseccion">ATRAS</button></li>
										</ul>
									</div>
								</div>
								<div class="box-header with-border">
									<small class="title-ushop-instrucciones"><i>Está opción solo guardaría un bloque de información y su imagen</i></small>
								</div>
								<hr/>
								<div class="row">
									<div class="col-xs-12 col-md-12">
										@if($modo=='h')
											@if(count($lista)==3)
												<button type="button" id="btnGuardarCompleto" class="btn btn-primary btn-raised btn-block btn-flat btn-ushop" disabled>
													<i class="fa fa-save" aria-hidden="true"></i> GUARDAR TODO
													<div class="ripple-container"></div>
												</button>
											@else
												<button type="button" id="btnGuardarCompleto" class="btn btn-primary btn-raised btn-block btn-flat btn-ushop">
													<i class="fa fa-save" aria-hidden="true"></i> GUARDAR TODO
													<div class="ripple-container"></div>
												</button>
											@endif
										@else
											<button type="button" id="btnGuardarCompleto" class="btn btn-primary btn-raised btn-block btn-flat btn-ushop">
												<i class="fa fa-save" aria-hidden="true"></i> GUARDAR TODO
												<div class="ripple-container"></div>
											</button>
										@endif
									</div>
								</div>
							</div>
							<div class="clearfix"></div>
						</div>
					</form>
				</div>
			</section>
		</div>
	</div>
	<hr/>
	<!-- /.box -->
	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title">ELEMENTOS REGISTRADOS</h3>
			<div class="box-tools pull-right">
				<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Minimizar">
				<i class="fa fa-minus"></i></button>
			</div>
		</div>
		<div class="box-body">
			<div class="row">
				<div class="col-xs-12">
					<table id="list_imagenesprincipales" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>Nombre</th>
								<th>Sección</th>
								<th>Preview</th>
								<th>Opciones</th>
							</tr>
						</thead>
						<tbody>
							@foreach($lista as $elementos)
								<tr data-codigogaleria="{{e($elementos->galeria_codigo)}}" data-modo="{{base64_encode($modo)}}">
									<td>{{e($elementos->item_name)}}</td>
									<td>
										<div class="row">
											@if($elementos->galeria_section=="0")
												<div class="col-xs-12 flex-center">
													<img src="{{asset('img/logo/backend/home.svg')}}" width="128" height="64">
												</div>
											@elseif($elementos->galeria_section=="1")
												<div class="col-xs-12 flex-center">
													<img src="{{asset('img/logo/backend/empresa.svg')}}" width="128" height="64">
												</div>
											@elseif($elementos->galeria_section=="2")
												<div class="col-xs-12 flex-center">
													<img src="{{asset('img/logo/backend/productos.svg')}}" width="128" height="64">
												</div>
											@elseif($elementos->galeria_section=="3")
												<div class="col-xs-12 flex-center">
													<img src="{{asset('img/logo/backend/contacto.svg')}}" width="128" height="64">
												</div>
											@endif
										</div>
									</td>
									<td>
										<div class="row">
											<div class="col-xs-12">
												@if($elementos->galeria_tipo=="0")
													<a id="logo-a" class="logo-link flex-center" href="{{url('/').'/img/media/'.$elementos->item_ruta}}" data-lightbox="logo" class="spacing-picture">
														<img id="logo-img" class="logo logo-table" src="{{url('/').'/img/media/'.$elementos->item_ruta}}" class="img-responsive img-thumbnail" width="256" height="64">
													</a>
												@else
													<div class="flex-center">
														<img id="logo-img" class="logo logo-table" src="{{url('/').'/img/defaults/defaults.svg'}}" class="img-responsive img-thumbnail" width="256" height="64">
													</div>
												@endif
											</div>
										</div>
									</td>
									<td>
										<div class="row">
											<div class="col-xs-12 ushop-col-6-12">
												<button type="button" id="btnModificarMedia" class="btn btn-primary btn-raised btn-block btn-flat btn-ushop">
													<i class="fa fa-edit" aria-hidden="true"></i> MODIFICAR
													<div class="ripple-container"></div>
												</button>
											</div>
											<div class="col-xs-12 ushop-col-6-12">
												<button type="button" id="btnRemoverMedia" class="btn btn-primary btn-raised btn-block btn-flat btn-ushop">
													<i class="fa fa-times" aria-hidden="true"></i> REMOVER
													<div class="ripple-container"></div>
												</button>
											</div>
										</div>
									</td>
								</tr>
							@endforeach
						</tbody>
						<tfoot>
							<tr>
								<th>Nombre</th>
								<th>Sección</th>
								<th>Preview</th>
								<th>Opciones</th>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
			<hr/>
			<div class="row">
				@if($modo!='cp' && $modo!='bp')
					<div class="col-xs-12 col-sm-12 col-md-6 is-empty">
						@if($modo=='h')
							<a href="{{url('ushop/imagen/principal/'.base64_encode('e'))}}" class="btn btn-primary btn-raised btn-block btn-flat btn-ushop-link">
							<i class="fa fa-file-picture-o" aria-hidden="true"></i> EMPRESA
							<div class="ripple-container"></div>
						@elseif($modo=='c')
							<a href="{{url('ushop/imagen/principal/'.base64_encode('h'))}}" class="btn btn-primary btn-raised btn-block btn-flat btn-ushop-link">
							<i class="fa fa-file-picture-o" aria-hidden="true"></i> HOME/INICIO
							<div class="ripple-container"></div>
						@elseif($modo=='p')
							<a href="{{url('ushop/imagen/principal/'.base64_encode('c'))}}" class="btn btn-primary btn-raised btn-block btn-flat btn-ushop-link">
							<i class="fa fa-file-picture-o" aria-hidden="true"></i> CONTACTO
							<div class="ripple-container"></div>
						@elseif($modo=='pi')
							<a href="{{url('ushop/imagen/principal/'.base64_encode('bp'))}}" class="btn btn-primary btn-raised btn-block btn-flat btn-ushop-link">
							<i class="fa fa-comments-o " aria-hidden="true"></i> CHAT
							<div class="ripple-container"></div>
						@elseif($modo=='e')
							<a href="{{url('ushop/imagen/principal/'.base64_encode('p'))}}" class="btn btn-primary btn-raised btn-block btn-flat btn-ushop-link">
							<i class="fa fa-file-picture-o" aria-hidden="true"></i> PRODUCTOS
							<div class="ripple-container"></div>
						@endif
						</a>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-6 is-empty">
						<a href="{{url('ushop/dashboard')}}" class="btn btn-primary btn-raised btn-block btn-flat btn-ushop-link">
							<i class="fa fa-dashboard" aria-hidden="true"></i> DASHBOARD
							<div class="ripple-container"></div>
						</a>
					</div>
				@else
					<div class="col-xs-12 col-sm-12 col-md-12 is-empty">
						<a href="{{url('ushop/dashboard')}}" class="btn btn-primary btn-raised btn-block btn-flat btn-ushop-link">
							<i class="fa fa-dashboard" aria-hidden="true"></i> DASHBOARD
							<div class="ripple-container"></div>
						</a>
					</div>
				@endif
			</div>
		</div>
		<!-- /.box-body -->
	</div>
	<div class="modal fade" id="modal_alerta" role="dialog" data-backdrop="static" data-keyboard="false">
		<div class="modal-dialog">
			<div class="modal-content item-modal-border">
				<div class="modal-header item-modal-header">
					<img src="{{asset('img/logo/logosimple_cf_ushop.svg')}}" width="128" height="64">
				</div>
				<div class="modal-body">
					<p id="messageadvertencia" class="text-center delsay-fonts"></p>
				</div>
				<div class="modal-footer item-modal-footer">
					<button type="button" class="btn btn-primary btn-raised btn-block btn-flat btn-ushop" data-dismiss="modal">
						<i class="fa fa-times" aria-hidden="true"></i> CERRAR
						<div class="ripple-container"></div>
					</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<div class="modal fade" id="modal_wait" role="dialog" data-backdrop="static" data-keyboard="false">
		<div class="modal-dialog">
			<div class="modal-content item-modal-border">
				<div class="modal-header item-modal-header">
					<img src="{{asset('img/logo/logosimple_cf_ushop.svg')}}" width="128" height="64">
				</div>
				<div class="modal-body">
					<p id="messageadvertencia" class="text-center delsay-fonts">Espere un momento.</p>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-xs-12 col-md-12 flex-center">
							<i class="fa fa-spinner fa-spin fa-2x"></i>
						</div>
					</div>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<div class="modal fade" id="modal_confirmar" role="dialog" data-backdrop="static" data-keyboard="false">
		<div class="modal-dialog">
			<div class="modal-content item-modal-border">
				<div class="modal-header item-modal-header">
					<img src="{{asset('img/logo/logosimple_cf_ushop.svg')}}" width="128" height="64">
				</div>
				<div class="modal-body">
					<p id="messageadvertencia" class="text-center delsay-fonts"><strong>¿Está usted seguro de remover su archivo media?</strong></p>
				</div>
				<div class="modal-footer item-modal-footer">
					<button id="btnConfirmarRemoverMedia" type="button" class="btn btn-primary btn-raised btn-block btn-flat btn-ushop">
						<i class="fa fa-trash" aria-hidden="true"></i> REMOVER
						<div class="ripple-container"></div>
					</button>
					&nbsp;
					<button type="button" class="btn btn-primary btn-raised btn-block btn-flat btn-ushop" data-dismiss="modal">
						<i class="fa fa-times" aria-hidden="true"></i> CERRAR
						<div class="ripple-container"></div>
					</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<!-- /.box -->
@endsection
@section('mi-scripts')
	<script src="{{asset('js/controlador/callajax/principal.js')}}"></script>
	<script src="{{asset('js/wizard.js')}}"></script>
	<script src="{{asset('js/controlador/principal.js')}}"></script>
@endsection