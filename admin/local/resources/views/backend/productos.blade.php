@extends("layout.dashboard")
@section("section-title")
	Productos
	<small>Registre sus productos</small>
@endsection
@section("section-breadcrumb")
	<li><a href="{{url('ushop/dashboard')}}"><i class="fa fa-dashboard"></i> Inicio</a></li>
	<li class="active">Productos</li>
@endsection
@section("mi-css")
	<link rel="stylesheet" href="{{asset('css/wizardv3.css')}}">
	<link rel="stylesheet" href="{{asset('css/productos.css')}}">
@endsection
@section('homeproductos')
	active
@endsection
@section("section-body")
	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title">GESTIONE SU BLOQUE #PRODUCTOS</h3>
			<div class="box-tools pull-right">
				<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Minimizar">
				<i class="fa fa-minus"></i></button>
				<button id="goinstrucciones" type="button" class="btn btn-box-tool" title="Instrucciones" data-modo="{{base64_encode('hp')}}">
				<i class="fa fa-question"></i></button>
			</div>
		</div>
		<div class="box-header with-border">
			<small class="title-ushop-instrucciones"><i>Haga click en <i class="fa fa-question"></i> para visualizar sus instrucciones</i></small>
		</div>
		<div class="box-body">
			<section>
				<div class="wizard">
					<div class="wizard-inner">
						<div class="connecting-line"></div>
						<ul class="nav nav-tabs" role="tablist">
							<li role="presentation" class="active section mostrar">
								<a href="#step1" data-toggle="tab" aria-controls="step1" role="tab" title="Productos">
									<span class="round-tab">
										<i class="fa fa-sort-numeric-asc"></i>
									</span>
								</a>
							</li>
						</ul>
					</div>
					<form>
						<div class="tab-content">
							<div class="tab-pane active" role="tabpanel" id="step1">
								<div class="row">
									<div class="col-xs-12 col-sm-12 col-md-6 is-empty">
										<div id="bloquenombre" class="form-group has-feedback">
											<label class="label-ushop url">Nombre.</label>
											<input class="form-control" type="text" id="productoname" name="productoname" placeholder="INGRESE EL NOMBRE DE SU PRODUCTO">
										</div>
									</div>
									<div class="col-xs-12 col-sm-12 col-md-6 is-empty">
										<div id="bloquetipo" class="form-group has-feedback">
											<label class="label-ushop url">Tipo.</label>
											<input class="form-control" type="text" id="tipoproducto" name="tipoproducto" placeholder="INGRESE EL TIPO DE SU PRODUCTO">
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-xs-12 col-sm-12 col-md-12 is-empty">
										<div id="bloqueestiloone" class="form-group has-feedback">
											<label class="label-ushop url">Estilo #1.</label>
											<input class="form-control" type="text" id="estilo_one" name="estilo_one" placeholder="INGRESE SU PRIMER ESTILO">
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-xs-12 col-sm-12 col-md-12 is-empty">
										<div id="bloqueestilotwo" class="form-group has-feedback">
											<label class="label-ushop url">Estilo #2.</label>
											<input class="form-control" type="text" id="estilo_two" name="estilo_two" placeholder="INGRESE SU SEGUNDO ESTILO">
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-xs-12 col-sm-12 col-md-12 is-empty">
										<div id="bloqueestilotthrid" class="form-group has-feedback">
											<label class="label-ushop url">Estilo #3.</label>
											<input class="form-control" type="text" id="estilo_thrid" name="estilo_thrid" placeholder="INGRESE SU TERCER ESTILO">
										</div>
									</div>
								</div>
								<hr/>
								<div class="row">
									<div class="col-xs-12 col-sm-12 col-md-12 is-empty">
										<h3 class="box-title">GESTIONE SU DESCRIPCIÓN.</h3>
									</div>
								</div>
								<div class="row">
									<div class="col-xs-12 col-sm-12 col-md-12 is-empty">
										<div id="bloquedescripcion" class="form-group has-feedback">
											<label class="label-ushop url">Descripción.</label>
											<textarea class="form-control" id="descripcionproducto" name="descripcionproducto" row="5" cols="5" placeholder="INGRESE SU DESCRIPCIÓN"></textarea>
										</div>
									</div>
								</div>
								<div id="rowpicture" class="row">
									<div class="col-xs-12 col-md-12 is-empty">
										<div class="form-group">
											<label for="logodown">Ingrese su imagen</label>
											<a id="icologodown" href="#logo" data-toggle="collapse" data-parent="#accordion"><i class="fa fa-chevron-down" aria-hidden="true"></i></a></a>
											<div id="logo" class="panel-collapse in collapse" aria-expanded="false">
												<div class="box-body">
													<div class="item-grid-picture">
														<a id="logo-a" class="logo-link logo-productos" href="{{url('/').'/img/logo/logosidebar.png'}}" data-lightbox="logo" class="spacing-picture">
															<img id="logo-img" class="logo" src="{{url('/').'/img/logo/logosimple_cf_ushop.svg'}}" class="img-responsive img-thumbnail"/>
														</a>
														<div class="row item-grid-picture-file">
															<div class="col-xs-12 col-md-12 is-empty">
																<div class="form-group item-grid-picture-file">
																	<label for="imagenpicture" class="clearfix control-label label-ushop">SUBA LA IMAGEN DE SU AGRADO</label>
																	<input readonly="" id="namefile" class="form-control" placeholder="Suba su imagen" type="text">
																	<input type="file" id="imagenpicture" name="imagenpicture" accept="image/x-png,image/jpeg,image/jpg" placeholder="Suba la imagen de su agrado." onchange="javascript:openFile(event)"  fileread="banner.imagen"/>
																	<p class="help-block">Solo es permitido imágenes jpg/png.</p>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<hr/>
								<div class="row">
									<div class="col-xs-12 col-md-12">
										@if(count($productos) == 1 || count($productos) == 15)
											<button type="button" id="btnGuardarCompleto" class="btn btn-primary btn-raised btn-block btn-flat btn-ushop" disabled>
										@else
											<button type="button" id="btnGuardarCompleto" class="btn btn-primary btn-raised btn-block btn-flat btn-ushop">
										@endif
											<i class="fa fa-save" aria-hidden="true"></i> GUARDAR TODO
											<div class="ripple-container"></div>
										</button>
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
			</section>
		</div>
	</div>
	<!-- /.box -->
	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title">ELEMENTOS REGISTRADOS</h3>
			<div class="box-tools pull-right">
				<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Minimizar">
				<i class="fa fa-minus"></i></button>
			</div>
		</div>
		<div class="box-body">
			<div class="row">
				<div class="col-xs-12">
					<table id="list_productos" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>Nombre</th>
								<th>Preview/Imagen</th>
								<th>Descripción</th>
								<th>Estilo #1</th>
								<th>Estilo #2</th>
								<th>Estilo #3</th>
								<th>Opciones</th>
							</tr>
						</thead>
						<tbody>
							@foreach($productos as $p)
								<tr data-codigoproducto="{{$p->productos_codigo}}">
									<td>{{$p->productos_name}}</td>
									<td>
										<div class="row">
											<div class="col-xs-12 flex-center">
												<a id="logo-a" class="logo-link logo-productos" href="{{url('/').'/'.$p->productos_ruta}}" data-lightbox="logo" class="spacing-picture">
													<img id="logo-img" class="logo" src="{{url('/').'/'.$p->productos_ruta}}" class="img-responsive img-thumbnail"/>
												</a>
											</div>
										</div>
									</td>
									<td>{{substr($p->productos_descripcion,0,50)."..."}}</td>
									@if($p->productos_estilo_one!=NULL)
										<td>{{$p->productos_estilo_one}}</td>
									@else
										<td>
											<div clasS="row">
												<div class="col-xs-12 flex-center">
													<i class="fa fa-ban fa-2x" aria-hidden="true"></i>
												</div>
											</div>
										</td>
									@endif
									@if($p->productos_estilo_two!=NULL)
										<td>{{$p->productos_estilo_two}}</td>
									@else
										<td>
											<div clasS="row">
												<div class="col-xs-12 flex-center">
													<i class="fa fa-ban fa-2x" aria-hidden="true"></i>
												</div>
											</div>
										</td>
									@endif
									@if($p->productos_estilo_thrid!=NULL)
										<td>{{$p->productos_estilo_thrid}}</td>
									@else
										<td>
											<div clasS="row">
												<div class="col-xs-12 flex-center">
													<i class="fa fa-ban fa-2x" aria-hidden="true"></i>
												</div>
											</div>
										</td>
									@endif
									<td>
										<div class="row">
											<div class="col-xs-12 col-md-12">
												<button type="button" id="btnModificarMedia" class="btn btn-primary btn-raised btn-block btn-flat btn-ushop">
													<i class="fa fa-edit" aria-hidden="true"></i> MODIFICAR
													<div class="ripple-container"></div>
												</button>
											</div>
											<div class="col-xs-12 col-md-12">
												@if(count($productos) == 1)
													<button type="button" id="btnRemoverMedia" class="btn btn-primary btn-raised btn-block btn-flat btn-ushop" disabled>
												@else
													<button type="button" id="btnRemoverMedia" class="btn btn-primary btn-raised btn-block btn-flat btn-ushop">
												@endif
													<i class="fa fa-times" aria-hidden="true"></i> REMOVER
													<div class="ripple-container"></div>
												</button>
											</div>
										</div>
									</td>
								</tr>
							@endforeach
						</tbody>
						<tfoot>
							<tr>
								<th>Nombre</th>
								<th>Preview/Imagen</th>
								<th>Descripción</th>
								<th>Estilo #1</th>
								<th>Estilo #2</th>
								<th>Estilo #3</th>
								<th>Opciones</th>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
			<hr/>
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-6 is-empty">
					<a href="{{url('ushop/imagen/publicidad/'.base64_encode('h'))}}" class="btn btn-primary btn-raised btn-block btn-flat btn-ushop-link">
						<i class="fa fa-shopping-bag" aria-hidden="true"></i> PUBLICIDAD
						<div class="ripple-container"></div>
					</a>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 is-empty">
					<a href="{{url('ushop/dashboard')}}" class="btn btn-primary btn-raised btn-block btn-flat btn-ushop-link">
						<i class="fa fa-dashboard" aria-hidden="true"></i> DASHBOARD
						<div class="ripple-container"></div>
					</a>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="modal_wait" role="dialog" data-backdrop="static" data-keyboard="false">
		<div class="modal-dialog">
			<div class="modal-content item-modal-border">
				<div class="modal-header item-modal-header">
					<img src="{{asset('img/logo/logosimple_cf_ushop.svg')}}" width="128" height="64">
				</div>
				<div class="modal-body">
					<p id="wait_messageadvertencia" class="text-center delsay-fonts">Espere un momento.</p>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-xs-12 col-md-12 flex-center">
							<i class="fa fa-spinner fa-spin fa-2x"></i>
						</div>
					</div>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<div class="modal fade" id="modal_alerta" role="dialog" data-backdrop="static" data-keyboard="false">
		<div class="modal-dialog">
			<div class="modal-content item-modal-border">
				<div class="modal-header item-modal-header">
					<img src="{{asset('img/logo/logosimple_cf_ushop.svg')}}" width="128">
				</div>
				<div class="modal-body">
					<p id="messageadvertencia" class="text-center delsay-fonts"></p>
				</div>
				<div class="modal-footer item-modal-footer">
					<button type="button" class="btn btn-primary btn-raised btn-block btn-flat btn-ushop" data-dismiss="modal">
						<i class="fa fa-times" aria-hidden="true"></i> CERRAR
						<div class="ripple-container"></div>
					</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<div class="modal fade" id="modal_confirmar" role="dialog" data-backdrop="static" data-keyboard="false">
		<div class="modal-dialog">
			<div class="modal-content item-modal-border">
				<div class="modal-header item-modal-header">
					<img src="{{asset('img/logo/logosimple_cf_ushop.svg')}}" width="128" height="64">
				</div>
				<div class="modal-body">
					<p id="messageadvertencia" class="text-center delsay-fonts"><strong>¿Está usted seguro de remover su producto?</strong></p>
				</div>
				<div class="modal-footer item-modal-footer">
					<button id="btnConfirmarRemoverProductos" type="button" class="btn btn-primary btn-raised btn-block btn-flat btn-ushop">
						<i class="fa fa-trash" aria-hidden="true"></i> REMOVER
						<div class="ripple-container"></div>
					</button>
					&nbsp;
					<button type="button" class="btn btn-primary btn-raised btn-block btn-flat btn-ushop" data-dismiss="modal">
						<i class="fa fa-times" aria-hidden="true"></i> CERRAR
						<div class="ripple-container"></div>
					</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<!-- /.box -->
@endsection
@section('mi-scripts')
	<script src="{{asset('js/controlador/callajax/productos.js')}}"></script>
	<script src="{{asset('js/wizard.js')}}"></script>
	<script src="{{asset('js/controlador/productos.js')}}"></script>
@endsection