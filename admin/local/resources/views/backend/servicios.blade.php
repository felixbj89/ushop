@extends("layout.dashboard")
@section("section-title")
	Servicios
	<small>Administre sus servicios</small>
@endsection
@section("section-breadcrumb")
	<li><a href="{{url('ushop/dashboard')}}"><i class="fa fa-dashboard"></i> Inicio</a></li>
	<li class="active">Servicios</li>
@endsection
@section("mi-css")
	<link rel="stylesheet" href="{{asset('css/wizardv3.css')}}">
	<link rel="stylesheet" href="{{asset('css/productos.css')}}">
@endsection
@section('serviciosempresa')
	active
@endsection
@section("section-body")
	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title">GESTIONE SU BLOQUE #SERVICIOS</h3>
			<div class="box-tools pull-right">
				<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Minimizar">
				<i class="fa fa-minus"></i></button>
			</div>
		</div>
		<div class="box-body">
			<section>
				<div class="wizard">
					<div class="wizard-inner">
						<div class="connecting-line"></div>
						<ul class="nav nav-tabs" role="tablist">
							<li role="presentation" class="active section mostrar">
								<a href="#step1" data-toggle="tab" aria-controls="step1" role="tab" title="SERVICIOS">
									<span class="round-tab">
										<i class="fa fa-th"></i>
									</span>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</section>
		</div>
	</div>
	<hr/>
	<!-- /.box -->
	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title">SERVICIOS REGISTRADOS</h3>
			<div class="box-tools pull-right">
				<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Minimizar">
				<i class="fa fa-minus"></i></button>
			</div>
		</div>
		<div class="box-body">
			<div class="row">
				<div class="col-xs-12">
					<table id="list_servicios" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>Nombre</th>
								<th>Descripción</th>
								<th>Logo</th>
								<th>Estado</th>
								<th>Opciones</th>
							</tr>
						</thead>
						<tbody>
							@foreach($servicios as $s)
								<tr data-codigoservicios="{{$s->servicios_codigo}}" data-estado="{{base64_encode($s->servicios_estado)}}">
									<td>{{$s->servicios_name}}</td>
									<td>{!!substr($s->servicios_descripcion,0,100)."..."!!}</td>
									<td>
										<div class="row">
											<div class="col-xs-12 flex-center">
												<img id="logo-img" class="logo-table" src="{{url('/').'/'.$s->servicios_logo}}" class="img-responsive img-thumbnail" width="256" height="64">
											</div>
										</div>
									</td>
									<td>
										<div class="row">
											@if($s->servicios_estado=="1")
												<div class="col-xs-12">
													<button type="button" id="btnActivado" class="btn btn-primary btn-raised btn-block btn-flat btn-ushop-link">
														<i class="fa fa-power-off" aria-hidden="true"></i> DESACTIVAR
														<div class="ripple-container"></div>
													</button>
												</div>
											@else
												<div class="col-xs-12">
													<button type="button" id="btnActivado" class="btn btn-primary btn-raised btn-block btn-flat btn-ushop">
														<i class="fa fa-power-off" aria-hidden="true"></i> ACTIVAR
														<div class="ripple-container"></div>
													</button>
												</div>
											@endif
										</div>
									</td>
									<td>
										<div class="row">
											<div class="col-xs-12">
												<button type="button" id="btnGestionarServicios" class="btn btn-primary btn-raised btn-block btn-flat btn-ushop">
													<i class="fa fa-cog" aria-hidden="true"></i> ADMINISTRAR
													<div class="ripple-container"></div>
												</button>
											</div>
										</div>
									</td>
								</tr>
							@endforeach
						</tbody>
						<tfoot>
							<tr>
								<th>Nombre</th>
								<th>Descripción</th>
								<th>Logo</th>
								<th>Estado</th>
								<th>Opciones</th>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
			<hr/>
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-6 is-empty">
					<a href="{{url('ushop/empresa/equipo')}}" class="btn btn-primary btn-raised btn-block btn-flat btn-ushop-link">
						<i class="fa fa-users" aria-hidden="true"></i> EQUIPO
						<div class="ripple-container"></div>
					</a>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 is-empty">
					<a href="{{url('ushop/dashboard')}}" class="btn btn-primary btn-raised btn-block btn-flat btn-ushop-link">
						<i class="fa fa-dashboard" aria-hidden="true"></i> DASHBOARD
						<div class="ripple-container"></div>
					</a>
				</div>
			</div>
		</div>
		<!-- /.box-body -->
	</div>
	<div class="modal fade" id="modal_alerta" role="dialog" data-backdrop="static" data-keyboard="false">
		<div class="modal-dialog">
			<div class="modal-content item-modal-border">
				<div class="modal-header item-modal-header">
					<img src="{{asset('img/logo/logosimple_cf_ushop.svg')}}" width="128">
				</div>
				<div class="modal-body">
					<p id="messageadvertencia" class="text-center delsay-fonts"></p>
				</div>
				<div class="modal-footer item-modal-footer">
					<button type="button" class="btn btn-primary btn-raised btn-block btn-flat btn-ushop" data-dismiss="modal">
						<i class="fa fa-times" aria-hidden="true"></i> CERRAR
						<div class="ripple-container"></div>
					</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<div class="modal fade" id="modal_activar" role="dialog" data-backdrop="static" data-keyboard="false">
		<div class="modal-dialog">
			<div class="modal-content item-modal-border">
				<div class="modal-header item-modal-header">
					<img src="{{asset('img/logo/logosimple_cf_ushop.svg')}}" width="128" height="64">
				</div>
				<div class="modal-body">
					<p id="confirmardadvertencia" class="text-center delsay-fonts"><strong></strong></p>
				</div>
				<div class="modal-footer item-modal-footer">
					<button id="btnConfirmarRemoverMedia" type="button" class="btn btn-primary btn-raised btn-block btn-flat btn-ushop">
						<i class="fa fa-power-off" aria-hidden="true"></i>
						<div class="ripple-container"></div>
					</button>
					&nbsp;
					<button type="button" class="btn btn-primary btn-raised btn-block btn-flat btn-ushop" data-dismiss="modal">
						<i class="fa fa-times" aria-hidden="true"></i> CERRAR
						<div class="ripple-container"></div>
					</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<!-- /.box -->
@endsection
@section('mi-scripts')
	<script src="{{asset('js/controlador/callajax/tecnicas.js')}}"></script>
	<script src="{{asset('js/wizard.js')}}"></script>
	<script src="{{asset('js/controlador/servicios.js')}}"></script>
@endsection