@extends("layout.dashboard")
@section("section-title")
	Solicitudes
	<small>Solicitudes realizadas desde el sitio web</small>
@endsection
@section("section-breadcrumb")
	<li><a href="{{url('ushop/dashboard')}}"><i class="fa fa-dashboard"></i> Inicio</a></li>
	<li class="active">Solicitudes</li>
@endsection
@section("mi-css")
	<link rel="stylesheet" href="{{asset('css/wizardv3.css')}}">
	<link rel="stylesheet" href="{{asset('css/productos.css')}}">
@endsection
@section('dashboard')
	active
@endsection
@section("dashboard-notificaciones")
	<li class="dropdown messages-menu">
		<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
			<i class="fa fa-bell-o"></i>
			@if(count($contactos) > 0)
				<span class="label label-danger">{{count($contactos)}}</span>
			@else
				<span class="label label-danger">0</span>
			@endif
			<div class="ripple-container"></div>
		</a>
		<ul class="dropdown-menu">
			<li class="header">Solicitudes</li>
			<li>
				<!-- inner menu: contains the actual data -->
				<ul class="menu">
					@foreach($contactos as $c)
						<li><!-- start message -->
							<a href="{{url('ushop/dashboard/solicitudes')}}">
								<div class="pull-left">
									<i class="fa fa-globe fa-2x fa-color-ushop" style="color:#a40000"></i>
								</div>
								<h4>
									{{$c->contacto_nombre}}
								</h4>
								<p>{{$c->contacto_correo}}</p>
							</a>
						</li>
					@endforeach
					<!-- end message -->
				</ul>
			</li>
			<li class="footer"><a href="{{url('ushop/dashboard/solicitudes')}}">Listar</a></li>
		</ul>
	</li>
@endsection
@section("section-body")
	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title">SOLICITUDES REALIZADAS DESDE LA WEB</h3>
			<div class="box-tools pull-right">
				<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Minimizar">
				<i class="fa fa-minus"></i></button>
			</div>
		</div>
		<div class="box-body">
			<section>
				<div class="wizard">
					<div class="wizard-inner">
						<div class="connecting-line"></div>
						<ul class="nav nav-tabs" role="tablist">
							<li role="presentation" class="active section mostrar">
								<a href="#step1" data-toggle="tab" aria-controls="step1" role="tab" title="Catálogo">
									<span class="round-tab">
										<i class="fa fa-globe"></i>
									</span>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</section>
		</div>
	</div>
	<hr/>
	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title">SOLICITUDES REGISTRADAS</h3>
			<div class="box-tools pull-right">
				<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Minimizar">
				<i class="fa fa-minus"></i></button>
			</div>
		</div>
		<div class="box-body">
			<div class="row">
				<div class="col-xs-12">
					<table id="list_contactos" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>Nombre</th>
								<th>Correo</th>
								<th>Requerimiento</th>
								<th>Opciones</th>
							</tr>
						</thead>
						<tbody>
							@foreach($contactos as $c)
								<tr data-codigocontacto="{{e($c->contacto_codigo)}}">
									<td>{{$c->contacto_nombre}}</td>
									<td>{{$c->contacto_correo}}</td>
									<td>{{$c->contacto_requerimiento}}</td>
									<td>
										<div class="row">
											<div class="col-xs-12">
												<button type="button" id="btnRemoverSolicitud" class="btn btn-primary btn-raised btn-block btn-flat btn-ushop">
													<i class="fa fa-trash" aria-hidden="true"></i> REMOVER
													<div class="ripple-container"></div>
												</button>
											</div>
										</div>
									</td>
								</tr>
							@endforeach
						</tbody>
						<tfoot>
							<tr>
								<th>Nombre</th>
								<th>Correo</th>
								<th>Requerimiento</th>
								<th>Opciones</th>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
			<hr/>
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 is-empty">
					<a href="{{url('ushop/dashboard')}}" class="btn btn-primary btn-raised btn-block btn-flat btn-ushop-link">
						<i class="fa fa-undo" aria-hidden="true"></i> REGRESAR
						<div class="ripple-container"></div>
					</a>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="modal_alerta" role="dialog" data-backdrop="static" data-keyboard="false">
		<div class="modal-dialog">
			<div class="modal-content item-modal-border">
				<div class="modal-header item-modal-header">
					<img src="{{asset('img/logo/logosimple_cf_ushop.svg')}}" width="128" height="64">
				</div>
				<div class="modal-body">
					<p id="messageadvertencia" class="text-center delsay-fonts"></p>
				</div>
				<div class="modal-footer item-modal-footer">
					<button type="button" class="btn btn-primary btn-raised btn-block btn-flat btn-ushop" data-dismiss="modal">
						<i class="fa fa-times" aria-hidden="true"></i> CERRAR
						<div class="ripple-container"></div>
					</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<div class="modal fade" id="modal_wait" role="dialog" data-backdrop="static" data-keyboard="false">
		<div class="modal-dialog">
			<div class="modal-content item-modal-border">
				<div class="modal-header item-modal-header">
					<img src="{{asset('img/logo/logosimple_cf_ushop.svg')}}" width="128" height="64">
				</div>
				<div class="modal-body">
					<p id="messageadvertencia" class="text-center delsay-fonts">Espere un momento.</p>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-xs-12 col-md-12 flex-center">
							<i class="fa fa-spinner fa-spin fa-2x"></i>
						</div>
					</div>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<div class="modal fade" id="modal_confirmar" role="dialog" data-backdrop="static" data-keyboard="false">
		<div class="modal-dialog">
			<div class="modal-content item-modal-border">
				<div class="modal-header item-modal-header">
					<img src="{{asset('img/logo/logosimple_cf_ushop.svg')}}" width="128" height="64">
				</div>
				<div class="modal-body">
					<p id="messageadvertencia" class="text-center delsay-fonts"><strong>¿Está usted seguro de remover su archivo media?</strong></p>
				</div>
				<div class="modal-footer item-modal-footer">
					<button id="btnConfirmarRemoverMedia" type="button" class="btn btn-primary btn-raised btn-block btn-flat btn-ushop">
						<i class="fa fa-trash" aria-hidden="true"></i> REMOVER
						<div class="ripple-container"></div>
					</button>
					&nbsp;
					<button type="button" class="btn btn-primary btn-raised btn-block btn-flat btn-ushop" data-dismiss="modal">
						<i class="fa fa-times" aria-hidden="true"></i> CERRAR
						<div class="ripple-container"></div>
					</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
@endsection
@section('mi-scripts')
	<script src="{{asset('js/controlador/callajax/solicitudes.js')}}"></script>
	<script src="{{asset('js/wizard.js')}}"></script>
	<script src="{{asset('js/controlador/solicitudes.js')}}"></script>
@endsection