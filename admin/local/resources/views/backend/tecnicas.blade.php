@extends("layout.dashboard")
@section("section-title")
	Técnicas
	<small>Administre sus técnicas</small>
@endsection
@section("section-breadcrumb")
	<li><a href="{{url('ushop/dashboard')}}"><i class="fa fa-dashboard"></i> Inicio</a></li>
	<li class="active">Técnicas</li>
@endsection
@section("mi-css")
	<link rel="stylesheet" href="{{asset('css/wizardv3.css')}}">
	<link rel="stylesheet" href="{{asset('css/productos.css')}}">
@endsection
@section('menutecnicas')
	active
@endsection
@section('tecnicascrear')
	active
@endsection
@section("section-body")
	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title">GESTIONE SU BLOQUE #TÉCNICAS</h3>
			<div class="box-tools pull-right">
				<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Minimizar">
				<i class="fa fa-minus"></i></button>
			</div>
		</div>
		<div class="box-body">
			<section>
				<div class="wizard">
					<div class="wizard-inner">
						<div class="connecting-line"></div>
						<ul class="nav nav-tabs" role="tablist">
							<li role="presentation" class="active section mostrar">
								<a href="#step1" data-toggle="tab" aria-controls="step1" role="tab" title="Técnicas">
									<span class="round-tab">
										<i class="fa fa-pencil"></i>
									</span>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</section>
		</div>
	</div>
	<hr/>
	<!-- /.box -->
	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title">TÉCNICAS REGISTRADAS</h3>
			<div class="box-tools pull-right">
				<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Minimizar">
				<i class="fa fa-minus"></i></button>
			</div>
		</div>
		<div class="box-body">
			<div class="row">
				<div class="col-xs-12">
					<table id="list_tecnicas" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>Nombre</th>
								<th>Descripción</th>
								<th>Imagen</th>
								<th>Opciones</th>
							</tr>
						</thead>
						<tbody>
							@foreach($tecnicas as $t)
								<tr data-codigotecnica="{{e($t->tecnicas_codigo)}}">
									<td>{{e($t->tecnicas_name)}}</td>
									<td>{!!$t->tecnicas_descripcion!!}</td>
									<td>
										<div class="row">
											<div class="col-xs-12">
												<a id="logo-a" class="logo-link flex-center" href="{{url('/').'/'.$t->tecnicas_ruta}}" data-lightbox="logo" class="spacing-picture">
													<img id="logo-img" class="logo logo-table" src="{{url('/').'/'.$t->tecnicas_ruta}}" class="img-responsive img-thumbnail" width="256" height="64">
												</a>
											</div>
										</div>
									</td>
									<td>
										<div class="row">
											<div class="col-xs-12">
												<button type="button" id="btnGestionarTecnica" class="btn btn-primary btn-raised btn-block btn-flat btn-ushop">
													<i class="fa fa-cog" aria-hidden="true"></i> ADMINISTRAR
													<div class="ripple-container"></div>
												</button>
											</div>
										</div>
									</td>
								</tr>
							@endforeach
						</tbody>
						<tfoot>
							<tr>
								<th>Nombre</th>
								<th>Descripción</th>
								<th>Imagen</th>
								<th>Opciones</th>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
			<hr/>
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-6 is-empty">
					<a href="{{url('ushop/imagen/principal/'.base64_encode('pi'))}}" class="btn btn-primary btn-raised btn-block btn-flat btn-ushop-link">
						<i class="fa fa-th" aria-hidden="true"></i> IMPORTAMOS
						<div class="ripple-container"></div>
					</a>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 is-empty">
					<a href="{{url('ushop/dashboard')}}" class="btn btn-primary btn-raised btn-block btn-flat btn-ushop-link">
						<i class="fa fa-dashboard" aria-hidden="true"></i> DASHBOARD
						<div class="ripple-container"></div>
					</a>
				</div>
			</div>
		</div>
		<!-- /.box-body -->
	</div>
@endsection
@section('mi-scripts')
	<script src="{{asset('js/controlador/callajax/tecnicas.js')}}"></script>
	<script src="{{asset('js/wizard.js')}}"></script>
	<script src="{{asset('js/controlador/tecnicas.js')}}"></script>
@endsection