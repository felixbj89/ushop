@extends("layout.dashboard")
@section("section-title")
	Servicios
	<small>Administrando #{{$servicios->servicios_name}}</small>
@endsection
@section("section-breadcrumb")
	<li><a href="{{url('ushop/dashboard')}}"><i class="fa fa-dashboard"></i> Inicio</a></li>
	<li class="active">Servicios</li>
@endsection
@section("mi-css")
	<link rel="stylesheet" href="{{asset('css/wizardv2.css')}}">
	<link rel="stylesheet" href="{{asset('css/productos.css')}}">
@endsection
@section('serviciosempresa')
	active
@endsection
@section("section-body")
	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title">GESTIONANDO #{{$servicios->servicios_name}}</h3>
			<div class="box-tools pull-right">
				<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Minimizar">
				<i class="fa fa-minus"></i></button>
			</div>
		</div>
		<div class="box-body">
			<section>
				<div class="wizard">
					<div class="wizard-inner">
						<div class="connecting-line"></div>
						<ul class="nav nav-tabs" role="tablist">
							<li role="presentation" class="active section mostrar">
								<a href="#step1" data-toggle="tab" aria-controls="step1" role="tab" title="Servicio">
									<span class="round-tab">
										<i class="fa fa-th"></i>
									</span>
								</a>
							</li>
							<li role="presentation" class="disabled imagenes ocultar">
								<a href="#step2" data-toggle="tab" aria-controls="step2" role="tab" title="Imágenes">
									<span class="round-tab">
										<i class="fa fa-picture-o"></i>
									</span>
								</a>
							</li>
						</ul>
					</div>
					<form>
						<input type="hidden" id="codigoservicios" name="codigoservicios" value="{{e($servicios->servicios_codigo)}}"/>
						<div class="tab-content">
							<div class="tab-pane" role="tabpanel" id="step2">
								<div id="rowpicture" class="row">
									<div class="col-xs-12 col-md-12 is-empty">
										<ul class="list-inline pull-right">
											<li><button type="button" class="btn btn-default prev-step goseccion">ATRAS</button></li>
										</ul>
									</div>
								</div>
								<div id="rowpicture" class="row">
									<div class="col-xs-12 col-md-4 is-empty">
										<div class="form-group">
											<label for="logodown">Ingrese su imagen</label>
											<a id="icologodown" href="#pictureone" data-toggle="collapse" data-parent="#accordion"><i class="fa fa-chevron-down" aria-hidden="true"></i></a></a>
											<div id="pictureone" class="panel-collapse in collapse" aria-expanded="false">
												<div class="box-body">
													<div class="item-grid-picture">
														<a id="logo-a_one" class="logo-link" href="{{url('/').'/'.$servicios->servicios_one}}" data-lightbox="logo" class="spacing-picture">
															<img id="logo-img_one" class="logo logo-alt" src="{{url('/').'/'.$servicios->servicios_one}}" class="img-responsive img-thumbnail"/>
														</a>
														<div class="row item-grid-picture-file">
															<div class="col-xs-12 col-md-12 is-empty">
																<div class="form-group item-grid-picture-file">
																	<label for="imagenpicture_one" class="clearfix control-label label-ushop">SUBA LA IMAGEN DE SU AGRADO</label>
																	<input readonly="" id="namefile" class="form-control" placeholder="Suba su imagen" type="text">
																	<input type="file" id="imagenpicture_one" name="imagenpicture_one" accept="image/x-png,image/jpeg,image/jpg,image/svg+xml" placeholder="Suba la imagen de su agrado." onchange="javascript:openFile1(event)"  fileread="banner.imagen"/>
																	<p class="help-block">Solo es permitido imágenes jpg/png/svg.</p>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="col-xs-12 col-md-4 is-empty">
										<div class="form-group">
											<label for="logodown">Ingrese su imagen</label>
											<a id="icologodown" href="#pictureone" data-toggle="collapse" data-parent="#accordion"><i class="fa fa-chevron-down" aria-hidden="true"></i></a></a>
											<div id="pictureone" class="panel-collapse in collapse" aria-expanded="false">
												<div class="box-body">
													<div class="item-grid-picture">
														<a id="logo-a_two" class="logo-link" href="{{url('/').'/'.$servicios->servicios_two}}" data-lightbox="logo" class="spacing-picture">
															<img id="logo-img_two" class="logo logo-alt" src="{{url('/').'/'.$servicios->servicios_two}}" class="img-responsive img-thumbnail"/>
														</a>
														<div class="row item-grid-picture-file">
															<div class="col-xs-12 col-md-12 is-empty">
																<div class="form-group item-grid-picture-file">
																	<label for="imagenpicture_two" class="clearfix control-label label-ushop">SUBA LA IMAGEN DE SU AGRADO</label>
																	<input readonly="" id="namefile" class="form-control" placeholder="Suba su imagen" type="text">
																	<input type="file" id="imagenpicture_two" name="imagenpicture_two" accept="image/x-png,image/jpeg,image/jpg" placeholder="Suba la imagen de su agrado." onchange="javascript:openFile2(event)"  fileread="banner.imagen"/>
																	<p class="help-block">Solo es permitido imágenes jpg/png.</p>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="col-xs-12 col-md-4 is-empty">
										<div class="form-group">
											<label for="logodown">Ingrese su imagen</label>
											<a id="icologodown" href="#pictureone" data-toggle="collapse" data-parent="#accordion"><i class="fa fa-chevron-down" aria-hidden="true"></i></a></a>
											<div id="pictureone" class="panel-collapse in collapse" aria-expanded="false">
												<div class="box-body">
													<div class="item-grid-picture">
														<a id="logo-a_thrid" class="logo-link" href="{{url('/').'/'.$servicios->servicios_thrid}}" data-lightbox="logo" class="spacing-picture">
															<img id="logo-img_thrid" class="logo logo-alt" src="{{url('/').'/'.$servicios->servicios_thrid}}" class="img-responsive img-thumbnail"/>
														</a>
														<div class="row item-grid-picture-file">
															<div class="col-xs-12 col-md-12 is-empty">
																<div class="form-group item-grid-picture-file">
																	<label for="imagenpicture_thrid" class="clearfix control-label label-ushop">SUBA LA IMAGEN DE SU AGRADO</label>
																	<input readonly="" id="namefile" class="form-control" placeholder="Suba su imagen" type="text">
																	<input type="file" id="imagenpicture_thrid" name="imagenpicture_thrid" accept="image/x-png,image/jpeg,image/jpg" placeholder="Suba la imagen de su agrado." onchange="javascript:openFile3(event)"  fileread="banner.imagen"/>
																	<p class="help-block">Solo es permitido imágenes jpg/png.</p>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div id="rowpicture" class="row">
									<div class="col-xs-12 col-md-12 is-empty">
										<ul class="list-inline pull-right">
											<li><button type="button" class="btn btn-default prev-step goseccion">ATRAS</button></li>
										</ul>
									</div>
								</div>
								<div class="row">
									<div class="col-xs-12 col-md-6">
										<button type="button" id="btnGuardarPrincipal" class="btn btn-primary btn-raised btn-block btn-flat btn-ushop">
											<i class="fa fa-save" aria-hidden="true"></i> GUARDAR
											<div class="ripple-container"></div>
										</button>
									</div>
									<div class="col-xs-12 col-md-6">
										<a id="administrarGaleria" href="{{url('ushop/empresa/servicios/galeia/'.$servicios->servicios_codigo)}}" class="btn btn-primary btn-raised btn-block btn-flat btn-ushop-link">
											<i class="fa fa-cog" aria-hidden="true"></i> ADMINISTRAR/GALERÍA
											<div class="ripple-container"></div>
										</a>
									</div>
								</div>
							</div>
							<div class="tab-pane active" role="tabpanel" id="step1">
								<div class="row">
									<div class="col-xs-12 col-sm-12 col-md-12 is-empty">
										<div id="bloquenombre" class="form-group has-feedback">
											<label class="label-ushop url">Servicios.</label>
											<input class="form-control" type="text" id="servicios" name="servicios" placeholder="{{$servicios->servicios_name}}" value="{{$servicios->servicios_name}}" readonly>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-xs-12 col-sm-12 col-md-12 is-empty">
										<h3 class="box-title">GESTIONE SU DESCRIPCIÓN.</h3>
									</div>
								</div>
								<div class="box-body">
									<div class="row">
										<div class="col-xs-12 col-md-12 is-empty">
											<div id="bloquetexto" class="form-group has-feedback">
												<label for="descripcionnotificacion">DESCRIPCIÓN</label>
												<div id="descripcion">{!!$servicios->servicios_descripcion!!}</div>
											</div>
											<span class="help-block-contacto"></span>
										</div>
									</div>
								</div>
								<div id="rowpicture" class="row">
									<div class="col-xs-12 col-md-12 is-empty">
										<div class="form-group">
											<label for="logodown">Ingrese su imagen</label>
											<a id="icologodown" href="#logo" data-toggle="collapse" data-parent="#accordion"><i class="fa fa-chevron-down" aria-hidden="true"></i></a></a>
											<div id="logo" class="panel-collapse in collapse" aria-expanded="false">
												<div class="box-body">
													<div class="item-grid-picture">
														<a id="logo-a" class="logo-link" href="{{url('/').'/'.$servicios->servicios_logo}}" data-lightbox="logo" class="spacing-picture">
															<img id="logo-img" class="logo" src="{{url('/').'/'.$servicios->servicios_logo}}" class="img-responsive img-thumbnail"/>
														</a>
														<div class="row item-grid-picture-file">
															<div class="col-xs-12 col-md-12 is-empty">
																<div class="form-group item-grid-picture-file">
																	<label for="imagenpicture" class="clearfix control-label label-ushop">SUBA LA IMAGEN DE SU AGRADO</label>
																	<input readonly="" id="namefile" class="form-control" placeholder="Suba su imagen" type="text">
																	<input type="file" id="imagenpicture" name="imagenpicture" accept="image/x-png,image/jpeg,image/jpg" placeholder="Suba la imagen de su agrado." onchange="javascript:openFile(event)"  fileread="banner.imagen"/>
																	<p class="help-block">Solo es permitido imágenes jpg/png.</p>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-xs-12 col-sm-12 col-md-12 is-empty">
										<ul class="list-inline pull-right">
											<li><button type="button" class="btn btn-primary next-step goimagenes">CONTINUAR</button></li>
										</ul>
									</div>
								</div>
								<div class="row">
									<div class="col-xs-12 col-md-12">
										<a href="{{url('ushop/empresa/servicios')}}" class="btn btn-primary btn-raised btn-block btn-flat btn-ushop-link">
											<i class="fa fa-undo" aria-hidden="true"></i> REGRESAR
											<div class="ripple-container"></div>
										</a>
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
			</section>
		</div>
	</div>
	<div class="modal fade" id="modal_alerta" role="dialog" data-backdrop="static" data-keyboard="false">
		<div class="modal-dialog">
			<div class="modal-content item-modal-border">
				<div class="modal-header item-modal-header">
					<img src="{{asset('img/logo/logosimple_cf_ushop.svg')}}" width="128">
				</div>
				<div class="modal-body">
					<p id="messageadvertencia" class="text-center delsay-fonts"></p>
				</div>
				<div class="modal-footer item-modal-footer">
					<button type="button" class="btn btn-primary btn-raised btn-block btn-flat btn-ushop" data-dismiss="modal">
						<i class="fa fa-times" aria-hidden="true"></i> CERRAR
						<div class="ripple-container"></div>
					</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<div class="modal fade" id="modal_wait" role="dialog" data-backdrop="static" data-keyboard="false">
		<div class="modal-dialog">
			<div class="modal-content item-modal-border">
				<div class="modal-header item-modal-header">
					<img src="{{asset('img/logo/logosimple_cf_ushop.svg')}}" width="128" height="64">
				</div>
				<div class="modal-body">
					<p id="messageadvertencia" class="text-center delsay-fonts">Espere un momento.</p>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-xs-12 col-md-12 flex-center">
							<i class="fa fa-spinner fa-spin fa-2x"></i>
						</div>
					</div>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
@endsection
@section('mi-scripts')
	<script src="{{asset('js/controlador/callajax/tecnicas.js')}}"></script>
	<script src="{{asset('js/wizard.js')}}"></script>
	<script src="{{asset('js/controlador/views/servicios.js')}}"></script>
@endsection