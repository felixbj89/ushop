@extends("layout.dashboard")
@section("section-title")
	Servicios
	<small>Administrando #{{$servicios->servicios_name}}</small>
    
@endsection
@section("section-breadcrumb")
	<li><a href="{{url('ushop/dashboard')}}"><i class="fa fa-dashboard"></i> Inicio</a></li>
	<li class="active">Servicios</li>
@endsection
@section("mi-css")
	<link rel="stylesheet" href="{{asset('css/galeria.css')}}">
@endsection
@section('serviciosempresa')
	active
@endsection
@section("section-body")
	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title">GESTIONANDO #{{$servicios->servicios_name}}</h3>
			<div class="box-tools pull-right">
				<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Minimizar">
				<i class="fa fa-minus"></i></button>
			</div>
		</div>
		<div class="box-body">

			<form id="formgaleriaemp">
				<div class="media">
					<div class="media-img media-left media-middle">
						<div id="managimg" class="text-center">
							<i class="fa fa-file-o fa-4x" aria-hidden="true"></i><br/>
							<label>Selecciona una imagen</label>
						</div>
						<input type="file" id="img" accept="image/*">
						<span class="spanvalidate isvalid"><i class="fa-2x fa fa-check"></i></span>
						<span class="spanvalidate novalid"><i class="fa-2x fa fa-exclamation-triangle"></i></span>
					</div>
					<div class="media-body">
						<div class="row">
							<div class="col-sm-12">
								<div class="form-group">
									<label class="word-break-normal">Titulo del proyecto</label>
									<input id="des" type="text" class="form-control">
									<span class="spanvalidate isvalid"><i class="fa-2x fa fa-check"></i></span>
									<span class="spanvalidate novalid"><i class="fa-2x fa fa-exclamation-triangle"></i></span>
								</div>
							</div>
							<div class="col-sm-12">
								<button id="addpregal" type="button" class="btn btn-ushop" disabled><i class="fa fa-plus" area-hidden="true"></i> Agregar</button>						
							</div>
						</div>
					</div>
				</div>
			</form>
	
        </div>
	</div>

	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title">GALERIA #{{$servicios->servicios_name}}</h3>
			<div class="box-tools pull-right">
				<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Minimizar">
				<i class="fa fa-minus"></i></button>
			</div>
		</div>
		<div class="box-body">
			<form id="formtablegal">
				<input type="hidden" name="servicio" value="{{$servicios->servicios_codigo}}">
				<div id="inputsfile">
					@foreach ($items as $item)
						<input accept="image/*" class="valid atras img" name="img_{{ $item->galeria_codigo }}" type="file">
					@endforeach
				</div>
				<div id="inputstext">
					@foreach ($items as $item)
						<input class="valid des" name="des_{{ $item->galeria_codigo }}" type="text" value="{{ $item->galeria_descripcion }}">
					@endforeach
				</div>
			</form>	
			<table id="example" class="display" style="width:100%">
				<thead>
					<tr>
						<th class="text-center">#{{$servicios->servicios_name}}</th>
					</tr>
				</thead>
				<tbody>
				@foreach ($items as $item)
					<tr>
						<td>
							<div class="media" data-count="{{ $item->galeria_codigo }}">
								<div class="media-left media-middle">
									<a class="lb" href="{{ $rutabase.$item->galeria_imagen }}" data-lightbox="example-set" data-title="">
										<img id="img_{{ $item->galeria_codigo }}" src="{{ $rutabase.$item->galeria_imagen }}" width="128px">
									</a>
								</div>
								<div class="media-body">
									<div class="row" style="width:100%;">
										<div class="col-xs-12">
											<div class="form-group w-100">
												<input type="text" class="des form-control w-100" value="{{ $item->galeria_descripcion }}" data-text="des_{{ $item->galeria_codigo }}" disabled>
											</div>
										</div>
										<div class="col-xs-12">
											<a class="btn btn-default btn-sm mt-0 btn-editpic" href="img_{{ $item->galeria_codigo }}">
											<i class="fa fa-picture-o"></i> Cambiar</a>
											<a class="btn btn-default btn-sm mt-0 btn-editgal" href="#">
											<i class="fa fa-pencil"></i> Editar</a>
											<a class="btn btn-default btn-sm  mt-0 btn-removgal" href="{{ $item->galeria_codigo }}">
											<i class="fa fa-trash-o"></i> Remover</a>
										</div>
									</div>
								</div>
							</div>
						</td>
					</tr>
				@endforeach
				</tbody>
			</table>
			<div class="row">
				<div class="col-xs-12 col-md-6">
					<button type="button" id="btnGuardarPrincipal" class="btn btn-primary btn-raised btn-block btn-flat btn-ushop">
						<i class="fa fa-save" aria-hidden="true"></i> GUARDAR
						<div class="ripple-container"></div>
					</button>
				</div>
				<div class="col-xs-12 col-md-6">
					<a href="{{url('ushop/empresa/servicios')}}" class="btn btn-primary btn-raised btn-block btn-flat btn-ushop-link">
						<i class="fa fa-undo" aria-hidden="true"></i> REGRESAR
						<div class="ripple-container"></div>
					</a>
				</div>
			</div>
        </div>
    </div>

	<div class="modal fade" id="modal_confirmar" role="dialog" data-backdrop="static" data-keyboard="false">
		<div class="modal-dialog">
			<div class="modal-content item-modal-border">
				<div class="modal-header item-modal-header">
					<img src="{{asset('img/logo/logosimple_cf_ushop.svg')}}" width="128">
				</div>
				<div class="modal-body">
					<p class="text-center delsay-fonts"><strong>¿Está usted seguro de remover el item de la galeria?</strong></p>
				</div>
				<div class="modal-footer item-modal-footer">
					<button id="btnConfirmarRemoverMedia" type="button" class="btn btn-primary btn-raised btn-block btn-flat btn-ushop">
						<i class="fa fa-trash" aria-hidden="true"></i> REMOVER
						<div class="ripple-container"></div>
					</button>
					&nbsp;
					<button type="button" class="btn btn-primary btn-raised btn-block btn-flat btn-ushop" data-dismiss="modal">
						<i class="fa fa-times" aria-hidden="true"></i> CERRAR
						<div class="ripple-container"></div>
					</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<div class="modal fade" id="modal_alerta" role="dialog" data-backdrop="static" data-keyboard="false">
		<div class="modal-dialog">
			<div class="modal-content item-modal-border">
				<div class="modal-header item-modal-header">
					<img src="{{asset('img/logo/logosimple_cf_ushop.svg')}}" width="128">
				</div>
				<div class="modal-body">
					<p id="messageadvertencia" class="text-center delsay-fonts"></p>
				</div>
				<div class="modal-footer item-modal-footer">
					<button type="button" class="btn btn-primary btn-raised btn-block btn-flat btn-ushop" data-dismiss="modal">
						<i class="fa fa-times" aria-hidden="true"></i> CERRAR
						<div class="ripple-container"></div>
					</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<div class="modal fade" id="modal_wait" role="dialog" data-backdrop="static" data-keyboard="false">
		<div class="modal-dialog">
			<div class="modal-content item-modal-border">
				<div class="modal-header item-modal-header">
					<img src="{{asset('img/logo/logosimple_cf_ushop.svg')}}" width="128" height="64">
				</div>
				<div class="modal-body">
					<p class="text-center delsay-fonts">Espere un momento.</p>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-xs-12 col-md-12 flex-center">
							<i class="fa fa-spinner fa-spin fa-2x"></i>
						</div>
					</div>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<!-- /.box -->	
@endsection
@section('mi-scripts')
	<script src="{{asset('js/controlador/callajax/galeria.js')}}"></script>
	<script src="{{asset('js/controlador/galeria.js')}}"></script>
@endsection