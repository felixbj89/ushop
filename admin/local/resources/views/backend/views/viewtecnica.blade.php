@extends("layout.dashboard")
@section("section-title")
	Técnicas
	<small>Administrando #{{$tecnica->tecnicas_name}}</small>
@endsection
@section("section-breadcrumb")
	<li><a href="{{url('ushop/dashboard')}}"><i class="fa fa-dashboard"></i> Inicio</a></li>
	<li class="active">Técnica</li>
@endsection
@section("mi-css")
	<link rel="stylesheet" href="{{asset('css/wizardv3.css')}}">
	<link rel="stylesheet" href="{{asset('css/productos.css')}}">
@endsection
@section('menutecnicas')
	active
@endsection
@section('tecnicascrear')
	active
@endsection
@section("section-body")
	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title">GESTIONANDO #{{$tecnica->tecnicas_name}}</h3>
			<div class="box-tools pull-right">
				<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Minimizar">
				<i class="fa fa-minus"></i></button>
			</div>
		</div>
		<div class="box-body">
			<section>
				<div class="wizard">
					<div class="wizard-inner">
						<div class="connecting-line"></div>
						<ul class="nav nav-tabs" role="tablist">
							<li role="presentation" class="active section mostrar">
								<a href="#step1" data-toggle="tab" aria-controls="step1" role="tab" title="Catálogo">
									<span class="round-tab">
										<i class="fa fa-pencil"></i>
									</span>
								</a>
							</li>
						</ul>
					</div>
					<form>
						<input type="hidden" id="codigotecnica" name="codigotecnica" value="{{e($tecnica->tecnicas_codigo)}}"/>
						<div class="tab-content">
							<div class="tab-pane active" role="tabpanel" id="step1">
								<div class="row">
									<div class="col-xs-12 col-sm-12 col-md-12 is-empty">
										<div id="bloquenombre" class="form-group has-feedback">
											<label class="label-ushop url">Técnica.</label>
											<input class="form-control" type="text" id="tecnica" name="tecnica" placeholder="{{$tecnica->tecnicas_name}}" value="{{$tecnica->tecnicas_name}}" readonly>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-xs-12 col-sm-12 col-md-12 is-empty">
										<h3 class="box-title">GESTIONE SU DESCRIPCIÓN.</h3>
									</div>
								</div>
								<div class="box-body">
									<div class="row">
										<div class="col-xs-12 col-md-12 is-empty">
											<div id="bloquetexto" class="form-group has-feedback">
												<label for="descripcionnotificacion">DESCRIPCIÓN</label>
												<div id="descripcion">{!!$tecnica->tecnicas_descripcion!!}</div>
											</div>
											<span class="help-block-contacto"></span>
										</div>
									</div>
								</div>
								<div id="rowpicture" class="row">
									<div class="col-xs-12 col-md-12 is-empty">
										<div class="form-group">
											<label for="logodown">Ingrese su imagen</label>
											<a id="icologodown" href="#logo" data-toggle="collapse" data-parent="#accordion"><i class="fa fa-chevron-down" aria-hidden="true"></i></a></a>
											<div id="logo" class="panel-collapse in collapse" aria-expanded="false">
												<div class="box-body">
													<div class="item-grid-picture">
														<a id="logo-a" class="logo-link" href="{{url('/').'/'.$tecnica->tecnicas_ruta}}" data-lightbox="logo" class="spacing-picture">
															<img id="logo-img" class="logo" src="{{url('/').'/'.$tecnica->tecnicas_ruta}}" class="img-responsive img-thumbnail"/>
														</a>
														<div class="row item-grid-picture-file">
															<div class="col-xs-12 col-md-12 is-empty">
																<div class="form-group item-grid-picture-file">
																	<label for="imagenpicture" class="clearfix control-label label-ushop">SUBA LA IMAGEN DE SU AGRADO</label>
																	<input readonly="" id="namefile" class="form-control" placeholder="Suba su imagen" type="text">
																	<input type="file" id="imagenpicture" name="imagenpicture" accept="image/x-png,image/jpeg,image/jpg" placeholder="Suba la imagen de su agrado." onchange="javascript:openFile(event)"  fileread="banner.imagen"/>
																	<p class="help-block">Solo es permitido imágenes jpg/png.</p>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-xs-12 col-md-6">
										<button type="button" id="btnGuardarPrincipal" class="btn btn-primary btn-raised btn-block btn-flat btn-ushop">
											<i class="fa fa-save" aria-hidden="true"></i> GUARDAR
											<div class="ripple-container"></div>
										</button>
									</div>
									<div class="col-xs-12 col-md-6">
										<a href="{{url('ushop/productos/tecnicas/listar')}}" class="btn btn-primary btn-raised btn-block btn-flat btn-ushop-link">
											<i class="fa fa-undo" aria-hidden="true"></i> REGRESAR
											<div class="ripple-container"></div>
										</a>
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
			</section>
		</div>
	</div>
	<div class="modal fade" id="modal_wait" role="dialog" data-backdrop="static" data-keyboard="false">
		<div class="modal-dialog">
			<div class="modal-content item-modal-border">
				<div class="modal-header item-modal-header">
					<img src="{{asset('img/logo/logosimple_cf_ushop.svg')}}" width="128" height="64">
				</div>
				<div class="modal-body">
					<p id="wait_messageadvertencia" class="text-center delsay-fonts">Espere un momento.</p>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-xs-12 col-md-12 flex-center">
							<i class="fa fa-spinner fa-spin fa-2x"></i>
						</div>
					</div>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<div class="modal fade" id="modal_alerta" role="dialog" data-backdrop="static" data-keyboard="false">
		<div class="modal-dialog">
			<div class="modal-content item-modal-border">
				<div class="modal-header item-modal-header">
					<img src="{{asset('img/logo/logosimple_cf_ushop.svg')}}" width="128">
				</div>
				<div class="modal-body">
					<p id="messageadvertencia" class="text-center delsay-fonts"></p>
				</div>
				<div class="modal-footer item-modal-footer">
					<button type="button" class="btn btn-primary btn-raised btn-block btn-flat btn-ushop" data-dismiss="modal">
						<i class="fa fa-times" aria-hidden="true"></i> CERRAR
						<div class="ripple-container"></div>
					</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
@endsection
@section('mi-scripts')
	<script src="{{asset('js/controlador/callajax/tecnicas.js')}}"></script>
	<script src="{{asset('js/wizard.js')}}"></script>
	<script src="{{asset('js/controlador/views/tecnicas.js')}}"></script>
@endsection