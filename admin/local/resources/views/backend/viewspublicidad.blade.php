@extends("layout.dashboard")
@section("section-title")
	Publicidad
	<small>Publicidad presente en su sección Home/Inicio</small>
@endsection
@section("section-breadcrumb")
	<li><a href="{{url('ushop/dashboard')}}"><i class="fa fa-dashboard"></i> Inicio</a></li>
	@if($modo=='co')
		<li class="active">Catálogo</li>
	@else
		<li class="active">Publicidad</li>
	@endif
@endsection
@section("mi-css")
	<link rel="stylesheet" href="{{asset('css/wizardv2.css')}}">
@endsection
@if($modo=='co')
	@section('menuimagenescatalogo')
		active
	@endsection
@elseif($modo!='p' && $modo!='bp')
	@section('menuimagenespublicidad')
		active
	@endsection
@endif
@if($modo=='h')
	@section('homeparallax')
		active
	@endsection
@elseif($modo=='co')
	@section('contactocatalogo')
		active
	@endsection
@elseif($modo=='p')
	@section('productosparallax')
		active
	@endsection
@elseif($modo=='bp')
	@section('menuchat')
		active
	@endsection
@else
	@section('contactoparallax')
		active
	@endsection
@endif
@section("section-body")
	<div class="box">
		<div class="box-header with-border">
			<input type="hidden" id="modo" value="{{e(base64_encode($modo))}}"/>
			@if($modo=='co')
				<h3 class="box-title">GESTIONE SU BLOQUE #CATÁLOGO ONLINE</h3>
			@else
				<h3 class="box-title">GESTIONE SU BLOQUE #IMAGEN PUBLICITARIA</h3>
			@endif
			<div class="box-tools pull-right">
				<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Minimizar">
				<i class="fa fa-minus"></i></button>
				<button id="goinstrucciones" type="button" class="btn btn-box-tool" title="Instrucciones" data-modo="{{base64_encode($modo)}}">
				<i class="fa fa-question"></i></button>
			</div>
		</div>
		<div class="box-header with-border">
			<small class="title-ushop-instrucciones"><i>Haga click en <i class="fa fa-question"></i> para visualizar sus instrucciones</i></small>
		</div>
        <div class="box-body">
			<section>
				<div class="wizard">
					<div class="wizard-inner">
						<div class="connecting-line"></div>
						<ul class="nav nav-tabs" role="tablist">
							<li role="presentation" class="active section mostrar">
								<a href="#step1" data-toggle="tab" aria-controls="step1" role="tab" title="Img Publicitaria">
									<span class="round-tab">
										<i class="fa fa-file-picture-o"></i>
									</span>
								</a>
							</li>
							<li role="presentation" class="disabled imagenes ocultar">
								<a href="#step2" data-toggle="tab" aria-controls="step2" role="tab" title="Imágenes">
									<span class="round-tab">
										<i class="fa fa-pencil"></i>
									</span>
								</a>
							</li>
						</ul>
					</div>
                    <form>
                        <input type="hidden" id="codigo" name="codigo" value="{{$codigo}}"/>
                        <input type="hidden" id="seccion" namE="seccion" value="{{e(base64_encode($publicidad->galeria_section))}}"/>
						<div class="tab-content">
							<div class="tab-pane active" role="tabpanel" id="step1">
								<div class="row">
									<div class="col-xs-12 col-sm-12 col-md-12 is-empty">
										<div id="bloqueestado" class="form-group has-feedback">
											<label for="secciones" class="secciones">Secciones</label>
											<select class="form-control" id="secciones" name="estadochocie" disabled> 
												<option selected="selected" value="NULL">Seleccione una sección de su agrado</option>
												@if($modo=='c')
													<option value="{{e(base64_encode('3'))}}" selected>Contacto</option>
												@elseif($modo=='p' || $modo=='bp')
													<option value="{{e(base64_encode('2'))}}" selected>Productos</option>
												@elseif($modo=='h')
													<option value="{{e(base64_encode('0'))}}" selected>Home/Inicio</option>
												@else
													<option value="{{e(base64_encode('1'))}}" selected>Empresa</option>
												@endif
											</select>
										</div>
										<span class="help-block-estado"></span>
									</div>
								</div>
                                <div class="row">
									<div class="col-xs-12 col-md-12 is-empty">
										<div class="form-group">
											<label for="logodown">Ingrese su imagen</label>
											<a id="icologodown" href="#logo" data-toggle="collapse" data-parent="#accordion"><i class="fa fa-chevron-down" aria-hidden="true"></i></a></a>
											<div id="logo" class="panel-collapse in collapse" aria-expanded="false">
												<div class="box-body">
													<div class="item-grid-picture">
														<a id="logo-a" class="logo-link" href="{{url('/').'/img/media/'.$publicidad->item_ruta}}" data-lightbox="logo" class="spacing-picture">
															<img id="logo-img" class="logo" src="{{url('/').'/img/media/'.$publicidad->item_ruta}}" class="img-responsive img-thumbnail"/>
														</a>
														<div class="row item-grid-picture-file">
															<div class="col-xs-12 col-md-12 is-empty">
																<div class="form-group item-grid-picture-file">
																	<label for="imagenpicture" class="clearfix control-label label-ushop">SUBA LA IMAGEN DE SU AGRADO</label>
																	<input readonly="" id="namefile" class="form-control" placeholder="Suba su imagen" type="text">
																	<input type="file" id="imagenpicture" name="imagenpicture" accept="image/x-png,image/jpeg,image/jpg" placeholder="Suba la imagen de su agrado." onchange="javascript:openFile(event)"  fileread="banner.imagen"/>
																	<p class="help-block">Solo es permitido imágenes jpg/png.</p>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<hr/>
								<div class="row">
									<div class="col-xs-12 col-sm-12 col-md-12 is-empty">
										<ul class="list-inline pull-right">
											<li><button type="button" class="btn btn-primary next-step goimagenes">CONTINUAR</button></li>
										</ul>
									</div>
								</div>
                            </div>
                            <div class="tab-pane" role="tabpanel" id="step2">
								<!-- One "tab" for each step in the form: -->
								<div id="rowpicture" class="row">
									<div class="col-xs-12 col-md-12 is-empty">
										<ul class="list-inline pull-right">
											<li><button type="button" class="btn btn-default prev-step goseccion">ATRAS</button></li>
										</ul>
									</div>
								</div>
								<hr/>
                                @if($modo=='p')
									<div class="row">
										<div class="col-xs-12 col-sm-12 col-md-12 is-empty">
											<h3 class="box-title">GESTIONE SU U.R.L.</h3>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-12 col-sm-12 col-md-12 is-empty">
											<div id="bloqueurl" class="form-group has-feedback">
												<label class="label-ushop url">U.R.L.</label>
												<input class="form-control" type="text" id="url" name="url" placeholder="{{$publicidad->bloque_url}}" value="{{$publicidad->bloque_url}}">
											</div>
										</div>
									</div>
								@else
									<div class="row">
										<div class="col-xs-12 col-sm-12 col-md-12 is-empty">
											<h3 class="box-title">GESTIONE SU FUENTE.</h3>
										</div>
									</div>
								@endif
                                <div class="row">
									<div class="col-xs-12 col-sm-12 col-md-6 is-empty">
										<div id="bloquefuentes" class="form-group has-feedback">
											<label for="secciones" class="secciones label-ushop">Fuentes</label>
											<select class="form-control" id="fuentes" name="fuentes"> 
												<option value="NULL">Seleccione la fuente de su agrado</option>
												@foreach(json_decode($fuentes,true) as $nombrefuente => $fonts)
													<option value="{{e(base64_encode(json_encode($fonts)))}}">{{$nombrefuente}}</option>
												@endforeach
											</select>
										</div>
										<span class="help-block-fuentes"></span>
									</div>
									<div class="col-xs-12 col-sm-12 col-md-6 is-empty">
										<div id="bloquetipofuentes" class="form-group has-feedback">
											<label for="secciones" class="secciones label-ushop">Tipo de fuente</label>
											<select class="form-control" id="tipofuentes" name="tipofuentes" disabled> 
												<option selected="selected" value="NULL">Seleccione la fuente de su agrado</option>
											</select>
										</div>
										<span class="help-block-fuentes"></span>
									</div>
								</div>
								<div class="row">
									<div class="col-xs-12 col-sm-12 col-md-12 is-empty">
										<h3 class="box-title">GESTIONE SU BLOQUE DE TEXTO.</h3>
									</div>
								</div>
                                <div class="box-body">
									<div class="row">
										<div class="col-xs-12 col-md-12 is-empty">
											<div id="bloquetexto" class="form-group has-feedback">
												<label for="descripcionnotificacion">TEXTO</label>
												<div id="descripcion_publicidad">{!!base64_decode($publicidad->bloque_textos)!!}</div>
											</div>
											<span class="help-block-contacto"></span>
										</div>
									</div>
								</div>
								<div id="rowpicture" class="row">
									<div class="col-xs-12 col-md-12 is-empty">
										<ul class="list-inline pull-right">
											<li><button type="button" class="btn btn-default prev-step goseccion">ATRAS</button></li>
										</ul>
									</div>
								</div>
                                <div class="row">
									<div class="col-xs-12 col-md-6">
										<button type="button" id="btnModificarPublicidad" class="btn btn-primary btn-raised btn-block btn-flat btn-ushop">
											<i class="fa fa-edit" aria-hidden="true"></i> MODIFICAR
											<div class="ripple-container"></div>
										</button>
									</div>
									<div class="col-xs-12 col-sm-12 col-md-6 is-empty">
										<a href="{{url('ushop/imagen/publicidad/'.base64_encode($modo))}}" class="btn btn-primary btn-raised btn-block btn-flat btn-ushop-link">
											<i class="fa fa-undo" aria-hidden="true"></i> REGRESAR
											<div class="ripple-container"></div>
										</a>
									</div>
								</div>
                            </div>
                        </div>
                    </form>
                </div>
            </section>
        </div>
    </div>
    <div class="modal fade" id="modal_alerta" role="dialog" data-backdrop="static" data-keyboard="false">
		<div class="modal-dialog">
			<div class="modal-content item-modal-border">
				<div class="modal-header item-modal-header">
					<img src="{{asset('img/logo/logosimple_cf_ushop.svg')}}" width="128" height="64">
				</div>
				<div class="modal-body">
					<p id="messageadvertencia" class="text-center delsay-fonts"></p>
				</div>
				<div class="modal-footer item-modal-footer">
					<button type="button" class="btn btn-primary btn-raised btn-block btn-flat btn-ushop" data-dismiss="modal">
						<i class="fa fa-times" aria-hidden="true"></i> CERRAR
						<div class="ripple-container"></div>
					</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<div class="modal fade" id="modal_wait" role="dialog" data-backdrop="static" data-keyboard="false">
		<div class="modal-dialog">
			<div class="modal-content item-modal-border">
				<div class="modal-header item-modal-header">
					<img src="{{asset('img/logo/logosimple_cf_ushop.svg')}}" width="128" height="64">
				</div>
				<div class="modal-body">
					<p id="messageadvertencia" class="text-center delsay-fonts">Espere un momento.</p>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-xs-12 col-md-12 flex-center">
							<i class="fa fa-spinner fa-spin fa-2x"></i>
						</div>
					</div>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
@endsection
@section('mi-scripts')
	<script src="{{asset('js/controlador/callajax/publicidad.js')}}"></script>
	<script src="{{asset('js/wizard.js')}}"></script>
	<script src="{{asset('js/controlador/views/publicidad.js')}}"></script>
@endsection