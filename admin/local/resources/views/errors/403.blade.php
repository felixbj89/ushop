<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Ushop | 403</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link rel="shortcut icon" type="img/png" href="{{asset('img/logo/fav.png')}}">
        <link rel="stylesheet" href="{{asset('plugins/font-awesome-4.7.0/css/font-awesome.min.css')}}">
        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .sub-title {
                font-size: 42px;
            }

            .links > a {
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
				color:#a40000;
            }

            .links > a:hover{
              background-color:#a40000;
              padding: 1rem;
              border-radius: 1rem;
			  color: #ffffff;
            }

            .links > a.salir:hover::after{
                content: " Salir";
            }

            .links > a.acceder:hover::after{
                content: " Acceder";
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            <div class="content">
                <div class="title m-b-md">
                    <img src="{{asset('img/logo/logosimple_cf_ushop.svg')}}" class="img-responsive display-center" width="160" height="64"/>
                </div>
				<div class="sub-title m-b-md">
					ACCESO INVÁLIDO.
                </div>
                <div class="links">
                    <a id="clicksalir" href="#" class="salir"><i class="fa fa-sign-out fa-2x"></i></a>
                </div>
            </div>
        </div>
		<script src="{{asset('plugins/bower_components/jquery/dist/jquery.min.js')}}"></script>
		<script src="{{asset('js/errors.js')}}"></script>
    </body>
</html>