<!DOCTYPE html>
	<html>
		<head>
			<meta charset="utf-8">
			<meta http-equiv="X-UA-Compatible" content="IE=edge">
			<meta name="csrf-token" content="{{ csrf_token() }}">
			<title>Ushop | Inicio</title>
			<!-- Tell the browser to be responsive to screen width -->
			<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
			<!-- Bootstrap 3.3.7 -->
			<link rel="stylesheet" href="{{asset('plugins/bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
			<!-- Font Awesome -->
			<link rel="stylesheet" href="{{asset('plugins/bower_components/font-awesome/css/font-awesome.min.css')}}">
			<!-- Ionicons -->
			<link rel="stylesheet" href="{{asset('plugins/bower_components/Ionicons/css/ionicons.min.css')}}">
			<!-- Theme style -->
			<link rel="stylesheet" href="{{asset('plugins/dist/css/AdminLTE.min.css')}}">
			<!-- Material Design -->
			<link rel="stylesheet" href="{{asset('plugins/dist/css/skins/all-md-skins.min.css')}}">
			<link rel="stylesheet" href="{{asset('plugins/DataTables-1.10.13/media/css/dataTables.bootstrap.min.css')}}">
			<link rel="stylesheet" href="{{asset('plugins/DataTables-1.10.13/extensions/FixedHeader/css/fixedHeader.dataTables.min.css')}}">
			<link rel="stylesheet" href="{{asset('plugins/DataTables-1.10.13/extensions/FixedHeader/css/fixedHeader.bootstrap.min.css')}}">
			<link rel="stylesheet" href="{{asset('plugins/DataTables-1.10.13/extensions/Buttons/css/buttons.bootstrap.min.css')}}">
			<link rel="stylesheet" href="{{asset('plugins/DataTables-1.10.13/extensions/Responsive/css/responsive.bootstrap.min.css')}}">
			<link rel="stylesheet" href="{{asset('plugins/dist/css/bootstrap-material-design.min.css')}}">
			<link rel="stylesheet" href="{{asset('plugins/dist/css/ripples.min.css')}}">
			<link rel="stylesheet" href="{{asset('plugins/dist/css/MaterialAdminLTE.min.css')}}">
			<link rel="stylesheet" href="{{asset('plugins/summernote-0.8.9-dist/dist/summernote.css')}}">
			<link rel="stylesheet" href="{{asset('plugins/HoverEffectIdeas/css/normalize.css')}}" rel="stylesheet"/>
			<link rel="stylesheet" href="{{asset('plugins/HoverEffectIdeas/css/set2.css')}}" rel="stylesheet"/>
			<link rel="stylesheet" href="{{asset('plugins/lightbox2-master/dist/css/lightbox.min.css')}}">
			<!-- Bootstrap Color Picker -->
			<link rel="stylesheet" href="{{asset('plugins/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css')}}">
			@yield("mi-css")
			<link rel="stylesheet" href="{{asset('css/master.css')}}">
			<link rel="stylesheet" href="{{asset('css/modales.css')}}">
			<link rel="stylesheet" href="{{asset('css/textos.css')}}">
			<!-- AdminLTE Skins. Choose a skin from the css/skins
			folder instead of downloading all of them to reduce the load. -->
			<link rel="stylesheet" href="{{asset('plugins/dist/css/skins/all-md-skins.min.css')}}">
			<link rel="shortcut icon" type="img/png" href="{{asset('img/logo/fav.png')}}">
			<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
			<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
			<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
			<![endif]-->
			<!-- Google Font -->
			<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
		</head>
		<body id="top" class="skin-blue fixed sidebar-mini sidebar-mini-expand-feature" style="height: auto; min-height: 100%;padding-rigt:0px !important">
			<!-- Site wrapper -->
			<div class="wrapper">
				<header class="main-header">
					<!-- Logo -->
					<a href="{{url('ushop/dashboard')}}" class="logo">
					<!-- mini logo for sidebar mini 50x50 pixels -->
						<span class="logo-mini"><img src="{{asset('img/logo/logosidebar.png')}}" width="32" height="32"></span>
						<!-- logo for regular state and mobile devices -->
						<span class="logo-lg"><img src="{{asset('img/logo/logosimple_cf_ushop.svg')}}" width="110" height="64"></span>
					</a>
					<!-- Header Navbar: style can be found in header.less -->
					<nav class="navbar navbar-static-top">
						<!-- Sidebar toggle button-->
						<a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</a>
						<div class="navbar-custom-menu">
							<ul class="nav navbar-nav">
								<!-- Messages: style can be found in dropdown.less-->
								<!-- Control Sidebar Toggle Button -->
								@yield("dashboard-notificaciones")
								@yield("dashboard-config")
								@yield("dashboard-link")
								<li class="cerrarsession_header">					
									<a id="clicksalirheader" href="#" data-toggle="control-sidebar"><i class="fa fa-sign-out"></i></a>
								</li>
								<li class="bloquear_header">					
									<a id="lock" href="#" data-toggle="control-sidebar"><i class="fa fa-lock"></i></a>
								</li>
							</ul>
						</div>
					</nav>
				</header>
				<aside class="main-sidebar">
					<!-- sidebar: style can be found in sidebar.less -->
					<section class="sidebar">
						<!-- Sidebar user panel -->
						<div class="user-panel">
							<div class="pull-left info">
								<p id="nombresidebarusuario"></p>
								<a href="#"><i class="fa fa-circle text-success"></i> Online</a>
							</div>
						</div>
						<!-- sidebar menu: : style can be found in sidebar.less -->
						<ul class="sidebar-menu" data-widget="tree">
							<li class="header">MENU PRINCIPAL</li>
							<li class="li-style @yield('dashboard')">
								<a href="{{url('ushop/dashboard')}}">
									<i class="fa fa-dashboard"></i> <span>Dashboard</span>
								</a>
							</li>
							<li class="header">BANNERS PRINCIPALES</li>
							<li class="treeview @yield('menubanners')">
								<a href="#">
									<i class="fa fa-cog"></i>
									<span>Administrar</span>
									<span class="pull-right-container">
										<i class="fa fa-angle-left pull-right"></i>
									</span>
								</a>
								<ul class="treeview-menu menu-banner">
									<li class="@yield('bannerhome')">
										<a href="{{url('ushop/imagen/principal/'.base64_encode('h'))}}">
											<i class="fa fa-file-picture-o"></i> HOME/INICIO
										</a>
									</li>
									<li class="@yield('bannerempresa')">
										<a href="{{url('ushop/imagen/principal/'.base64_encode('e'))}}">
											<i class="fa fa-file-picture-o"></i> EMPRESA
										</a>
									</li>
									<li class="@yield('bannerproductos')">
										<a href="{{url('ushop/imagen/principal/'.base64_encode('p'))}}">
											<i class="fa fa-file-picture-o"></i> PRODUCTOS
										</a>
									</li>
									<li class="@yield('bannercontacto')">
										<a href="{{url('ushop/imagen/principal/'.base64_encode('c'))}}">
											<i class="fa fa-file-picture-o"></i> CONTACTO
										</a>
									</li>
								</ul>
							</li>
							<li class="header">CLIENTES</li>
							<li class="li-style @yield('clientes')">
								<a href="{{url('ushop/clientes/gestionar')}}">
									<i class="fa fa-male"></i> CLIENTES
								</a>
							</li>
							<li class="header">CATÁLOGO</li>
							<li class="li-style @yield('menucatalogos')">
								<a href="{{url('ushop/catalogos/pdf/'.base64_encode('body'))}}">
									<i class="fa fa-file-pdf-o"></i> GESTIONAR
								</a>
							</li>
							<li class="header">HOME/INICIO</li>
							<li class="li-style @yield('homeproductos')">
								<a href="{{url('ushop/productos/crear')}}">
									<i class="fa fa-th"></i> PRODUCTOS
								</a>
							</li>
							<li class="li-style @yield('homeparallax')">
								<a href="{{url('ushop/imagen/publicidad/'.base64_encode('h'))}}">
									<i class="fa fa-shopping-bag"></i> PUBLICIDAD
								</a>
							</li>
							<li class="header">EMPRESA</li>
							<li class="li-style @yield('serviciosempresa')">
								<a href="{{url('ushop/empresa/servicios')}}">
									<i class="fa fa-th"></i> SERVICIOS
								</a>
							</li>
							<li class="li-style @yield('equipoempresa')">
								<a href="{{url('ushop/empresa/equipo')}}">
									<i class="fa fa-users"></i> EQUIPO
								</a>
							</li>
							<li class="header">PRODUCTOS</li>
							<li class="li-style @yield('productosparallax')">
								<a href="{{url('ushop/imagen/publicidad/'.base64_encode('p'))}}">
									<i class="fa fa-book"></i> CATÁLOGO
								</a>
							</li>
							<li class="li-style @yield('menutecnicas')">
								<a href="{{url('ushop/productos/tecnicas/listar')}}">
									<i class="fa fa-pencil"></i> <span>TÉCNICAS</span>
								</a>
							</li>
							<li class="li-style @yield('menuimportadores')">
								<a href="{{url('ushop/imagen/principal/'.base64_encode('pi'))}}">
									<i class="fa fa-th"></i> <span>IMPORTAMOS</span>
								</a>
							</li>
							<li class="li-style @yield('menuchat')">
								<a href="{{url('ushop/imagen/publicidad/'.base64_encode('bp'))}}">
									<i class="fa fa-comments-o "></i> <span>CHAT</span>
								</a>
							</li>
							<li class="header">CONTACTO</li>
							<li class="li-style @yield('contactoparallax')">
								<a href="{{url('ushop/imagen/publicidad/'.base64_encode('c'))}}">
									<i class="fa fa-users"></i> EQUIPO
								</a>
							</li>
							<li class="li-style @yield('contactocatalogo')">
								<a href="{{url('ushop/imagen/principal/'.base64_encode('cp'))}}">
									<i class="fa fa-search"></i> CONOCERTE</a>
								</li>
							</li>
							<li class="header">CERRAR SESIÓN</li>
							<li class="li-style">
								<a id="clicksalir" href="{{url('ushop/salir')}}">
									<i class="fa fa-sign-out"></i> <span>Salir</span>
								</a>
							</li>
						</ul>
					</section>
					<!-- /.sidebar -->
				</aside>
				<!-- =============================================== -->
				<!-- Content Wrapper. Contains page content -->
				<div class="content-wrapper">
					<!-- Content Header (Page header) -->
					<section class="content-header">
						<h1>
							@yield("section-title")
						</h1>
						<ol class="breadcrumb">
							@yield("section-breadcrumb")
						</ol>
					</section>
					<!-- Main content -->
					<section class="content">
						<!-- Default box -->
						@yield("section-body")	
					</section>
					<!-- /.content -->
				</div>
				<!-- /.content-wrapper -->
				<footer class="main-footer">
					<div class="pull-right hidden-xs">
						<b>Version</b> 5.0.3
					</div>
					<strong>Copyright &copy; 2018 <a href="#">Ushop</a>
					All rights reserved, 
					Powered By
					<a href="https://www.haciendacreativa.net">Hacienda C.A.</a>.</strong>
				</footer>
				<!-- /.control-sidebar -->
				<!-- Add the sidebar's background. This div must be placed
				immediately after the control sidebar -->
			</div>
			<!-- ./wrapper -->
			<!-- jQuery 3 -->
			<script src="{{asset('plugins/bower_components/jquery/dist/jquery.min.js')}}"></script>
			<!-- Bootstrap 3.3.7 -->
			<script src="{{asset('plugins/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
			<!-- OTROS -->
			<script src="{{asset('plugins/lightbox2-master/dist/js/lightbox.min.js')}}"></script>
			<!-- ChartJS -->
			<script src="{{asset('plugins/bower_components/chart.js/Chart.js')}}"></script>
			<script src="{{asset('plugins/DataTables-1.10.13/media/js/jquery.dataTables.min.js')}}"></script>
			<script src="{{asset('plugins/DataTables-1.10.13/media/js/dataTables.bootstrap.min.js')}}"></script>
			<script src="{{asset('plugins/DataTables-1.10.13/extensions/FixedHeader/js/dataTables.fixedHeader.min.js')}}"></script>
			<script src="{{asset('plugins/DataTables-1.10.13/extensions/FixedHeader/js/dataTables.fixedHeader.min.js')}}"></script>
			<script src="{{asset('plugins/DataTables-1.10.13/extensions/Buttons/js/dataTables.buttons.min.js')}}"></script>
			<script src="{{asset('plugins/DataTables-1.10.13/extensions/Buttons/js/buttons.bootstrap.min.js')}}"></script>
			<script src="{{asset('plugins/DataTables-1.10.13/extensions/Buttons/js/buttons.html5.min.js')}}"></script>
			<script src="{{asset('plugins/DataTables-1.10.13/extensions/Buttons/js/buttons.print.min.js')}}"></script>
			<script src="{{asset('plugins/DataTables-1.10.13/extensions/Buttons/js/buttons.flash.min.js')}}"></script>
			<script src="{{asset('plugins/DataTables-1.10.13/extensions/Responsive/js/dataTables.responsive.min.js')}}"></script>
			<script src="{{asset('plugins/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js')}}"></script>
			<script src="{{asset('plugins/dist/js/material.min.js')}}"></script>
			<script src="{{asset('plugins/dist/js/ripples.min.js')}}"></script>
			<script src="{{asset('plugins/summernote-0.8.9-dist/dist/summernote.js')}}"></script>
			<script src="{{asset('plugins/summernote-0.8.9-dist/dist/es-ES.js')}}"></script>
			<script src="{{asset('js/validaciones.js')}}"></script>
			<script src="{{asset('js/tabla.js')}}"></script>
			<script src="{{asset('js/charts.js')}}"></script>
			<script src="{{asset('js/controlador/callajax/dashboard.js')}}"></script>
			<script src="{{asset('js/controlador/callajax/account.js')}}"></script>
			<script src="{{asset('js/controlador/dashboard.js')}}"></script>
			@yield("mi-scripts")
			<script>
				$.material.init();
			</script>
			<!-- SlimScroll -->
			<script src="{{asset('plugins/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
			<!-- FastClick -->
			<script src="{{asset('plugins/bower_components/fastclick/lib/fastclick.js')}}"></script>
			<!-- AdminLTE App -->
			<script src="{{asset('plugins/dist/js/adminlte.min.js')}}"></script>
			<!-- AdminLTE for demo purposes -->
			<script src="{{asset('plugins/dist/js/demo.js')}}"></script>
			<script>
				$(document).ready(function () {
					$('.sidebar-menu').tree()
				})
			</script>
		</body>
	</html>
