<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(["prefix"  => "ushop"], function () {
	//GET
	Route::get('login', ['uses' => 'LoginController@login', 'as' => 'ushop/login']);
	Route::get('api/list/pictures/{section}/{origen}', ['uses' => 'RestController@listPictues', 'as' => 'ushop/api/list/pictures/{section}/{origen}']);
	Route::get('api/list/tecnicas', ['uses' => 'RestController@listecnicas', 'as' => 'ushop/api/list/tecnicas']);
	Route::get('api/list/servicios', ['uses' => 'RestController@listServicios', 'as' => 'ushop/api/list/servicios']);
	Route::get('api/list/equipos', ['uses' => 'RestController@listequipos', 'as' => 'ushop/api/list/equipos']);
	Route::get('api/list/customers', ['uses' => 'RestController@listCustomers', 'as' => 'ushop/api/list/customers']);
	Route::get('api/list/catalogo', ['uses' => 'RestController@listcatalogospdf', 'as' => 'ushop/api/list/catalogo']);
	Route::get('api/list/productos', ['uses' => 'RestController@listProductos', 'as' => 'ushop/api/list/productos']);
	Route::get('api/list/enlaces', ['uses' => 'RestController@listenlaces', 'as' => 'ushop/api/list/enlaces']);
	Route::get('api/list/galeria', ['uses' => 'RestController@listGaleria', 'as' => 'ushop/api/list/galeria']);
	
	Route::middleware(['lock'])->group(function () {
		Route::get('instrucciones/{modo}', ['uses' => 'DashboardController@instrucciones', 'as' => 'ushop/instrucciones/{modo}']);
		Route::get('dashboard', ['uses' => 'DashboardController@dashboard', 'as' => 'ushop/dashboard']);
		Route::get('dashboard/solicitudes', ['uses' => 'DashboardController@solicitudes', 'as' => 'ushop/dashboard/solicitudes']);
		Route::get('imagen/principal', ['uses' => 'DashboardController@managmentprincipal', 'as' => 'ushop/imagen/principal']);
		Route::get('imagen/principal/{modo}', ['uses' => 'DashboardController@principal', 'as' => 'ushop/imagen/principal/{modo}']);
		Route::get('catalogos/pdf/{desplazamiento}', ['uses' => 'DashboardController@catalogospdf', 'as' => 'ushop/catalogos/pdf/{desplazamiento}']);
		Route::get('imagen/publicidad/{modo}', ['uses' => 'DashboardController@publicidad', 'as' => 'ushop/imagen/publicidad/{modo}']);
		Route::get('imagen/equipo', ['uses' => 'DashboardController@equipo', 'as' => 'ushop/imagen/equipo']);
		Route::get('empresa/equipo', ['uses' => 'DashboardController@empresaEquipo', 'as' => 'ushop/empresa/equipo']);
		Route::get('empresa/servicios', ['uses' => 'DashboardController@servicios', 'as' => 'ushop/empresa/servicios']);
		Route::get('empresa/servicios/views/{codigo}', ['uses' => 'DashboardController@viewServicios', 'as' => 'ushop/empresa/servicios/views/{codigo}']);
		Route::get('productos/crear', ['uses' => 'DashboardController@productos', 'as' => 'ushop/productos/crear']);
		Route::get('productos/tecnicas/listar', ['uses' => 'DashboardController@tecnicas', 'as' => 'ushop/productos/tecnicas/listar']);
		Route::get('salir', ['uses' => 'DashboardController@salir', 'as' => 'ushop/salir']);
		/*VIEWS*/
		Route::get('productos/tecnicas/views/{codigo}', ['uses' => 'DashboardController@viewstecnicas', 'as' => 'ushop/productos/tecnicas/views/{codigo}']);
		Route::get('principal/views/{codigo}/{modo}', ['uses' => 'DashboardController@viewPrincipal', 'as' => 'ushop/principal/views/{codigo}/{modo}']);
		Route::get('publicidad/views/{codigo}/{modo}', ['uses' => 'DashboardController@viewPublicidad', 'as' => 'ushop/publicidad/views/{codigo}/{modo}']);
		Route::get('empresa/equipo/views/{codigo}', ['uses' => 'DashboardController@viewsEquipo', 'as' => 'ushop/empresa/equipo/views/{codigo}']);
		Route::get('clientes/gestionar', ['uses' => 'DashboardController@clientes', 'as' => 'ushop/clientes/gestionar']);
		Route::get('clientes/views/{codigo}', ['uses' => 'DashboardController@viewsClientes', 'as' => 'ushop/clientes/views/{codigo}']);
		/*GALERIA*/
		Route::get('empresa/servicios/galeia/{codigo}', ['uses' => 'DashboardController@viewgaleriaServicios', 'as' => 'ushop/empresa/servicios/galeia/{codigo}']);
	});
	
	//POST
	Route::post('login', ['uses' => 'LoginController@autenticarse', 'as' => 'ushop/login']);
	Route::post('imagen/principal', ['uses' => 'MediaController@imagenPrincipal', 'as' => 'ushop/imagen/principal']);
	Route::post('imagen/principal/{codigo}', ['uses' => 'MediaController@actualizarPrincipal', 'as' => 'ushop/imagen/principal/{codigo}']);
	Route::post('imagen/publicidad', ['uses' => 'MediaController@imagenPublicidad', 'as' => 'ushop/imagen/publicidad']);
	Route::post('imagen/principal/delete/{codigogaleria}', ['uses' => 'MediaController@eliminarImagen', 'as' => 'ushop/imagen/principal/delete/{codigogaleria}']);
	Route::post('principal/propiedades', ['uses' => 'MediaController@imagenPrincipalAll', 'as' => 'ushop/principal/propiedades']);
	Route::post('imagen/principal/all/{codigo}', ['uses' => 'MediaController@actualizarPrincipalAll', 'as' => 'ushop/imagen/principal/all/{codigo}']);
	Route::post('imagen/publicidad/all/{codigo}', ['uses' => 'MediaController@actualizarPublicidadAll', 'as' => 'ushop/imagen/publicidad/all/{codigo}']);
	Route::post('imagen/publicidad/all', ['uses' => 'MediaController@imagenPublicidadAll', 'as' => 'ushop/imagen/publicidad/all']);
	Route::post('empresas/servicios/{codigo}', ['uses' => 'ServiciosController@servicios', 'as' => 'ushop/empresas/servicios/{codigo}']);
	Route::put('empresas/servicios/{codigo}', ['uses' => 'ServiciosController@actualizarServicios', 'as' => 'ushop/empresas/servicios/{codigo}']);
	Route::post('empresas/clientes/{codigo}', ['uses' => 'CustomersController@actualizarClientes', 'as' => 'ushop/empresas/clientes/{codigo}']);
	/*POST-PRODUCTOS*/
	Route::post('productos/tecnicas/{codigo}', ['uses' => 'ProductosController@tecnicas', 'as' => 'ushop/productos/tecnicas/{codigo}']);
	Route::post('productos', ['uses' => 'ProductosController@productos', 'as' => 'ushop/productos']);
	Route::post('productos/view/{codigo}', ['uses' => 'ProductosController@viewProductos', 'as' => 'ushop/productos/view/{codigo}']);
	Route::post('productos/delete/{codigo}', ['uses' => 'ProductosController@eliminarProductos', 'as' => 'ushop/productos/delete/{codigo}']);
	/*POST-EQUIPO*/
	Route::post('equipo/empresa/{codigo}', ['uses' => 'MediaController@equipo', 'as' => 'ushop/equipo/empresa/{codigo}']);
	Route::post('catalogos/pdf', ['uses' => 'FileController@catalogospdf', 'as' => 'ushop/catalogos/pdf']);
	Route::post('catalogos/active/{catalogoactive}', ['uses' => 'FileController@utilizarCatalogo', 'as' => 'ushop/catalogos/active/{catalogoactive}']);
	Route::post('catalogos/pdf/delete/{codigopdf}', ['uses' => 'FileController@eliminarcatalogo', 'as' => 'ushop/catalogos/pdf/delete/{codigopdf}']);
	Route::post('dashboard/solicitud/delete/{codigo}', ['uses' => 'DashboardController@eliminarSolicitud', 'as' => 'ushop/dashboard/solicitud/delete/{codigo}']);
	Route::post('dashboard/unlock', ['uses' => 'DashboardController@unlock', 'as' => 'ushop/dashboard/unlock']);
	Route::post('dashboard/config', ['uses' => 'DashboardController@config', 'as' => 'ushop/dashboard/config']);
	/*POST-GALERIA*/
	Route::post('galeria/servivio/{codigo}', ['uses' => 'DashboardController@galeriaservices', 'as' => 'ushop/galeria/servivio/{codigo}']);
	Route::post('galeria/servivio/d/{codigo}', ['uses' => 'DashboardController@removegaleriaservices', 'as' => 'ushop/galeria/servivio/d/{codigo}']);
	/*POST-CLIENTES*/
	Route::post('empresas/clientes/d/{codigo}', ['uses' => 'CustomersController@deletecliente', 'as' => 'ushop/empresas/clientes/d/{codigo}']);
});