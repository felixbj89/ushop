$('#fontactoform #su_contact').on('click', function (){
    if ($('#name').hasClass('valid') && $('#address').hasClass('valid') && $('#rut').hasClass('valid') && $('#region').hasClass('valid') && $('#email').hasClass('valid') && $('#city').hasClass('valid') && $('#phone').hasClass('valid') && $('#contact').hasClass('valid') && $('#request').hasClass('valid')){
        if ($('#name').val().length >=2 && $('#address').val().length >=2 && $('#rut').val().length >=2 && $('#region').val().length >=2 && $('#email').val().length >=2 && $('#city').val().length >=2 && $('#phone').val().length >=2 && $('#contact').val().length >=2 && $('#request').val().length >=2){
            var expresion = /^[a-zA-Z0-9_\&\'\*\+\.\-]+(.[a-zA-Z0-9_\&\'\*\+\.\-]+)*@[a-z0-9]+(.[a-z0-9_\.\-]+)*(\.[a-z]{2,4})+$/;
	        if(expresion.test($('#email').val())){
                var url = window.location.href;
                $.ajax({
                    url: url + "/request",
                    method:"post",
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    data:{"data": btoa(Utf8.encode($('#fontactoform').serialize()))},
                    timeout: 60000,
                    beforeSend:function(){
                        $('#fontactoform #su_contact').prop('disabled', 'disabled');
                        $('#sending').show();
                    },
                    success:function(respuesta){
                        $('#fontactoform input').removeClass('valid');
                        $('#fontactoform textarea').removeClass('invalid');
                        $('#fontactoform input.form-control').val('');
                        $('#fontactoform textarea.form-control').val('');
                        $('#fontactoform .spanvalidate').hide();
                        $('#sending .evn').hide();
                        $('#sending .mensaje').html('<h5>'+respuesta.mensaje+'</h5>');
                        $('#sending .mensaje').show();
                        $('#fontactoform #su_contact').prop('disabled', 'disabled');
                        $('#dismis').show();
                        setTimeout(function(){
                            $('#sending').hide();
                            $('#dismis').hide();
                            $('#sending .evn').show();
                            $('#sending .mensaje').hide();
                            $('#sending .mensaje').html('');
                        },3000);
                    },
                    error:function(e){
                        $('#sending .evn').hide();
                        $('#sending .mensaje').show();
                        $('#sending .mensaje').html('<h5>'+e.responseJSON.mensaje+'</h5>');
                        setTimeout(function(){
                            $('#sending').hide();
                            $('#dismis').hide();
                            $('#sending .evn').show();
                            $('#sending .mensaje').hide();
                            $('#sending .mensaje').html('');
                        },3000);
                        $('#fontactoform #su_contact').prop('disabled', false);
                    }
                });
            }
        }
	}
});
//var itemRespuesta = JSON.parse(Utf8.decode(atob(respuesta.producto)));
//item = btoa(Utf8.encode(JSON.stringify(item)));
/****/
$('#fontactoform input,#fontactoform textarea').on('keyup blur', function (){
    if ($('#name').hasClass('valid') && $('#address').hasClass('valid') && $('#rut').hasClass('valid') && $('#region').hasClass('valid') && $('#email').hasClass('valid') && $('#city').hasClass('valid') && $('#phone').hasClass('valid') && $('#contact').hasClass('valid') && $('#request').hasClass('valid')){
		$('#fontactoform #su_contact').prop('disabled', false);
	} else {
		$('#fontactoform #su_contact').prop('disabled', 'disabled');
	}
});
$('#fontactoform input').bind('focusin focusout keyup',function(eventObject){
	if(!$(this).val() || ($(this).val().length<2)){
        $(this).addClass('invalid');
        $(this).removeClass('valid');
        $(this).siblings('span.novalid').show();
        $(this).siblings('span.isvalid').hide();
	}else{
        $(this).removeClass('invalid');
        $(this).addClass('valid');
        $(this).siblings('span.novalid').hide();
        $(this).siblings('span.isvalid').show();
	}
});
$('#fontactoform textarea').bind('focusin focusout keyup',function(eventObject){
	if(!$(this).val() || ($(this).val().length<20)){
        $(this).addClass('invalid');
        $(this).removeClass('valid');
        $(this).siblings('span.novalid').show();
        $(this).siblings('span.isvalid').hide();
	}else{
        $(this).removeClass('invalid');
        $(this).addClass('valid');
        $(this).siblings('span.novalid').hide();
        $(this).siblings('span.isvalid').show();
	}
});
$("#rut").bind('focusout keyup',function(event){
	if(!$(this).val() || ($(this).val().length<12)){//si esto no es valido
		$(this).addClass('invalid');
		$(this).removeClass('valid');
		$(this).siblings('span.novalid').show();
        $(this).siblings('span.isvalid').hide();
	}else{
		$(this).removeClass('invalid');
		$(this).addClass('valid');
		$(this).siblings('span.novalid').hide();
        $(this).siblings('span.isvalid').show();
    }
});
$("#phone").bind('focusout keyup',function(event){
	if(!$(this).val() || ($(this).val().length<15)){//si esto no es valido
		$(this).addClass('invalid');
		$(this).removeClass('valid');
		$(this).siblings('span.novalid').show();
        $(this).siblings('span.isvalid').hide();
	}else{
		$(this).removeClass('invalid');
		$(this).addClass('valid');
		$(this).siblings('span.novalid').hide();
        $(this).siblings('span.isvalid').show();
    }
});
$("#email").bind("keypress", function (event) {//caracteres validos
	if (event.charCode!=0) {
		var regex = new RegExp("^[a-zA-Z0-9@&'*+-._]$");
		var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
		if (!regex.test(key)) {
			event.preventDefault();
			return false;
		}
	}
});
$("#email").bind('focusout keyup',function(eventObject){
	var expresion = /^[a-zA-Z0-9_\&\'\*\+\.\-]+(.[a-zA-Z0-9_\&\'\*\+\.\-]+)*@[a-z0-9]+(.[a-z0-9_\.\-]+)*(\.[a-z]{2,4})+$/;
	if(!expresion.test($(this).val())){
		$(this).addClass('invalid');
        $(this).removeClass('valid');
        $(this).siblings('span.novalid').show();
        $(this).siblings('span.isvalid').hide();
	}else{
		$(this).removeClass('invalid');
		$(this).addClass('valid');
		$(this).siblings('span.novalid').hide();
        $(this).siblings('span.isvalid').show();
	}
});
/****/
/*Utf8 encode function*/
var Utf8 ={
    // public method for url encoding
    encode : function (string) {
        string = string.replace(/\r\n/g,"\n");
        var utftext = "";
        for (var n = 0; n < string.length; n++) {
            var c = string.charCodeAt(n);
            if (c < 128){
                utftext += String.fromCharCode(c);
            }
            else if((c > 127) && (c < 2048)){
                utftext += String.fromCharCode((c >> 6) | 192);
                utftext += String.fromCharCode((c & 63) | 128);
            }else{
                utftext += String.fromCharCode((c >> 12) | 224);
                utftext += String.fromCharCode(((c >> 6) & 63) | 128);
                utftext += String.fromCharCode((c & 63) | 128);
            }
        }
        return utftext;
    },
    // public method for url decoding
    decode : function (utftext){
        var string = "";
        var i = 0;
        var c = c1 = c2 = 0;
        while ( i < utftext.length ){
            c = utftext.charCodeAt(i);
            if (c < 128){
                string += String.fromCharCode(c);
                i++;
            }else if((c > 191) && (c < 224)){
                c2 = utftext.charCodeAt(i+1);
                string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
                i += 2;
            }else{
                c2 = utftext.charCodeAt(i+1);
                c3 = utftext.charCodeAt(i+2);
                string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
                i += 3;
            }
        }
        return string;
    }
}