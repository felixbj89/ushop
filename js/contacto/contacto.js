$(document).ready(function(){
    //.+56 (por default)   +56 9 (el 9 puede ser 2 si es local) y quedaria +56 9 1234 5678
    //00.000.000-0 y el ultimo 0 podria ser K (Ejemplo mi rut es 25.776.351-K)
    $.mask.definitions['a'] = "[9|2]";
    $.mask.definitions['b'] = "[0-9|k]";
    $.mask.definitions['c'] = "[0-3]";
    $("#phone").mask("+56 a 9999 9999",{autoclear: false,placeholder:""});
    $("#rut").mask("c9.999.999-b",{autoclear: false,placeholder:""});
});
//$('#modalCurriculum').modal({backdrop: 'static', keyboard: false});