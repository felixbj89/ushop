// add the animation to the modal
$( ".modal" ).each(function(index) {
	$(this).on('show.bs.modal', function (e) { 
		$('.modal-dialog').velocity('transition.bounceRightIn',{ duration: 1500 });
	});
});
$('.modal').on('show.bs.modal', function (e) {
	setTimeout(function() {
		var swiper = new Swiper('.swiper-container', {
			slidesPerView: 3,
			slidesPerColumn: 2,
			spaceBetween: 15,
			pagination: {
				el: '.swiper-pagination',
				clickable: true,
			},
			mousewheel: true,
			keyboard: {
				enabled: true,
			},
			resizeReInit: true,
			breakpoints: {
				991: {
					slidesPerView: 2,
					slidesPerColumn:2
				},
				575: {
					slidesPerView: 1,
					slidesPerColumn: 2
				},
			}
		});
	}, 10);
});
$(document).ready(function(){
	/*$('#bannerI .owl-carousel').owlCarousel({
		items:1,
		loop:true,
		nav:true,
		navText: ["<i class=\"fas fa-chevron-left fa-3x\"></i>","<i class=\"fas fa-chevron-right fa-3x\"></i>"],
        margin:0,
        lazyLoad:true,
		center:true,
		animateOut: 'fadeOut'
	});*/
	$('#customers .owl-carousel').owlCarousel({
		loop:false,
		responsiveClass:true,
		nav:true,
		navText: ["<i class=\"fas fa-chevron-left fa-3x\"></i>","<i class=\"fas fa-chevron-right fa-3x\"></i>"],
		margin:10,
		lazyLoad:true,
		responsive:{
			0:{
				items:1
			},
			400:{
				items:2
			},
			600:{
				items:3
			},
			1000:{
				items:6
			}
		}
	});
	$('#empresa .owl-carousel').owlCarousel({
		loop:false,
		responsiveClass:true,
		nav:true,
		navText: ["<i class=\"fas fa-chevron-left fa-3x\"></i>","<i class=\"fas fa-chevron-right fa-3x\"></i>"],
		margin:10,
		lazyLoad:true,
		responsive:{
			0:{
				items:1
			},
			992:{
				items:3
			}
		}
	});
});
$(window).scroll(function(){
    if($(window).scrollTop() >= 450){
			$('#navproductos').addClass('fixed-top toproducts');
    } else {
			$('#navproductos').removeClass('fixed-top toproducts');
    }
});
$('#navproductos .nav-link').on('click',function(){
	$('#navproductos .nav-link').removeClass('active');
	$(this).addClass('active');
});
/************/
var sections = $('section'), nav = $('nav'), nav_height = nav.outerHeight();
$(window).on('scroll', function () {
  var cur_pos = $(this).scrollTop();
 
  sections.each(function() {
		var top = $(this).offset().top - nav_height - 110,
		bottom = top + $(this).outerHeight();

		if($(this).attr('id') == 'equipo' && cur_pos >= top){
			$('#navproductos').removeClass('fixed-top toproducts');
		}else{
			if (cur_pos >= top && cur_pos <= bottom) {
				nav.find('a').removeClass('active');
				sections.removeClass('active');
				
				$(this).addClass('active');
				nav.find('a[href="#'+$(this).attr('id')+'"]').addClass('active');
			}
		}
  });
});