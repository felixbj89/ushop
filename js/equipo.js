var uploadField = document.getElementById("myfile");
uploadField.onchange = function() {
    $('#errorfilesize').removeClass('visible');
    if(this.files[0].size > 25165824){
        $('#errorfilesize').addClass('visible');
        setTimeout(function(){
            $('#errorfilesize').removeClass('visible');
        },3000);
        this.value = "";
    }else{
        $(this).removeClass('invalid');
        $(this).addClass('valid');
        $(this).siblings('span.novalid').hide();
        $(this).siblings('span.isvalid').show(); 
    };
};
$('#su_curriculum').on('click', function (){
    if ($('#myemail').hasClass('valid') && $('#myfile').hasClass('valid') && $('#myname').hasClass('valid')){
        if ($('#myname').val().length >=2 && $('#myemail').val().length >=2 && $("#myfile")[0].files.length != 0){
            var expresion = /^[a-zA-Z0-9_\&\'\*\+\.\-]+(.[a-zA-Z0-9_\&\'\*\+\.\-]+)*@[a-z0-9]+(.[a-z0-9_\.\-]+)*(\.[a-z]{2,4})+$/;
	        if(expresion.test($('#myemail').val())){
                var url = window.location.href;
                var formData = new FormData();
                formData.append("file", $("#myfile")[0].files[0]);
                formData.append("email", $("#myemail").val());
                formData.append("name", $("#myname").val());
                $.ajax({
                    url: url + "/equipo",
                    method:"post",
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    timeout: 60000,
                    beforeSend:function(){
                        $('#modalCurriculum input').prop('disabled', 'disabled');
                        $('#modalCurriculum .eviando').show();
                    },
                    success:function(respuesta){
                        $('#modalCurriculum input').removeClass('valid');
                        $('#modalCurriculum .spanvalidate').hide()
                        $('#modalCurriculum input').prop('disabled', false);
                        $('#su_curriculum').prop('disabled', 'disabled');
                        $("#myemail").val('');$("#myname").val('');$("#myfile").val('');
                        $('#modalCurriculum .listo').html('<h5>'+respuesta.mensaje+'</h5>');
                        $('#modalCurriculum .procesing').hide();
                        $('#modalCurriculum .listo').show();
                        $('#dismissenv').show();
                        setTimeout(function(){
                            $('#modalCurriculum .eviando').hide();
                            $('#dismissenv').hide();
                            $('#modalCurriculum .procesing').show();
                            $('#modalCurriculum .listo').hide();
                            $('#smodalCurriculum .listo').html('');
                        },3000);
                    },
                    error:function(e){
                        $('#modalCurriculum input').prop('disabled', false);
                        $('#modalCurriculum .listo').html('<h5>'+e.responseJSON.mensaje+'</h5>');
                        $('#modalCurriculum .procesing').hide();
                        $('#modalCurriculum .listo').show();
                        $('#dismissenv').show();
                        setTimeout(function(){
                            $('#modalCurriculum .eviando').hide();
                            $('#dismissenv').hide();
                            $('#modalCurriculum .procesing').show();
                            $('#modalCurriculum .listo').hide();
                            $('#smodalCurriculum .listo').html('');
                        },3000);
                    }
                });
            }
        }
	}
});
$('#myname').bind('focusin focusout keyup',function(eventObject){
	if(!$(this).val() || ($(this).val().length<2)){
        $(this).addClass('invalid');
        $(this).removeClass('valid');
        $(this).siblings('span.novalid').show();
        $(this).siblings('span.isvalid').hide();
	}else{
        $(this).removeClass('invalid');
        $(this).addClass('valid');
        $(this).siblings('span.novalid').hide();
        $(this).siblings('span.isvalid').show();
	}
});
$('#formEquipo input').on('keyup blur', function (){
    if ($('#myemail').hasClass('valid') && $('#myfile').hasClass('valid')){
		$('#su_curriculum').prop('disabled', false);
	} else {
		$('#su_curriculum').prop('disabled', 'disabled');
	}
});
$("#myemail").bind("keypress", function (event) {//caracteres validos
	if (event.charCode!=0) {
		var regex = new RegExp("^[a-zA-Z0-9@&'*+-._]$");
		var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
		if (!regex.test(key)) {
			event.preventDefault();
			return false;
		}
	}
});
$("#myemail").bind('focusout keyup',function(eventObject){
	var expresion = /^[a-zA-Z0-9_\&\'\*\+\.\-]+(.[a-zA-Z0-9_\&\'\*\+\.\-]+)*@[a-z0-9]+(.[a-z0-9_\.\-]+)*(\.[a-z]{2,4})+$/;
	if(!expresion.test($(this).val())){
		$(this).addClass('invalid');
        $(this).removeClass('valid');
        $(this).siblings('span.novalid').show();
        $(this).siblings('span.isvalid').hide();
	}else{
		$(this).removeClass('invalid');
		$(this).addClass('valid');
		$(this).siblings('span.novalid').hide();
        $(this).siblings('span.isvalid').show();
	}
});
$(document ).on('change','#myfile' , function(){
    if($("#myfile")[0].files.length == 0){
        $(this).addClass('invalid');
        $(this).removeClass('valid');
        $(this).siblings('span.novalid').show();
        $(this).siblings('span.isvalid').hide();
    }else{
        $(this).removeClass('invalid');
        $(this).addClass('valid');
        $(this).siblings('span.novalid').hide();
        $(this).siblings('span.isvalid').show();
    }
 });
 /****/
$("#modalCurriculum").on('hidden.bs.modal', function () {
    $("#myfile").val('');
    $("#myfile").addClass('invalid');
    $("#myfile").removeClass('valid');
    $("#myfile").siblings('span.novalid').show();
    $("#myfile").siblings('span.isvalid').hide();
    $('#su_curriculum').prop('disabled', 'disabled');
});