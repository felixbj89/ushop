function initMap() {
	var ushop = {lat: -33.2979592, lng: -70.71166699999998};
	var contentString = '<div style="display: inline-block; overflow: auto; max-height: 654px; max-width: 654px;">\
	<div dir="ltr" style="" jstcache="0">\
		<div jstcache="33" class="poi-info-window gm-style">\
			<div jstcache="2">\
				<div jstcache="3" class="title full-width" jsan="7.title,7.full-width">\
					<div class="gTitle"></div>\
				</div>\
				<div class="address">\
					<div jstcache="4" jsinstance="0" class="address-line full-width" jsan="7.address-line,7.full-width">\
						Javiera Carrera 114\
					</div>\
					<div jstcache="4" jsinstance="1" class="address-line full-width" jsan="7.address-line,7.full-width">\
						Colina\
					</div>\
					<div jstcache="4" jsinstance="2" class="address-line full-width" jsan="7.address-line,7.full-width">\
						Región Metropolitana\
					</div>\
					<div jstcache="4" jsinstance="*3" class="address-line full-width" jsan="7.address-line,7.full-width">\
						Chile\
					</div>\
				</div>\
			</div>\
			<div jstcache="5" style="display:none"></div>\
			<div class="view-link">\
				<a target="_blank" jstcache="6" href="https://maps.google.com/maps?ll=-33.298183,-70.712095&amp;z=17&amp;t=m&amp;hl=es-ES&amp;gl=US&amp;mapclient=apiv3&amp;cid=8690667053447646463">\
					<span>Ver en Google Maps</span>\
				</a>\
			</div>\
		</div>\
		</div>\
	</div>';
	var map = new google.maps.Map(document.getElementById('map'), {
		center: ushop,
		zoom: 15.5
	});
	var marker = new google.maps.Marker({
		position: ushop,
		map: map,
		title: 'Ushop'
	});
	var infowindow = new google.maps.InfoWindow({
		content: contentString
	});
	marker.addListener('click', function() {
		infowindow.open(map, marker);
	});
	infowindow.open(map,marker);	
}