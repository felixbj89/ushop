var myVideo = document.getElementById("clipUno"); //filter('.active')
function playPause() {
	if (myVideo.paused){
		$('#bannerI #play').hide();
		$('#bannerI #pause').show();
        myVideo.play(); 
	}else{
		$('#bannerI #pause').hide();
		$('#bannerI #play').show();
        myVideo.pause(); 
	}
}
$('#clipUno').on('ended',function(){
	$('#bannerI #pause').hide();
	$('#bannerI #play').show();
});
$('#clipUno').on('onprogress',function(){
	$('#bannerI #pause').hide();
	$('#bannerI #play').show();
});
$('#bannerI .owl-carousel').on('changed.owl.carousel', function(event) {
	$('#bannerI #pause').hide();
	$('#bannerI #play').show();
	if(myVideo!=null){
		myVideo.pause();
	}
});
$(document).ready(function(){
	if($(window).width() < 767){
		$('#collapseFooter').removeClass('show');	
	}else{
		$('#collapseFooter').addClass('show');
	}
	
	$(".ircatalogo").on("click",function(){
	    window.location.href = "http://cotizador.ushop.cl";
	});
	
	$('#bannerI .owl-carousel').owlCarousel({
		items:1,
		loop:true,
		nav:true,
		navText: ["<i class=\"fas fa-chevron-left back fa-3x\"></i>","<i class=\"fas fa-chevron-right front fa-3x\"></i>"],
        margin:0,
        video:true,
        lazyLoad:true,
		center:true,
		videoWidth: true, // Default false; Type: Boolean/Number
		videoHeight: 500, // Default false; Type: Boolean/Number
		animateOut: 'fadeOut'
	});
		
	$('#services .owl-carousel').owlCarousel({
		loop:true,
		responsiveClass:true,
		nav:true,
		navText: ["<i class=\"fas fa-chevron-left fa-3x\"></i>","<i class=\"fas fa-chevron-right fa-3x\"></i>"],
		margin:10,
		lazyLoad:true,
		responsive:{
			0:{
				items:1
			},
			700:{
				items:2	
			},
			800:{
				items:3
			}
		}
	});
	$('#produts .owl-carousel').owlCarousel({
		loop:true,
		responsiveClass:true,
		nav:true,
		navText: ["<i class=\"fas fa-chevron-left fa-3x\"></i>","<i class=\"fas fa-chevron-right fa-3x\"></i>"],
		margin:10,
		lazyLoad:true,
		responsive:{
			0:{
				items:1
			},
			600:{
				items:2	
			},
			800:{
				items:3
			},
			1200:{
				items:4
			},
			1369:{
				items:5
			}
		}
	});
	$('#packideal .owl-carousel').owlCarousel({
		loop:true,
		responsiveClass:true,
		nav:true,
		navText: ["<i class=\"fas fa-chevron-left fa-3x\"></i>","<i class=\"fas fa-chevron-right fa-3x\"></i>"],
		margin:10,
		lazyLoad:true,
		responsive:{
			0:{
				items:1,
			},
			800:{
				items:3
			}
		}
	});
	$('#customers .owl-carousel').owlCarousel({
		loop:true,
		responsiveClass:true,
		nav:true,
		navText: ["<i class=\"fas fa-chevron-left fa-3x\"></i>","<i class=\"fas fa-chevron-right fa-3x\"></i>"],
		margin:10,
		lazyLoad:true,
		responsive:{
			0:{
				items:1
			},
			400:{
				items:2
			},
			600:{
				items:3
			},
			1000:{
				items:6
			}
		}
	});	
});
$('#bannerI .owl-carousel').on('changed.owl.carousel', function (e) {
	setTimeout(function(){
		if($('#bannerI .owl-item.active>.item.video').length == 1){
			myVideo.play(); 
		}
	},500);
})