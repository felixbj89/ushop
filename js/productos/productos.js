$(document).ready(function(){
    $.mask.definitions['a'] = "[9|2]";
    $.mask.definitions['b'] = "[0-9|k]";
    $.mask.definitions['c'] = "[0-3]";
    $("#phone").mask("+56 a 9999 9999");
    $("#rut").mask("c9.999.999-b");
	$('#tecnicas .owl-carousel').owlCarousel({
		loop:false,
		responsiveClass:true,
		nav:true,
		navText: ["<i class=\"fas fa-chevron-left fa-3x\"></i>","<i class=\"fas fa-chevron-right fa-3x\"></i>"],
		margin:10,
		lazyLoad:true,
		responsive:{
			0:{
				items:1
			},
			992:{
				items:3
			}
		}
	});
});
var cl = false;
$('.listec a').click(function(event){
	event.preventDefault();
	$('.listec a').removeClass('active');
	$(this).addClass('active');
	$('#tecnicas .owl-carousel').trigger('to.owl.carousel', [$(this).attr('href'),500,true]);
	cl = true;
});
$('#tecnicas .owl-carousel').on('changed.owl.carousel', function (e) {
    //console.log("current: ",e.relatedTarget.current())
    //console.log("current: ",e.item.index) //same
	//console.log("total: ",e.item.count)   //total
	setTimeout(function(){
		if(!cl){
			$('.listec a').removeClass('active');
			$('.listec a[href="'+e.relatedTarget.current()+'"').addClass('active');
		}
		cl = false;
	},500);
})