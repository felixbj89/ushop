$(window).scroll(function(){
    if($(window).scrollTop() >= 250){
			$('#navbar').addClass('fixed-top');
			$('#c_navb1').removeClass('col-12 col-xl-11').addClass('col-sm-4 col-md-5 col-xl-6');
			$('#c_navb2').removeClass('col-12 col-xl-11	').addClass('col-sm-9 col-md-8 col-xl-7');
			$('#navb1,#navb2').removeClass('float-right').addClass('max-width-none');
			$('#navb2').removeClass('justify-content-end');
			$('#c_navb1,#c_navb2').addClass('absolute');
			$('#cc_navb').removeClass('col-sm-11');
			$('.fixed_s.list-inline-item').css('display','inline-block');
			$('.fixed_h.list-inline-item').css('display','none');
			$('.fixed_h.solo').css('display','none');
    } else {
			$('#navbar').removeClass('fixed-top');
			$('#c_navb1').removeClass('col-sm-4 col-md-5 col-xl-6').addClass('col-12 col-xl-11');
			$('#c_navb2').removeClass('col-sm-9 col-md-8 col-xl-7').addClass('col-12 col-xl-11');
			$('#navb1,#navb2').addClass('float-right').removeClass('max-width-none');
			$('#navb2').addClass('justify-content-end');
			$('#c_navb1,#c_navb2').removeClass('absolute');
			$('.fixed_s.list-inline-item').css('display','none');
			$('.fixed_h.list-inline-item').css('display','inline-block');
			$('.fixed_h.solo').css('display','block');
    }
});
$( window ).resize(function() {
	if($(window).width() <= 767){
		$('#collapseFooter').removeClass('show');	
	}else{
		$('#collapseFooter').addClass('show');
	}
});
$(document).on('click', '.animatescroll', function (event) {
	event.preventDefault();
	$('html, body').animate({scrollTop: ($($.attr(this, 'href')).offset().top) - 110}, 1500);
});
/****/
$( "#searchString" ).autocomplete({
	source: function(request, response) {
		$.ajax({
			url: baseurl+"/display-search-queries",
			dataType: "json",
			method: 'GET',
			data: {
				term: request.term
			},
			beforeSend: function(){
				$('.list-ppp').empty();
				$('.spinner').show();
			},
			success: function(data){
				$('.spinner').hide();
				response(data);
			}
		});
	},
	minLength: 3,
	select:function(event, ui){
		var Content = ui.item;
		var key = Object.keys(Content)[1];
		var val = Object.values(Content)[1];
		window.location.href = baseurl+"/quest/"+key+"/"+val;
		//redireccionar
	}
}).autocomplete("instance")._renderItem = function(ul, item) {
	ul.addClass("list-ppp pt-2");
	return $("<li>").append("<i class=\"ushopicon\" aria-hidden=\"true\"></i>&nbsp;<span>"+item.nombre+"</span>").appendTo(ul);
};
$( "#searchStringI" ).autocomplete({
	source: function(request, response) {
		$.ajax({
			url: baseurl+"/display-search-queries",
			dataType: "json",
			method: 'GET',
			data: {
				term: request.term
			},
			beforeSend: function(){
				$('.list-ppp').empty();
				$('.spinner').show();
			},
			success: function(data){
				$('.spinner').hide();
				response(data);
			}
		});
	},
	minLength: 3,
	select:function(event, ui){
		var Content = ui.item;
		var key = Object.keys(Content)[1];
		var val = Object.values(Content)[1];
		window.location.href = baseurl+"/quest/"+key+"/"+val;
		//redireccionar
	}
}).autocomplete("instance")._renderItem = function(ul, item) {
	ul.addClass("list-ppp pt-2");
	return $("<li>").append("<i class=\"ushopicon\" aria-hidden=\"true\"></i>&nbsp;<span>"+item.nombre+"</span>").appendTo(ul);
};
$( "#searchStringII" ).autocomplete({
	source: function(request, response) {
		$.ajax({
			url: baseurl+"/display-search-queries",
			dataType: "json",
			method: 'GET',
			data: {
				term: request.term
			},
			beforeSend: function(){
				$('.list-ppp').empty();
				$('.spinner').show();
			},
			success: function(data){
				$('.spinner').hide();
				response(data);
			}
		});
	},
	minLength: 3,
	select:function(event, ui){
		var Content = ui.item;
		var key = Object.keys(Content)[1];
		var val = Object.values(Content)[1];
		window.location.href = baseurl+"/quest/"+key+"/"+val;
		//redireccionar
	}
}).autocomplete("instance")._renderItem = function(ul, item) {
	ul.addClass("list-ppp pt-2");
	return $("<li>").append("<i class=\"ushopicon\" aria-hidden=\"true\"></i>&nbsp;<span>"+item.nombre+"</span>").appendTo(ul);
};