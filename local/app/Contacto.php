<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contacto extends Model
{
	protected $table = "contacto";

	protected $fillable = [
		"contacto_nombre",
		"contacto_codigo",
		"contacto_correo",
		"contacto_requerimiento",
		"users_id"
	];

	protected $hidden = ["id"];
	
    public static function saveUsers($inputs){
		Contacto::create([
			"contacto_nombre" => $inputs["nombre"],
			"contacto_codigo" => str_random(10),
			"contacto_correo" => $inputs["correo"],
			"contacto_requerimiento" => $inputs["requerimiento"],
			"users_id" => 1
		]);
		
		return [NULL,200];
	}
}
