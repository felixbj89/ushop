<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Mail;
use App\Contacto;

class FormController extends Controller{
    
	private function procesarPOST($route,$request){
		$url = $route;
		//  Initiate curl
		$ch = curl_init();
		// Disable SSL verification
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		// indicamos el tipo de petición: POST
		curl_setopt($ch, CURLOPT_POST, TRUE);
		//le decimos qué paramáetros enviamos (pares nombre/valor, también acepta un array)
		curl_setopt ($ch, CURLOPT_POSTFIELDS, $request);
		// Will return the response, if false it print the response
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_VERBOSE, 1);
		// Set the url
		curl_setopt($ch, CURLOPT_URL,$url);
		// Execute
		$result=curl_exec($ch);
		// Closing
		curl_close($ch); return $result;
	}
	
    public function team  (Request $request){

        $parts = $request->all();
        if(count($parts) == 3){
            if (!preg_match("/^[a-z0-9_\.\-]+(.[a-z0-9_\.\-]+)*@[a-z0-9]+(.[a-z0-9_\.\-]+)*(\.[a-z]{2,4})+$/",$parts['email'])){
				return response()->json([
                    "respuesta" => $parts,
                    "mensaje" => \Lang::get('mensajes.error_email')
                ],409);
            }
            try{
                //ALMACENANDO EL CV
                $file = $parts["file"]->getClientOriginalName();
                \Storage::disk('filecv')->put($file,  \File::get($parts["file"]));
                $asunto = 'Curriculum #'.$parts['email'];
                $archivo = "data/cv/".$file;
                Mail::send('email.curriculum', ["correo" => $parts["email"],"nombre" => $parts["name"]], function($message) use($archivo, $asunto){
					$message->to('contacto@ushop.cl')->subject($asunto);//contacto@ushop.cl
					$message->attach($archivo);
                });
                //ELEMINO EL CV
                if(file_exists("data/cv/".$file)){
                    unlink("data/cv/".$file);
                }
                //\Storage::disk('filecv')->delete("data/cv/".$file);
			}catch(\Exception $e){
                return response()->json([
                    "respuesta" => $parts,
                    "mensaje" => \Lang::get('mensajes.error_sendmail')
                ],500); 
            }
            return response()->json([
                "respuesta" => $request->all(),
                "mensaje" => \Lang::get('mensajes.sus_sendmail')
            ],200); 
        }else{
            return response()->json([
                "respuesta" => $parts,
                "mensaje" => \Lang::get('mensajes.error_parametros')
            ],409);
        } 
    }

    public function requerimiento   (Request $request){
        $data = urldecode(base64_decode($request->data));
        $parts = explode("&",$data);
        foreach($parts as $clave => $valor){
            $parts[$clave] = explode("=",$valor)['1'];
        }
        if(count($parts) == 9){ 
            if (!preg_match("/^[a-z0-9_\.\-]+(.[a-z0-9_\.\-]+)*@[a-z0-9]+(.[a-z0-9_\.\-]+)*(\.[a-z]{2,4})+$/",$parts[4])){
				return response()->json([
                    "respuesta" => $parts,
                    "mensaje" => \Lang::get('mensajes.error_email')
                ],409);
            }
			
			$parametros = [
				"nombre" => $parts[0],
				"correo" => $parts[4],
				"requerimiento" => $parts[8]
			];
			
			Contacto::saveUsers($parametros);
			
            try{
				$asunto = 'Contacto #'.$parts[0];
                Mail::send('email.contacto', ["nombre" => $parts[0],"direccion" => $parts[1],"rut" => $parts[2],"region" => $parts[3],"correo" => $parts[4],"ciudad" => $parts[5],"telefono" => $parts[6],"contacto" => $parts[7],"requerimiento" => $parts[8]], function($message) use($asunto){
                    $message->to('contacto@ushop.cl')->subject($asunto);//contacto@ushop.cl
                });
			}catch(\Exception $e){
                return response()->json([
                    "respuesta" => $parts,
                    "mensaje" => \Lang::get('mensajes.error_sendmail')
                ],500); 
            }
            return response()->json([
                "respuesta" => $parts,
                "mensaje" => \Lang::get('mensajes.sus_sendmail')
            ],200);  
        }else{
            return response()->json([
                "respuesta" => $parts,
                "mensaje" => \Lang::get('mensajes.error_parametros')
            ],409);
        }
	}
}
