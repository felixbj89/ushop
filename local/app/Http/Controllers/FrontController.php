<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\TrackerController;
use App\Quest;

class FrontController extends Controller
{
	private function procesarGET($route){
		$url = $route;
		//  Initiate curl
		$ch = curl_init();
		// Disable SSL verification
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		// Will return the response, if false it print the response
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		// Set the url
		curl_setopt($ch, CURLOPT_URL,$url);
		// Execute
		$result=curl_exec($ch);
		// Closing
		curl_close($ch); return $result;
	}
	
	private function procesarPOST($route,$request){
		$url = $route;
		//  Initiate curl
		$ch = curl_init();
		// Disable SSL verification
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		// indicamos el tipo de petición: POST
		curl_setopt($ch, CURLOPT_POST, TRUE);
		//le decimos qué paramáetros enviamos (pares nombre/valor, también acepta un array)
		curl_setopt ($ch, CURLOPT_POSTFIELDS, $request);
		// Will return the response, if false it print the response
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_VERBOSE, 1);
		// Set the url
		curl_setopt($ch, CURLOPT_URL,$url);
		// Execute
		$result=curl_exec($ch);
		// Closing
		curl_close($ch); return $result;
	}
	
	private function obtenerActivo($catalogos){
		foreach($catalogos as $cat){
			if($cat["catalogo_estado"]==1){
				return $cat;
			}
		}
		
		return NULL;
	}
	
	public function index(){
		$route = url('/');
		$rutabase = substr($route,0,strrpos($route,"contacto"))."admin/";
		$ruta = substr($route,0,strrpos($route,"contacto"))."admin/ushop/api/list/pictures/".base64_encode('0')."/".base64_encode("0");
		$banner = json_decode($this->procesarGET(url($ruta)))->respuesta;
		
		$especificaciones = NULL;
		if($banner!=NULL){
			$especificaciones = json_decode($banner,true);
		}
		
		$ruta = substr($route,0,strrpos($route,"contacto"))."admin/ushop/api/list/pictures/".base64_encode('0')."/".base64_encode("1");
		$publicidad = json_decode($this->procesarGET(url($ruta)))->respuesta;
		$publicidadparallax = NULL;
		if($publicidad!=NULL){
			$publicidadparallax = json_decode($publicidad,true);
		}
		
		$ruta = substr($route,0,strrpos($route,"productos"))."admin/ushop/api/list/customers";
		$customers = json_decode($this->procesarGET(url($ruta)))->respuesta;
		$customersLista = NULL;
		if($customers!=NULL){
			$customersLista = json_decode($customers,true);
		}
		
		$ruta = substr($route,0,strrpos($route,"productos"))."admin/ushop/api/list/productos";
		$productos = json_decode($this->procesarGET(url($ruta)))->respuesta;
		$productosList = NULL;
		if($productos!=NULL){
			$productosList = json_decode($productos,true);
		}
	
		$ruta = substr($route,0,strrpos($route,"productos"))."admin/ushop/api/list/catalogo";
		$catalogo = $this->obtenerActivo(json_decode($this->procesarGET(url($ruta)),true)["respuesta"]);
		$catalogoLista = NULL;
		if($catalogo!=NULL){
			$catalogoLista = $catalogo;
		}
		
		$ruta = substr($route,0,strrpos($route,"productos"))."admin/ushop/api/list/enlaces";
		$enlaces = json_decode($this->procesarGET(url($ruta)),true);
		$enlacesLista = NULL;
		if($enlaces!=NULL){
			$enlacesLista = $enlaces;
		}
		
		//$clientes = $this->procesarSVG($customersLista);
		return view("frontend.index")->with([
			"uno" => "0",
			"section" => "",
			"especificaciones" => $especificaciones,
			"publicidadparallax" => $publicidadparallax[0],
			"productoslist" => $productosList,
			"urlbase" => $rutabase,
			"enlaces" => $enlacesLista,
			"pdf" => $catalogoLista,
			"clientes" => $customersLista
		]);
	}

	public function _index($section = null){
		$route = url('/');
		$rutabase = substr($route,0,strrpos($route,"contacto"))."admin/";
		$ruta = substr($route,0,strrpos($route,"contacto"))."admin/ushop/api/list/pictures/".base64_encode('0')."/".base64_encode("0");
		$banner = json_decode($this->procesarGET(url($ruta)))->respuesta;
		$especificaciones = NULL;
		if($banner!=NULL){
			$especificaciones = json_decode($banner,true);
		}
		
		$ruta = substr($route,0,strrpos($route,"contacto"))."admin/ushop/api/list/pictures/".base64_encode('0')."/".base64_encode("1");
		$publicidad = json_decode($this->procesarGET(url($ruta)))->respuesta;
		$publicidadparallax = NULL;
		if($publicidad!=NULL){
			$publicidadparallax = json_decode($publicidad,true);
		}
		
		$ruta = substr($route,0,strrpos($route,"productos"))."admin/ushop/api/list/customers";
		$customers = json_decode($this->procesarGET(url($ruta)))->respuesta;
		$customersLista = NULL;
		if($customers!=NULL){
			$customersLista = json_decode($customers,true);
		}
		
		$ruta = substr($route,0,strrpos($route,"productos"))."admin/ushop/api/list/productos";
		$productos = json_decode($this->procesarGET(url($ruta)))->respuesta;
		$productosList = NULL;
		if($productos!=NULL){
			$productosList = json_decode($productos,true);
		}
	
		$ruta = substr($route,0,strrpos($route,"productos"))."admin/ushop/api/list/catalogo";
		$catalogo = $this->obtenerActivo(json_decode($this->procesarGET(url($ruta)),true)["respuesta"]);
		$catalogoLista = NULL;
		if($catalogo!=NULL){
			$catalogoLista = $catalogo;
		}
		
		$ruta = substr($route,0,strrpos($route,"productos"))."admin/ushop/api/list/enlaces";
		$enlaces = json_decode($this->procesarGET(url($ruta)),true);
		$enlacesLista = NULL;
		if($enlaces!=NULL){
			$enlacesLista = $enlaces;
		}
		
		//$clientes = $this->procesarSVG($customersLista);
		return view("frontend.index")->with([
			"uno" => "0",
			"section" => $section,
			"especificaciones" => $especificaciones,
			"publicidadparallax" => $publicidadparallax[0],
			"productoslist" => $productosList,
			"enlaces" => $enlacesLista,
			"urlbase" => $rutabase,
			"clientes" => $customersLista,
			"pdf" => $catalogoLista,
		]);
	}
	
	public function contacto($section = null){
		$route = url('/');
		$rutabase = substr($route,0,strrpos($route,"contacto"))."admin/";
		$ruta = substr($route,0,strrpos($route,"contacto"))."admin/ushop/api/list/pictures/".base64_encode('3')."/".base64_encode("0");
		$banner = json_decode($this->procesarGET(url($ruta)))->respuesta;
		$especificaciones = NULL;
		if($banner!=NULL){
			$especificaciones = json_decode($banner,true);
		}
		
		$ruta = substr($route,0,strrpos($route,"contacto"))."admin/ushop/api/list/pictures/".base64_encode('3')."/".base64_encode("1");
		$catalogonline = json_decode($this->procesarGET(url($ruta)))->respuesta;
		$catalogoespecificaciones = NULL;
		if($catalogonline!=NULL){
			$catalogoespecificaciones = json_decode($catalogonline,true);
		}
		
		$ruta = substr($route,0,strrpos($route,"contacto"))."admin/ushop/api/list/pictures/".base64_encode('3')."/".base64_encode("2");
		$fijaimagen = json_decode($this->procesarGET(url($ruta)))->respuesta;
		$fijaimagenespecifica = NULL;
		if($fijaimagen!=NULL){
			$fijaimagenespecifica = json_decode($fijaimagen,true);
		}
		
		$ruta = substr($route,0,strrpos($route,"productos"))."admin/ushop/api/list/enlaces";
		$enlaces = json_decode($this->procesarGET(url($ruta)),true);
		$enlacesLista = NULL;
		if($enlaces!=NULL){
			$enlacesLista = $enlaces;
		}
		
		return view("frontend.contacto")->with([
			"uno" => "0",
			"especificaciones" => $especificaciones[0],
			"catalogoespecificaciones" => $catalogoespecificaciones[0],
			"fijaespecificaciones" => $fijaimagenespecifica[0],
			"enlaces" => $enlacesLista,
			"urlbase" => $rutabase,
			"section" => $section
		]);
	}
	
	public function productos($section = null){
		$route = url('/');
		$rutabase = substr($route,0,strrpos($route,"productos"))."admin/";
		$ruta = substr($route,0,strrpos($route,"productos"))."admin/ushop/api/list/pictures/".base64_encode('2')."/".base64_encode("0");
		$banner = json_decode($this->procesarGET(url($ruta)))->respuesta;
		$especificaciones = NULL;
		if($banner!=NULL){
			$especificaciones = json_decode($banner,true);
		}

		$ruta = substr($route,0,strrpos($route,"productos"))."admin/ushop/api/list/pictures/".base64_encode('2')."/".base64_encode("1");
		$banner = json_decode($this->procesarGET(url($ruta)))->respuesta;
		$tienda = NULL;
		if($banner!=NULL){
			$tienda = json_decode($banner,true);
		}

		$ruta = substr($route,0,strrpos($route,"productos"))."admin/ushop/api/list/tecnicas";
		$banner = json_decode($this->procesarGET(url($ruta)))->respuesta;
		$tecnicas = NULL;
		if($banner!=NULL){
			$tecnicas = json_decode($banner,true);
		}
				
		$ruta = substr($route,0,strrpos($route,"productos"))."admin/ushop/api/list/pictures/".base64_encode('2')."/".base64_encode("3");
		$importamosbanner = json_decode($this->procesarGET(url($ruta)))->respuesta;
		$bannercuatro = NULL;
		if($importamosbanner!=NULL){
			$bannercuatro = json_decode($importamosbanner,true);
		}
		
		$ruta = substr($route,0,strrpos($route,"productos"))."admin/ushop/api/list/pictures/".base64_encode('2')."/".base64_encode("4");
		$banner = json_decode($this->procesarGET(url($ruta)))->respuesta;
		$bannertres = NULL;
		if($banner!=NULL){
			$bannertres = json_decode($banner,true);
		}
		
		$ruta = substr($route,0,strrpos($route,"productos"))."admin/ushop/api/list/enlaces";
		$enlaces = json_decode($this->procesarGET(url($ruta)),true);
		$enlacesLista = NULL;
		if($enlaces!=NULL){
			$enlacesLista = $enlaces;
		}
		
		return view("frontend.productos")->with([
			"uno" => "0",
			"especificaciones" => $especificaciones[0],
			"tienda" => $tienda[0],
			"tecnicas" => $tecnicas,
			"bannertres" => $bannertres[0],
			"bannercuatro" => $bannercuatro[0],
			"enlaces" => $enlacesLista,
			"urlbase" => $rutabase,
			"section" => $section
		]);
	}
	
	public function empresa($section = null){
		$route = url('/');
		$rutabase = substr($route,0,strrpos($route,"empresa"))."admin/";
		$ruta = substr($route,0,strrpos($route,"empresa"))."admin/ushop/api/list/pictures/".base64_encode('1')."/".base64_encode("0");
		$banner = json_decode($this->procesarGET(url($ruta)))->respuesta;
		$bannerempresa = NULL;
		if($banner!=NULL){
			$bannerempresa = json_decode($banner,true);
		}
		
		$ruta = substr($route,0,strrpos($route,"empresa"))."admin/ushop/api/list/equipos";
		$banner = json_decode($this->procesarGET(url($ruta)))->respuesta;
		$equipo = NULL;
		if($banner!=NULL){
			$equipo = json_decode($banner,true);
		}
		
		$ruta = substr($route,0,strrpos($route,"productos"))."admin/ushop/api/list/servicios";
		$servicios = json_decode($this->procesarGET(url($ruta)))->respuesta;
		$serviciosLista = NULL;
		if($servicios!=NULL){
			$serviciosLista = json_decode($servicios,true);
		}
		
		$ruta = substr($route,0,strrpos($route,"productos"))."admin/ushop/api/list/customers";
		$customers = json_decode($this->procesarGET(url($ruta)))->respuesta;
		$customersLista = NULL;
		if($customers!=NULL){
			$customersLista = json_decode($customers,true);
		}
		
		$ruta = substr($route,0,strrpos($route,"productos"))."admin/ushop/api/list/enlaces";
		$enlaces = json_decode($this->procesarGET(url($ruta)),true);
		$enlacesLista = NULL;
		if($enlaces!=NULL){
			$enlacesLista = $enlaces;
		}
		
		$ruta = substr($route,0,strrpos($route,"productos"))."admin/ushop/api/list/galeria";
		$galeria = json_decode($this->procesarGET(url($ruta)))->respuesta;
		$galeriaservicios = []; 
		foreach($galeria as $key => $value){
			$galeriaservicios[$value->servicios_codigo][] = $value;
		}
		
		//$clientes = $this->procesarSVG($customersLista);
		return view("frontend.empresa")->with([
			"uno" => "0",
			"section" => $section,
			"urlbase" => $rutabase,
			"bannerempresa" => $bannerempresa[0],
			"enlaces" => $enlacesLista,
			"servicioslista" => $serviciosLista,
			"equipopictures" => $equipo,
			"clientes" => $customersLista,
			"galeriaservicios" => $galeriaservicios
		]);
	}

	public function quest($type = null, $item = null){
		$route = url('/');
		$especificaciones = NULL;
		if(strcmp($type, "pitem") == 0){
			$rutabase = $route."/admin/";
			$ruta = substr($route,0,strrpos($route,"quest"))."admin/ushop/api/list/pictures/".base64_encode('2')."/".base64_encode("0");
			$banner = json_decode($this->procesarGET(url($ruta)))->respuesta;
			$especificaciones = NULL;
			if($banner!=NULL){
				$especificaciones = json_decode($banner,true);
			}
			$tecnicas_in = Quest::tecnicas_in($item);
		}else{
			$rutabase = $route."/admin/";
			$ruta = substr($route,0,strrpos($route,"quest"))."admin/ushop/api/list/pictures/".base64_encode('1')."/".base64_encode("0");
			$banner = json_decode($this->procesarGET(url($ruta)))->respuesta;
			$especificaciones = NULL;
			if($banner!=NULL){
				$especificaciones = json_decode($banner,true);
			}	
		}	
				
		$found = Quest::searchfor($type,$item);
		if(is_null($found[0])){
			return redirect()->route('/');
		}
		$estilos = array();
		if(isset($tecnicas_in) && !is_null($tecnicas_in[0])){
			if(!is_null($tecnicas_in[0]->productos_estilo_one)){
				$res = Quest::tecnica_name($tecnicas_in[0]->productos_estilo_one);
				if(!is_null($res[0]))
					array_push($estilos,$res[0]->tecnica);
				else
					array_push($estilos,null);
			}
			if(!is_null($tecnicas_in[0]->productos_estilo_two)){
				$res = Quest::tecnica_name($tecnicas_in[0]->productos_estilo_two);
				if(!is_null($res[0]))
					array_push($estilos,$res[0]->tecnica);
				else
					array_push($estilos,null);
			}
			if(!is_null($tecnicas_in[0]->productos_estilo_thrid)){
				$res = Quest::tecnica_name($tecnicas_in[0]->productos_estilo_thrid);
				if(!is_null($res[0]))
					array_push($estilos,$res[0]->tecnica);
				else
					array_push($estilos,null);
			}
		}
		return view("frontend.quest")->with([
			"uno" => "0",
			"especificaciones" => $especificaciones[0],
			"urlbase" => $rutabase,
			"routebase" => $route,
			"type" => $type,
			"found" => $found[0],
			"estilos" => $estilos
		]);
	}

 	/*AUTOCOMPLETAR REGISTER*/
	 public function searchData(Request $request) {
		$term = strtolower($request->get('term'));
		
		$tecnicas = \DB::table('tecnicas')
		->where(\DB::raw('LOWER(tecnicas.tecnicas_name)'),"LIKE","%$term%")
		->orWhere(\DB::raw('LOWER(tecnicas.tecnicas_descripcion)'),"LIKE","%$term%")
		->select('tecnicas.tecnicas_name AS nombre',\DB::raw('TO_BASE64(tecnicas_codigo) AS titem'))
		->get()->toArray();
		$productos = \DB::table('productos')
		->where(\DB::raw('LOWER(productos.productos_name)'),"LIKE","%$term%")
		->orWhere(\DB::raw('LOWER(productos.productos_descripcion)'),"LIKE","%$term%")
		->orWhere(\DB::raw('LOWER(productos.productos_tipo)'),"LIKE","%$term%")
		->orWhere(\DB::raw('LOWER(productos.productos_estilo_one)'),"LIKE","%$term%")
		->orWhere(\DB::raw('LOWER(productos.productos_estilo_two)'),"LIKE","%$term%")
		->orWhere(\DB::raw('LOWER(productos.productos_estilo_thrid)'),"LIKE","%$term%")
		->select('productos.productos_name AS nombre',\DB::raw('TO_BASE64(productos_codigo) AS pitem'))
		->get()->toArray();
		$servicios = \DB::table('servicios')
		->where(\DB::raw('LOWER(servicios.servicios_name)'),"LIKE","%$term%")
		->orWhere(\DB::raw('LOWER(servicios.servicios_descripcion)'),"LIKE","%$term%")
		->select('servicios.servicios_name AS nombre',\DB::raw('TO_BASE64(servicios_codigo) AS sitem'))
		->get()->toArray();

		$resultado = array_merge($productos, $tecnicas,$servicios);
		return $resultado;
	}   	
}
