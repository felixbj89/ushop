<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Config;

class TrackerController extends Controller
{
	private static function resetCount($s){
		$sectiones = \DB::table("section")->where("section_type",$s)->get();
		$fechaActual = explode("-",explode(" ",Carbon::now(-4,"America/Caracas"))[0]);
		foreach($sectiones as $sec){
			$histFecha = explode("-",explode(" ",$sec->section_fecha)[0]);
			if(intval($histFecha[0]) < intval($fechaActual[0])){
				\DB::table("browser")->where("section_id",$sec->id)->delete();
				\DB::table("section")->where("section_codigo",$sec->section_codigo)->delete();
				if($s=="3"){
					\DB::table("section")->insert([
						"section_name" => "CONTACTO",
						"section_codigo" => str_random(10),
						"section_type" => "3",
						"section_visitas" => 0,
						"section_fecha" => Carbon::now(-4,"America/Caracas")
					]);
				}else if($s=="2"){
					\DB::table("section")->insert([
						"section_name" => "PRODUCTOS",
						"section_codigo" => str_random(10),
						"section_type" => "2",
						"section_visitas" => 0,
						"section_fecha" => Carbon::now(-4,"America/Caracas")
					]);
				}else if($s=="1"){
					\DB::table("section")->insert([
						"section_name" => "EMPRESA",
						"section_codigo" => str_random(10),
						"section_type" => "1",
						"section_visitas" => 0,
						"section_fecha" => Carbon::now(-4,"America/Caracas")
					]);
				}else{
					\DB::table("section")->insert([
						"section_name" => "HOME/INICIO",
						"section_codigo" => str_random(10),
						"section_type" => "0",
						"section_visitas" => 0,
						"section_fecha" => Carbon::now(-4,"America/Caracas")
					]);
				}
			}
		}
	}
	
	public static function saveIP($ip){
		$location = strtoupper(\Location::get($ip)->countryCode);
		$paiS = NULL;
		$codigo = NULL;
		if($location!=NULL){
			$codigo = $location;
			$pais = Config::get("pais.".$location);
		}else{
			$codigo = "XX";
			$pais = Config::get("pais.".strtoupper("OTROS")); 
		}
		
		\DB::table("location")->insert([
			"location_codigo" => $codigo,
			"location_pais" => $pais,
			"location_ip" => $ip,
		]);
	}
	
	public static function countBrowser($user_agent,$section){
		$browser = "";
		$navegador = "";
		
		/*if(strpos($user_agent, 'MSIE') !== FALSE)
			return 'Internet explorer';
		else */if(strpos($user_agent, 'Edge') !== FALSE){ //Microsoft Edge
			$navegador = "Microsoft Edge"; $browser = "1";
		}/*elseif(strpos($user_agent, 'Trident') !== FALSE) //IE 11
			return 'Internet explorer';
		elseif(strpos($user_agent, 'Opera Mini') !== FALSE)
			return "Opera Mini";
		elseif(strpos($user_agent, 'Opera') || strpos($user_agent, 'OPR') !== FALSE)
			return "Opera";*/
		else if(strpos($user_agent, 'Firefox') !== FALSE){
			$navegador = "Mozilla Firefox"; $browser = "0";
		}elseif(strpos($user_agent, 'Chrome') !== FALSE){
			$navegador = "Google Chrome"; $browser = "2";
		}/*elseif(strpos($user_agent, 'Safari') !== FALSE)
			return "Safari";*/
			
		$section = \DB::table("section")->where("section_type",$section)->first();
		\DB::table("browser")->insert([
			"browser_name" => $navegador,
			"browser_codigo" => str_random(10),
			"browser_type" => $browser,
			"section_id" => $section->id,
		]);
	}
	
	public static function countEmpresa(){
		$section = \DB::table("section")->where("section_type","1");
		//RESETO EL CONTADOR
		TrackerController::resetCount("1");
		$section->update([
			"section_name" => "EMPRESA",
			"section_codigo" => str_random(10),
			"section_type" => "1",
			"section_visitas" => ($section->first()->section_visitas+1),
			"section_fecha" => Carbon::now(-4,"America/Caracas")
		]);
	}
	
    public static function countContacto(){
		$section = \DB::table("section")->where("section_type","3");
		//RESETO EL CONTADOR
		TrackerController::resetCount("3");
		$section->update([
			"section_name" => "CONTACTO",
			"section_codigo" => str_random(10),
			"section_type" => "3",
			"section_visitas" => ($section->first()->section_visitas+1),
			"section_fecha" => Carbon::now(-4,"America/Caracas")
		]);
	}
	
	public static function countProductos(){
		$section = \DB::table("section")->where("section_type","2");
		//RESETO EL CONTADOR
		TrackerController::resetCount("2");
		$section->update([
			"section_name" => "PRODUCTOS",
			"section_codigo" => str_random(10),
			"section_type" => "2",
			"section_visitas" => ($section->first()->section_visitas+1),
			"section_fecha" => Carbon::now(-4,"America/Caracas")
		]);
	}
}
