<?php

namespace App\Http\Middleware;

use Closure;
use  App\Http\Controllers\TrackerController;

class TrackerWeb
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
		$section = $request->path();
		if($section=="empresa"){
			//VISITAS
			TrackerController::countEmpresa();
			
			//BROWSER
			$navegador = $_SERVER['HTTP_USER_AGENT'];
			TrackerController::countBrowser($navegador,"1");
		}else if($section=="productos"){
			//VISITAS
			TrackerController::countProductos();
			
			//BROWSER
			$navegador = $_SERVER['HTTP_USER_AGENT'];
			TrackerController::countBrowser($navegador,"2");
		}else if($section=="contacto"){
			//VISITAS
			TrackerController::countContacto();
			
			//BROWSER
			$navegador = $_SERVER['HTTP_USER_AGENT'];
			TrackerController::countBrowser($navegador,"3");
		}
		
		
		//SAVE IP
		TrackerController::saveIP($_SERVER["REMOTE_ADDR"]);
		return $next($request);
    }
}
