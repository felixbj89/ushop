<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quest extends Model
{
    public static function searchfor($type,$item){
        $table = "";
        switch ($type) {
            case "titem":
                $table = "tecnicas";
                break;
            case "pitem":
                $table = "productos";
                break;
            case "sitem":
                $table = "servicios";
                break;
        }
        if(strcmp($table, "") !== 0){
            $find = \DB::table($table)
            ->where($table."_codigo",base64_decode($item))
            ->first();
            return [$find,200];
		}else{
            return [NULL,200];
		}
    }

    public static function tecnicas_in($item){
        if(strcmp($item, "") !== 0){
            $find = \DB::table("productos")
            ->where("productos_codigo",base64_decode($item))
            ->select("productos_estilo_one","productos_estilo_two","productos_estilo_thrid")
            ->first();
            return [$find,200];
		}else{
            return [NULL,200];
		}
    }

    public static function tecnica_name($item){
        if(strcmp($item, "") !== 0){
            $find = \DB::table("tecnicas")
            ->where("tecnicas_name",$item)
            ->select(\DB::raw('TO_BASE64(tecnicas.tecnicas_codigo) AS tecnica'))
            ->first();
            return [$find,200];
		}else{
            return [NULL,200];
		}
    }
          
}
