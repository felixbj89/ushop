<?php
return [

    /*
    |--------------------------------------------------------------------------
    | Ushop Language Lines
    |--------------------------------------------------------------------------
    | <b>…</b> Poner texto en negrita
    | <i>…</i> Poner texto en cursiva
    | <u>…</u> Poner texto subrayado
    | <br/> Salto de linea
    */
//@lang('ushop.')
    'cerrar' => 'Cerrar',
    /* INDEX */
    'titulo_servicios' => 'Nos <b>AGRUPAMOS</b> para ampliar<br/>el <b><i>abanico</i></b> de opciones que te ofrecemos',
    'servi_muebleria' => 'Diseñamos y armamos diferentes tipos de mobiliarias para exposiciones de productos y diferentes eventos.',
    'servi_eco' => 'Acordes con la necesidad del mercado actual ofrecemos gran variedad de productos ecológicos aplicados a las técnicas que manejamos.',
    'servi_impresos' => 'Ofrecemos diferentes productos de imprenta acordes con las necesidades de nuestros clientes con la mejor calidad.',
    'servi_eventos' => 'Planificamos y gestionamos eventos de distinta índole para tu empresa.',
    'servi_seguridad' => 'Equipamiento de seguridad personalizados y con los más altos estándares.',
    'servi_medios' => 'Gestionamos tus redes sociales, sitio web, campañas SEM y posicionamos tu marca en el entorno digital.',
    'servi_gourmet' => 'Personaliza tus regalos con los mejores productos premium elaborados en Chile.',
	"servi_pop" => "Líderes en el mercado de artículos publicitarios, contamos con la más amplia variedad, la mejor calidad y la mayor experiencia.",
    'btn_compras' => 'Ir al Catálogo <b><i>Online.</i></b>',
    'contacto_texto' => 'Te <b><i>escuchamos</i></b> y buscamos ajustarnos a tu presupuesto',
    'contacto_btn' => 'Deseo contactar',
    'titulo_pack' => 'Somos <b>Creadores</b> y <b>Productores</b><br/>el <b><i>Pack ideal</i></b> para tus requerimientos',
    'pack_tituloI' => 'CLIENTES',
    'pack_pI' => 'Mas de 1.500 clientes a lo largo de todo Chile avalan el compromiso que afianzamos con ellos al darles los mejores productos.',
    'pack_tituloII' => 'EXPERIENCIA',
    'pack_pII' => 'Por mas de 18 años hemos trabajado de la mano de los mejores expertos para asegurar un producto de calidad.',
    'pack_tituloIII' => 'PRODUCTOS',
    'pack_pIII' => 'Contamos con un stock de mas de 10.000 productos dispuestos para plasmar tu logotipo con las diferentes técnicas que usamos.',
    'titulo_clientes' => 'Algunos de Nuestros <b><i>CLIENTES</i></b>',
    /* EMPRESA */
    'titulo_valoresI' => 'Misión',
    'valores_pI' => 'Convertirnos en <b><i>la solución</i></b> publicitaria para todas las diferentes empresas del mercado ofreciendoles todo un conjunto de posibilidades para hacer reales sus ideas publicitarias, con la mejor calidad en productos nacionales e importados.',
    'titulo_valoresII' => 'Visión',
    'valores_pII' => 'Ser la empresa numero uno en Chile a nivel de publicidad y <b><i>productos publicitarios</i></b>, ofreciendo los mejores productos del mercado y la mejor calidad.',
    'titulo_valoresIII' => 'Protecciones',
    'valores_pIII' => 'Nos movemos en un mercado bastante competitivo pero 18 años nos dan la experiencia para manejar cualquier tipo de medio publicitario con <b><i>la mejor calidad</i></b>. Cada vez nos arriesgamos más en ofrecerles soluciones a todos requerimientos de nuestros clientes de la mano de <b><i>profesionales comprometidos</i></b> con sus proyectos.',
    'btn_vgaleria' => 'Ver Galeria',
    'btn_toproduct' => 'ir a Productos',
    'titulo_equipo' => 'Ellos son el <b class="rojo">MOTOR</b> de <b><i>Nuestra Empresa</i></b>',
    'equipo_I' => 'Creativos',
    'equipo_II' => 'Producción',
    'equipo_III' => 'Administrativos',
    'equipo_IV' => 'Ventas',
    'equipo_V' => 'Directivos',
    'navproductos_I' => 'Merchandising',
    'navproductos_II' => 'Productos Eco',
    'navproductos_III' => 'Gourmet',
    'navproductos_IV' => 'Eventos',
    'navproductos_V' => 'Medios Digitales',
    'navproductos_VI' => 'Mueblería',
    'navproductos_VII' => 'Seguridad',
    'navproductos_VIII' => 'Impresos',
    /* PRODUCTOS */
    'titulo_tecnica' => 'Conoce más sobre las <b class="rojo">TÉCNICAS</b> que utilizamos en <b><i>Nuestra Empresa</b></i>',
    'titulo_importar' => 'Importamos nuestros productos segun tús <b class="rojo"><i>NECESIDADES</i></b>',
    'linktecnina_I' => 'Serigrafía',
    'linktecnina_II' => 'Tampografía',
    'linktecnina_III' => 'Grabado Láser',
    'linktecnina_IV' => 'Pantografía',
    'linktecnina_V' => 'Sublimación',
    'linktecnina_VI' => 'Bordado',
    'linktecnina_VII' => 'Transfer',
    'catalogo_oline' => '¿Sabes de Nuestros Productos? Ve nuestro',
    /* CONTACTO */
    'titulo_contacto' => 'Cotiza online con <b>Nosotros</b>',
    'btn_eviar' => 'ENVIAR',
    'label_' => 'Nombre',
    'label_I' => 'Nombre o Empresa',
    'label_II' => 'Dirección',
    'label_III' => 'RUT',
    'label_IV' => 'Región',
    'label_V' => 'Correo Electrónico',
    'label_VI' => 'Ciudad',
    'label_VII' => 'Teléfono o Celular',
    'label_VIII' => 'Contacto',
    'label_IX' => 'Requerimiento',
    'label_X' => 'Sube tu Curriculum',
    'label_XI' => 'Contactenme',
    'label_XII' => 'El archivo no debe superar los 24 MB.',
    'label_XIII' => 'ENVIANDO...',
    'banner_equipo' => '¿Quieres formar parte de Nuestro Equipo? Aplica',
    'modalEquipo_titulo' => '¿Quieres formar parte de Nuestro Equipo?',
    /* FOOTER */
    'footer_direccion' => 'Loteo Industrial Los Libertadoders, Carretera San Martin 16500, Calle Javiera Carrera, Sitio 115 Colina',
    'footer_phone' => '+56 2 2596 1900',
    'footer_contacto' => 'contacto@ushop.cl',
];