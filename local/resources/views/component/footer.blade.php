<div class="row">
	<div class="col col-sm-11 self-center">
        <div class="row">
            <div class="col-12" style="height: 5px;">
                <hr class="self-center" id="footertop"/>
            </div>
            <div class="col-12">
                <div class="row h100">
                    <div id="collapseFooter" class="col-11 self-center col-sm-12 col-md-7  col-xl-6 order-2 order-md-1 align-self-center collapse show">
                        <div class="row">
                            <div class="col-sm-6 col-md-4">
                                <h4><small>Home</small></h4>
                                <p><small><a class="scroll-link" href="{{url('/')}}">Home</a></small></p>
                                <p><small><a href="{{url('home/services')}}">Reseña</a></small></p>
                                <p><small><a href="{{url('home/produts')}}">Promociones</a></small></p>
                                <p><small><a href="{{url('home/packideal')}}">Categorias</a></small></p>
                                <p><small><a href="{{url('home/customers')}}">Clientes</a></small></p>
                            </div>
                            <div class="col-sm-6 col-md-4">
                                <h4><small>Empresa</small></h4>
                                <p><small><a href="{{url('empresa/empresa')}}">Misión, Visión y Proyecciones</a></small></p>
                                <p><small>Segmentos Ushop</small></p>
                                <!-- sublist -->
                                <p><small class="sublist"><a href="{{url('empresa/materialpop')}}">Material P.O.P</a></small></p>
                                <p><small class="sublist"><a href="{{url('empresa/gourmet')}}">Gourmet</a></small></p>
                                <p><small class="sublist"><a href="{{url('empresa/impresos')}}">Impresos</a></small></p>
                                <p><small class="sublist"><a href="{{url('empresa/eventos')}}">Eventos</a></small></p>
                                <p><small class="sublist"><a href="{{url('empresa/muebleria')}}">Muebleria</a></small></p>
                                <p><small class="sublist"><a href="{{url('empresa/productoseco')}}">Productos Eco</a></small></p>
                                <p><small class="sublist"><a href="{{url('empresa/digitales')}}">Medios Digitales</a></small></p>
                                <p><small class="sublist"><a href="{{url('empresa/seguridad')}}">Seguridad</a></small></p>
                                <!-- sublist -->
                                <p><small><a href="{{url('empresa/equipo')}}">Nuestro equipo</a></small></p>
                                <p><small><a href="{{url('empresa/customers')}}">Clientes</a></small></p>
                            </div>
                            <div class="col-sm-6 col-md-4">
                                <h4><small>Productos</small></h4>
                                <p><small><a href="http://cotizador.ushop.cl" target="_blank">Catálogo Online</a></small></p>
                                <p><small><a href="#">Compras</a></small></p>
                                <p><small><a href="{{url('productos/tecnicas')}}">Técnicas Publicitarias</a></small></p>
                                <p><small><a href="{{url('productos/importadores')}}">Importaciones</a></small></p>
                                <p><small><a href="{{url('productos/formcontacto')}}">Cotiza Online</a></small></p>
                                <br/>
                                <h4><small>Contacto</small></h4>
                                <p><small><a data-toggle="modal" href="#modalCurriculum">Trabaja con nosotros</a></small></p>
                            </div>
                            <div class="col">
								@if(isset($pdf['catalogo_ruta']))
									<p class="rojo"><a href="{{$urlbase.'img/catalogo/'.$pdf['catalogo_ruta']}}" target="_blank"><b>Descargar Catalogo</b></a></small></p>
								@endif
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-5 col-xl-6 order-1 order-md-2 align-self-center">
                        <div class="row">
                            <div class="col-2 align-self-center" style="margin-left: 10px;">
                                <button id="btnCollapsefooter" class="btn" type="button" data-toggle="collapse" data-target="#collapseFooter" aria-expanded="false" aria-controls="collapseFooter"></button>
                            </div>
                            <div id="borderfooter" class="col-10 col-sm-10 col-md-12">
                                <div class="row">
                                    <div class="col">
                                        <p><small><i class="fas fa-map-marker-alt"></i> &nbsp; @lang('ushop.footer_direccion')</small></p>
                                        <p><small><i class="fas fa-phone"></i> &nbsp; @lang('ushop.footer_phone')</small></p>
                                        <p><small><i class="fas fa-envelope"></i> &nbsp; <a href="mailto:@lang('ushop.footer_contacto')">@lang('ushop.footer_contacto')</a></small></p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <ul class="list-inline m0 d-flex justify-content-end">
                                            <li class="list-inline-item">
                                                <a class="social menu_face" href="https://www.facebook.com/UshopCL/" target="_blank">
                                                    <!--span class="fa-layers fa-fw">
                                                        <i class="far fa-circle" style="font-size:20px;"></i-->
                                                        <i class="fab fa-facebook-f"></i>
                                                    <!--/span-->
                                                </a>
                                            </li>
                                            <li class="list-inline-item">
                                                <a class="social insta" href="https://www.instagram.com/ushop.cl/?hl=es-la" target="_blank">
                                                    <!--span class="fa-layers fa-fw">
                                                        <i class="far fa-circle" style="font-size:20px;"></i-->
                                                        <i class="fab fa-instagram"></i>
                                                    <!--/span-->
                                                </a>
                                            </li>
                                            <li class="list-inline-item">
                                                <a class="social lind" href="https://www.linkedin.com/in/ushop-cl-68266215b/" target="_blank">
                                                    <!--span class="fa-layers fa-fw">
                                                        <i class="far fa-circle" style="font-size:20px;"></i-->
                                                        <i class="fab fa-linkedin-in"></i>
                                                    <!--/span-->
                                                </a>
                                            </li>
                                        </ul>                      
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>