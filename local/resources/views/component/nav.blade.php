<div class="row">
	<div id="cc_navb" class="col col-sm-11 self-center">
        <div class="row">
	        <div id="c_navb1" class="col-12 col-xl-11 p0 flex justify-content-end">
                <nav id="navb1" class="navbar  flex justify-content-end">
                    <ul class="list-inline">
                        <li class="fixed_s list-inline-item">
                            <a href="{{url('/')}}">
								<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
									 width="22px" height="22px" viewBox="295 385 22 22" enable-background="new 295 385 22 22" xml:space="preserve">
								<g>
									<g>
										<path fill="#A40000" d="M313.395,395.78c-0.352,0-0.615,0.264-0.615,0.615v8.658h-4.527v-3.516c0-0.352-0.264-0.615-0.615-0.615
											h-3.252c-0.352,0-0.66,0.308-0.66,0.615v3.516h-4.439v-8.351c0-0.352-0.264-0.615-0.615-0.615c-0.352,0-0.616,0.264-0.616,0.615
											v9.011c0,0.352,0.264,0.615,0.616,0.615h5.758c0.352,0,0.615-0.264,0.615-0.615v-3.517h2.022v3.517
											c0,0.352,0.264,0.615,0.615,0.615h5.714c0.352,0,0.615-0.264,0.615-0.615v-9.318C313.966,396.044,313.702,395.78,313.395,395.78
											L313.395,395.78z"/>
										<path fill="#A40000" d="M316.823,396.264l-5.011-5.011v-3.429c0-0.352-0.308-0.615-0.659-0.615s-0.615,0.264-0.615,0.615v2.154
											l-4.088-4.131c-0.132-0.132-0.264-0.176-0.44-0.176s-0.308,0.088-0.439,0.176l-10.373,10.329c-0.264,0.264-0.264,0.659,0,0.879
											c0.22,0.264,0.616,0.264,0.879,0l9.933-9.89l9.89,9.978c0.088,0.088,0.264,0.176,0.439,0.176s0.308-0.044,0.439-0.176
											C317.043,396.923,317.087,396.527,316.823,396.264L316.823,396.264z"/>
									</g>
								</g>
								</svg>
							</a>
                        </li>
                        <li class="fixed_h list-inline-item">
                            <form class="form-inline" style="top: -2px;">
                                <input id="searchString" class="form-control" type="text" aria-label="Search">
                                <i class="spinner fas fa-spin fa-circle-notch"></i>
                                <span id="btnsearch" class="btnsearch btn-alt">
									<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
										 width="15px" height="15px" viewBox="0 0 26.333 27.9" enable-background="new 0 0 26.333 27.9" xml:space="preserve">
									<g>
										<g>
											<path fill="#4B4B4B" d="M23.771,22.174l-6.02-5.976c0.044-0.088,0.088-0.22,0.132-0.396c0.615-1.054,1.011-2.24,1.143-3.471
												c0.044-0.264,0.044-0.57,0.044-0.878c0-0.923-0.176-1.89-0.483-2.769c-0.396-1.186-1.099-2.24-1.977-3.163
												C13.402,2.228,7.911,2.228,4.66,5.479C3.561,6.577,2.858,7.852,2.462,9.301c-0.396,1.45-0.396,2.899,0,4.35
												s1.099,2.724,2.197,3.822c1.625,1.626,3.778,2.46,5.975,2.46c1.714,0,3.207-0.482,4.657-1.361l0.22-0.132l6.063,6.019
												c0.22,0.22,0.483,0.352,0.703,0.396c0.044,0,0.088,0,0.132,0c0.176,0,0.308-0.044,0.439-0.132c0.176-0.088,0.352-0.22,0.527-0.439
												l0.264-0.264c0.176-0.176,0.308-0.352,0.439-0.527c0.088-0.132,0.088-0.263,0.088-0.482v-0.088
												C24.167,22.657,24.035,22.438,23.771,22.174L23.771,22.174z M17.356,13.255c-0.308,1.187-0.922,2.241-1.801,3.119
												c-1.318,1.318-3.031,2.021-4.877,2.021c-1.845,0-3.559-0.747-4.921-2.021c-2.68-2.504-2.68-7.249-0.044-9.797
												c1.318-1.274,3.076-2.065,4.921-2.065c1.934,0,3.559,0.703,4.877,2.065c0.879,0.879,1.493,1.933,1.801,3.119
												C17.664,10.883,17.664,12.069,17.356,13.255L17.356,13.255z"/>
										</g>
									</g>
									</svg>
                                </span>
                            </form>
                        </li>                        
                        <li class="fixed_h list-inline-item">
                            <a class="social menu_face" href="https://www.facebook.com/UshopCL/" target="_blank">
                                <!--span class="fa-layers fa-fw"-->
                                    <!--i class="far fa-circle" style="font-size:20px;"></i-->
                                    <i class="fab fa-facebook-f"></i>
                                <!--/span-->
                            </a>
                        </li>
                        <li class="fixed_h list-inline-item">
                            <a class="social insta" href="https://www.instagram.com/ushop.cl/?hl=es-la" target="_blank">
                                <!--span class="fa-layers fa-fw">
                                    <i class="far fa-circle" style="font-size:20px;"></i-->
                                    <i class="fab fa-instagram"></i>
                                <!--/span-->
                            </a>
                        </li>
                        <li class="fixed_h list-inline-item">
                            <a class="social lind" href="https://www.linkedin.com/in/ushop-cl-68266215b/" target="_blank">
                                <!--span class="fa-layers fa-fw"-->
                                    <!--i class="far fa-circle" style="font-size:20px;"></i-->
                                    <i class="fab fa-linkedin-in"></i>
                                <!--/span-->
                            </a>
                        </li>
                    </ul>
                </nav>  
            </div> 
            <div id="c_navb2" class="col-12 col-xl-11 p0 p0 flex justify-content-end">
                <nav id="navb2" class="navbar  flex justify-content-end">
                    <ul class="list-inline">
                        <li class="fixed_s list-inline-item">
                            <form class="form-inline">
                                <input id="searchStringI" class="form-control" type="text" aria-label="Search">
                                <i class="spinner fas fa-spin fa-circle-notch text-white"></i>
                                <span id="btnsearchI" class="btnsearch"><i class="fas fa-search"></i></span>
                            </form>
                        </li>  
                        <li class="fixed_h list-inline-item">
                            <a href="{{url('/')}}"><img src="{{url('img/home.svg')}}" class="">
								<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
									 width="22px" height="22px" viewBox="295 385 22 22" enable-background="new 295 385 22 22" xml:space="preserve">
								<g>
									<g>
										<path fill="#A40000" d="M313.395,395.78c-0.352,0-0.615,0.264-0.615,0.615v8.658h-4.527v-3.516c0-0.352-0.264-0.615-0.615-0.615
											h-3.252c-0.352,0-0.66,0.308-0.66,0.615v3.516h-4.439v-8.351c0-0.352-0.264-0.615-0.615-0.615c-0.352,0-0.616,0.264-0.616,0.615
											v9.011c0,0.352,0.264,0.615,0.616,0.615h5.758c0.352,0,0.615-0.264,0.615-0.615v-3.517h2.022v3.517
											c0,0.352,0.264,0.615,0.615,0.615h5.714c0.352,0,0.615-0.264,0.615-0.615v-9.318C313.966,396.044,313.702,395.78,313.395,395.78
											L313.395,395.78z"/>
										<path fill="#A40000" d="M316.823,396.264l-5.011-5.011v-3.429c0-0.352-0.308-0.615-0.659-0.615s-0.615,0.264-0.615,0.615v2.154
											l-4.088-4.131c-0.132-0.132-0.264-0.176-0.44-0.176s-0.308,0.088-0.439,0.176l-10.373,10.329c-0.264,0.264-0.264,0.659,0,0.879
											c0.22,0.264,0.616,0.264,0.879,0l9.933-9.89l9.89,9.978c0.088,0.088,0.264,0.176,0.439,0.176s0.308-0.044,0.439-0.176
											C317.043,396.923,317.087,396.527,316.823,396.264L316.823,396.264z"/>
									</g>
								</g>
								</svg>
							</a>
                        </li>
                        <li class="list-inline-item @yield('active-empresa')">
                            <a href="{{url('empresa')}}">EMPRESA</a>
                        </li>
                        <li class="list-inline-item @yield('active-productos')">
                            <a href="{{url('productos')}}">PRODUCTOS</a>
                        </li>
                        <li class="list-inline-item @yield('active-contacto')">
                            <a href="{{url('contacto')}}">CONTACTO</a>
                        </li>
						<li class="fixed_s list-inline-item">
							@if(isset($enlaces['AI']))
								<a href="{{$enlaces['AI']}}" target="_blank">CATÁLOGO</a>
							@else
								<a href="#">CATÁLOGO</a>
							@endif
                        </li>
                    </ul>
                </nav>
                <div id="c_btnin" class="fixed_h solo">
					@if(isset($enlaces['AI']))
						<a href="{{$enlaces['AI']}}" target="_blank" class="btn-gray-bld">CATÁLOGO</a>    
					@else
						<a href="#"  target="_blank" class="btn-gray-bld">CATÁLOGO</a>    
					@endif
                </div>
            </div>    
        </div>
        <a href="{{url('/')}}" class="logoUshop"></a>
    </div>
    <div id="cc_navbrespon" class="col-12 self-center">
        <div class="row">
            <div class="col-12">
                <div href="{{url('/')}}" class="logoUshop responsiveLogo self-center"></div>
            </div>
            <div class="col-12">
                <div class="collapse" id="navbarToggleExternalContent">
                    <ul class="navbar-nav"> 
                        <li class="nav-item nav-ushop @yield('active-empresa')">
                            <a href="{{url('empresa')}}">EMPRESA</a>
                        </li>
                        <li class="nav-item nav-ushop @yield('active-productos')">
                            <a href="{{url('productos')}}">PRODUCTOS</a>
                        </li>
                        <li class="nav-item nav-ushop @yield('active-contacto')">
                            <a href="{{url('contacto')}}">CONTACTO</a>
                        </li>
                        <li><hr/></li>
                        <li class="nav-item">
							@if(isset($enlaces['AI']))
								<a href="{{$enlaces['AI']}}" id="btnrespin" target="_blank" class="btn-gray-bld">CATÁLOGO</a>    
							@else
								<a href="#" id="btnrespin" class="btn-gray-bld">CATÁLOGO</a>
							@endif
                            <ul id="socialreposn" class="list-inline float-right">                     
                                <li class="list-inline-item">
                                    <a class="social menu_face" href="https://www.facebook.com/UshopCL/" target="_blank">
                                        <i class="fab fa-facebook-f"></i>
                                    </a>
                                </li>
                                <li class="list-inline-item">
                                    <a class="social insta" href="https://www.instagram.com/ushop.cl/?hl=es-la" target="_blank">
                                        <i class="fab fa-instagram"></i>
                                    </a>
                                </li>
                                <li class="list-inline-item">
                                    <a class="social lind" href="https://www.linkedin.com/in/ushop-cl-68266215b/" target="_blank">
                                        <i class="fab fa-linkedin-in"></i>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li><hr/></li>
                        <li class="nav-item">
                            <form class="form-inline">
                                <input id="searchStringII" class="form-control" type="text" aria-label="Search" autocomplete="on">
                                <i class="spinner fas fa-spin fa-circle-notch text-white"></i>
                                <span id="btnsearchII" class="btnsearch" style="right: 15px;"><i class="fas fa-search"></i></span>
                            </form>
                        </li>                        
                    </ul>
                </div>
                <nav id="responav" class="navbar justify-content-end">
                    <a href="{{url('/')}}"><svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
									 width="22px" height="22px" viewBox="295 385 22 22" enable-background="new 295 385 22 22" xml:space="preserve">
								<g>
									<g>
										<path fill="#FFFFFF" d="M313.395,395.78c-0.352,0-0.615,0.264-0.615,0.615v8.658h-4.527v-3.516c0-0.352-0.264-0.615-0.615-0.615
											h-3.252c-0.352,0-0.66,0.308-0.66,0.615v3.516h-4.439v-8.351c0-0.352-0.264-0.615-0.615-0.615c-0.352,0-0.616,0.264-0.616,0.615
											v9.011c0,0.352,0.264,0.615,0.616,0.615h5.758c0.352,0,0.615-0.264,0.615-0.615v-3.517h2.022v3.517
											c0,0.352,0.264,0.615,0.615,0.615h5.714c0.352,0,0.615-0.264,0.615-0.615v-9.318C313.966,396.044,313.702,395.78,313.395,395.78
											L313.395,395.78z"/>
										<path fill="#FFFFFF" d="M316.823,396.264l-5.011-5.011v-3.429c0-0.352-0.308-0.615-0.659-0.615s-0.615,0.264-0.615,0.615v2.154
											l-4.088-4.131c-0.132-0.132-0.264-0.176-0.44-0.176s-0.308,0.088-0.439,0.176l-10.373,10.329c-0.264,0.264-0.264,0.659,0,0.879
											c0.22,0.264,0.616,0.264,0.879,0l9.933-9.89l9.89,9.978c0.088,0.088,0.264,0.176,0.439,0.176s0.308-0.044,0.439-0.176
											C317.043,396.923,317.087,396.527,316.823,396.264L316.823,396.264z"/>
									</g>
								</g>
								</svg></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarToggleExternalContent" aria-controls="navbarToggleExternalContent" aria-expanded="false" aria-label="Toggle navigation"></button>
                </nav>           
            </div>
        </div>
    </div>
</div>