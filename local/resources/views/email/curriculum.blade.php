<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0">
    <title>Contacto</title>
</head>
<body>
    <p>Hola! Se ha hecho un nuevo contacto.</p>
    <p>Estos son los datos del usuario que ha realizado el contacto:</p>
    <ul>
        <li>nombre: {{e($nombre)}}</li>
        <li>correo: {{e($correo)}}</li>
    </ul>
</body>
</html>