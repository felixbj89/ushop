<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Ushop | 404</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link rel="shortcut icon" type="img/png" href="{{asset('img/logo/fav.png')}}">
        <link rel="stylesheet" href="{{asset('plugins/font-awesome-4.7.0/css/font-awesome.min.css')}}">
        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .sub-title {
                font-size: 42px;
            }
			
			.links{
				position: relative;
				top: 2em;
			}
			
            .links > a {
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
				background-color:#ffffff;
				padding: 1rem;
				border-radius: 1rem;
				color: #a40000;
            }

            .links > a:hover{
              background-color:#a40000;
              padding: 1rem;
              border-radius: 1rem;
			  color: #ffffff;
            }

            .links > a.salir:hover::after{
                content: " Regresar";
            }

            .m-b-md {
                margin-bottom: 30px;
            }
			
			.background{
				background:url(./img/404.jpg) no-repeat;
				background-size:100% 100%;
				display: flex;
				width: 100%;
				flex-wrap: wrap;
				height: 100%;
				justify-content: center;
				align-items: center;
			}
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            <div class="content background">
				<div class="sub-title m-b-md">
					
                </div>
                <div class="links">
                    <a id="clicksalir" href="{{url('/')}}" class="salir"><i class="fa fa-undo fa-2x"></i></a>
                </div>
			</div>
		</div>
		<script src="{{ asset('plugins/fontawesome-free-5.0.13/svg-with-js/js/fontawesome-all.min.js') }}"></script>
    </body>
</html>
