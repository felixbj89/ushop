<div class="row">
    <div class="col align-self-center">
        <div class="row">
            <div class="col-11 col-md-10 col-lg-11 self-center">
                <h5>@lang('ushop.titulo_contacto')</h5>    
                <form id="fontactoform">
                    <div class="row m0">
                        <div class="col-md-6">
                            <div class="form-group row justify-content-start">
                                <label for="name" class="col-form-label-sm text-right"><h6>@lang('ushop.label_I')</h6></label>
                                <div class="col-12 max400">
                                    <input type="text" class="form-control form-control-sm" id="name" name="name" required placeholder="">
                                    <span class="spanvalidate isvalid"><i class="fas fa-check"></i></span>
                                    <span class="spanvalidate novalid"><i class="fas fa-exclamation-triangle"></i></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group row justify-content-end">
                                <label for="address" class="col-form-label-sm text-right"><h6>@lang('ushop.label_II')</h6></label>
                                <div class="col-12 max400">
                                    <input type="text" class="form-control form-control-sm" id="address" name="address" required placeholder="">
                                    <span class="spanvalidate isvalid"><i class="fas fa-check"></i></span>
                                    <span class="spanvalidate novalid"><i class="fas fa-exclamation-triangle"></i></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row m0">
                        <div class="col-md-6">
                            <div class="form-group row justify-content-start">
                                <label for="rut" class="col-form-label-sm text-right"><h6>@lang('ushop.label_III')</h6></label>
                                <div class="col-12 max400">
                                    <input type="text" class="form-control form-control-sm" id="rut" name="rut" required placeholder="">
                                    <span class="spanvalidate isvalid"><i class="fas fa-check"></i></span>
                                    <span class="spanvalidate novalid"><i class="fas fa-exclamation-triangle"></i></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group row justify-content-end">
                                <label for="region" class="col-form-label-sm text-right"><h6>@lang('ushop.label_IV')</h6></label>
                                <div class="col-12 max400">
                                    <input type="text" class="form-control form-control-sm" id="region" name="region" required placeholder="">
                                    <span class="spanvalidate isvalid"><i class="fas fa-check"></i></span>
                                    <span class="spanvalidate novalid"><i class="fas fa-exclamation-triangle"></i></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row m0">
                        <div class="col-md-6">
                            <div class="form-group row justify-content-start">
                                <label for="email" class="col-form-label-sm text-right"><h6>@lang('ushop.label_V')</h6></label>
                                <div class="col-12 max400">
                                    <input type="text" class="form-control form-control-sm" id="email" name="email" required placeholder="">
                                    <span class="spanvalidate isvalid"><i class="fas fa-check"></i></span>
                                    <span class="spanvalidate novalid"><i class="fas fa-exclamation-triangle"></i></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group row justify-content-end">
                                <label for="city" class="col-form-label-sm text-right"><h6>@lang('ushop.label_VI')</h6></label>
                                <div class="col-12 max400">
                                    <input type="text" class="form-control form-control-sm" id="city" name="city" required placeholder="">
                                    <span class="spanvalidate isvalid"><i class="fas fa-check"></i></span>
                                    <span class="spanvalidate novalid"><i class="fas fa-exclamation-triangle"></i></span>
                                </div>
                            </div>
                        </div>
                    </div>        
                    <div class="row m0">
                        <div class="col-md-6">
                            <div class="form-group row justify-content-start">
                                <label for="phone" class="col-form-label-sm text-right"><h6>@lang('ushop.label_VII')</h6></label>
                                <div class="col-12 max400">
                                    <input type="text" class="form-control form-control-sm" id="phone" name="phone" required placeholder="">
                                    <span class="spanvalidate isvalid"><i class="fas fa-check"></i></span>
                                    <span class="spanvalidate novalid"><i class="fas fa-exclamation-triangle"></i></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group row justify-content-end">
                                <label for="contact" class="col-form-label-sm text-right"><h6>@lang('ushop.label_VIII')</h6></label>
                                <div class="col-12 max400">
                                    <input type="text" class="form-control form-control-sm" id="contact" name="contact" required placeholder="">
                                    <span class="spanvalidate isvalid"><i class="fas fa-check"></i></span>
                                    <span class="spanvalidate novalid"><i class="fas fa-exclamation-triangle"></i></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row m0">
                        <div class="col-12">
                            <div class="form-group row justify-content-end">
                                <label for="request" class="col-form-label-sm text-right"><h6>@lang('ushop.label_IX')</h6></label>
                                <div class="col max1050">
                                    <textarea class="form-control" id="request" name="request" rows="3" required placeholder=""></textarea>
                                    <span class="spanvalidate isvalid"><i class="fas fa-check"></i></span>
                                    <span class="spanvalidate novalid"><i class="fas fa-exclamation-triangle"></i></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row m0">
                        <div class="col-12">
                            <div class="form-group row">
                                <div class="col-12">
                                    <!--input class="btn-gray-bld self-center" type="button" id="su_contact" value="@lang('ushop.btn_eviar')"/-->
                                    <input id="su_contact" type="button" class="btn-gray-bld self-center" value="@lang('ushop.btn_eviar')"  disabled/>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- ENVIANDO -->
        <div id="sending" class="row">
            <div class="col-11 self-center alert alert-dismissible" role="alert">
                <di class="row h100">
                    <div class="col-12">
                        <div class="logobanner self-center" style="background-image: url(img/logosimple_ushop.svg);"></div>
                        <button id="dismis" type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button> 
                    </div>
                    <div class="col-12 align-self-center evn">
                        <div class="row">
                            <div class="col-12 fa-2x text-center">
                                <i class="fas fa-spinner fa-pulse"></i>
                            </div>
                            <div class="col-12 text-center">
                                ENVIANDO...
                            </div>
                        </div>
                    </div>
                    <div class="col-12 align-self-center mensaje text-center">
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>