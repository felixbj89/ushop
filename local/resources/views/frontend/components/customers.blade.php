<div class="row">
	<div class="col-12 align-self-center text-center">
		<h5>@lang('ushop.titulo_clientes')</h5>
		<div class="owl-carousel col-11 self-center">
			@for($i = 0; $i < count($clientes); $i++)
				@if(strcmp($clientes[$i]['customers_logo_on'], 'img/defaults/defaults.svg') !== 0)
					<div class="w-250 my-1 px-3">
						<img src="{{asset('admin/'.$clientes[$i]['customers_logo_on'])}}"/>
					</div>
				@endif
			@endfor
		</div>
	</div>
</div>