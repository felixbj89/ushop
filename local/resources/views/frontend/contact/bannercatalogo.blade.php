<div class="row">
    <div class="col align-self-center">
        <div class="row">
            <div class="col-11 col-md-10 col-lg-11 self-center">
				@if(isset($catalogoespecificaciones["textos"]) && $catalogoespecificaciones!=NULL)
					<h5 class="text-center self-center">{!!base64_decode($catalogoespecificaciones["textos"])!!} APLICA <a data-toggle="modal" href="#modalCurriculum">AQUI</a></h5>  
				@else
					<h5 class="text-center self-center">@lang('ushop.banner_equipo')<a data-toggle="modal" href="#modalCurriculum">AQUI</a></h5>  
				@endif
            </div>
        </div>
    </div>
</div>