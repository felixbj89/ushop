@extends("layout.master")
@section("active-contacto")
    active
@endsection
@section("Mycss")
    <link href="{{ asset('css/contacto.css') }}" rel="stylesheet">
@endsection
@section("body-page")
<div class="row">
	@if(isset($especificaciones) && $especificaciones!=NULL)
		<section id="bannerI" class="col-12 background-banner" style="background-image:url({{$urlbase}}img/media/{{$especificaciones['imagen']}});">
	@else
		<section id="bannerI" class="col-12 background-banner" style="background-image:url(img/contacts/Banner_secc4_contacto_2048x500.jpg);">
	@endif
		@include('frontend.contact.banner')
	</section>
	<section id="formcontacto" class="col-12">
        @include('frontend.components.contacts')
	</section>
	@if(isset($catalogoespecificaciones) && $catalogoespecificaciones!=NULL)
		<section id="bannercatalogo" class="col-12 background-banner" style="background-image:url({{$urlbase}}img/media/{{$catalogoespecificaciones['imagen']}});">
	@else
		<section id="bannercatalogo" class="col-12 background-banner" style="background-image:url(img/contacts/Fondo_secc3_contacto_2048x500.jpg);">
	@endif
		@include('frontend.contact.bannercatalogo')
	</section>
	@if(isset($fijaespecificaciones) && $fijaespecificaciones!=NULL)
		<section id="bannerII" class="col-12 background-banner" style="background-image:url({{$urlbase}}img/media/{{$fijaespecificaciones['imagen']}});">
	@else
		<section id="bannerII" class="col-12 background-banner" style="background-image:url(img/contacts/Banner_secc4_contacto_2048x500.jpg);">
	@endif
		@include('frontend.contact.bannerii')
	</section>
	<section id="map" class="col-12">
	</section>
</div>
@endsection
@section("Myscript")
	<script>
		var seccion = '{{e($section)}}';
		if(seccion != "")
			$('html, body').animate({scrollTop: ($('#'+seccion).offset().top) - 150}, 1500);
	</script>
	<script src="{{ asset('js/contacto/contacto.js') }}"></script>
    <script src="{{ asset('js/contact_validate.js') }}"></script>
	<script src="{{ asset('plugins/masked/jquery.maskedinput.min.js') }}"></script>
	<script src="{{ asset('js/googlemapa.js') }}"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCX3K0JjRqFqTnmJoLjQ4_RpDTSYaN4Kj0&callback=initMap" async defer></script>
@endsection