@extends("layout.master")
@section("active-empresa")
    active
@endsection
@section("Mycss")
    <link href="{{ asset('css/empresa.css') }}" rel="stylesheet">
    <link href="{{ asset('plugins/OwlCarousel2-2.2.1/assets/owl.carousel.min.css') }}" rel="stylesheet">
	<link href="{{ asset('plugins/OwlCarousel2-2.2.1/assets/animate.css') }}" rel="stylesheet">
	<link href="{{ asset('plugins/swiper/css/swiper.min.css') }}" rel="stylesheet">
@endsection
@section("body-page")
<div class="row">
	<section id="bannerI" class="col-12">
		@include('frontend.empresa.banner1')
    </section>
	<section id="navservi" class="col-12 p0">
		@include('frontend.empresa.navservi')
	</section>
	<section id="empresa" class="col-12">
		@include('frontend.empresa.empresa')
	</section>
	@if(isset($servicioslista[0]) && $servicioslista[0]["servicios_estado"]=="1")
		<section id="materialpop" class="col-12 servicios background-banner text-white font-weight-bold" style="background-color:#4b4b4b;">
			@include('frontend.empresa.materialpop')
		</section>
	@endif
	@if(isset($servicioslista[1]) && $servicioslista[1]["servicios_estado"]=="1")
		<section id="productoseco" class="col-12 servicios background-banner text-dark font-weight-bold" style="background-image:url({{ asset('img/empresa/secc5_empresa_2048x500.jpg') }});">
			@include('frontend.empresa.productoseco')
		</section>
	@endif
	@if(isset($servicioslista[2]) && $servicioslista[2]["servicios_estado"]=="1")
		<section id="gourmet" class="col-12 servicios background-banner text-white font-weight-bold" style="background-color:#a40000;">
			@include('frontend.empresa.gourmet')
		</section>
	@endif
	@if(isset($servicioslista[3]) && $servicioslista[3]["servicios_estado"]=="1")
		<section id="eventos" class="col-12 servicios background-banner text-white font-weight-bold" style="background-image:url({{ asset('img/empresa/secc7_empresa_2048x500.jpg') }});">
			@include('frontend.empresa.eventos')
		</section>
	@endif
	@if(isset($servicioslista[4]) && $servicioslista[4]["servicios_estado"]=="1")
		<section id="digitales" class="col-12 servicios background-banner text-white font-weight-bold" style="background-color:#4b4b4b;">
			@include('frontend.empresa.digitales')
		</section>
	@endif
	@if(isset($servicioslista[5]) && $servicioslista[5]["servicios_estado"]=="1")
		<section id="muebleria" class="col-12 servicios background-banner text-dark font-weight-bold" style="background-image:url({{ asset('img/empresa/secc5_empresa_2048x500.jpg') }});">
			@include('frontend.empresa.muebleria')
		</section>
	@endif
	@if(isset($servicioslista[6]) && $servicioslista[6]["servicios_estado"]=="1")
		<section id="seguridad" class="col-12 servicios background-banner text-white font-weight-bold" style="background-color:#a40000;">
			@include('frontend.empresa.seguridad')
		</section>
	@endif
	@if(isset($servicioslista[7]) && $servicioslista[7]["servicios_estado"]=="1")
		<section id="impresos" class="col-12 servicios background-banner text-white font-weight-bold" style="background-image:url({{ asset('img/empresa/secc7_empresa_2048x500.jpg') }});">
			@include('frontend.empresa.impresos')
		</section>
	@endif
	<section id="equipo" class="col-12">
		@include('frontend.empresa.equipo')
	</section>
	<section id="customers" class="col-12 text-rojo">
		@include('frontend.components.customers')
	</section>
</div>
@endsection
@section("Myscript")
	<script>
		var seccion = '{{e($section)}}';
		if(seccion != "")
			$('html, body').animate({scrollTop: ($('#'+seccion).offset().top) - 230}, 1500);
	</script>
	<script type="text/javascript" src="{{ asset('plugins/swiper/js/swiper.min.js') }}"></script>
    <script src="{{ asset('plugins/OwlCarousel2-2.2.1/owl.carousel.min.js') }}"></script>
	<script src="{{ asset('plugins/velocity/velocity.min.js')}}"></script>
	<script src="{{ asset('plugins/velocity/velocity.ui.min.js')}}"></script>
	<script src="{{ asset('js/empresa/empresa.js') }}"></script>
@endsection