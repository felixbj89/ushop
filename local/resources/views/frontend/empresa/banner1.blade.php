<div class="row">
	<div class="col p0 align-self-center">
		@if(isset($bannerempresa) && $bannerempresa!=NULL)
			<div class="row background-banner" style="background-image:url({{$urlbase}}img/media/{{$bannerempresa['imagen']}});">
				<div class="col-11 col-md-10 col-lg-9 align-self-center self-center">
					<div class="row">
						<div class="col">
							@if(isset($bannerempresa["textos"]))
								{!!base64_decode($bannerempresa["textos"])!!}
							@endif
						</div>
					</div>
				</div>
			</div>
		@else
			<div class="row background-banner" style="background-image:url({{ asset('img/empresa/banners/secc1_productos_2048x500.jpg') }});">
				<div class="col-11 col-md-10 col-lg-9 align-self-center self-center">
					<div class="row">
						<div class="col">
							<p>Tenemo la mejod disposición para atender tur requerimientos 24/7</p>
							<p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sunt in culpa qui officia deserunt mollit anim id est laborum."</p>
						</div>
					</div>
				</div>
			</div>
		@endif
	</div>
</div>