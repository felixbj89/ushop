<div class="row">
    <div class="col align-self-center">
		<div class="row">
			<div class="col-12 col-sm-12 col-md-11 self-center text-center">
				<div class="row">
					<div class="col">
						<div class="owl-carousel self-center row">
							<div class="valores self-center col"><div class="row"><div class="itemval col-11 self-center"><h4><b>@lang('ushop.titulo_valoresI')</b></h4><p>@lang('ushop.valores_pI')</p></div></div></div>
							<div class="valores self-center col"><div class="row">
								<div class="align-self-center vborder"></div>
								<div class="itemval col-11 self-center"><h4><b>@lang('ushop.titulo_valoresII')</b></h4><p>@lang('ushop.valores_pII')</p></div>
								<div class="align-self-center vborder"></div>
							</div></div>
							<div class="valores self-center col"><div class="row"><div class="itemval col-11 self-center"><h4><b>@lang('ushop.titulo_valoresIII')</b></h4><p>@lang('ushop.valores_pIII')</p></div></div></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>