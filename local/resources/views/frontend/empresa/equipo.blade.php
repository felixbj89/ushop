<div class="row">
    <div class="col-11 col-lg-10 align-self-center self-center text-center">
        <div class="row">
            <div class="col-12 p0">
                <h5>@lang('ushop.titulo_equipo')</h5>
                <div id="carouselEquipo" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
						<div class="carousel-item active background-banner" style="background-image:url({{$urlbase}}{{$equipopictures[0]['equipo_ruta']}});">
						</div>
                        <div class="carousel-item background-banner" style="background-image:url({{$urlbase}}{{$equipopictures[1]['equipo_ruta']}});">
						</div>
                        <div class="carousel-item background-banner" style="background-image:url({{$urlbase}}{{$equipopictures[2]['equipo_ruta']}});">
						</div>
                        <div class="carousel-item background-banner" style="background-image:url({{$urlbase}}{{$equipopictures[3]['equipo_ruta']}});">
						</div>
                        <div class="carousel-item background-banner" style="background-image:url({{$urlbase}}{{$equipopictures[4]['equipo_ruta']}});">
						</div>
                    </div>
                    <br/>
                    <ol class="carousel-indicators row d-flex justify-content-between">
                        <li data-target="#carouselEquipo" data-slide-to="0" class="col active"><h5>@lang('ushop.equipo_I')</h5></li>
                        <li data-target="#carouselEquipo" data-slide-to="1" class="col"><h5>@lang('ushop.equipo_II')</h5></li>
                        <li data-target="#carouselEquipo" data-slide-to="2" class="col"><h5>@lang('ushop.equipo_III')</h5></li>
                        <li data-target="#carouselEquipo" data-slide-to="3" class="col"><h5>@lang('ushop.equipo_IV')</h5></li>
                        <li data-target="#carouselEquipo" data-slide-to="4" class="col"><h5>@lang('ushop.equipo_V')</h5></li>
                    </ol>            
                </div>
            </div>
        </div>
    </div>
</div>