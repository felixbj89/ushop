<div class="row">
    <div class="col-11 col-lg-10 self-center">
        <div class="row">
            <div class="col-12 col-lg-5 servicios_in text-right">
                <div class="row">
                    <div class="col-12 align-self-center">
						@if(isset($servicioslista[2]["servicios_logo"]))
							<div class="logobanner logoright" style="background-image:url({{$urlbase}}{{$servicioslista[2]['servicios_logo']}});"></div>
						@endif
                        <div>
							@if(isset($servicioslista[2]["servicios_descripcion"]))
								{!!$servicioslista[2]["servicios_descripcion"]!!}
							@endif
                        </div>
                        @if(isset($enlaces['AP']))
							<!--<a href ="{{$enlaces['AP']}}" class="text-white refplus"><svg class="pluservi blanco" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 42 42" style="enable-background:new 0 0 42 42;" xml:space="preserve"><polygon points="42,20 22,20 22,0 20,0 20,20 0,20 0,22 20,22 20,42 22,42 22,22 42,22 "/><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g></svg><span class="pluspan">@lang('ushop.btn_toproduct')</span></a>-->
						@else
							<!--<a href ="#" class="text-white refplus"><svg class="pluservi blanco" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 42 42" style="enable-background:new 0 0 42 42;" xml:space="preserve"><polygon points="42,20 22,20 22,0 20,0 20,20 0,20 0,22 20,22 20,42 22,42 22,22 42,22 "/><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g></svg><span class="pluspan">@lang('ushop.btn_toproduct')</span></a>-->
						@endif	
                    </div>
                </div> 
            </div>
            <div class="col-12 col-lg-7 servicios_in">
                <div class="row">
                    <div class="col-12 align-self-center">
                        <div class="row rowgal">
                            <div class="band_black right"></div>
                            <!--<div class="col w120 d-flex align-items-center justify-content-center"><a class="linkrojo" @if(isset($galeriaservicios[$servicioslista[2]["servicios_codigo"]])) href="#myModal3" @else href="#" @endif role="button" data-toggle="modal">@lang('ushop.btn_vgaleria')</a></div>-->
							@if(isset($servicioslista[2]["servicios_one"]) && isset($servicioslista[2]["servicios_two"]) && isset($servicioslista[2]["servicios_thrid"]))
								<div class="col w250 background-banner" style="background-image:url({{$urlbase}}{{$servicioslista[2]['servicios_one']}});"></div>
								<div class="col w200 background-banner" style="background-image:url({{$urlbase}}{{$servicioslista[2]['servicios_two']}});"></div>
								<div class="col w150 background-banner" style="background-image:url({{$urlbase}}{{$servicioslista[2]['servicios_thrid']}});"></div>
							@endif
                        </div>
                    </div>
                </div> 
            </div>
        </div>
    </div>
</div>

<div id="myModal3" class="modalempushop modal row no-gutters w-100 p-0" data-easein="flipBounceYIn" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog m-0 col-12 h-100" style="max-width:100%;">
        <div class="modal-content w-100 h-100" style="border-radius:0;">
            <div class="modal-header">
                <h4 class="">@lang('ushop.navproductos_III')</h4>
                <button type="button" class="" data-dismiss="modal" aria-hidden="true">x</button>
            </div>
            <div class="modal-body">
                <div class="swiper-container h-100">
                    <div class="swiper-wrapper">
                        @if(isset($galeriaservicios[$servicioslista[2]["servicios_codigo"]]))
                            @foreach ($galeriaservicios[$servicioslista[2]["servicios_codigo"]] as $item)
                                <div class="swiper-slide d-flex justify-content-center">
                                    <div class="boxall mx-auto">
                                        <a class="" href="{{$urlbase.'/'.$item->galeria_imagen}}" data-lightbox="mmp-lb2" data-title="{{$item->galeria_descripcion}}">
                                            <div class="img" style="background-image: url({{$urlbase.'/'.$item->galeria_imagen}}" title="{{$item->galeria_descripcion}}"></div>
                                        </a>
                                    </div>
                                </div>
                            @endforeach
                        @endif
                    </div>
                    <div class="swiper-pagination"></div>
                    
                </div>
                <button type="button" class="cerrar btn-gray-bld"  data-dismiss="modal">@lang('ushop.cerrar')</button>
            </div>
        </div>
    </div>
</div>