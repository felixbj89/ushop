<nav id="navproductos" class="navbar navbar-expand-lg navbar-light bg-light">
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarempresa" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse self-center" id="navbarempresa">
    <ul class="navbar-nav mr-auto mt-2 mt-lg-0 self-center row">
		@if(isset($servicioslista[0]) && $servicioslista[0]["servicios_estado"]=="1")
			<li id="li_materialpop" class="nav-item col-12 col-sm">
				<a class="nav-link animatescroll" href="#materialpop">@lang('ushop.navproductos_I')</a>
			</li>
		@endif
		@if(isset($servicioslista[1]) && $servicioslista[1]["servicios_estado"]=="1")
			<li id="li_productoseco" class="nav-item col-12 col-sm">
				<a class="nav-link animatescroll" href="#productoseco">@lang('ushop.navproductos_II')</a>
			</li>
		@endif
		@if(isset($servicioslista[2]) && $servicioslista[2]["servicios_estado"]=="1")
			<li id="li_gourmet" class="nav-item col-12 col-sm">
				<a class="nav-link animatescroll" href="#gourmet">@lang('ushop.navproductos_III')</a>
			</li>
		@endif
		@if(isset($servicioslista[3]) && $servicioslista[3]["servicios_estado"]=="1")
			<li id="li_eventos" class="nav-item col-12 col-sm">
				<a class="nav-link animatescroll" href="#eventos">@lang('ushop.navproductos_IV')</a>
			</li>
		@endif
		@if(isset($servicioslista[4]) && $servicioslista[4]["servicios_estado"]=="1")
			<li id="li_digitales" class="nav-item col-12 col-sm">
				<a class="nav-link animatescroll" href="#digitales">@lang('ushop.navproductos_V')</a>
			</li>
		@endif
		@if(isset($servicioslista[5]) && $servicioslista[5]["servicios_estado"]=="1")
			<li id="li_muebleria" class="nav-item col-12 col-sm">
				<a class="nav-link animatescroll" href="#muebleria">@lang('ushop.navproductos_VI')</a>
			</li>
		@endif
		@if(isset($servicioslista[6]) && $servicioslista[6]["servicios_estado"]=="1")
			<li id="li_seguridad" class="nav-item col-12 col-sm">
				<a class="nav-link animatescroll" href="#seguridad">@lang('ushop.navproductos_VII')</a>
			</li>
		@endif
		@if(isset($servicioslista[7]) && $servicioslista[7]["servicios_estado"]=="1")
			<li id="li_impresos" class="nav-item col-12 col-sm">
				<a class="nav-link animatescroll" href="#impresos">@lang('ushop.navproductos_VIII')</a>
			</li>
		@endif
    </ul>
  </div>
</nav>