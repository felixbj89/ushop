@extends("layout.master")
@section("Mycss")
    <link href="{{ asset('css/index.css') }}" rel="stylesheet">
    <link href="{{ asset('plugins/OwlCarousel2-2.2.1/assets/owl.carousel.min.css') }}" rel="stylesheet">
	<link href="{{ asset('plugins/OwlCarousel2-2.2.1/assets/animate.css') }}" rel="stylesheet">
@endsection
@section("body-page")
<div class="row">
	<section id="bannerI" class="col-12">
		@include('frontend.index.baneri')
	</section>
	<section id="services" class="col-12">
		@include('frontend.index.services')
	</section>
	<section id="produts" class="col-12">
		@include('frontend.index.produts')
	</section>
	@if(isset($publicidadparallax) && $publicidadparallax!=NULL)
		<section id="bannerII" class="col-12" style="background-image:url({{$urlbase}}img/media/{{$publicidadparallax['imagen']}});">
	@else
		<section id="bannerII" class="col-12" style="background-image:url({{ asset('img/paralaxsec_4.jpg') }});">
	@endif
		@include('frontend.index.banerii')
	</section>
	<section id="bannercontact" class="col-12">
		@include('frontend.index.bannercontact')
	</section>
	<section id="packideal" class="col-12 text-blanco ">
		@include('frontend.index.packideal')	
	</section>
	<section id="customers" class="col-12 text-rojo">
		@include('frontend.components.customers')
	</section>
	<section id="map" class="col-12">
	</section>
</div>
@endsection
@section("Myscript")
	<script>
		var seccion = '{{e($section)}}';
		if(seccion != "")
			$('html, body').animate({scrollTop: ($('#'+seccion).offset().top) - 150}, 1500);
	</script>
	<script src="{{ asset('plugins/OwlCarousel2-2.2.1/owl.carousel.min.js') }}"></script>
	<script src="{{ asset('js/index/index.js') }}"></script>
	<script src="{{ asset('js/googlemapa.js') }}"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCX3K0JjRqFqTnmJoLjQ4_RpDTSYaN4Kj0&callback=initMap" async defer></script>
@endsection