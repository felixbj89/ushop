<div class="row">
	<div class="col align-self-center">
		@if(isset($especificaciones) && $especificaciones!=NULL)
			<input type="hidden" id="urls" value="{{base64_encode(json_encode($especificaciones))}}"/>
		@endif
		
        <div id="prinbanner" class="owl-carousel">     
			@if(isset($especificaciones) && $especificaciones!=NULL)
				@foreach($especificaciones as $key => $e)
					<?php $tipo = explode(".",$e['imagen']); ?>
					@if(!isset($tipo[1]))
						<div data-posicion="{{$key}}" class="item background-banner picture" style="background-image:url({{$urlbase}}img/media/{{$e['imagen']}});">            
							@if(isset($e['textos']))
								<div id="picture" class="row">
									<div class="col-11 col-sm-10 col-md-10 align-self-center self-center">
										{!!base64_decode($e['textos'])!!}
									</div>
								</div>
							@endif
						</div>
					@else
						<div data-posicion="{{$key}}" class="item video">
							<video id="clipUno"  class="vjs-tech" preload="auto" data-setup="{}" tabindex="-1">
								<!--  poster="{{url('.jpg')}}" -->
								<source src="{{$urlbase}}img/media/{{$e['imagen']}}" type="video/mp4">
								<p class="vjs-no-js">
									Para ver este video, habilite JavaScript y considere actualizarse a un navegador web que es compatible con video HTML5
								</p>
							</video>
							<button id="controlClip" onclick="playPause()">
								<i id="play" class="far fa-play-circle fa-5x"></i>
								<i id="pause"class="far fa-pause-circle fa-5x"></i>
							</button>
						</div>
					@endif
				@endforeach
			@else
				<div class="item background-banner" style="background-image:url({{url('img/banners/secc1_homeI_2048x500.jpg')}});">            
					<div class="row">
						<div class="col-11 col-sm-10 col-md-10 align-self-center self-center">
							<p>Tenemo la mejod disposición para atender tur requerimientos 24/7</p>
							<p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sunt in culpa qui officia deserunt mollit anim id est laborum."</p>
						</div>
					</div>
				</div>
				<div class="item video">
					<video id="clipUno"  class="vjs-tech" preload="auto" data-setup="{}" tabindex="-1">
						<!--  poster="{{url('.jpg')}}" -->
						<source src="{{url('video/SampleVideo_1280x720_1mb.mp4')}}" type="video/mp4">
						<p class="vjs-no-js">
							Para ver este video, habilite JavaScript y considere actualizarse a un navegador web que es compatible con video HTML5
						</p>
					</video>
					<button id="controlClip" onclick="playPause()">
						<i id="play" class="far fa-play-circle fa-5x"></i>
						<i id="pause"class="far fa-pause-circle fa-5x"></i>
					</button>
				</div>
				<div class="item background-banner" style="background-image:url({{url('img/banners/secc1_homeI_2048x500.jpg')}});"></div>
			@endif
        </div>
    </div>
</div>
<div id="bottombanner" class="row">
    <div class="col-11 col-sm-10 col-md-10 align-self-center self-center">
        <div class="row row-alt">
            <div class="col text-left">
                <div class="logobanner" style="background-image: url({{url('img/logoblanco_ushop.svg')}});"></div>
            </div>			
            <div class="col text-right botonurl">
				@if(isset($especificaciones) && $especificaciones!=NULL)
					@if(isset($especificaciones[0]['url']))
						<a href="{{$especificaciones[0]['url']}}" type="button"  class="btn btn-red-lgt">@lang('ushop.label_XI')</a>
					@endif
				@else
					<a href="http://www.ushop.cl/contacto/formcontacto" type="button"  class="btn btn-red-lgt">@lang('ushop.label_XI')</a>
				@endif
			</div>
        </div>
    </div>
</div>