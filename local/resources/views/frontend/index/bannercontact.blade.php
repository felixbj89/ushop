<div class="row"><!-- justify-content-around -->
	<div class="col-12 col-sm-11 align-self-center self-center">
		<div class="row">
			<div class="col-12 col-md-7 align-self-center">
				<h5 class="text-blanco text-center">@lang('ushop.contacto_texto')</h5>
			</div>
			<div class="col-12 col-md-5 align-self-center text-center">
				<a href="{{url('contacto/formcontacto')}}" type="button"  class="btn btn-red-lgt">@lang('ushop.contacto_btn')</a>
			</div>
		</div>
	</div>
</div>