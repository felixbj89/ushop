<div class="row">
	<div class="col-12 align-self-center text-center">
		<h5>@lang('ushop.titulo_pack')</h5>
		<div class="owl-carousel">
			<div>
				<p>@lang('ushop.pack_tituloI')</p>
				<div class="pack-icon row">
					<div class="col align-self-center">
						<a href="#"><img class="img" src="{{url('img/packideal/clientes.png')}}" alt="clientes"/></a>
					</div>
				</div>
				<p>@lang('ushop.pack_pI')</p>
				<a href="#"><i class="fas fa-plus"></i></a>
			</div>
			<div>
				<p>@lang('ushop.pack_tituloII')</p>
				<div class="pack-icon row">
					<div class="col align-self-center">
						<a href="#"><img class="img" src="{{url('img/packideal/experiencia.png')}}" alt="experiencia"/></a>
					</div>
				</div>
				<p>@lang('ushop.pack_pII')</p>
				<a href="#"><i class="fas fa-plus"></i></a>
			</div>
			<div>
				<p>@lang('ushop.pack_tituloIII')</p>
				<div class="pack-icon row">
					<div class="col align-self-center">
						<a href="#"><img class="img" src="{{url('img/packideal/productos.png')}}" alt="productos"/></a>
					</div>
				</div>
				<p>@lang('ushop.pack_pIII')</p>
				<a href="#"><i class="fas fa-plus"></i></a>
			</div>
		</div>
	</div>
</div>