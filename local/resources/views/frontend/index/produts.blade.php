<div class="row">
	<div class="col-11 align-self-center">
		<div class="owl-carousel">
			@for($i = 0; $i < count($productoslist); $i++)
				<div>
					<div class="card">
						<div class="row">
							<div class="col align-self-center">
								<div class="img card-img-top row align-items-center" alt="Card image cap">
									<img class="col" src="{{url($urlbase.$productoslist[$i]['productos_ruta'])}}" alt="{{$productoslist[$i]['productos_tipo']}}"/>
								</div>
								<div class="card-body">
									<div class="row text-left">
										<div class="col-12 align-self-start">
											<h5 class="card-title text-center">{{$productoslist[$i]['productos_name']}}</h5>
											<p class="card-text">{{substr(strip_tags($productoslist[$i]['productos_descripcion']),0,200)."..."}}</p>
										</div>
										<div class="col-12 align-self-end">
											<span>{{$productoslist[$i]['productos_tipo']}}</span>	
											<nav class="pdescrip" aria-label="breadcrumb">
												<ol class="breadcrumb">
													@if($productoslist[$i]['productos_estilo_one']!=NULL)
														<li class="breadcrumb-item"><a href="#">{{$productoslist[$i]['productos_estilo_one']}}</a></li>
													@endif
													@if($productoslist[$i]['productos_estilo_two']!=NULL)
														<li class="breadcrumb-item"><a href="#">{{$productoslist[$i]['productos_estilo_two']}}</a></li>
													@endif
												</ol>
											</nav>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			@endfor
			<!--<div>
				<div class="card">
					<div class="row">
						<div class="col align-self-center">
							<div class="img card-img-top row align-items-center" alt="Card image cap">
								<img class="col" src="{{url('img/productos/Pop_bolso.jpg')}}" alt="Card title"/>
							</div>
							<div class="card-body">
								<div class="row text-left">
									<div class="col-12 align-self-start">
										<h5 class="card-title text-center">Card title</h5>
										<p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
									</div>
									<div class="col-12 align-self-end">
										<span>Logo</span>	
										<nav class="pdescrip" aria-label="breadcrumb">
											<ol class="breadcrumb">
												<li class="breadcrumb-item"><a href="#">estampado</a></li>
												<li class="breadcrumb-item"><a href="#">bordado</a></li>
												<li class="breadcrumb-item"><a href="#">bordado</a></li>
											</ol>
										</nav>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div>
				<div class="card">
					<div class="row">
						<div class="col align-self-center">
							<div class="img card-img-top row align-items-center" alt="Card image cap">
								<img class="col" src="{{url('img/productos/Pop_lapices1.jpg')}}" alt="Card title"/>
							</div>
							<div class="card-body">
								<div class="row text-left">
									<div class="col-12 align-self-start">
										<h5 class="card-title text-center">Card title</h5>
										<p class="card-text">Title and make up the bulk of the card's content.</p>
									</div>
									<div class="col-12 align-self-end">
										<span>Logo</span>	
										<nav class="pdescrip" aria-label="breadcrumb">
											<ol class="breadcrumb">
												<li class="breadcrumb-item"><a href="#">estampado</a></li>
												<li class="breadcrumb-item"><a href="#">bordado</a></li>
											</ol>
										</nav>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div>
				<div class="card">
					<div class="row">
						<div class="col align-self-center">
							<div class="img card-img-top row align-items-center" alt="Card image cap">
								<img class="col" src="{{url('img/productos/Pop_lapices2.jpg')}}" alt="Card title"/>
							</div>
							<div class="card-body">
								<div class="row text-left">
									<div class="col-12 align-self-start">
										<h5 class="card-title text-center">Card title</h5>
										<p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
									</div>
									<div class="col-12 align-self-end">
										<span>Logo</span>	
										<nav class="pdescrip" aria-label="breadcrumb">
											<ol class="breadcrumb">
												<li class="breadcrumb-item"><a href="#">estampado</a></li>
											</ol>
										</nav>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div>
				<div class="card">
					<div class="row">
						<div class="col align-self-center">
							<div class="img card-img-top row align-items-center" alt="Card image cap">
								<img class="col" src="{{url('img/productos/Pop_linterna.jpg')}}" alt="Card title"/>
							</div>
							<div class="card-body">
								<div class="row text-left">
									<div class="col-12 align-self-start">
										<h5 class="card-title text-center">Card title</h5>
										<p class="card-text">Title and make up the bulk of the card's content.</p>
									</div>
									<div class="col-12 align-self-end">
										<span>Logo</span>	
										<nav class="pdescrip" aria-label="breadcrumb">
											<ol class="breadcrumb">
												<li class="breadcrumb-item"><a href="#">estampado</a></li>
												<li class="breadcrumb-item"><a href="#">bordado</a></li>
											</ol>
										</nav>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div>
				<div class="card">
					<div class="row">
						<div class="col align-self-center">
							<div class="img card-img-top row align-items-center" alt="Card image cap">
								<img class="col" src="{{url('img/productos/Pop_mochila.jpg')}}" alt="Card title"/>
							</div>
							<div class="card-body">
								<div class="row text-left">
									<div class="col-12 align-self-start">
										<h5 class="card-title text-center">Card title</h5>
										<p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
									</div>
									<div class="col-12 align-self-end">
										<span>Logo</span>	
										<nav class="pdescrip" aria-label="breadcrumb">
											<ol class="breadcrumb">
												<li class="breadcrumb-item"><a href="#">estampado</a></li>
												<li class="breadcrumb-item"><a href="#">bordado</a></li>
											</ol>
										</nav>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div>
				<div class="card">
					<div class="row">
						<div class="col align-self-center">
							<div class="img card-img-top row align-items-center" alt="Card image cap">
								<img class="col" src="{{url('img/productos/Pop_morral.jpg')}}" alt="Card title"/>
							</div>
							<div class="card-body">
								<div class="row text-left">
									<div class="col-12 align-self-start">
										<h5 class="card-title text-center">Card title</h5>
										<p class="card-text">Title and make up the bulk of the card's content.</p>
									</div>
									<div class="col-12 align-self-end">
										<span>Logo</span>	
										<nav class="pdescrip" aria-label="breadcrumb">
											<ol class="breadcrumb">
												<li class="breadcrumb-item"><a href="#">estampado</a></li>
												<li class="breadcrumb-item"><a href="#">bordado</a></li>
											</ol>
										</nav>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div>
				<div class="card">
					<div class="row">
						<div class="col align-self-center">
							<div class="img card-img-top row align-items-center" alt="Card image cap">
								<img class="col" src="{{url('img/productos/Pop_mug.jpg')}}" alt="Card title"/>
							</div>
							<div class="card-body">
								<div class="row text-left">
									<div class="col-12 align-self-start">
										<h5 class="card-title text-center">Card title</h5>
										<p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
									</div>
									<div class="col-12 align-self-end">
										<span>Logo</span>	
										<nav class="pdescrip" aria-label="breadcrumb">
											<ol class="breadcrumb">
												<li class="breadcrumb-item"><a href="#">estampado</a></li>
												<li class="breadcrumb-item"><a href="#">bordado</a></li>
											</ol>
										</nav>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div>
				<div class="card">
					<div class="row">
						<div class="col align-self-center">
							<div class="img card-img-top row align-items-center" alt="Card image cap">
								<img class="col" src="{{url('img/productos/Pop_mugmetal.jpg')}}" alt="Card title"/>
							</div>
							<div class="card-body">
								<div class="row text-left">
									<div class="col-12 align-self-start">
										<h5 class="card-title text-center">Card title</h5>
										<p class="card-text">Title and make up the bulk of the card's content.</p>
									</div>
									<div class="col-12 align-self-end">
										<span>Logo</span>	
										<nav class="pdescrip" aria-label="breadcrumb">
											<ol class="breadcrumb">
												<li class="breadcrumb-item"><a href="#">estampado</a></li>
												<li class="breadcrumb-item"><a href="#">bordado</a></li>
											</ol>
										</nav>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div>
				<div class="card">
					<div class="row">
						<div class="col align-self-center">
							<div class="img card-img-top row align-items-center" alt="Card image cap">
								<img class="col" src="{{url('img/productos/Pop_necessaire.jpg')}}" alt="Card title"/>
							</div>
							<div class="card-body">
								<div class="row text-left">
									<div class="col-12 align-self-start">
										<h5 class="card-title text-center">Card title</h5>
										<p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
									</div>
									<div class="col-12 align-self-end">
										<span>Logo</span>	
										<nav class="pdescrip" aria-label="breadcrumb">
											<ol class="breadcrumb">
												<li class="breadcrumb-item"><a href="#">estampado</a></li>
												<li class="breadcrumb-item"><a href="#">bordado</a></li>
											</ol>
										</nav>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div>
				<div class="card">
					<div class="row">
						<div class="col align-self-center">
							<div class="img card-img-top row align-items-center" alt="Card image cap">
								<img class="col" src="{{url('img/productos/Pop_parlante.jpg')}}" alt="Card title"/>
							</div>
							<div class="card-body">
								<div class="row text-left">
									<div class="col-12 align-self-start">
										<h5 class="card-title text-center">Card title</h5>
										<p class="card-text">Title and make up the bulk of the card's content.</p>
									</div>
									<div class="col-12 align-self-end">
										<span>Logo</span>	
										<nav class="pdescrip" aria-label="breadcrumb">
											<ol class="breadcrumb">
												<li class="breadcrumb-item"><a href="#">estampado</a></li>
												<li class="breadcrumb-item"><a href="#">bordado</a></li>
											</ol>
										</nav>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>-->
		</div>
		<div class="col text-center">
			<button class="btn-red-bld ircatalogo"><h5 class="bloque-center">@lang('ushop.btn_compras')</h5></button>
		</div>
	</div>
</div>