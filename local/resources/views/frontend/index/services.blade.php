<div class="row">
	<div class="col-11 align-self-center text-center">
		<div class="row">
			<div class="col align-self-center">
				<h5>@lang('ushop.titulo_servicios')</h5>
			</div>
		</div>
		<div class="row">
			<div class="col">
				<div class="owl-carousel">
					<div class="pop self-center"><div class="row justify-content-start"><div class="col-11"><div class="row"><div class="col-11 col-xl-10 self-center"><div class="row align-items-center paragrahservi"><p class="col">@lang('ushop.servi_pop')</p></div></div></div></div></div></div>
					<div class="muebleria self-center"><div class="row justify-content-start"><div class="col-11"><div class="row"><div class="col-11 col-xl-10 self-center"><div class="row align-items-center paragrahservi"><p class="col">@lang('ushop.servi_muebleria')</p></div></div></div></div></div></div>
					<div class="eco self-center"><div class="row justify-content-start"><div class="col-11"><div class="row"><div class="col-11 col-xl-10 self-center"><div class="row align-items-center paragrahservi"><p class="col">@lang('ushop.servi_eco')</p></div></div></div></div></div></div>
					<div class="impresos self-center"><div class="row justify-content-start"><div class="col-11"><div class="row"><div class="col-11 col-xl-10 self-center"><div class="row align-items-center paragrahservi"><p class="col">@lang('ushop.servi_impresos')</p></div></div></div></div></div></div>
					<div class="eventos self-center"><div class="row justify-content-start"><div class="col-11"><div class="row"><div class="col-11 col-xl-10 self-center"><div class="row align-items-center paragrahservi"><p class="col">@lang('ushop.servi_eventos')</p></div></div></div></div></div></div>
					<div class="seguridad self-center"><div class="row justify-content-start"><div class="col-11"><div class="row"><div class="col-11 col-xl-10 self-center"><div class="row align-items-center paragrahservi"><p class="col">@lang('ushop.servi_seguridad')</p></div></div></div></div></div></div>
					<div class="medios self-center"><div class="row justify-content-start"><div class="col-11"><div class="row"><div class="col-11 col-xl-10 self-center"><div class="row align-items-center paragrahservi"><p class="col">@lang('ushop.servi_medios')</p></div></div></div></div></div></div>
					<div class="gourmet self-center"><div class="row justify-content-start"><div class="col-11"><div class="row"><div class="col-11 col-xl-10 self-center"><div class="row align-items-center paragrahservi"><p class="col">@lang('ushop.servi_gourmet')</p></div></div></div></div></div></div>
				</div>
			</div>
		</div>
	</div>
</div>