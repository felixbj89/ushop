<div class="modal fade" id="modalCurriculum" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content background-banner" style="background-image:url({{url('img/empresa/secc5_empresa_2048x500.jpg')}});">
            <div class="modal-header">
                <h5 class="modal-title">@lang('ushop.modalEquipo_titulo')</h5>
            </div>
            <div class="modal-body">
                <form id="formEquipo" enctype="multipart/form-data">
                    <div class="form-group row">
                        <div class="col-12 col-sm m170">
                            <label for="myname">@lang('ushop.label_')</label>
                        </div>
                        <div class="col-12 col-sm">
                            <input type="text" class="form-control form-control-sm" id="myname" name="myname">
                            <span class="spanvalidate isvalid"><i class="fas fa-check"></i></span>
                            <span class="spanvalidate novalid"><i class="fas fa-exclamation-triangle"></i></span>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-12 col-sm m170">
                            <label for="myemail">@lang('ushop.label_V')</label>
                        </div>
                        <div class="col-12 col-sm">
                            <input type="email" class="form-control form-control-sm" id="myemail" name="myemail">
                            <span class="spanvalidate isvalid"><i class="fas fa-check"></i></span>
                            <span class="spanvalidate novalid"><i class="fas fa-exclamation-triangle"></i></span>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-12 col-sm m170">
                            <label for="myfile">@lang('ushop.label_X')</label>
                        </div>
                        <div class="col-12 col-sm">
                            <input type="file" class="form-control-file form-control-sm" id="myfile" name="myfile" accept="application/pdf">
                            <span class="spanvalidate isvalid"><i class="fas fa-check"></i></span>
                            <span class="spanvalidate novalid"><i class="fas fa-exclamation-triangle"></i></span>
                        </div>
                    </div>
                </form>
                <div id="errorfilesize" class="alert alert-danger">
                    @lang('ushop.label_XII')
                </div>
                <div class="row eviando alert" role="alert">
                    <div class="col-12 h100">
                        <div class="row h100">
                            <div class="procesing col align-self-center text-center">
                                <i class="fas fa-file-alt fa-3x faa-slow faa-passing animated"></i><span>@lang('ushop.label_XIII')</span>
                            </div>
                            <div class="listo col align-self-center text-center">
                            </div>
                            <button id="dismissenv" type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>           
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <input id="su_curriculum" type="button" class="btn-red-bld" value="@lang('ushop.btn_eviar')"  disabled/>
                <input type="button" class="btn-gray-bld" data-dismiss="modal" value="Cerrar"/>
            </div>
        </div>
    </div>
</div>