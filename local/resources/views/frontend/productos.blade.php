@extends("layout.master")
@section("active-productos")
    active
@endsection
@section("Mycss")
	<link href="{{ asset('css/productos.css') }}" rel="stylesheet">
	<link href="{{ asset('plugins/OwlCarousel2-2.2.1/assets/owl.carousel.min.css') }}" rel="stylesheet">
	<link href="{{ asset('plugins/OwlCarousel2-2.2.1/assets/animate.css') }}" rel="stylesheet">
@endsection
@section("body-page")
<div class="row">
	@if(isset($especificaciones) && $especificaciones!=NULL)
		<section id="bannerI" class="col-12 background-banner" style="background-image:url({{$urlbase}}img/media/{{$especificaciones['imagen']}});">
	@else
		<section id="bannerI" class="col-12 background-banner" style="background-image:url({{ asset('img/contacts/secc1_productos_2048x500.jpg') }});">
	@endif
		@include('frontend.products.banneri')
	</section>
	@if(isset($tienda) && $tienda!=NULL)
		<section id="tienda" class="col-12 background-banner" style="background-image:url({{$urlbase}}img/media/{{$tienda['imagen']}});">
	@else
		<section id="tienda" class="col-12 background-banner" style="background-image:url({{ asset('img/contacts/secc1_productos_2048x500.jpg') }});">
	@endif	   
		@include('frontend.products.bannertienda')
	</section>
	<section id="tecnicas" class="col-12">
       	@include('frontend.products.tecnicas')
	</section>
	@if(isset($bannercuatro) && $bannercuatro!=NULL)
		<section id="importadores" class="col-12 background-banner" style="background-image:url({{$urlbase}}img/media/{{$bannercuatro['imagen']}});">
	@else
		<section id="importadores" class="col-12 background-banner" style="background-image:url({{ asset('img/produts/banners/secc4_productos_2048x500.jpg') }});">
	@endif
		@include('frontend.products.importadores')
	</section>
	<section id="formcontacto" class="col-12">
        @include('frontend.components.contacts')
	</section>
	@if(isset($bannertres) && $bannertres!=NULL)
		<section id="catalogo" class="col-12 background-banner" style="background-image:url({{$urlbase}}img/media/{{$bannertres['imagen']}});">
	@else
		<section id="catalogo" class="col-12 background-banner" style="background-image:url({{ asset('img/contacts/secc1_productos_2048x500.jpg') }});">
	@endif
		@include('frontend.products.catalogo')
	</section>
</div>
@endsection
@section("Myscript")
	<script>
		var seccion = '{{e($section)}}';
		if(seccion != "")
			$('html, body').animate({scrollTop: ($('#'+seccion).offset().top) - 130}, 1500);
	</script>
	<script src="{{ asset('plugins/OwlCarousel2-2.2.1/owl.carousel.min.js') }}"></script>
	<script src="{{ asset('js/contact_validate.js') }}"></script>
	<script src="{{ asset('plugins/masked/jquery.maskedinput.min.js') }}"></script>
	<script src="{{ asset('js/productos/productos.js') }}"></script>
@endsection