<div class="row">
    <div class="col align-self-center">
        <div class="row">
            <div class="col-11 col-md-10 col-lg-11 self-center">
                <h5 class="text-center self-center">
                    @if(isset($tienda) && $tienda!=NULL)
                        {!!base64_decode($tienda["textos"])!!}
                    @else
                        @lang('ushop.catalogo_oline')
                    @endif                     
                <a href="{{$tienda['url']}}" target="_blank">Catálogo Online</a></h5>  
            </div>
        </div>
    </div>
</div>