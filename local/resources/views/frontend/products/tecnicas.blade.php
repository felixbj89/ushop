<div class="row">
	<div class="col align-self-center">
        <div class="row">
			<div class="col-12 col-sm-12 col-md-11 self-center text-center">
				<div class="row">
					<div class="col-12">
						<h5 class="spacing-text">@lang('ushop.titulo_tecnica')</h5>
					</div>			
					<div class="col">
						<div class="owl-carousel self-center row">
							<div class="valores self-center col">
								<div class="row">
									<div class="itemval col-11 self-center d-flex justify-content-between flex-wrap">
										<div class="textecnica">
											<h4>@lang('ushop.linktecnina_I')</h4>
											@if(isset($tecnicas[0]) && $tecnicas[0]!=NULL)
												{!!$tecnicas[0]["tecnicas_descripcion"]!!}
											@else
												<p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sunt in culpa qui officia deserunt mollit anim id est laborum."</p><p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sunt in culpa qui officia deserunt mollit anim id est laborum."</p>
											@endif
										</div>
										<div class="cimg d-flex justify-content-end flex-column">
											@if(isset($tecnicas[0]) && $tecnicas[0]!=NULL)
												<img src="{{url('admin/'.$tecnicas[0]['tecnicas_ruta'])}}" alt="1"/>
											@else
												<img src="{{url('img/produts/banners/1secc3_productos_2048x500.jpg')}}" alt="1"/>
											@endif
										</div>
									</div>
								</div>
							</div>
							<div class="valores self-center col">
								<div class="row">
									<div class="align-self-center vborder"></div>
									<div class="itemval col-11 self-center d-flex justify-content-between flex-wrap">
										<div class="textecnica">
											<h4>@lang('ushop.linktecnina_II')</h4>
											@if(isset($tecnicas[1]) && $tecnicas[1]!=NULL)
												{!!$tecnicas[1]["tecnicas_descripcion"]!!}
											@else
												<p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sunt in culpa qui officia deserunt mollit anim id est laborum."</p><p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sunt in culpa qui officia deserunt mollit anim id est laborum."</p>
											@endif
										</div>
										<div class="cimg d-flex justify-content-end flex-column">
											@if(isset($tecnicas[1]) && $tecnicas[1]!=NULL)
												<img src="{{url('admin/'.$tecnicas[1]['tecnicas_ruta'])}}" alt="1"/>
											@else
												<img src="{{url('img/produts/banners/1secc3_productos_2048x500.jpg')}}" alt="1"/>
											@endif
										</div>
									</div>
									<div class="align-self-center vborder"></div>
								</div>
							</div>
							<div class="valores self-center col">
								<div class="row">
									<div class="itemval col-11 self-center d-flex justify-content-between flex-wrap">
										<div class="textecnica">
											<h4>@lang('ushop.linktecnina_III')</h4>
											@if(isset($tecnicas[2]) && $tecnicas[2]!=NULL)
												{!!$tecnicas[2]["tecnicas_descripcion"]!!}
											@else
												<p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sunt in culpa qui officia deserunt mollit anim id est laborum."</p><p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sunt in culpa qui officia deserunt mollit anim id est laborum."</p>
											@endif
										</div>
										<div class="cimg d-flex justify-content-end flex-column">
											@if(isset($tecnicas[2]) && $tecnicas[2]!=NULL)
												<img src="{{url('admin/'.$tecnicas[2]['tecnicas_ruta'])}}" alt="1"/>
											@else
												<img src="{{url('img/produts/banners/1secc3_productos_2048x500.jpg')}}" alt="1"/>
											@endif
										</div>
									</div>
								</div>
							</div>
							<div class="valores self-center col">
								<div class="row">
									<div class="align-self-center vborder"></div>
									<div class="itemval col-11 self-center d-flex justify-content-between flex-wrap">
										<div class="textecnica">
											<h4>@lang('ushop.linktecnina_IV')</h4>
											@if(isset($tecnicas[3]) && $tecnicas[3]!=NULL)
												{!!$tecnicas[3]["tecnicas_descripcion"]!!}
											@else
												<p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sunt in culpa qui officia deserunt mollit anim id est laborum."</p><p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sunt in culpa qui officia deserunt mollit anim id est laborum."</p>
											@endif
										</div>
										<div class="cimg d-flex justify-content-end flex-column">
											@if(isset($tecnicas[3]) && $tecnicas[3]!=NULL)
												<img src="{{url('admin/'.$tecnicas[3]['tecnicas_ruta'])}}" alt="1"/>
											@else
												<img src="{{url('img/produts/banners/1secc3_productos_2048x500.jpg')}}" alt="1"/>
											@endif
										</div>
									</div>
									<div class="align-self-center vborder"></div>
								</div>
							</div>
							<div class="valores self-center col">
								<div class="row">
									<div class="itemval col-11 self-center d-flex justify-content-between flex-wrap">
										<div class="textecnica">
											<h4>@lang('ushop.linktecnina_V')</h4>
											@if(isset($tecnicas[4]) && $tecnicas[4]!=NULL)
												{!!$tecnicas[4]["tecnicas_descripcion"]!!}
											@else
												<p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sunt in culpa qui officia deserunt mollit anim id est laborum."</p><p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sunt in culpa qui officia deserunt mollit anim id est laborum."</p>
											@endif
										</div>
										<div class="cimg d-flex justify-content-end flex-column">
											@if(isset($tecnicas[4]) && $tecnicas[4]!=NULL)
												<img src="{{url('admin/'.$tecnicas[4]['tecnicas_ruta'])}}" alt="1"/>
											@else
												<img src="{{url('img/produts/banners/1secc3_productos_2048x500.jpg')}}" alt="1"/>
											@endif
										</div>
									</div>
								</div>
							</div>
							<div class="valores self-center col">
								<div class="row">
									<div class="align-self-center vborder"></div>
									<div class="itemval col-11 self-center d-flex justify-content-between flex-wrap">
										<div class="textecnica">
											<h4>@lang('ushop.linktecnina_VI')</h4>
											@if(isset($tecnicas[5]) && $tecnicas[5]!=NULL)
												{!!$tecnicas[5]["tecnicas_descripcion"]!!}
											@else
												<p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sunt in culpa qui officia deserunt mollit anim id est laborum."</p><p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sunt in culpa qui officia deserunt mollit anim id est laborum."</p>
											@endif
										</div>
										<div class="cimg d-flex justify-content-end flex-column">
											@if(isset($tecnicas[5]) && $tecnicas[5]!=NULL)
												<img src="{{url('admin/'.$tecnicas[5]['tecnicas_ruta'])}}" alt="1"/>
											@else
												<img src="{{url('img/produts/banners/1secc3_productos_2048x500.jpg')}}" alt="1"/>
											@endif
										</div>
									</div>
									<div class="align-self-center vborder"></div>
								</div>
							</div>
							<div class="valores self-center col">
								<div class="row">
									<div class="itemval col-11 self-center d-flex justify-content-between flex-wrap">
										<div class="textecnica">
											<h4>@lang('ushop.linktecnina_VII')</h4>
											@if(isset($tecnicas[6]) && $tecnicas[6]!=NULL)
												{!!$tecnicas[6]["tecnicas_descripcion"]!!}
											@else
												<p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sunt in culpa qui officia deserunt mollit anim id est laborum."</p><p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sunt in culpa qui officia deserunt mollit anim id est laborum."</p>
											@endif
										</div>
										<div class="cimg d-flex justify-content-end flex-column">
											@if(isset($tecnicas[6]) && $tecnicas[6]!=NULL)
												<img src="{{url('admin/'.$tecnicas[6]['tecnicas_ruta'])}}" alt="1"/>
											@else
												<img src="{{url('img/produts/banners/1secc3_productos_2048x500.jpg')}}" alt="1"/>
											@endif
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-12">
						<ul class="listec list-inline row">
							<li class="list-inline-item col"><a href="0" class="active">@lang('ushop.linktecnina_I')</a></li>
							<li class="list-inline-item col"><a href="1">@lang('ushop.linktecnina_II')</a></li>
							<li class="list-inline-item col"><a href="2">@lang('ushop.linktecnina_III')</a></li>
							<li class="list-inline-item col"><a href="3">@lang('ushop.linktecnina_IV')</a></li>
							<li class="list-inline-item col"><a href="4">@lang('ushop.linktecnina_V')</a></li>
							<li class="list-inline-item col"><a href="5">@lang('ushop.linktecnina_VI')</a></li>
							<li class="list-inline-item col"><a href="6">@lang('ushop.linktecnina_VII')</a></li>
						</ul>
					</div>	
				</div>
			</div>
		</div>
	</div>
</div>