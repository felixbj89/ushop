@extends("layout.master")
@section("Mycss")
<link href="{{ asset('css/quest.css') }}" rel="stylesheet">
@endsection
@section("body-page")
<div class="row">
	<section id="banner" class="col-12">
		@include('frontend.quest.banner')
	</section>
	<section id="resultado" class="col-12 d-flex">
		@switch($type)
			@case('titem')
				@include('frontend.quest.tecnicas');
				@break
			@case('pitem')
				@include('frontend.quest.productos');
				@break
			@case('sitem')
				@include('frontend.quest.servicios');
				@break
		@endswitch		
	</section>
	<section id="map" class="col-12">
	</section>
</div>
@endsection
@section("Myscript")
	<script src="{{ asset('js/quest.js') }}"></script>
	<script src="{{ asset('js/googlemapa.js') }}"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCX3K0JjRqFqTnmJoLjQ4_RpDTSYaN4Kj0&callback=initMap" async defer></script>
@endsection