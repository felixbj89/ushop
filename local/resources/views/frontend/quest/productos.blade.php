<div id="producto" class="row no-gutters my-2  mx-auto w-100 align-self-center">
    <div class="col-12 col-sm-6">
        <div class="w-100 p-2">
            <h4 class="text-center">{{e($found->productos_name)}}</h4>
            <p>{!!$found->productos_descripcion!!}</p>
            <div class="w-100 d-flex justify-content-between">
                <span>{{$found->productos_tipo}}</span>	
                <nav class="" aria-label="breadcrumb">
                    <ol class="breadcrumb mb-0">
                        @if($found->productos_estilo_one != NULL)
                            <li class="breadcrumb-item"><a @if(isset($estilos[0]) && !is_null($estilos[0])) href="{{$routebase.'/quest/titem/'.$estilos[0]}}" @else href="#" class="noclick" @endif>{{$found->productos_estilo_one}}</a></li>
                        @endif
                        @if($found->productos_estilo_two != NULL)
                            <li class="breadcrumb-item"><a @if(isset($estilos[1]) && !is_null($estilos[1])) href="{{$routebase.'/quest/titem/'.$estilos[1]}}" @else href="#" class="noclick" @endif>{{$found->productos_estilo_two}}</a></li>
                        @endif
                        @if($found->productos_estilo_thrid != NULL)
                            <li class="breadcrumb-item"><a @if(isset($estilos[2]) && !is_null($estilos[2])) href="{{$routebase.'/quest/titem/'.$estilos[2]}}" @else href="#" class="noclick" @endif>{{$found->productos_estilo_thrid}}</a></li>
                        @endif
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <div class="col-12 col-sm-6">
        <img src="{{url('admin/'.$found->productos_ruta)}}" alt="{{e($found->productos_name)}}"/>
    </div>
</div>