<div id="servicio" class="row no-gutters my-2  mx-auto w-100 align-self-center">
    <div class="col-12 col-sm-6">
        <div class="w-100 p-2">
            <h4 class="text-center">{{e($found->servicios_name)}}</h4>
            <p>{!!$found->servicios_descripcion!!}</p>
        </div>
    </div>
    <div class="col-12 col-sm-6">
        <div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img src="{{url('admin/'.$found->servicios_one)}}" alt="{{e($found->servicios_name)}}"/>
                </div>
                <div class="carousel-item">
                    <img src="{{url('admin/'.$found->servicios_two)}}" alt="{{e($found->servicios_name)}}"/>
                </div>
                <div class="carousel-item">
                    <img src="{{url('admin/'.$found->servicios_thrid)}}" alt="{{e($found->servicios_name)}}"/>
                </div>
            </div>
        </div>
    </div>
</div>