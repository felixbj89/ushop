<div id="tecnica" class="row no-gutters my-2  mx-auto w-100 align-self-center">
    <div class="col-12">
        <div class="w-100 p-2">
            <h4 class="text-center">{{e($found->tecnicas_name)}}</h4>
            <p>{!!$found->tecnicas_descripcion!!}</p>
        </div>
    </div>
    <div class="col-12">
        <img src="{{url('admin/'.$found->tecnicas_ruta)}}" alt="{{e($found->tecnicas_name)}}"/>
    </div>
</div>