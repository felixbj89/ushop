<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<meta name="csrf-token" content="{{ csrf_token() }}">
  	<title>Ushop |	</title>
  	<link rel="shortcut icon" type="image/png" href="{{url('img/favicon_Ushop_32.png')}}"/>
	<!-- Bootstrap -->
	<link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <!-- Styles ushop -->
	<link href="{{ asset('css/master.css') }}" rel="stylesheet">
	<link href="{{ asset('plugins/lightbox/css/lightbox.min.css') }}" rel="stylesheet">
	<link href="{{ asset('plugins/font-awesome-animation/font-awesome-animation.min.css') }}" rel="stylesheet">
	<link href="{{ asset('plugins/jQueryUI/jquery-ui.min.css') }}" rel="stylesheet">
  	@yield('Mycss')
  </head>
  <body>
	<div class="container">
		<div class="row">
			<div class="col-12 align-self-start">
				<div class="row align-items-start">
					<header id="navbar" class="col-12 align-self-start">
						@include('component.nav')
					</header>
					<section class="col-12 align-self-end">
						@yield('body-page')
					</section>
				</div>			
			</div>
		</div>
		<div id="primfooter" class="row">
			<div class="col-12 align-self-end">
				<div class="row">
					<footer class="col-12">
						@include('component.footer')
					</footer>
				</div>
			</div>
		</div>		
	</div>
	@include('frontend.modal.equipo')
	<script src="{{ asset('plugins/lightbox/js/lightbox-plus-jquery.min.js')}}"></script>
	<script src="{{ asset('plugins/fontawesome-free-5.0.13/svg-with-js/js/fontawesome-all.min.js') }}"></script>
	<script src="{{ asset('js/app.js') }}"></script>
	<script src="{{ asset('plugins/jQueryUI/jquery-ui.min.js') }}"></script>
	<script src="{{ asset('js/ushop.js') }}"></script>
	<script src="{{ asset('js/equipo.js') }}"></script>
	<script>var baseurl = "{{url('/')}}";</script>
	@yield("Myscript")
  </body>
</html>
