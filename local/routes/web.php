<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//ROUTER FRONT 
Route::middleware(['tracker'])->group(function () {
	Route::get('/', ["uses" => "FrontController@index", "as" => "/"]);
	Route::get('home/{section?}', 'FrontController@_index');
	Route::get('contacto/{section?}', 'FrontController@contacto');
	Route::get('productos/{section?}', 'FrontController@productos');
	Route::get('empresa/{section?}', 'FrontController@empresa');
	Route::get('quest/{type}/{item}', 'FrontController@quest');
	//POST FORMULARIOS productos
	Route::post('contacto/request', 'FormController@requerimiento');
	Route::post('productos/request', 'FormController@requerimiento');
	Route::post('home/services/equipo', 'FormController@team');
	Route::post('{directorio}/equipo', 'FormController@team');
});
//BUSQUEDA
Route::get('display-search-queries', ["uses" => "FrontController@searchData", "as" => "display-search-queries"]);